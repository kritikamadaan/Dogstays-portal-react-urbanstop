const config = {
  apiUrl: "http://3.215.2.1:8000/api",
};

export const ownerService = {
  getOwnerProfile,
  getOwnerPets,
};

function getOwnerProfile(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(`${config.apiUrl}/petownerprofile/?user=${id}`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function getOwnerPets(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(`${config.apiUrl}/petprofile/?user=${id}`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        window.location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
