const config = {
  apiUrl: "http://3.215.2.1:8000/api",
};

export const sitterService = {
  searchAll,
  getSelectedSitter,
  getSitterDogs,
  getSitterPets,
  getSitter,
  editSitterProfile,
  getSitterServices,
  getAllSitterServices,
};

function searchAll(search) {
  console.log(search);
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(
    `${config.apiUrl}/sitterprofile/list/?service_id=${search.selectedService}&has_dog=${search.hasDog}&has_cat=${search.hasCat}&other_animal=${search.hasOther}&work_hours=${search.workHours}`,
    requestOptions
  )
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function getSelectedSitter(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(`${config.apiUrl}/sitterprofile/list/${id}`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function getSitter(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(`${config.apiUrl}/sitterprofile/?user=${id}`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function getSitterDogs(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(`${config.apiUrl}/dogprofile/?user=${id}`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function getSitterPets(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(`${config.apiUrl}/petprofile/?user=${id}`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function editSitterProfile(data) {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  };
  return fetch(`${config.apiUrl}/sitterprofile/${data.user}`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function getAllSitterServices(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(`${config.apiUrl}/servicesprice/list`, requestOptions)
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function getSitterServices(id) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return fetch(
    `${config.apiUrl}/servicesprice/list?user_id=${id}`,
    requestOptions
  )
    .then(handleResponse)
    .then((data) => {
      return data;
    });
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        window.location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
