import { sitterConstants } from '../constants';

const initialState = {
    sitterList: [],
    searching: false,
    selectedSitter: {},
    sitterPets: [],
    sitter: {},
    sitterServices: [],
    services: []
}

export function sitter(state = initialState, action) {
    switch(action.type) {
        case sitterConstants.SEARCH_REQUEST:
            return Object.assign({}, state, {searching: true})
        case sitterConstants.SEARCH_SUCCESS:
            return Object.assign({}, state, {
                sitterList: action.data,
                searching: false
            })
        case sitterConstants.SEARCH_FAILURE:
            return state
        case sitterConstants.GET_SITTER_REQUEST:
            return state
        case sitterConstants.GET_SITTER_SUCCESS:
            return Object.assign({}, state, {selectedSitter: action.data})
        case sitterConstants.GET_SITTER_FAILURE:
            return state
        case sitterConstants.GET_SINGLE_SITTER_REQUEST:
            return state
        case sitterConstants.GET_SINGLE_SITTER_SUCCESS:
            return Object.assign({}, state, {sitter: action.data[0]})
        case sitterConstants.GET_SINGLE_SITTER_FAILURE:
            return state
        case sitterConstants.EDIT_SITTER_PROFILE:
            return state
        case sitterConstants.EDIT_SITTER_PROFILE_SUCCESS:
            return Object.assign({}, state, {sitter: action.res})
        case sitterConstants.EDIT_SITTER_PROFILE_FAILURE:
            return state
        case sitterConstants.GET_ALL_SITTER_SERVICES_REQUEST:
            return state
        case sitterConstants.GET_ALL_SITTER_SERVICES_SUCCESS:
            return Object.assign({}, state, {services: action.res})
        case sitterConstants.GET_ALL_SITTER_SERVICES_FAILURE:
            return state
        case sitterConstants.GET_SITTER_SERVICES_REQUEST:
            return state
        case sitterConstants.GET_SITTER_SERVICES_SUCCESS:
            return Object.assign({}, state, {sitterServices: action.res})
        case sitterConstants.GET_SITTER_SERVICES_FAILURE:
            return state
        case sitterConstants.SITTER_PETS_REQUEST:
            return state
        case sitterConstants.SITTER_PETS_SUCCESS:
            return Object.assign({}, state, {sitterPets: action.data})
        case sitterConstants.SITTER_PETS_FAILURE:
            return state
        default:
            return state
        
    }
}