import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { sitter } from './sitter.reducer';
import { owner } from './owner.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  sitter,
  owner
});

export default rootReducer;