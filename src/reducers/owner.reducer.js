import { ownerConstants } from '../constants';

const initialState = {
    ownerDetails: {},
    ownerPets: []
}

export function owner(state = initialState, action) {
    switch(action.type) {
        case ownerConstants.GET_OWNER_DETAILS_REQUEST:
            return state
        case ownerConstants.GET_OWNER_DETAILS_SUCCESS:
            return Object.assign({}, state, {ownerDetails: action.data[0]})
        case ownerConstants.GET_OWNER_DETAILS_FAILURE:
            return state
        case ownerConstants.GET_OWNER_PETS_REQUEST:
            return state
        case ownerConstants.GET_OWNER_PETS_SUCCESS:
            return Object.assign({}, state, {ownerPets: action.data})
        case ownerConstants.GET_OWNER_PETS_FAILURE:
            return state
        default: 
            return state
    }
}