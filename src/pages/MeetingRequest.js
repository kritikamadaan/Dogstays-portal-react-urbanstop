import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import homeBanner from "../assets/home/home-banner.jpg";
import {
  Typography,
  Grid,
  Divider,
  IconButton,
  Avatar,
  FormControlLabel,
  FormControl,
  Checkbox,
  Button,
  Radio,
  TextField,
} from "@material-ui/core";
import {
  DatePicker,
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import moment from "moment";
import AddIcon from "@material-ui/icons/Add";
import CalendarTodayIcon from "@material-ui/icons/CalendarToday";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import { sitterActions } from "../actions";

let SelectedPetsArray = [];
let AddOnArray = [];
let PostAddOnArray = [];
let PetArray = [];

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  homeBanner: {
    background: `linear-gradient(0deg, rgba(94, 90, 90, 0.5), rgba(94, 90, 90, 0.5)), url(${homeBanner})`,
    backgroundSize: "cover",
    backgroundPosition: "top",
    minHeight: "100vh",
    padding: theme.spacing(12),
  },
  bookingOverlay: {
    backgroundColor: "rgba(245, 130, 32, 0.85)",
    width: "100%",
  },
  bookingHeader: {
    color: "white",
    fontSize: 30,
    fontFamily: "Fredoka One",
    textAlign: "left",
  },
  leftGrid: {
    borderRight: "1px solid #FFFFFF",
  },
  dateTextField: {
    color: "white !important",
    "& .MuiInput-underline:before": {
      display: "none",
    },
    "& .MuiInput-underline:after": {
      display: "none",
    },
  },
  dateInput: {
    color: "white !important",
  },
  dateIcon: {
    "& button": {
      color: "white !important",
    },
  },
  timeIcon: {
    "& button": {
      paddingLeft: "0 !important",
      color: "white !important",
    },
  },
  whiteDivider: {
    backgroundColor: "white",
    marginTop: 16,
    marginBottom: 8,
    height: 32,
    width: 1,
    marginRight: 48,
  },
  selectPetHeader: {
    fontSize: 22,
    fontWeight: 500,
    color: "white",
    marginTop: "2rem",
    textAlign: "left",
  },
  serviceWrapper: {
    marginRight: theme.spacing(6),
    border: "1px solid #FFFFFF",
    padding: theme.spacing(1),
    width: "209px",
    borderRadius: 4,
  },
  bookingBtn: {
    color: "#FFFFFF",
    fontSize: 16,
    fontWeight: 500,
    border: "1px solid #FFFFFF",
    padding: theme.spacing(1),
  },
  meetingSummary: {
    borderTop: "1px solid #FFFFFF",
    borderRight: "1px solid #FFFFFF",
    padding: theme.spacing(2, 5),
  },
  meetingSummaryLeftGrid: {
    borderRight: "1px solid #FFFFFF",
  },
}));

const MeetingRequest = (props) => {
  const classes = useStyles();

  const [startDate, setStartDate] = useState(null);
  const [selectDate1, setSelectDate1] = useState(null);
  const [selectDate2, setSelectDate2] = useState(null);
  const [selectDate3, setSelectDate3] = useState(null);
  const [selectDate4, setSelectDate4] = useState(null);
  const [ownerAddress, setOwnerAddress] = useState(null);
  const [sitterAddress, setSitterAddress] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [endDate2, setEndDate2] = useState(null);
  const [endDate3, setEndDate3] = useState(null);
  const [endDate4, setEndDate4] = useState(null);
  const [petData, setPetData] = useState([]);
  const [serviceList, setServiceList] = useState({});
  const [serviceName, setServiceName] = useState({});
  const [servicePrice, setServicePrice] = useState({});
  const [ownerData, setOwnerData] = useState(null);
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [startTime1, setStartTime1] = useState(null);
  const [successBooking, setSuccessBooking] = useState(false);
  const [endTime1, setEndTime1] = useState(null);
  const [startTime2, setStartTime2] = useState(null);
  const [endTime2, setEndTime2] = useState(null);
  const [startTime3, setStartTime3] = useState(null);
  const [endTime3, setEndTime3] = useState(null);
  const [startTime4, setStartTime4] = useState(null);
  const [endTime4, setEndTime4] = useState(null);
  const [meetingLocation, setMeetingLocation] = useState("");
  const [serviceType, setServiceType] = useState(null);
  const [selectedPetsBorder, setSelectedPetsBorder] = useState(false);
  const [selectedPets, setSelectedPets] = useState([]);
  const [totalDays, setTotalDays] = useState(0);
  const [countStartDate, setCountStartDate] = useState(null);
  const [countEndDate, setCountEndDate] = useState(null);
  const [petArrayData, setPetArrayData] = useState([]);
  const [selectedFinalPrice, setSelectedFinalPrice] = useState(0);
  const dispatch = useDispatch();
  const sitterServices = useSelector((state) => state.sitter.sitterServices);
  const sitter = props.sitterData;

  // const getMyPet = () => {
  //   const userData = JSON.parse(localStorage.getItem("user"));
  //   const user_id = userData.user.id;

  //   fetch(`http://3.215.2.1:8000/api/petprofile/?user=${user_id}`, {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //   })
  //     .then((res) => {
  //       return res.json();
  //     })
  //     .then((data) => {
  //       console.log(data);
  //       setPetData(data);
  //     });
  // };

  const getOwnerAddress = () => {
    const userData = JSON.parse(localStorage.getItem("user"));
    const user_id = userData.user.id;

    fetch(`http://3.215.2.1:8000/api/petownerprofile/list/?user=${user_id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log("Address", data);
        setOwnerAddress(
          `${data[0].house_no}, ${data[0].street_address}, ${data[0].city}, ${data[0].state}`
        );
      });
  };
  const getSitterAddress = (sitter_id) => {
    fetch(`http://3.215.2.1:8000/api/sitterprofile/list/?user=${sitter_id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log("Address2", data);
        setSitterAddress(
          `${data[0].house_no}, ${data[0].street_address}, ${data[0].city}, ${data[0].state}`
        );
      });
  };
  const getServiceData = () => {
    fetch(`http://3.215.2.1:8000/api/servicesprice/list/${props.bookingID}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setServiceList(data);
        setServiceName(data.service_id.service_name);
        setServicePrice(data.serviceRate);
      });
  };

  const serviceListData = () => {
    return serviceList;
  };

  const getPetOwnerData = () => {
    const userData = JSON.parse(localStorage.getItem("user"));
    const user_id = userData.user.id;

    fetch(`http://3.215.2.1:8000/api/petownerprofile/list/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log("*********************", data);
        data.filter((d) => {
          if (d.user.id === user_id) {
            setOwnerData(d.id);
          }
        });
      });
  };

  const sendMeetingRequest = () => {
    console.log('the service type is',serviceType)
    const user_id = JSON.parse(localStorage.getItem("user")).user.id;
    if (petArrayData.length === 0) {
      alert("Please select your pet");
    } else if (meetingLocation === "") {
      alert("Please select meeting Address");
    } else if (serviceType === null && startDate === null && endDate === null) {
      alert("Please select the service");
    } else {
      setServiceType({
        core_service: props.selectedService,
      });

      const data = {
        meeting_id: `M${moment(startDate).format("DDMMYYYY")}`,
        startDate: startDate,
        startDate2: selectDate1,
        startDate3: selectDate2,
        startDate4: selectDate3,
        endDate: endDate,
        endDate2: endDate2,
        endDate3: endDate3,
        endDate4: endDate4,
        num_dogs: petArrayData.length,
        total: selectedFinalPrice,
        services: JSON.stringify(serviceType),
        status: "open",
        fromTime: startTime,
        fromTime2: startTime1,
        fromTime3: startTime2,
        fromTime4: startTime3,
        toTime: endTime,
        toTime2: endTime1,
        toTime3: endTime2,
        toTime4: endTime3,
        textArea: null,
        sitterID: props.sitterID,
        ownerID: ownerData,
        pets: petArrayData,
        location:
          meetingLocation === "owner" && meetingLocation !== ""
            ? ownerAddress
            : sitterAddress,
      };

      fetch("http://3.215.2.1:8000/api/meeting/", {
        method: "POST", // or 'PUT'
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          setSuccessBooking(true);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  };

  const serviceFinalPrice = () => {
    const final =
      (parseFloat(countTotalDays()) === 0 ? 1 : parseFloat(countTotalDays())) *
      parseFloat(servicePrice) *
      (petArrayData.length === 0 ? 1 : petArrayData.length);
    setSelectedFinalPrice(final);
    // return final;
  };

  // const countTotalDays = () => {
  //   const oneDay = 1000 * 60 * 60 * 24;
  //   const firstDate = new Date(countStartDate);
  //   const secondDate = new Date(countEndDate);
  //   const diffDays = Math.round(Math.abs(secondDate - firstDate / oneDay));
  //   setTotalDays(diffDays);
  // };
  const countTotalDays = () => {
    if (countEndDate === null) {
      return 0;
    } else {
      var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
      var firstDate = new Date(countEndDate); // 29th of Feb at noon your timezone
      var secondDate = new Date(countStartDate); // 2st of March at noon

      var diffDays = Math.round(
        Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay)
      );
      // setTotalDays(diffDays);
      return diffDays;
    }
  };

  const handlePetChange = (event) => {
    if (event.target.checked === true) {
      PetArray.push(event.target.value);
      // setSelectPet(event.target.value);
    } else {
      PetArray.pop(event.target.value);
    }
    setPetArrayData(PetArray);
    serviceFinalPrice();
  };

  const handleStartDate = (date) => {
    const formatted = moment(date).format("YYYY-MM-DD");
    setCountStartDate(date);
    setStartDate(formatted);
  };

  const handleEndDate = (date) => {
    const formatted = moment(date).format("YYYY-MM-DD");
    setCountEndDate(date);
    setEndDate(formatted);
    countTotalDays();
  };

  const handleStartTime = (time) => {
    const formatted = moment(time);
    setStartTime(formatted);
  };
  const handleStartTime1 = (time) => {
    const formatted = moment(time);
    setStartTime1(formatted);
  };
  const handleStartTime2 = (time) => {
    const formatted = moment(time);
    setStartTime2(formatted);
  };

  const handleEndTime = (time) => {
    const formatted = moment(time);
    setEndTime(formatted);
  };
  const handleEndTime1 = (time) => {
    const formatted = moment(time);
    setEndTime1(formatted);
  };
  const handleEndTime2 = (time) => {
    const formatted = moment(time);
    setEndTime2(formatted);
  };

  const handleSelectDate1 = (date) => {
    const formatted = moment(date).format("YYYY-MM-DD");
    setSelectDate1(formatted);
  };
  const handleSelectDate2 = (date) => {
    const formatted = moment(date).format("YYYY-MM-DD");
    setSelectDate2(formatted);
  };
  const handleSelectDate3 = (date) => {
    const formatted = moment(date).format("YYYY-MM-DD");
    setSelectDate3(formatted);
  };

  const handleServiceChange = (event, service) => {
    console.log('handle Service Chnage',event.target.checked,service);
    setServiceType({
      core_service: service || props.selectedService,
    });
  };

  const handleMeetingLocation = (event) => {
    console.log(event.target.value);
    setMeetingLocation(event.target.value);
  };

  const selectedSitterServices = () => {
    return (
      sitterServices &&
      sitterServices.filter((service) => {
        if (service.sitters.user.id === sitter.user.id) {
          console.log(service);
          return service;
        }
      })
    );
  };

  // let selectPet = (id) => {
  //   setSelectedPetsBorder(!selectedPetsBorder);
  //   SelectedPetsArray.push(id);

  //   console.log(SelectedPetsArray);

  //   setSelectedPets(SelectedPetsArray);
  // };

  const getPetData = () => {
    const userData = JSON.parse(localStorage.getItem("user"));
    const user_id = userData.user.id;

    fetch(`http://3.215.2.1:8000/api/petprofile/?user=${user_id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setPetData(data);
        // console.log("*********************", data);
        // data.filter((d) => {
        //   if (d.user.id === user_id) {
        //     setOwnerData(d.id);
        //   }
        // });
      });
  };

  useEffect(() => {
    dispatch(sitterActions.getSingleSitter(4));
    dispatch(sitterActions.getSitterServices(4));
    getServiceData();
    // getMyPet();
    getPetOwnerData();
    serviceListData();
    getOwnerAddress();
    getSitterAddress(props.sitterProfileAddressPurposeId);
    getPetData();
  }, []);

  return successBooking === false ? (
    <div className={classes.root}>
      <div className={classes.bookingOverlay}>
        <div style={{ padding: "0px 56px" }}>
          <Grid container>
            <Grid item xs={5} className={classes.leftGrid}>
              <Typography className={classes.selectPetHeader}>
                Your Booking Info
              </Typography>
              <Typography className={classes.selectPetHeader}>
                Service Type
              </Typography>
              <div style={{ display: "flex", marginTop: "1rem" }}>
                {selectedSitterServices().map((service) => {
                  if (service.id === props.bookingID) {
                    return (
                      <div className={classes.serviceWrapper}>
                        <FormControlLabel
                          control={
                            <Checkbox
                              onChange={(event) => {
                                handleServiceChange(event, service);
                              }}
                              name={`service${service.id}`}
                              value={service.id}
                              style={{
                                color: "white",
                                textAlign: "center",
                              }}
                              checked="true"
                            />
                          }
                          label={service.service_id.service_name}
                          style={{ color: "white" }}
                        />
                        <div style={{ textAlign: "right" }}>
                          <Typography style={{ color: "white" }}>
                            {service.serviceRate}/
                            {service.service_id.id === 1 ||
                            service.service_id.id === 2
                              ? "day"
                              : "night"}
                          </Typography>
                        </div>
                      </div>
                    );
                  } else return <div />;
                })}
              </div>
              <Typography className={classes.selectPetHeader}>
                Dates When Service Required
              </Typography>
              <div style={{ display: "flex", marginTop: 16 }}>
             
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    margin="normal"
                    format="dd/MM/yyyy"
                    id="date-picker-inline"
                    value={startDate}
                    onChange={handleStartDate}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{
                      className: classes.dateIcon,
                      position: "start",
                    }}
                    className={classes.dateTextField}
                    invalidDateMessage=""
                    placeholder="Start Date"
                  />
                </MuiPickersUtilsProvider>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    margin="normal"
                    format="dd/MM/yyyy"
                    id="date-picker-inline"
                    value={endDate}
                    onChange={handleEndDate}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{
                      className: classes.dateIcon,
                      position: "start",
                    }}
                    className={classes.dateTextField}
                    invalidDateMessage=""
                    placeholder="End Date"
                  />
                </MuiPickersUtilsProvider>
              </div>
              <Typography className={classes.selectPetHeader}>
                Select Your Pet
              </Typography>
              <Grid container>
                {/* {petData.map((d, i) => {
                    return (
                      <Grid item xs={3}>
                        <div
                          onClick={() => {
                            selectPet(d.id);
                          }}
                        >
                          <IconButton
                            id={d.id}
                            data-value={d.id}
                            style={{
                              border:
                                selectedPetsBorder === true && d.id
                                  ? "1px solid #ffffff"
                                  : "none",
                            }}
                          >
                            {d.profile_photo !== null ? (
                              // <Avatar src={require(`${d.profile_photo}`)} />
                              <Avatar src={`${d.profile_photo}`} />
                            ) : (
                              <Avatar
                                style={{
                                  backgroundColor: "white",
                                  color: "#F58220",
                                }}
                              >
                                N/A
                              </Avatar>
                            )}
                          </IconButton>
                          <Typography
                            style={{ textAlign: "center", color: "white" }}
                          >
                            {d.name}
                          </Typography>
                        </div>
                      </Grid>
                    );
                  })} */}

                <FormControl
                  component="fieldset"
                  style={{
                    color: "white",
                    textAlign: "left",
                    display: "block",
                  }}
                >
                  {petData.map((d, i) => {
                    if (d.pet_choice === "dog") {
                      return (
                        <div
                          style={{ display: "flex", alignItems: "center" }}
                          key={i}
                        >
                          <input
                            type="checkbox"
                            name={d.id}
                            value={d.id}
                            onChange={handlePetChange}
                          />
                          <label style={{ color: "#ffffff", fontSize: 16 }}>
                            {d.name}
                          </label>
                        </div>
                      );
                    } else {
                      return null;
                    }
                  })}
                </FormControl>

                {/* <div style={{ marginRight: "1rem" }}>
                    <IconButton>
                      <Avatar
                        style={{ backgroundColor: "white", color: "#F58220" }}
                      >
                        <AddIcon />
                      </Avatar>
                    </IconButton>
                    <Typography style={{ textAlign: "center", color: "white" }}>
                      Add
                    </Typography>
                  </div> */}
              </Grid>
              {/* <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    marginTop: "3rem"
                  }}
                >
                  
                </div> */}
            </Grid>
            <Grid item xs={6}>
              <div
                style={{
                  paddingLeft: 40,
                  textAlign: "left",
                  marginTop: "1rem",
                }}
              >
                <Typography className={classes.selectPetHeader}>
                  Choose your Meeting schedule
                </Typography>
                <div
                  style={{
                    marginTop: "2rem",
                    display: "flex",
                  }}
                >
                  <div
                    className={classes.serviceWrapper}
                    style={{ width: "270px", margin: "10px" }}
                  >
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          margin="normal"
                          format="dd/MM/yyyy"
                          id="date-picker-inline"
                          value={selectDate1}
                          onChange={handleSelectDate1}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                          inputProps={{ className: classes.dateInput }}
                          InputLabelProps={{ className: classes.dateInput }}
                          InputAdornmentProps={{
                            className: classes.dateIcon,
                            position: "start",
                          }}
                          className={classes.dateTextField}
                          invalidDateMessage=""
                          placeholder="Select Date"
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <MuiPickersUtilsProvider
                        utils={DateFnsUtils}
                        style={{ marginLeft: 24 }}
                      >
                        <KeyboardTimePicker
                          variant="inline"
                          margin="normal"
                          id="time-picker1"
                          value={startTime}
                          onChange={handleStartTime}
                          KeyboardButtonProps={{
                            "aria-label": "change time",
                          }}
                          inputProps={{ className: classes.dateInput }}
                          InputLabelProps={{ className: classes.dateInput }}
                          InputAdornmentProps={{
                            className: classes.timeIcon,
                            position: "start",
                          }}
                          className={classes.dateTextField}
                          placeholder="From"
                        />
                      </MuiPickersUtilsProvider>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardTimePicker
                          variant="inline"
                          margin="normal"
                          id="time-picker2"
                          value={endTime}
                          onChange={handleEndTime}
                          KeyboardButtonProps={{
                            "aria-label": "change time",
                          }}
                          inputProps={{ className: classes.dateInput }}
                          InputLabelProps={{ className: classes.dateInput }}
                          InputAdornmentProps={{
                            className: classes.timeIcon,
                            position: "start",
                          }}
                          className={classes.dateTextField}
                          placeholder="To"
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                  </div>
                  <div
                    className={classes.serviceWrapper}
                    style={{ width: "270px", margin: "10px" }}
                  >
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <Typography
                        style={{
                          fontSize: 18,
                          fontWeight: 500,
                          color: "#FFFFFF",
                          marginLeft: 16,
                        }}
                      >
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            margin="normal"
                            format="dd/MM/yyyy"
                            id="date-picker-inline"
                            value={selectDate2}
                            onChange={handleSelectDate2}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.dateIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            invalidDateMessage=""
                            placeholder="Select Date"
                          />
                        </MuiPickersUtilsProvider>
                      </Typography>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardTimePicker
                          variant="inline"
                          margin="normal"
                          id="time-picker3"
                          value={startTime1}
                          onChange={handleStartTime1}
                          KeyboardButtonProps={{
                            "aria-label": "change time",
                          }}
                          inputProps={{ className: classes.dateInput }}
                          InputLabelProps={{ className: classes.dateInput }}
                          InputAdornmentProps={{
                            className: classes.timeIcon,
                            position: "start",
                          }}
                          className={classes.dateTextField}
                          placeholder="From"
                        />
                      </MuiPickersUtilsProvider>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardTimePicker
                          variant="inline"
                          margin="normal"
                          id="time-picker4"
                          value={endTime1}
                          onChange={handleEndTime1}
                          KeyboardButtonProps={{
                            "aria-label": "change time",
                          }}
                          inputProps={{ className: classes.dateInput }}
                          InputLabelProps={{ className: classes.dateInput }}
                          InputAdornmentProps={{
                            className: classes.timeIcon,
                            position: "start",
                          }}
                          className={classes.dateTextField}
                          placeholder="To"
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                  </div>
                </div>
                <div
                  className={classes.serviceWrapper}
                  style={{ width: "270px", margin: "10px" }}
                >
                  <div>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <Typography
                        style={{
                          fontSize: 18,
                          fontWeight: 500,
                          color: "#FFFFFF",
                        }}
                      >
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            margin="normal"
                            format="dd/MM/yyyy"
                            id="date-picker-inline"
                            value={selectDate3}
                            onChange={handleSelectDate3}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.dateIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            invalidDateMessage=""
                            placeholder="Select Date"
                          />
                        </MuiPickersUtilsProvider>
                        {/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      disableToolbar
                      variant="inline"
                      margin="normal"
                      format="dd/MM/yyyy"
                      id="date-picker-inline"
                      value={endDate}
                      onChange={handleEndDate}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                      inputProps={{ className: classes.dateInput }}
                      InputLabelProps={{ className: classes.dateInput }}
                      InputAdornmentProps={{
                        className: classes.dateIcon,
                        position: "start",
                      }}
                      className={classes.dateTextField}
                      invalidDateMessage=""
                      placeholder="End Date"
                    />
                  </MuiPickersUtilsProvider> */}
                      </Typography>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardTimePicker
                          variant="inline"
                          margin="normal"
                          id="time-picker5"
                          value={startTime2}
                          onChange={handleStartTime2}
                          KeyboardButtonProps={{
                            "aria-label": "change time",
                          }}
                          inputProps={{ className: classes.dateInput }}
                          InputLabelProps={{ className: classes.dateInput }}
                          InputAdornmentProps={{
                            className: classes.timeIcon,
                            position: "start",
                          }}
                          className={classes.dateTextField}
                          placeholder="From"
                        />
                      </MuiPickersUtilsProvider>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardTimePicker
                          variant="inline"
                          margin="normal"
                          id="time-picker6"
                          value={endTime2}
                          onChange={handleEndTime2}
                          KeyboardButtonProps={{
                            "aria-label": "change time",
                          }}
                          inputProps={{ className: classes.dateInput }}
                          InputLabelProps={{ className: classes.dateInput }}
                          InputAdornmentProps={{
                            className: classes.timeIcon,
                            position: "start",
                          }}
                          className={classes.dateTextField}
                          placeholder="To"
                        />
                      </MuiPickersUtilsProvider>
                    </div>
                  </div>
                </div>
                <Typography className={classes.selectPetHeader}>
                  Choose location
                </Typography>
                <div
                  style={{
                    marginTop: "2rem",
                    display: "flex",
                  }}
                >
                  <div
                    className={classes.serviceWrapper}
                    style={{ width: "300px", margin: "10px" }}
                  >
                    <FormControlLabel
                      control={
                        <Radio
                          checked={meetingLocation === "owner"}
                          onChange={handleMeetingLocation}
                          name="service3"
                          style={{ color: "white" }}
                        />
                      }
                      value="owner"
                      label="Your House"
                      style={{ color: "white" }}
                    />
                    <div style={{ display: "flex", alignItems: "flex-start" }}>
                      <LocationOnIcon style={{ color: "#FFFFFF" }} />
                      <Typography style={{ color: "white", marginLeft: "8px" }}>
                        {ownerAddress}
                      </Typography>
                    </div>
                  </div>
                  <div
                    className={classes.serviceWrapper}
                    style={{ width: "300px", margin: "10px" }}
                  >
                    <FormControlLabel
                      control={
                        <Radio
                          checked={meetingLocation === "sitter"}
                          onChange={handleMeetingLocation}
                          name="service3"
                          style={{ color: "white" }}
                        />
                      }
                      value="sitter"
                      label="Sitter's House"
                      style={{ color: "white" }}
                    />
                    <div style={{ display: "flex", alignItems: "flex-start" }}>
                      <LocationOnIcon style={{ color: "#FFFFFF" }} />
                      <Typography style={{ color: "white", marginLeft: "8px" }}>
                        {sitterAddress}
                      </Typography>
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
        <div style={{ marginTop: "3rem" }}>
          <Grid container>
            <Grid item xs={9} className={classes.meetingSummaryLeftGrid}>
              <div className={classes.meetingSummary}>
                <Grid container>
                  <Grid
                    item
                    xs={6}
                    style={{ borderRight: "1px solid #FFFFFF" }}
                  >
                    <Typography
                      style={{
                        fontSize: 22,
                        fontWeight: 600,
                        color: "white",
                        textAlign: "left",
                      }}
                    >
                      Booking Estimate
                    </Typography>
                    <div style={{ display: "flex", marginTop: "1rem" }}>
                      <Typography style={{ fontSize: 18, color: "white" }}>
                        No. of Days &nbsp;&nbsp; : &nbsp;&nbsp;
                        {countTotalDays()}
                      </Typography>
                    </div>
                    <Typography
                      style={{
                        fontSize: 18,
                        color: "#FFFFFF",
                        marginTop: "1rem",
                       // textAlign: "left,
                        display:"flex",
                        flexDirection:"row"
                      }}
                    > 
                      Chosen Services  &nbsp;&nbsp; :
                      {selectedSitterServices() &&
                        selectedSitterServices().map((service) => {
                          if (service.id === props.bookingID) {
                            return (
                              <Typography
                                style={{
                                  fontSize: 18,
                                  color: "white",
                                  textAlign: "left",
                                }}
                              >
                                {service.service_id.service_name} 
                          
                                {/* &nbsp;&nbsp; {service.serviceRate}/
                                {service.service_id.id === 1 ||
                                service.service_id.id === 2
                                  ? "day"
                                  : "night"} */}
                              </Typography>
                            );
                          } else return <div />;
                        })}
                    </Typography>
                    <div style={{ display: "flex", marginTop: "1rem" }}>
                    <Typography
                      style={{
                        fontSize: 18,
                        color: "#FFFFFF",
                       // marginTop: "1rem",
                       // textAlign: "left,
                        display:"flex",
                        flexDirection:"row"
                      }}
                    >
                      Rates For Service &nbsp;&nbsp; :
                    </Typography>
                      {selectedSitterServices() &&
                        selectedSitterServices().map((service) => {
                          if (service.id === props.bookingID) {
                            return (
                              <Typography
                                style={{
                                  fontSize: 18,
                                  color: "white",
                                  textAlign: "left",
                                  //marginTop: "1rem",
                                }}
                              >
                                {/* {service.service_id.service_name} &nbsp;&nbsp; : */}
                                &nbsp;&nbsp; {service.serviceRate}/
                                {service.service_id.id === 1 ||
                                service.service_id.id === 2
                                  ? "day"
                                  : "night"}
                              </Typography>
                            );
                          } else return <div />;
                        })}
                    </div>
                  </Grid>
                  <Grid item xs={6} style={{ paddingLeft: "2rem" }}>
                    <div style={{ display: "flex" }}>
                      <Typography
                        style={{
                          fontSize: 36,
                          fontWeight: 500,
                          color: "white",
                        }}
                      >
                        Total&nbsp;&nbsp; : &nbsp;&nbsp;€ {selectedFinalPrice}
                      </Typography>
                    </div>
                    <Typography
                      style={{
                        marginTop: "2rem",
                        color: "#FFFFFF",
                        fontSize: 14,
                        textAlign: "left",
                      }}
                    >
                      "This is the estimated cost of your booking based on the
                      details above. It is only payable if your booking is
                      confirmed after you meet the sitter"
                    </Typography>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Grid item xs={3}>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <Button
                  variant="outlined"
                  className={classes.bookingBtn}
                  onClick={sendMeetingRequest}
                >
                  Send Meeting Request &gt;&gt;
                </Button>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  ) : (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: 42,
      }}
    >
      <h2
        style={{
          color: "#E8770E",
          fontSize: 28,
          fontFamily: "Kanit",
        }}
      >
        Meeting Request Sent Successfully
      </h2>
    </div>
  );
};

export default MeetingRequest;
