import React from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Button,
  Tabs,
  Tab,
  Card,
  CardContent,
  TextField,
  Checkbox,
  FormControlLabel,
  Link,
  Icon,
} from "@material-ui/core";
import axios from "axios";
import loginBanner from "../assets/login/login-banner.png";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";
import google from "../assets/login/google.png";
import facebook from "../assets/login/facebook.svg";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import passwordUpdate from "../assets/accountSetting/passwordUpdate.png";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const styles = (theme) => ({
  loginRoot: {
    marginTop: 92,
  },
  tabWrapper: {
    width: 577,
    margin: "0 auto",
    marginTop: theme.spacing(13),
  },
  bannerContainer: {
    position: "absolute",
    padding: 64,
    "& h1": {
      fontSize: 32,
      fontFamily: "Fredoka One",
      color: "#ffffff",
      maxWidth: 380,
    },
    "& p": {
      fontSize: 24,
      fontFamily: "Fredoka One",
      color: "#ffffff",
      maxWidth: 380,
    },
  },
  loginCard: {
    border: "1px solid rgba(232, 119, 14, 0.7)",
    boxSizing: "border-box",
    marginTop: "-1px",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
  },
  login: {
    padding: theme.spacing(2),
    "& div": {
      width: "100%",
      marginBottom: theme.spacing(1),
    },
  },
  checkbox: {
    color: "#E8770E",
  },
  checkboxLabel: {
    color: "#E8770E !important",
    "& span": {
      fontSize: 14,
    },
  },
  forgotPassword: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    "& a": {
      color: "#E8770E",
    },
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    padding: theme.spacing(1, 4),
  },
  dividerContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: theme.spacing(3),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  dividerBorder: {
    borderBottom: "1px solid #E8770E",
    width: "20% !important",
    marginBottom: "0 !important",
  },
  dividerContent: {
    padding: theme.spacing(0, 1),
    color: "#E8770E",
  },
  socialButtons: {
    margin: theme.spacing(0, 5),
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
  },
  registerSection: {
    margin: theme.spacing(4, 0),
    textAlign: "center",
  },
  singleTab: {
    border: "1px solid rgba(232, 119, 14, 0.7)",
    borderRadius: "8px 8px 0px 0px",
    background: "#E8770E",
    opacity: 1,
    color: "#FFFFFF",
  },
  selectedTab: {
    background: "#FFFFFF",
    color: "#E8770E !important",
    borderBottom: "none",
  },
  indicatorTab: {
    display: "none",
  },
});

const TabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const a11yProps = (index) => {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
};

const Divider = (props, { children }) => {
  const { classes } = props.classes;

  return (
    <div className={classes.dividerContainer}>
      <div className={classes.dividerBorder} />
      <span className={classes.dividerContent}>{children}</span>
      <div className={classes.dividerBorder} />
    </div>
  );
};

const FeatureIcon = (props) => {
  return (
    <Icon>
      <img
        alt="edit"
        src={props.src}
        width={props.width ? props.width : "100%"}
        height={props.height ? props.height : ""}
      />
    </Icon>
  );
};

const CssTextField = withStyles({
  root: {
    color: "#E8770E",
    "& label": {
      color: "#E8770E",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "1px solid #E8770E",
    },
    "& label.Mui-focused": {
      color: "#3EB3CD",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#3EB3CD",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "2px solid #3EB3CD",
    },
  },
})(TextField);

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      tabValue: 0,
      email: "",
      password: "",
      checkedValue: false,
      showModal: false,
    };
  }

  handleTabChange = (event, value) => {
    this.setState(
      {
        email: "",
        password: "",
      },
      () => {
        this.setState({
          tabValue: value,
        });
      }
    );
  };

  handleCheckboxChange = (event) => {
    this.setState({
      checkedValue: event.target.checked,
    });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    console.log(name, value);
    this.setState((user) => ({ ...user, [name]: value }));
  };

  handleModal = () => {
    this.setState((prevState) => {
      return { showModal: !prevState.showModal };
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      email: this.state.email,
      password: this.state.password,
    };
    axios
      .post(`${config.apiUrl}/custom/login/`, data)
      .then((response) => {
        console.log(response);
        if (
          response.status === 200 &&
          response.data.error !== "Please verify your email id."
        ) {
          localStorage.setItem("user", JSON.stringify(response.data));
          if (response.data.user_type.is_petowner === true) {
            window.location.href = "/sitter-search";
          } else {
            window.location.href = "/dashboard";
          }
        } else {
          this.handleModal();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.loginRoot}>
        <Grid container justify="center">
          <Grid item xs={5}>
            <div className={classes.bannerContainer}>
              <h1>Welcome to DogStays</h1>
              <p>
                We’ll connect you with just the right dog person to look after
                your dog.
              </p>
            </div>
            <img
              src={loginBanner}
              alt="login banner"
              width="100%"
              height="100%"
            />
          </Grid>
          <Grid item xs={7}>
            <div className={classes.tabWrapper}>
              <Tabs
                value={this.state.tabValue}
                onChange={this.handleTabChange}
                aria-label="login & register tabs"
                classes={{ indicator: classes.indicatorTab }}
              >
                <Tab
                  label="Pet Parent"
                  {...a11yProps(0)}
                  classes={{
                    selected: classes.selectedTab,
                    root: classes.singleTab,
                  }}
                />
                <Tab
                  label="Pet Sitter"
                  {...a11yProps(1)}
                  classes={{
                    selected: classes.selectedTab,
                    root: classes.singleTab,
                  }}
                />
              </Tabs>
              <Card className={classes.loginCard}>
                <CardContent>
                  <TabPanel value={this.state.tabValue} index={0}>
                    <div>
                      <p
                        style={{
                          fontSize: "18px",
                          marginLeft: "16px",
                          color: "#e87837",
                        }}
                      >
                        Login as Dog Parent
                      </p>
                    </div>
                    <div className={classes.login}>
                      <CssTextField
                        id="email-id"
                        label="E-mail Address"
                        type="email"
                        name="email"
                        onChange={this.handleChange}
                        value={this.state.email}
                      />
                      <CssTextField
                        id="password"
                        label="Password"
                        type="password"
                        name="password"
                        onChange={this.handleChange}
                        value={this.state.password}
                      />
                      <Grid container justify="space-between">
                        <Grid item xs={6}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                checked={this.state.checkedValue}
                                onChange={this.handleCheckboxChange}
                                name="checked"
                                color="primary"
                                className={classes.checkbox}
                              />
                            }
                            label="Remember me"
                            className={classes.checkboxLabel}
                          />
                        </Grid>
                        <Grid item xs={6} className={classes.forgotPassword}>
                          <Link href="/forgot-password" to="/forgot-password">
                            Forgot Password ?
                          </Link>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        justify="center"
                        style={{ marginTop: "1rem" }}
                      >
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.orangeBtn}
                          onClick={this.handleSubmit}
                        >
                          Login
                        </Button>
                      </Grid>
                      <Divider classes={this.props}>Or Login With</Divider>
                      <Grid
                        container
                        justify="center"
                        style={{ marginTop: "2rem" }}
                      >
                        <Button
                          variant="outlined"
                          className={classes.socialButtons}
                          startIcon={<FeatureIcon src={google} width="18px" />}
                        >
                          Google
                        </Button>
                        <Button
                          variant="outlined"
                          className={classes.socialButtons}
                          startIcon={
                            <FeatureIcon src={facebook} height="100%" />
                          }
                        >
                          Facebook
                        </Button>
                      </Grid>
                      <Typography
                        variant="subtitle1"
                        component="div"
                        color="primary"
                        className={classes.registerSection}
                      >
                        New User ?
                        <Link
                          href="/register"
                          // onClick={(event) => event.preventDefault()}
                          color="secondary"
                          style={{ marginLeft: 10, cursor: "pointer" }}
                        >
                          Register
                        </Link>
                      </Typography>
                    </div>
                  </TabPanel>
                  <TabPanel value={this.state.tabValue} index={1}>
                    <div>
                      <p
                        style={{
                          fontSize: "18px",
                          marginLeft: "16px",
                          color: "#e87837",
                        }}
                      >
                        Login as Dog Sitter
                      </p>
                    </div>
                    <div className={classes.login}>
                      <CssTextField
                        id="email-id"
                        label="E-mail Address"
                        type="email"
                        name="email"
                        onChange={this.handleChange}
                        value={this.state.email}
                      />
                      <CssTextField
                        id="password"
                        label="Password"
                        type="password"
                        name="password"
                        onChange={this.handleChange}
                        value={this.state.password}
                      />
                      <Grid container justify="space-between">
                        <Grid item xs={6}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                checked={this.state.checkedValue}
                                onChange={this.handleCheckboxChange}
                                name="checked"
                                color="primary"
                                className={classes.checkbox}
                              />
                            }
                            label="Remember me"
                            className={classes.checkboxLabel}
                          />
                        </Grid>
                        <Grid item xs={6} className={classes.forgotPassword}>
                          <Link href="/forgot-password" to="/forgot-password">
                            Forgot Password ?
                          </Link>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        justify="center"
                        style={{ marginTop: "1rem" }}
                      >
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.orangeBtn}
                          onClick={this.handleSubmit}
                        >
                          Login
                        </Button>
                      </Grid>
                      <Divider classes={this.props}>Or Login With</Divider>
                      <Grid
                        container
                        justify="center"
                        style={{ marginTop: "2rem" }}
                      >
                        <Button
                          variant="outlined"
                          className={classes.socialButtons}
                          startIcon={<FeatureIcon src={google} width="18px" />}
                        >
                          Google
                        </Button>
                        <Button
                          variant="outlined"
                          className={classes.socialButtons}
                          startIcon={
                            <FeatureIcon src={facebook} height="100%" />
                          }
                        >
                          Facebook
                        </Button>
                      </Grid>
                      <Typography
                        variant="subtitle1"
                        component="div"
                        color="primary"
                        className={classes.registerSection}
                      >
                        New User ?
                        <Link
                          href="/register"
                          // onClick={(event) => event.preventDefault()}
                          color="secondary"
                          style={{ marginLeft: 10, cursor: "pointer" }}
                        >
                          Register
                        </Link>
                      </Typography>
                    </div>
                  </TabPanel>
                </CardContent>
              </Card>
            </div>
          </Grid>
        </Grid>

        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={this.state.showModal}
          onClose={this.handleModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.showModal}>
            <div className={classes.paper}>
              <Typography color="primary" className={classes.modalhead1}>
                Verify your email
              </Typography>
              <Typography className={classes.modalhead2}>
                Please check your mail for verification link.
              </Typography>
              <img src={passwordUpdate} />
            </div>
          </Fade>
        </Modal>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Login);
