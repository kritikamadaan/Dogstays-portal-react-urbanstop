import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Divider,
  Button,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  TextareaAutosize,
} from "@material-ui/core";
import dogImage from "../../assets/sitterSearch/Vector.png";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import DeleteIcon from "@material-ui/icons/Delete";
import axios from "axios";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import moment from "moment";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Autocomplete from "@material-ui/lab/Autocomplete";
import profileFallback from "../../assets/sitterDashboard/profileFallback.png";
import S3FileUpload from "react-s3";
import Slider from "@material-ui/core/Slider";
import Cropper from "react-easy-crop";
import Loader from "./components/LoaderComponent";
import { IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { DropzoneDialog } from "material-ui-dropzone";
import { Link } from "react-router-dom";

let imageUploader = null;

const user = JSON.parse(localStorage.getItem("user"));

console.log('user ',user)
const configBucket = user && user.user !== undefined
  ? {
      bucketName: "dogstayss3",
      dirName:
        "user/" +
        (user !== null || user !== undefined  ? user.user.id + "/" : "test/")
        ,
      region: "us-east-1",
      accessKeyId: "AKIA5YN7PDU5YXKJ3BW2",
      secretAccessKey: "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",
    }
  : null;

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    minHeight: "80vh",
  },
  dogImage: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  editField: {
    width: 250,
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    padding: theme.spacing(0.5, 2),
  },
  orangeBtnOutlined: {
    color: "#F58220",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    border: "1px solid #F58220",
    width: "max-content",
    padding: theme.spacing(0.5, 2),
    backgroundColor: "#ffffff",
    marginRight: 18,
  },
  dogInfoWrapper: {
    backgroundColor: theme.palette.primary.main,
    textAlign: "center",
    color: "white",
    width: 294,
    marginTop: -5,
    margin: "0 auto",
    padding: theme.spacing(2, 0),
  },
  profileDivider: {
    backgroundColor: theme.palette.primary.main,
    width: 1.5,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  aboutPet: {
    marginTop: theme.spacing(4),
    color: "#979797",
    fontSize: 22,
    fontWeight: 500,
  },
  aboutPetDesc: {
    color: "#5E5A5A",
    fontSize: 16,
  },
  featureWrapper: {
    margin: theme.spacing(2, 1),
  },
  featureHeadings: {
    fontSize: 16,
    fontWeight: 500,
  },
  featureDesc: {
    color: "#5E5A5A",
    fontSize: 16,
    fontWeight: 500,
  },
  moreImageHeader: {
    paddingBottom: theme.spacing(0.5),
    borderBottom: "1px solid #E8770E",
    fontSize: 20,
    fontWeight: 500,
    width: "fit-content",
    margin: "0 auto",
  },
  moreImageWrapper: {
    display: "flex",
    justifyContent: "space-between",
    margin: theme.spacing(5, 0),
  },
  rightFeatureWrapper: {
    margin: theme.spacing(3, 0),
  },
  rightFeatureHeading: {
    fontSize: 16,
    fontWeight: 500,
  },
  rightFeatureContent: {
    fontSize: 16,
    color: "#5E5A5A",
  },
  dogBreeds: {
    padding: theme.spacing(2),
  },
  cropperPaper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  cropContainer: {
    minHeight: 500,
  },
  sliderControls: {
    position: "absolute",
    bottom: 0,
    left: "50%",
    width: "50%",
    transform: "translateX(-50%)",
    height: 80,
    display: "flex",
    alignItems: "center",
  },
  multiplePhotos: {
    width: 200,
    height: 120,
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 8,
    objectFit: "cover",
  },
});
const DogBreeds = [
  {
    id: 1,
    title: "Retriever",
  },
  {
    id: 2,
    title: "German Shepherd",
  },
  {
    id: 3,
    title: "Golden Retriever",
  },
  {
    id: 4,
    title: "French Bulldog",
  },
  {
    id: 5,
    title: "Bulldog",
  },
  {
    id: 6,
    title: "Beagle",
  },
  // {
  //   id: 7,
  //   title: "",
  // },
  {
    id: 8,
    title: "Siberian Husky",
  },
  {
    id: 9,
    title: "Shiba Inu",
  },
  {
    id: 10,
    title: "Shih Tzu",
  },
  {
    id: 11,
    title: "St. Bernard",
  },
  {
    id: 12,
    title: "Basset Hound",
  },
  {
    id: 13,
    title: "Bernese Mountain Dog",
  },
  {
    id: 14,
    title: "Boston Terrier",
  },
  {
    id: 15,
    title: "Border Collie",
  },
  {
    id: 16,
    title: "Australian Shepherd",
  },
  {
    id: 17,
    title: "Chihuahua",
  },
  {
    id: 18,
    title: "Cocker Spaniel",
  },
  {
    id: 19,
    title: "Dachshund",
  },
  {
    id: 20,
    title: "Doberman Pinscher",
  },
  {
    id: 21,
    title: "French Bulldog",
  },
  {
    id: 22,
    title: "Great Dane",
  },
  {
    id: 23,
    title: "Havanese",
  },
  {
    id: 24,
    title: "Mastiff",
  },
  {
    id: 25,
    title: "Pembroke Welsh Corgi",
  },
  {
    id: 26,
    title: "Poodle",
  },
  {
    id: 27,
    title: "Pug",
  },
  {
    id: 28,
    title: "Rottweiler",
  },
  {
    id: 29,
    title: "Vizslas",
  },
  {
    id: 30,
    title: "Yorkshire Terrier",
  },
  {
    id: 31,
    title: "Dalmatian",
  },
  {
    id: 32,
    title: "Samoyed",
  },
  {
    id: 33,
    title: "Italian Greyhound",
  },
  {
    id: 34,
    title: "Russell Terrier",
  },
  {
    id: 35,
    title: "Japanese Chin",
  },
];
const CatBreeds = [
  {
    id: 1,
    title: "Abyssinian",
  },
  {
    id: 2,
    title: "Aegean",
  },
  {
    id: 3,
    title: "American Bobtail",
  },
];
// const DogProfileDashboard = (props) => {
//   const classes = useStyles();
//   const [dog, setDog] = useState(null);

//   useEffect(() => {
//     getDogProfile(props.id);
//   }, [props.id]);

// //   const getDogProfile = (id) => {
// //     fetch(`http://3.215.2.1:8000/api/petprofile/${id}`, {
// //       method: "GET",
// //       headers: {
// //         "Content-Type": "application/json",
// //       },
// //     })
// //       .then((res) => {
// //         return res.json();
// //       })
// //       .then((data) => {
// //         setDog(data);
// //       });
// //   };
// // };
let multiplePetImageUrl = [];

class DogProfileDashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      dog: [],
      dob: null,
      type_of_pets: null,
      editable: false,
      modalOpen: false,
      confirmModalOpen: false,
      isConfirm: false,
      profileFileName: "",
      imageCropperModal: false,
      imageSrc:
        "https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000",
      crop: { x: 0, y: 0 },
      zoom: 1,
      aspect: 1 / 1,
      PetImageFiles: [],
      PetImageFilesOpen: false,
    };
  }

  componentDidMount() {
    // const id = this.props.match.params.id;
    this.getDogProfile(this.props.petId);
  }

  handleEditable = () => {
    this.setState((prevState) => {
      return {
        editable: !prevState.editable,
      };
    });
  };

  handleTextChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleNuteredChange = (event) => {
    this.setState({
      nutered: event.target.value,
    });
  };

  handleStayHomeAloneChoiceChange = (event) => {
    this.setState({
      stay_home_alone_choice: event.target.value,
    });
  };

  handleToiletTrainedChoiceChange = (event) => {
    this.setState({
      toilet_trained_choice: event.target.value,
    });
  };

  handleFriendlyWithDogsChoiceChange = (event) => {
    this.setState({
      friendly_with_dogs_choice: event.target.value,
    });
  };

  handleFriendlyWithCatsChoiceChange = (event) => {
    this.setState({
      friendly_with_cats_choice: event.target.value,
    });
  };

  handleFriendlyWithStrangersChoiceChange = (event) => {
    this.setState({
      friendly_with_strangers_choice: event.target.value,
    });
  };

  handleFriendlyWithChildrensChoiceChange = (event) => {
    this.setState({
      friendly_with_children_choice: event.target.value,
    });
  };

  handleChewsThingsChoiceChange = (event) => {
    this.setState({
      chews_things_choice: event.target.value,
    });
  };

  handleAggressiveChoiceChange = (event) => {
    this.setState({
      is_aggressive_choice: event.target.value,
    });
  };
  handleProfileUpload = (e) => {
    this.refs.imageUploader.click();
  };

  fileUploader = (e) => {
    const file = e.target.files[0];
    let formData = new FormData();
    formData.append("file", file);
    configBucket.dirName = configBucket.dirName + "pet/profilePhotos/";
    console.log(configBucket);
    S3FileUpload.uploadFile(file, configBucket)
      .then((data) => {
        console.log("bucket_data", data);
        this.setState({
          profile_photo: data.location,
        });
      })
      .catch((err) => console.error(err));
    this.setState({
      profileFileName: e.target.files[0].name,
    });
  };
  handleRadioChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  onCropChange = (crop) => {
    this.setState({ crop });
  };

  onCropComplete = (croppedArea, croppedAreaPixels) => {
    const self = this;
    console.log(croppedArea, croppedAreaPixels);
    this.croppedImage(this.state.imageDataUrl, croppedAreaPixels, 0);
  };

  async croppedImage(imageDataUrl, croppedAreaPixels, rotation) {
    const croppedImage = await this.getCroppedImg(
      imageDataUrl,
      croppedAreaPixels,
      rotation
    );
    this.croppedImageData(croppedImage);
  }

  croppedImageData = (croppedImage) => {
    this.setState({
      croppedImage: croppedImage,
    });
  };

  async getCroppedImg(imageSrc, pixelCrop, rotation = 0) {
    const image = await this.createImage(imageSrc);
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    const maxSize = Math.max(image.width, image.height);
    const safeArea = 2 * ((maxSize / 2) * Math.sqrt(2));

    // set each dimensions to double largest dimension to allow for a safe area for the
    // image to rotate in without being clipped by canvas context
    canvas.width = safeArea;
    canvas.height = safeArea;

    // translate canvas context to a central location on image to allow rotating around the center.
    ctx.translate(safeArea / 2, safeArea / 2);
    ctx.rotate(rotation);
    ctx.translate(-safeArea / 2, -safeArea / 2);

    // draw rotated image and store data.
    ctx.drawImage(
      image,
      safeArea / 2 - image.width * 0.5,
      safeArea / 2 - image.height * 0.5
    );
    const data = ctx.getImageData(0, 0, safeArea, safeArea);

    // set canvas width to final desired crop size - this will clear existing context
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;

    // paste generated rotate image with correct offsets for x,y crop values.
    ctx.putImageData(
      data,
      Math.round(0 - safeArea / 2 + image.width * 0.5 - pixelCrop.x),
      Math.round(0 - safeArea / 2 + image.height * 0.5 - pixelCrop.y)
    );

    // As Base64 string
    return canvas.toDataURL("image/jpeg");

    // As a blob
    // return new Promise((resolve) => {
    //   canvas.toBlob((file) => {
    //     resolve(URL.createObjectURL(file));
    //   }, "image/jpeg");
    // });
  }

  createImage = (url) =>
    new Promise((resolve, reject) => {
      const image = new Image();
      image.addEventListener("load", () => resolve(image));
      image.addEventListener("error", (error) => reject(error));
      image.setAttribute("crossOrigin", "anonymous"); // needed to avoid cross-origin issues on CodeSandbox
      image.src = url;
    });

  onZoomChange = (zoom) => {
    this.setState({ zoom });
  };

  handleImageCropperModal = () => {
    this.setState((prevState) => {
      return {
        imageCropperModal: !prevState.imageCropperModal,
      };
    });
  };
  fileSelector = (event) => {
    this.setState({
      profileFileName: event.target.files[0].name,
    });
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        this.setState(
          {
            imageDataUrl: e.target.result,
          },
          () => {
            this.handleImageCropperModal();
          }
        );
      };
    }
  };
  onImageCropDone = (e) => {
    e.persist();
    // const file = this.state.croppedImage + "name.png";
    // let formData = new FormData();
    // formData.append("file", file);
    // configBucket.dirName = configBucket.dirName + "profilePhotos";

    // S3FileUpload.uploadFile(formData, configBucket)
    //   .then((data) => {
    //     this.setState(
    //       {
    //         profile_photo: data.location,
    //       },
    //       () => {
    //         console.log(this.state.profile_photo);
    //       }
    //     );
    //   })
    //   .catch((err) => console.error(err));
    this.handleImageCropperModal();
  };
  getBreed = (id) => {
    console.log("DogBreed", id);
    let DogBreed = DogBreeds.find((breed) => breed.id === parseInt(id));
    console.log(DogBreed, "DogBreed");
    return DogBreed.title;
  };
  getCatBreed = (id) => {
    console.log("DogBreed", id);
    let DogBreed = CatBreeds.find((breed) => breed.id === parseInt(id));
    console.log(DogBreed, "DogBreed");
    return DogBreed.title;
  };
  handleSave = () => {
    const id = this.props.petId;
    const data = {
      user: user.user.id,
      id: id,
      cust_id: this.state.cust_id,
      pet_id: `${this.state.cust_id}_1`,
      pet_choice: this.state.type_of_pet,
      name: this.state.name,
      type_of_pet: this.state.type_of_pets,
      breed: this.state.breed,
      profile_photo: this.state.croppedImage,
      dob: this.state.dob,
      nutered: this.state.nutered === "yes" ? true : false,
      gender: this.state.gender,
      stay_home_alone_choice: this.state.stay_home_alone_choice,
      stay_home_alone_detail: this.state.stay_home_alone_detail,
      toilet_trained_choice: this.state.toilet_trained_choice,
      toilet_trained_detail: this.state.toilet_trained_detail,
      friendly_with_dogs_choice: this.state.friendly_with_dogs_choice,
      friendly_with_dogs_detail: this.state.friendly_with_dogs_detail,
      friendly_with_cats_choice: this.state.friendly_with_cats_choice,
      friendly_with_cats_detail: this.state.friendly_with_cats_detail,
      friendly_with_strangers_choice: this.state.friendly_with_strangers_choice,
      friendly_with_strangers_detail: this.state.friendly_with_strangers_detail,
      friendly_with_children_choice:
        this.state.friendly_with_children_choice === "yes" ? true : false,
      friendly_with_children_detail: this.state.friendly_with_children_detail,
      chews_things_choice: this.state.chews_things_choice,
      chews_things_detail: this.state.chews_things_detail,
      is_aggressive_choice: this.state.is_aggressive_choice,
      is_aggressive_detail: this.state.is_aggressive_detail,
      additional_info: this.state.additional_info,
      pet_gallary: JSON.stringify(this.state.PetImageFiles),
    };

    axios
      .put(`${config.apiUrl}/api/petprofile/${id}`, data)
      .then((response) => {
        console.log("Add Pets response", response);
        this.setState((prevState) => {
          return {
            editable: !prevState.editable,
            isConfirm: true,
          };
        });
        this.getDogProfile(this.props.petId);
        this.props.getSitterPets();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getDogProfile = (id) => {
    fetch(`http://3.215.2.1:8000/api/petprofile/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState(
          {
            dog: data,
            dob: data.dob,
            type_of_pets: data.type_of_pet,
            nutered: data.nutered ? "yes" : "no",
            gender: data.gender,
            stay_home_alone_choice: data.stay_home_alone_choice,
            friendly_with_dogs_choice: data.friendly_with_dogs_choice,
            friendly_with_cats_choice: data.friendly_with_cats_choice,
            friendly_with_strangers_choice: data.friendly_with_strangers_choice,
            friendly_with_children_choice: data.friendly_with_children_choice
              ? "yes"
              : "no",
            chews_things_choice: data.chews_things_choice,
            is_aggressive_choice: data.is_aggressive_choice,
            profile_photo: data.profile_photo,
            PetImageFiles: data.pet_gallary ? JSON.parse(data.pet_gallary) : [],
          },
          () => {
            multiplePetImageUrl =
              data.pet_gallary !== null ||
              data.pet_gallary !== "" ||
              data.pet_gallary !== []
                ? this.state.PetImageFiles
                : [];
          }
        );
      });
  };

  deletePet = () => {
    console.log('deleted pet click')
    const id = this.props.petId;

    axios
      .delete(`${config.apiUrl}/api/petprofile/${id}`)
      .then((response) => {
        console.log("Add Pets response", response);
        console.log('window location href',window.location.href)
        window.location.href = "/dashboard/profile";
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleModal = () => {
    this.setState((prevState) => {
      return {
        modalOpen: !prevState.modalOpen,
      };
    });
  };
  handleConfirmModal = () => {
    this.setState({
      confirmModalOpen: !this.state.confirmModalOpen,
      isConfirm: false,
    });
  };
  onCropChange = (crop) => {
    this.setState({ crop });
  };

  onCropComplete = (croppedArea, croppedAreaPixels) => {
    const self = this;
    console.log(croppedArea, croppedAreaPixels);
    this.croppedImage(this.state.imageDataUrl, croppedAreaPixels, 0);
  };

  async croppedImage(imageDataUrl, croppedAreaPixels, rotation) {
    const croppedImage = await this.getCroppedImg(
      imageDataUrl,
      croppedAreaPixels,
      rotation
    );
    this.croppedImageData(croppedImage);
  }

  croppedImageData = (croppedImage) => {
    this.setState({
      croppedImage: croppedImage,
    });
  };

  async getCroppedImg(imageSrc, pixelCrop, rotation = 0) {
    const image = await this.createImage(imageSrc);
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    const maxSize = Math.max(image.width, image.height);
    const safeArea = 2 * ((maxSize / 2) * Math.sqrt(2));

    // set each dimensions to double largest dimension to allow for a safe area for the
    // image to rotate in without being clipped by canvas context
    canvas.width = safeArea;
    canvas.height = safeArea;

    // translate canvas context to a central location on image to allow rotating around the center.
    ctx.translate(safeArea / 2, safeArea / 2);
    ctx.rotate(rotation);
    ctx.translate(-safeArea / 2, -safeArea / 2);

    // draw rotated image and store data.
    ctx.drawImage(
      image,
      safeArea / 2 - image.width * 0.5,
      safeArea / 2 - image.height * 0.5
    );
    const data = ctx.getImageData(0, 0, safeArea, safeArea);

    // set canvas width to final desired crop size - this will clear existing context
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;

    // paste generated rotate image with correct offsets for x,y crop values.
    ctx.putImageData(
      data,
      Math.round(0 - safeArea / 2 + image.width * 0.5 - pixelCrop.x),
      Math.round(0 - safeArea / 2 + image.height * 0.5 - pixelCrop.y)
    );

    // As Base64 string
    return canvas.toDataURL("image/jpeg");

    // As a blob
    // return new Promise((resolve) => {
    //   canvas.toBlob((file) => {
    //     resolve(URL.createObjectURL(file));
    //   }, "image/jpeg");
    // });
  }

  createImage = (url) =>
    new Promise((resolve, reject) => {
      const image = new Image();
      image.addEventListener("load", () => resolve(image));
      image.addEventListener("error", (error) => reject(error));
      image.setAttribute("crossOrigin", "anonymous"); // needed to avoid cross-origin issues on CodeSandbox
      image.src = url;
    });

  onZoomChange = (zoom) => {
    this.setState({ zoom });
  };

  handleImageCropperModal = () => {
    this.setState((prevState) => {
      return {
        imageCropperModal: !prevState.imageCropperModal,
      };
    });
  };
  handlePetImageUpload = (files) => {
    this.setState({
      PetImageFilesOpen: false,
    });
    files.map((file) => {
      let formData = new FormData();
      formData.append("file", file);
      configBucket.dirName = configBucket.dirName + "photosWithPets";
      S3FileUpload.uploadFile(file, configBucket)
        .then((data) => {
          const obj = {
            location: data.location,
          };
          console.log(obj);
          multiplePetImageUrl.push(obj);
          console.log(multiplePetImageUrl);
          this.setState({
            PetImageFiles: multiplePetImageUrl,
          });
        })
        .catch((err) => console.error(err));
    });

    console.log("files", files, this.state.PetImageFiles);
  };
  handlePetMultipleOpen = (event) => {
    this.setState((prevState) => {
      return {
        PetImageFilesOpen: !prevState.PetImageFilesOpen,
      };
    });
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.state.dog && (
          <Grid container style={{ padding: 48 }}>
            <Grid item xs={5} style={{ paddingRight: "1rem" }}>
              <div className={classes.dogImage}>
                {!this.state.editable && (
                  <div
                    style={{
                      padding:
                        this.state.dog.profile_photo === null ||
                        this.state.dog.profile_photo === ""
                          ? 32
                          : 0,
                      height: 180,
                      width: 180,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src={
                        this.state.dog.profile_photo === null ||
                        this.state.dog.profile_photo === ""
                          ? dogImage
                          : this.state.dog.profile_photo
                      }
                      alt="profile-fallback"
                      style={{
                        height:
                          this.state.dog.profile_photo === null ||
                          this.state.dog.profile_photo === ""
                            ? 48
                            : 180,
                        width:
                          this.state.dog.profile_photo === null ||
                          this.state.dog.profile_photo === ""
                            ? 48
                            : 180,
                        borderRadius: 8,
                      }}
                    />
                  </div>
                )}
                {this.state.editable && (
                  <div
                    className={classes.profileImg}
                    style={{ cursor: "pointer" }}
                    onClick={this.handleProfileUpload}
                  >
                    <div style={{ padding: 12, textAlign: "center" }}>
                      {this.state.profile_image === "" ||
                      this.state.profile_image === null ||
                      this.state.profileFileName ? (
                        <img
                          src={profileFallback}
                          alt="profile-fallback"
                          style={{
                            height: 48,
                            width: 48,
                          }}
                        />
                      ) : (
                        <img
                          src={this.state.dog.profile_photo}
                          alt="profile-fallback"
                          style={{
                            height: this.state.dog.profile_photo ? 180 : 48,
                            width: this.state.dog.profile_photo ? 180 : 48,
                            borderRadius: 8,
                          }}
                        />
                      )}
                      <Typography
                        color="primary"
                        style={{
                          fontSize: 20,
                          fontWeight: 500,
                        }}
                      >
                        {this.state.profileFileName
                          ? this.state.profileFileName
                          : this.state.profile_photo === "" ||
                            this.state.profile_photo === null
                          ? "Upload"
                          : "Click to Upload"}
                      </Typography>
                    </div>
                    <input
                      type="file"
                      id="imageFile"
                      ref="imageUploader"
                      name="profile_photo"
                      style={{ display: "none" }}
                      onChange={this.fileSelector}
                    />
                  </div>
                )}
                {/* <img
                  src={
                    this.state.dog.profile_photo === null ||
                    this.state.dog.profile_photo === ""
                      ? dogImage
                      : this.state.dog.profile_photo
                  }
                  alt="dog profile"
                  width="390px"
                  height="240px"
                /> */}
                <div className={classes.dogInfoWrapper}>
                  {!this.state.editable && (
                    <Typography style={{ fontSize: 30 }}>
                      {this.state.dog.name}
                    </Typography>
                  )}
                  {this.state.editable && (
                    <TextField
                      label="name"
                      name="name"
                      defaultValue={this.state.dog.name}
                      value={this.state.name}
                      onChange={this.handleTextChange}
                      className={classes.editField}
                    />
                  )}
                  {!this.state.editable && this.state.dog.pet_choice === "other animal" &&(
                    <Typography style={{ fontSize: 18 }}>
                      {this.state.dog.type_of_pet}
                    </Typography>
                  )}
                  {this.state.editable && this.state.dog.pet_choice === "other animal" &&(
                    <TextField
                      label="Type of pet"
                      name="type_of_pets"
                      defaultValue={this.state.dog.type_of_pet}
                      value={this.state.type_of_pets}
                      onChange={this.handleTextChange}
                      className={classes.editField}
                    />
                  )}
                  {!this.state.editable &&
                    this.state.dog.pet_choice !== "cat" &&
                    this.state.dog.pet_choice !== "other animal" && (
                      <Typography style={{ fontSize: 18 }}>
                        {/* {DogBreeds.find( ({ id }) => id === this.state.dog.breed)} */}
                        {this.state.dog.breed
                          ? this.getBreed(this.state.dog.breed)
                          : null}
                      </Typography>
                    )}
                  {/* {!this.state.editable &&this.state.dog.pet_choice !== "dog" && this.state.dog.pet_choice !== "other animal" &&(
                    <Typography style={{ fontSize: 18 }}>
                      {this.state.dog.breed
                        ? this.getCatBreed(this.state.dog.breed)
                        : null}
                    </Typography>
                  )} */}
                  {this.state.editable &&
                    this.state.dog.pet_choice !== "other animal" &&
                    this.state.dog.pet_choice !== "cat" && (
                      <Autocomplete
                        id="combo-box-demo"
                        options={
                          this.state.dog.pet_choice === "cat"
                            ? CatBreeds
                            : DogBreeds
                        }
                        // defaultValue={this.state.dog.breed?this.getBreed(this.state.dog.breed):null}
                        getOptionLabel={(option) => option.title}
                        className={classes.dogBreeds}
                        onChange={(event, value) => {
                          event.preventDefault();
                          this.setState({
                            breed: value.id,
                          });
                        }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label={
                              this.state.dog.pet_choice === "cat"
                                ? "Cat Breed"
                                : "Dog Breed"
                            }
                            variant="outlined"
                          />
                        )}
                      />
                      // <TextField
                      //   label="Type of Pet"
                      //   name="breed"
                      //   defaultValue={this.state.dog.breed}
                      //   value={this.state.breed}
                      //   onChange={this.handleTextChange}
                      //   className={classes.editField}
                      // />
                    )}

                  {!this.state.editable && (
                    <Typography style={{ fontSize: 18 }}>{`Born on ${
                      this.state.dob
                        ? moment(this.state.dob, "YYYY-MM-DD").format(
                            "DD/MM/YYYY"
                          )
                        : " "
                    }`}</Typography>
                  )}
                  {this.state.editable && (
                    <TextField
                      name="dob"
                      type="date"
                      value={this.state.dob}
                      defaultValue={moment(this.state.dob, "DD/MM/YYYY").format(
                        "YYYY-MM-DD"
                      )}
                      onChange={this.handleTextChange}
                      className={classes.editField}
                    />
                  )}
                </div>
              </div>
              {/* <Typography className={classes.aboutPet}>
                About {this.state.dog.name}
              </Typography>

              {!this.state.editable && (
                <Typography className={classes.aboutPetDesc}>
                  {this.state.dog.additional_info}
                </Typography>
              )}
              {this.state.editable && (
                <TextField
                  label="Additional Info"
                  name="additional_info"
                  defaultValue={this.state.dog.additional_info}
                  value={this.state.additional_info}
                  onChange={this.handleTextChange}
                  className={classes.editField}
                />
              )} */}
              <div style={{ margin: "2rem 0" }}>
                <Grid container>
                  <Grid item xs={5}>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Gender
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            className={classes.featureDesc}
                            style={{ textTransform: "Capitalize" }}
                          >
                            {this.state.dog.gender}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <RadioGroup
                            aria-label="gender"
                            name="gender"
                            value={this.state.gender}
                            onChange={this.handleRadioChange}
                            row
                          >
                            <FormControlLabel
                              value="male"
                              control={<Radio color="primary" />}
                              label="Male"
                            />
                            <FormControlLabel
                              value="female"
                              control={<Radio color="primary" />}
                              label="Female"
                            />
                          </RadioGroup>
                        )}
                    </div>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Neutered
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.featureDesc}>
                            {this.state.dog.nutered ? "Yes" : "No"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <RadioGroup
                            aria-label="nutered"
                            name="nutered"
                            value={this.state.nutered}
                            onChange={this.handleNuteredChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                        )}
                    </div>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Stay Home Alone
                          </Typography>
                        )}
                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.featureDesc}>
                            {this.state.dog.stay_home_alone_choice === "yes"
                              ? "Yes"
                              : "No"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <RadioGroup
                            aria-label="stay_home_alone_choice"
                            name="stay_home_alone_choice"
                            value={this.state.stay_home_alone_choice}
                            onChange={this.handleStayHomeAloneChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value={"yes"}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={"no"}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                        )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Friendly With Dogs
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.dog.friendly_with_dogs_choice === "yes"
                            ? "Yes"
                            : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="friendly_with_dogs_choice"
                          name="friendly_with_dogs_choice"
                          value={this.state.friendly_with_dogs_choice}
                          onChange={this.handleFriendlyWithDogsChoiceChange}
                          row
                        >
                          <FormControlLabel
                            value={"yes"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"no"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Friendly With Cats
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.featureDesc}>
                            {this.state.dog.friendly_with_cats_choice === "yes"
                              ? "Yes"
                              : "No"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <RadioGroup
                            aria-label="friendly_with_cats_choice"
                            name="friendly_with_cats_choice"
                            value={this.state.friendly_with_cats_choice}
                            onChange={this.handleFriendlyWithCatsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value={"yes"}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={"no"}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                        )}
                    </div>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice === "cat" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Chews Things
                          </Typography>
                        )}
                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice === "cat" && (
                          <Typography className={classes.featureDesc}>
                            {this.state.dog.chews_things_choice === "1"
                              ? "Yes"
                              : this.state.dog.chews_things_choice === "2"
                              ? "No"
                              : this.state.dog.chews_things_choice === "3"
                              ? "Sometimes"
                              : "-"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice === "cat" && (
                          <RadioGroup
                            aria-label="chews_things_choice"
                            name="chews_things_choice"
                            value={this.state.chews_things_choice}
                            onChange={this.handleChewsThingsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value={"1"}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={"2"}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value={"3"}
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                        )}
                    </div>
                  </Grid>
                  <Grid item xs={2}></Grid>
                  <Grid item xs={5}>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Friendly With Strangers
                          </Typography>
                        )}
                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.featureDesc}>
                            {this.state.dog.friendly_with_strangers_choice ===
                            "yes"
                              ? "Yes"
                              : "No"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <RadioGroup
                            aria-label="friendly_with_strangers_choice"
                            name="friendly_with_strangers_choice"
                            value={this.state.friendly_with_strangers_choice}
                            onChange={
                              this.handleFriendlyWithStrangersChoiceChange
                            }
                            row
                          >
                            <FormControlLabel
                              value={"yes"}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={"no"}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                        )}
                    </div>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Friendly With Children
                          </Typography>
                        )}
                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.featureDesc}>
                            {this.state.dog.friendly_with_children_choice
                              ? "Yes"
                              : "No"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <RadioGroup
                            aria-label="friendly_with_children_choice"
                            name="friendly_with_children_choice"
                            value={this.state.friendly_with_children_choice}
                            onChange={
                              this.handleFriendlyWithChildrensChoiceChange
                            }
                            row
                          >
                            <FormControlLabel
                              value={"yes"}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={"no"}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                        )}
                    </div>
                    <div className={classes.featureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.featureHeadings}
                          >
                            Chews Things
                          </Typography>
                        )}
                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.featureDesc}>
                            {this.state.dog.chews_things_choice === "1"
                              ? "Yes"
                              : this.state.dog.chews_things_choice === "2"
                              ? "No"
                              : this.state.dog.chews_things_choice === "3"
                              ? "Sometimes"
                              : null}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <RadioGroup
                            aria-label="chews_things_choice"
                            name="chews_things_choice"
                            value={this.state.chews_things_choice}
                            onChange={this.handleChewsThingsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value={"1"}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={"2"}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value={"3"}
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                        )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Is Aggressive
                      </Typography>
                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.dog.is_aggressive_choice === "yes"
                            ? "Yes"
                            : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="is_aggressive_choice"
                          name="is_aggressive_choice"
                          value={this.state.is_aggressive_choice}
                          onChange={this.handleAggressiveChoiceChange}
                          row
                        >
                          <FormControlLabel
                            value={"yes"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"no"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div>
                <Typography className={classes.moreImageHeader} color="primary">
                  More Images
                </Typography>
                <div
                  style={{ display: "flex", justifyContent: "space-around" }}
                >
                  {!this.state.editable && this.state.PetImageFiles
                    ? this.state.PetImageFiles.map((file) => {
                        return (
                          <img
                            src={file.location}
                            alt="Pet 1"
                            width="200"
                            className={classes.multiplePhotos}
                            style={{ marginRight: 12 }}
                          />
                        );
                      })
                    : null}
                </div>
                {this.state.editable && (
                  <React.Fragment>
                    <Button
                      name="PetImageFilesOpen1"
                      onClick={this.handlePetMultipleOpen}
                      color="primary"
                    >
                      Upload
                    </Button>
                    <DropzoneDialog
                      name="PetImageFilesOpen1"
                      cancelButtonText={"cancel"}
                      submitButtonText={"save"}
                      open={this.state.PetImageFilesOpen}
                      onSave={this.handlePetImageUpload}
                      acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
                      showPreviews={true}
                      maxFileSize={5000000}
                      onClose={this.handlePetMultipleOpen}
                      className={classes.dropzoneStyles}
                    />
                  </React.Fragment>
                )}
              </div>
            </Grid>
            <Grid
              item
              xs={1}
              style={{ display: "flex", justifyContent: "center" }}
            >
              <Divider
                orientation="vertical"
                className={classes.profileDivider}
              />
            </Grid>
            <Grid item xs={6}>
              <div>
                <Grid container justify="space-between">
                  <Grid item xs={12}>
                    <div
                      style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "flex-end",
                      }}
                    >
                      {!this.state.editable && this.props.editable && (
                        <div>
                          <EditIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleEditable}
                            style={{ cursor: "pointer" }}
                          />

                          <DeleteIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleModal}
                            style={{ cursor: "pointer", marginLeft: 18 }}
                          />
                        </div>
                      )}
                      {this.state.editable && (
                        <div>
                          <SaveIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleConfirmModal}
                            style={{ cursor: "pointer" }}
                          />

                          <CancelIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleEditable}
                            style={{ cursor: "pointer", marginLeft: 18 }}
                          />
                        </div>
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={5}>
                    <div className={classes.rightFeatureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.rightFeatureHeading}
                          >
                            Stay Home Alone
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.rightFeatureContent}>
                            {this.state.dog.stay_home_alone_detail
                              ? this.state.dog.stay_home_alone_detail
                              : "-"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <TextField
                            label="Stay Home Alone Detail"
                            name="stay_home_alone_detail"
                            defaultValue={this.state.dog.stay_home_alone_detail}
                            value={this.state.stay_home_alone_detail}
                            onChange={this.handleTextChange}
                            className={classes.editField}
                          />
                        )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Friendly With Dogs
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.dog.friendly_with_dogs_detail
                            ? this.state.dog.friendly_with_dogs_detail
                            : "-"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Friendly With Dogs Detail"
                          name="friendly_with_dogs_detail"
                          defaultValue={
                            this.state.dog.friendly_with_dogs_detail
                          }
                          value={this.state.friendly_with_dogs_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.rightFeatureHeading}
                          >
                            Friendly With Cats
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.rightFeatureContent}>
                            {this.state.dog.friendly_with_cats_detail
                              ? this.state.dog.friendly_with_cats_detail
                              : "-"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <TextField
                            label="Friendly With Cats Detail"
                            name="friendly_with_cats_detail"
                            defaultValue={
                              this.state.dog.friendly_with_cats_detail
                            }
                            value={this.state.friendly_with_cats_detail}
                            onChange={this.handleTextChange}
                            className={classes.editField}
                          />
                        )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Additional Info
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.dog.additional_info
                            ? this.state.dog.additional_info
                            : "-"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Additional Info"
                          name="additional_info"
                          defaultValue={this.state.dog.additional_info}
                          value={this.state.additional_info}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={5}>
                    <div className={classes.rightFeatureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.rightFeatureHeading}
                          >
                            Friendly With Strangers
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.rightFeatureContent}>
                            {this.state.dog.friendly_with_strangers_detail
                              ? this.state.dog.friendly_with_strangers_detail
                              : "-"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <TextField
                            label="Friendly with strangers details"
                            name="friendly_with_strangers_detail"
                            defaultValue={
                              this.state.dog.friendly_with_strangers_detail
                            }
                            value={this.state.friendly_with_strangers_detail}
                            onChange={this.handleTextChange}
                            className={classes.editField}
                          />
                        )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.rightFeatureHeading}
                          >
                            Friendly With Children
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.rightFeatureContent}>
                            {this.state.dog.friendly_with_children_detail
                              ? this.state.dog.friendly_with_children_detail
                              : "-"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <TextField
                            label="Friendly with childrens details"
                            name="friendly_with_children_detail"
                            defaultValue={
                              this.state.dog.friendly_with_children_detail
                            }
                            value={this.state.friendly_with_children_detail}
                            onChange={this.handleTextChange}
                            className={classes.editField}
                          />
                        )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      {this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography
                            color="secondary"
                            className={classes.rightFeatureHeading}
                          >
                            Chews Things
                          </Typography>
                        )}

                      {!this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <Typography className={classes.rightFeatureContent}>
                            {this.state.dog.chews_things_detail
                              ? this.state.dog.chews_things_detail
                              : "-"}
                          </Typography>
                        )}
                      {this.state.editable &&
                        this.state.dog.pet_choice &&
                        this.state.dog.pet_choice !== "cat" &&
                        this.state.dog.pet_choice !== "other animal" && (
                          <TextField
                            label="Chews Things Details"
                            name="chews_things_detail"
                            defaultValue={this.state.dog.chews_things_detail}
                            value={this.state.chews_things_detail}
                            onChange={this.handleTextChange}
                            className={classes.editField}
                          />
                        )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Is Agrressive
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.dog.is_aggressive_detail
                            ? this.state.dog.is_aggressive_detail
                            : "-"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Is Agrressive Details"
                          name="is_aggressive_detail"
                          defaultValue={this.state.dog.is_aggressive_detail}
                          value={this.state.is_aggressive_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={this.state.modalOpen}
              onClose={this.handleModal}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
              className={classes.modal}
            >
              <Fade in={this.state.modalOpen}>
                <div className={classes.paper}>
                  <Grid container>
                    <Grid item xs={12}>
                      <p>Are you sure you want to delete your pet?</p>
                    </Grid>
                    <Grid item xs={12}>
                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.orangeBtnOutlined}
                        onClick={this.handleModal}
                      >
                        Cancel
                      </Button>
                                <Link
                                to={{
                                  pathname:  "/profile",
                                }}
                                style={{
                                  //color: "#ffffff",
                                  textDecoration: "none",
                                }}
                               // onClick={
                               //   this.deletePet
                               // }
                              >

                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.orangeBtn}
                        onClick={this.deletePet}
                      >
                        Confirm
                      </Button>
                      </Link>
                    </Grid>
                  </Grid>
                </div>
              </Fade>
            </Modal>
          </Grid>
        )}
        <Dialog
          open={this.state.confirmModalOpen}
          onClose={this.handleConfirmModal}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {!this.state.isConfirm ? "Are you sure?" : "Sucessfull"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {!this.state.isConfirm ? (
                <div>Are you sure you want to make changes?</div>
              ) : (
                <div>Profile updated Successfully</div>
              )}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleConfirmModal} color="primary">
              {!this.state.isConfirm ? "No" : "Close"}
            </Button>
            {!this.state.isConfirm ? (
              <Button onClick={this.handleSave} color="primary" autoFocus>
                Yes
              </Button>
            ) : (
              ""
            )}
          </DialogActions>
        </Dialog>
        {/* Image Cropper Modal */}

        <Dialog
          onClose={this.handleImageCropperModal}
          aria-labelledby="customized-dialog-title"
          open={this.state.imageCropperModal}
          fullWidth={false}
          maxWidth={"lg"}
          className={classes.petViewModal}
        >
          <DialogTitle
            id="customized-dialog-title"
            onClose={this.handleImageCropperModal}
          >
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={this.handleImageCropperModal}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <div className={classes.cropperPaper}>
              <div className={classes.cropContainer}>
                <Cropper
                  image={this.state.imageDataUrl}
                  crop={this.state.crop}
                  zoom={this.state.zoom}
                  aspect={this.state.aspect}
                  onCropChange={this.onCropChange}
                  onCropComplete={this.onCropComplete}
                  onZoomChange={this.onZoomChange}
                />
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <div className={classes.sliderControls}>
              <Slider
                value={this.state.zoom}
                min={1}
                max={3}
                step={0.1}
                aria-labelledby="Zoom"
                onChange={(e, zoom) => this.onZoomChange(zoom)}
                classes={{ container: "slider" }}
              />
            </div>
            <Button autoFocus onClick={this.onImageCropDone} color="primary">
              Done
            </Button>
            <Button
              autoFocus
              onClick={this.handleImageCropperModal}
              color="primary"
            >
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(DogProfileDashboard);
