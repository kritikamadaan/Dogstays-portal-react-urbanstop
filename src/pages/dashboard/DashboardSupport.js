import React, { useEffect, useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
    Typography, Grid, InputLabel, Button, Box, Menu, MenuItem, Select, DialogTitle, DialogContent, FormHelperText,
    DialogContentText
} from "@material-ui/core";
import DashboardHome from "./DashboardHome";
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from "@material-ui/core/TextField";
import MuiPhoneNumber from "react-phone-input-2";
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import Question from "../../assets/HelpDashboard/question.png";
import Training from "../../assets/HelpDashboard/training.png";
import Walking from "../../assets/HelpDashboard/walking.png";
import footerPaw from "../../assets/home/pawFooter.svg";
import Dialog from '@material-ui/core/Dialog';
import axios from "axios";
import Checkcircle from "../../assets/svgs/Checkcircle";
import Right from "../../assets/svgs/Right";

const config = {
    apiUrl: "http://3.215.2.1:8000/api",
};

const styles = (theme) => ({
    root: {
        //padding: theme.spacing(15),
        backgroundColor: "white",
        marginTop: theme.spacing(20),
    },
    header: {
        fontFamily: "Kanit",
        fontSize: 24,
        textAlign: "center",
        justifyContent: "center",
        marginTop: 4,
        color: theme.palette.secondary.main,
        left: "50%",
        transform: "translate(-50%,0)",
        position: "relative"
    },
    contentContainer: {
        margin: theme.spacing(4, 8),
        padding: theme.spacing(4, 8),
    },

    cardContainer: {
        '@media screen and (min-width: 900px)': {
            margin: theme.spacing(1, 20),
            padding: theme.spacing(1, 20),
        },

        '@media screen and (max-width: 900px) and (min-width :600)': {
            margin: "0px !important",
            padding: " 0px !important",
        }
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        border: "1px solid #E8770E",
        width: "296px",
        height: "96px"

    },
    cardHeader: {
        color: "#E8770E",
    },
    formHeader: {
        fontFamily: "Kanit",
        fontSize: 24,
        textAlign: "center",
        justifyContent: "center",
        marginTop: 4,
        color: theme.palette.secondary.main,
        left: "50%",
        transform: "translate(-50%,0)",
        position: "relative"
    },
    cardDescription: {
        marginTop: "10px",
        fontSize: 14
    },
    helpBanner: {
        marginTop: theme.spacing(5),
        //backgroundColor:"#F3D332",
        backgroundColor: "#E8770E",
        backgroundSize: "cover",
        minHeight: "100vh",
        width: "100%",
        zIndex: -1
    },
    helpTitle: {
        fontFamily: "Fredoka One",
        fontSize: 38,
        textAlign: "left",
        color: "white",
        marginLeft: theme.spacing(5),
        marginTop: theme.spacing(5)
    },
    helpsubTitle: {
        fontSize: 20,
        marginLeft: theme.spacing(5),
        textAlign: "left",
        color: "white",
    },
    formPaper: {
        backgroundColor: "white",
        backgroundSize: "cover",
        width: "calc(100% - 20px)",
        height: "calc(100%-20px)",
        margin: theme.spacing(3, 3),
        width: 540,
        height: 540,
    },
    formBanner: {
        backgroundColor: "white",
        backgroundSize: "cover",
        zIndex: 1,
        width: 540,
        height: 540,
    },
    radioGroup: {
        display: "flex",
        justifyContent: "space-between"
    },
    formTextField: {
        color: "#E8770E",
        width: "100%",
        borderRadius: "0px !important",
        borderBottom: "1px solid #E8770E !important",
        marginTop: theme.spacing(3)
    },
    formContainer: {
        padding: theme.spacing(3, 3),
    },
    mobileNumberInput: {
        border: "none !important",
        borderBottom: "2px solid #E8770E !important",
        borderRadius: "0px !important",
        marginTop: theme.spacing(3),
        width: "100%"
    },
    orangeBtn: {
        color: "white",
        fontSize: 25,
        textTransform: "none",
    },
    fabButton: {
        position: " absolute",
        top: "50%",
        left: "95%",
        transform: "translateY(-50%)"
    },
    selectInput: {
        border: "2px solid #E8770E !important",
        width: "100%",
        marginTop: theme.spacing(4),
    },
    textArea: {
        border: "2px solid #E8770E !important",
        width: "100%",
        marginTop: theme.spacing(4),
    },
    dialogPaper: {
        minHeight: '30vh',
        maxHeight: '60vh',
        minWidth: '50vh',
        maxWidth: '60vh',
    },
    dialogTitle: {
        color: "#E8770E",
        fontFamily: "Kanit",
        fontSize: 16,
        textAlign: "center",
        justifyContent: "center",
        marginTop: 4,
        left: "50%",
        transform: "translate(-50%,0)",
        position: "relative"
    },
    cardImage: {
        float: "left"
    },
    dialogContent: {
        padding: theme.spacing(2, 2)
    },
    checkMark: {
        textAlign: "center",
        justifyContent: "center",
        marginTop: 15,
        left: "50%",
        transform: "translate(-50%,0)",
        position: "relative"
    },
    marginRight: {
        marginRight: "180px",
        '@media screen and (max-width: 420px)': {
            marginRight: "0px"
        }
    },

    '@media screen and (max-width: 900px) and (min-width :600)': {
        cardContainer: {
            margin: 0,
            padding: 0,
        },
        fabButton: {
            display: "none"
        },
    },
    '@media screen and (max-width: 600px)': {
        contentContainer: {
            margin: theme.spacing(2, 2),
            padding: theme.spacing(2, 2),
        },
        cardContainer: {
            margin: theme.spacing(1, 1),
            padding: theme.spacing(1, 1),

        },
        dailogContainer: {
            width: "50px",
            height: "50px"
        },
        dialogContent: {
            padding: theme.spacing(1, 1)
        },
        fabButton: {
            display: "none"
        },

    },
    '@media screen and (max-width: 420px)': {
        formTextField: {
            width: "50%"
        },
        selectInput: {
            width: "50%"
        },
        textArea: {
            width: "50%"
        },
        mobileNumberInput: {
            width: "248px"
        },
        orangeBtn: {
            margin: 0,
            position: "absolute",
            left: "50%",
            transform: "translateX(-50%)"
        }
    }
});



class DashboardSupport extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inquery: "",
            query: "",
            sitterType: "",
            email: "",
            mobile: "",
            // enquiry:"",
            subject: "",
            username: "",
            userId: "",
            description: "",
            isSitter: false,
            open: false,
            ticketNo: "",
            categoryArray: ["Category 1", "Category 2", "Category 3", "Category 4", "Category 5"],
            questionArray: ["Question 1", "Question 2 ", "Question 3", "Question 4", "Question 5"]
        }
    }


    handleClickOpen = () => {
        this.setState({
            open: true
        })
    };

    handleClose = () => {
        this.setState({
            open: false
        })
    };

    handleEnquiryChange = (event) => {
        this.setState({
            inquery: event.target.value,
        });
    };
    handleQueryChange = (event) => {
        this.setState({
            query: event.target.value,
        });
    };

    handleChangeRadio = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    };

    handleChangeMobileCode = (value) => {
        this.setState({ mobile: value });
    };
    handleChange = (e) => {
        const { name, value } = e.target;
        console.log('the  sitter type is ', this.state.sitterType)
        console.log(e.target, [name], value);
        this.setState({
            [name]: value
        })
        if (this.state.parent_sitter === true) {
            const random_user_id = Math.floor(Math.random() * (999 - 100 + 1) + 100);
            this.setState((user) => ({
                ...user,
                [name]: value,
                username: `${this.state.name}_${random_user_id}`,
                // userId:`_${random_user_id}`,
                is_sitter: true,
            }));
        } else {
            this.setState((user) => ({
                ...user,
                [name]: value,
                //userId:`_${random_user_id}`,
                is_sitter: false,
            }));
        }
    };

    submitHandler = (e) => {
        e.preventDefault();
        const { email, inquery, subject, description, query } = this.state;
        const d = new Date();
        const random_number = Math.floor(Math.random() * 100);
        console.log('the random no is', random_number)
        const data = {
            ticket_id: random_number,
            category: inquery,
            question: query,
            subject: subject,
            body: "body or description",
            email: email
        }
        console.log('the data format is', data)
        axios
            .post(`${config.apiUrl}/help_and_support/`, data)
            .then((response) => {
                console.log("Res in the ************@@@@", response);
                this.setState(
                    (prevState) => {
                        return {
                            ...prevState,
                            ticketNo: response.data.ticket_id
                        }
                    },
                    () => {
                        this.handleClickOpen()
                    }
                );

            })
            .catch((err) => {
                console.log(err);
            });
    }
    render() {
        const { classes } = this.props;
        console.log('@@@@@@@@@@@@this state is ', this.state)
        return (
            <div className={classes.root}>

                <Grid container className={classes.main}>
                    <Typography className={classes.header}>
                        Go to FAQ's
            </Typography>
                    <Grid item xs={12} className={classes.contentContainer} >
                        <Typography className={classes.content}>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged.
            </Typography>

                        <img
                            src={footerPaw}
                            alt="Paw Footer"
                            className={classes.fabButton}
                        />
                    </Grid>

                    <Grid container md={12} sm={12} spacing={2}
                        className={classes.cardContainer}>
                        <Grid item xs={12} sm={6}>
                            <Paper elevation={3} className={classes.paper}>
                                <img src={Training} className={classes.cardImage} />
                                <Typography className={classes.cardHeader}>

                                    FAQ's for the Dog Parent
                  </Typography>
                                <Typography className={classes.cardDescription}>
                                    Open in the New Tab
                  <Right />
                                </Typography>
                            </Paper>
                        </Grid>


                        <Grid item xs={12} sm={6}>
                            <Paper elevation={3} className={classes.paper}>
                                <img src={Walking} className={classes.cardImage} />
                                <Typography className={classes.cardHeader}>
                                    FAQ's for the Dog Sitter
                  </Typography>
                                <Typography className={classes.cardDescription}>
                                    Open in the New Tab
                  <Right />
                                </Typography>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
                <Typography className={classes.formHeader}>
                    Use Help & support Instead
            </Typography>
                <Grid container xs={12} className={classes.helpBanner} >
                    <Grid item md={6} sm={12}>
                        <h1 className={classes.helpTitle}>
                            Help & support
              </h1>
                        <p className={classes.helpsubTitle}>
                            Let Us know Your Queries Here...
                </p>
                        <img src={Question} className={classes.cardImage} />
                    </Grid>

                    <Grid item md={6} sm={12}>
                        <Grid className={classes.formBanner}>
                            <Paper elevation={3} className={classes.formPaper}>
                                <Grid container className={classes.formContainer}>
                                    <Grid xs={12} sm={12} spacing={4} className={classes.radioGroup}>
                                        <FormControl component="fieldset">
                                            <RadioGroup
                                                name="parent_sitter"
                                                value={this.state.SitterType}
                                                onChange={this.handleChangeRadio}
                                                row
                                            >
                                                <FormControlLabel
                                                    value="yes"
                                                    control={<Radio />}
                                                    label="Dog Parent"
                                                    spacing={4}
                                                    className={classes.marginRight}
                                                    style={{ marginRight: "220px" }}
                                                />
                                                <FormControlLabel
                                                    value="No"
                                                    control={<Radio />}
                                                    label="Dog Sitter"
                                                    spacing={4}

                                                />
                                            </RadioGroup>
                                        </FormControl>
                                    </Grid>
                                    <Grid xs={12} >
                                        <TextField
                                            id="name"
                                            placeholder="Enter Name"
                                            name="name"
                                            value={this.state.name}
                                            className={classes.formTextField}
                                            onChange={this.handleChange}
                                        />
                                    </Grid>
                                    <Grid xs={12} >
                                        <TextField
                                            id="email"
                                            placeholder=" Enter Email"
                                            name="email"
                                            value={this.state.email}
                                            className={classes.formTextField}
                                            onChange={this.handleChange}
                                        />
                                    </Grid>
                                    <Grid xs={12}>
                                        <MuiPhoneNumber
                                            preferredCountries={["lu", "fr", "be", "de", "at"]}
                                            onChange={this.handleChangeMobileCode}
                                            inputClass={classes.mobileNumberInput}
                                            inputStyle={{ width: "100%" }}
                                            style={{ marginTop: "25px", width: "100%" }}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid xs={12}>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="simple-select"
                                            value={this.state.inquery}
                                            placeholder="Select Enquiry Type"
                                            onChange={this.handleEnquiryChange}
                                            className={classes.selectInput}
                                        >
                                            <MenuItem value={"Category 1"}>Category 1</MenuItem>
                                            <MenuItem value={"Category 2"}>Category 2</MenuItem>
                                            <MenuItem value={"Category 3"}>Category 3</MenuItem>
                                            <MenuItem value={"Category 4"}>Category 4</MenuItem>
                                            <MenuItem value={"Category 5"}>Category  5</MenuItem>
                                        </Select>
                                    </Grid>
                                    <Grid xs={12}>

                                        <Select
                                            labelId="demo-simple-select-placeholder-label-label"
                                            id="simple-select"
                                            value={this.state.query}
                                            placeholder="Select Enquiry Type"
                                            onChange={this.handleQueryChange}
                                            className={classes.selectInput}
                                        >
                                            <MenuItem value={"Question 1"}>Question 1</MenuItem>
                                            <MenuItem value={"Question 2"}>Question 2</MenuItem>
                                            <MenuItem value={"Question 3"}>Question 3 </MenuItem>
                                            <MenuItem value={"Question 4"}>Question 4</MenuItem>
                                            <MenuItem value={"Question 5"}>Question 5</MenuItem>
                                        </Select>
                                    </Grid>
                                    <Grid xs={12}>
                                        <TextField
                                            id="subject"
                                            placeholder="Enter Subject"
                                            name="subject"
                                            value={this.state.subject}
                                            className={classes.formTextField}
                                            onChange={this.handleChange}
                                        />
                                    </Grid>
                                    <Grid xs={12}>
                                        <textarea
                                            className={classes.textArea}
                                            name="description"
                                            value={this.state.description}
                                            onChange={this.handleChange}
                                            rows="4"
                                            placeholder="Please Elaborate"
                                        />
                                    </Grid>
                                    <Grid xs={12}>
                                    </Grid>
                                    {/* <TextBoxComponent 
                cssClass={classes.textArea}
                  name="description"
                  value={this.state.description}
                  onChange={this.handleChange}
                  multiline={true} 
                  maxlength="100%"
                  placeholder='Enter your address'>

                </TextBoxComponent> */}

                                </Grid>

                            </Paper>
                        </Grid>
                        <Grid
                            container
                            justify="center"
                            style={{ paddingTop: "5rem", paddingBottom: "5rem" }}
                        >
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={this.submitHandler}

                                className={classes.orangeBtn}
                            >
                                Submit
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Dialog
                    open={this.state.open}
                    keepMounted
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                    size={"sm"}
                    classes={{ paper: classes.dialogPaper }}
                >
                    <DialogTitle className={classes.dialogTitle} id="alert-dialog-slide-title" >
                        {" Query Sent"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText className={classes.dailogContent} id="alert-dialog-slide-description">
                            Your Ticket No. is {this.state.ticketNo}
                            <br />
                     An Email copy of your query is been send to your gmail id
                      <br />
                            <grid className={classes.checkMark}>
                                <Checkcircle />
                            </grid>
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(DashboardSupport);