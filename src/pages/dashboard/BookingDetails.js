import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Divider,
  Button,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  TextareaAutosize,
} from "@material-ui/core";
import dogImage from "../../assets/sitterSearch/Vector.png";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import DeleteIcon from "@material-ui/icons/Delete";
import axios from "axios";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import DateRangeIcon from "@material-ui/icons/DateRange";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import DogIcon from "../../assets/common/dog_icon.svg";
import moment from "moment";
import StripeCheckout from "react-stripe-checkout";
import { Link } from "react-router-dom";

import Logo from "../../assets/common/dogstays-logo.png";

const user = JSON.parse(localStorage.getItem("user"));
const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const styles = (theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
    minHeight: "80vh",
    backgroundColor: "#ffffff",
    padding: 24,
  },
  dogImage: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  breadcrumbs: {
    fontSize: 14,
    fontFamily: "Kanit",
    color: "#E8770E",
  },
  homeHeading: {
    fontSize: 30,
    fontFamily: "Fredoka One",
    color: "#3EB3CD",
  },
  subTopic: {
    color: "#979797",
    fontSize: 16,
    fontFamily: "Kanit",
    marginTop: 18,
  },
  subHeading: {
    color: "#5E5A5A",
    fontSize: 14,
    fontFamily: "Kanit",
    marginTop: 18,
  },
  TopicHeading: {
    color: "#3EB3CD",
    fontSize: 16,
    fontFamily: "Kanit",
    fontWeight: 500,
    marginTop: 18,
  },
  serviceCard: {
    border: "1px solid #E8770E",
    borderRadius: 4,
    padding: 18,
    maxWidth: 280,
    marginTop: 18,
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
  },
  priceSummary: {
    border: "1px solid #E8770E",
    borderRadius: 4,
    padding: 24,
    maxWidth: "100%",
    margin: "0px 72px",
    marginTop: 18,
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
  },
  petCard: {
    border: "1px solid #E8770E",
    borderRadius: 4,
    maxWidth: "100%",
    marginTop: 18,
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
  },
  editField: {
    width: 250,
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    padding: theme.spacing(0.5, 2),
  },
  orangeBtnOutlined: {
    color: "#F58220",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    border: "1px solid #F58220",
    width: "max-content",
    padding: theme.spacing(0.5, 2),
    backgroundColor: "#ffffff",
    marginRight: 18,
  },
  dogInfoWrapper: {
    backgroundColor: theme.palette.primary.main,
    textAlign: "center",
    color: "white",
    width: 294,
    marginTop: -5,
    margin: "0 auto",
    padding: theme.spacing(2, 0),
  },
  profileDivider: {
    backgroundColor: theme.palette.primary.main,
    width: 1.5,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  aboutPet: {
    marginTop: theme.spacing(4),
    color: "#979797",
    fontSize: 22,
    fontWeight: 500,
  },
  aboutPetDesc: {
    color: "#5E5A5A",
    fontSize: 16,
  },
  featureWrapper: {
    margin: theme.spacing(2, 1),
  },
  featureHeadings: {
    fontSize: 16,
    fontWeight: 500,
  },
  featureDesc: {
    color: "#5E5A5A",
    fontSize: 16,
    fontWeight: 500,
  },
  moreImageHeader: {
    paddingBottom: theme.spacing(0.5),
    borderBottom: "1px solid #E8770E",
    fontSize: 20,
    fontWeight: 500,
    width: "fit-content",
    margin: "0 auto",
  },
  moreImageWrapper: {
    display: "flex",
    justifyContent: "space-between",
    margin: theme.spacing(5, 0),
  },
  rightFeatureWrapper: {
    margin: theme.spacing(3, 0),
  },
  rightFeatureHeading: {
    fontSize: 22,
    fontWeight: 500,
  },
  rightFeatureContent: {
    fontSize: 16,
    color: "#5E5A5A",
  },
});

class BookingDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      otherPet: [],
      editable: false,
      modalOpen: false,
      bookingID: "",
      modalAcceptOpen: false,
      modalConfirmOpen: false,
      modalCancelOpen: false,
      bookingDetails: {},
      _id: "",
      Terms: false,
      bookingServices: null,
      petData: null,
    };
  }

  componentDidMount() {
    const bookingID = localStorage.getItem("bookingID");
    this.setState(
      {
        bookingID: bookingID,
      },
      () => {
        this.getBookingDetails(this.state.bookingID);
      }
    );
  }

  getBookingDetails = (id) => {
    axios
      .get(`${config.apiUrl}/api/bookinglist/${id}`)
      .then((response) => {
        this.setState(
          {
            bookingDetails: response.data,
            petData: response.data.pets,
            sitter_name: response.data.sitterID.user.first_name,
            owner_name: response.data.ownerID.user.first_name,
            _id: id,
          },
          () => {
            const bookingServices = JSON.parse(
              this.state.bookingDetails.services
            );
            console.log(bookingServices);
            this.setState(
              {
                bookingServices: bookingServices,
              },
              () => {
                console.log(this.state.bookingServices);
              }
            );
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  deletePet = () => {
    const id = this.props.match.params.id;

    axios
      .delete(`${config.apiUrl}/api/petprofile/${id}`)
      .then((response) => {
        console.log("Add Pets response", response);
        window.location.href = "/dashboard";
      })
      .catch((err) => {
        console.log(err);
      });
  };

  totalDays = () => {
    console.log(
      this.state.bookingDetails.endDate,
      this.state.bookingDetails.startDate
    );
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(this.state.bookingDetails.endDate); // 29th of Feb at noon your timezone
    var secondDate = new Date(this.state.bookingDetails.startDate); // 2st of March at noon

    var diffDays = Math.round(
      Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay)
    );
    return diffDays;
  };

  onToken = (amount, description) => (token) =>
    axios
      .post(`${config.apiUrl}/api/payment/`, {
        description,
        source: token.id,
        currency: "EUR",
        amount: amount,
      })
      .then((response) => {
        console.log("payment response", response);
        // window.location.href = "/dashboard";
      })
      .catch((err) => {
        console.log(err);
      });

  handlePayment = () => {
    // console.log("payment");
    const data = {
      amount: parseFloat(this.state.bookingDetails.total),
      currency: "EUR",
      description: "Booking Request",
      source: this.state.stripeToken,
    };
    axios
      .post(`${config.apiUrl}/api/payment/`, data)
      .then((response) => {
        console.log("payment response", response);
        if (response.status === 201) {
          this.handleConfirm();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleModal = () => {
    this.setState((prevState) => {
      return {
        modalOpen: !prevState.modalOpen,
      };
    });
  };

  handleAcceptModal = () => {
    this.setState((prevState) => {
      return {
        modalAcceptOpen: !prevState.modalAcceptOpen,
      };
    });
  };

  handleConfirmModal = () => {
    this.setState((prevState) => {
      return {
        modalConfirmOpen: !prevState.modalConfirmOpen,
      };
    });
  };

  handleCancelModal = () => {
    this.setState((prevState) => {
      return {
        modalCancelOpen: !prevState.modalCancelOpen,
      };
    });
  };

  handleCancel = () => {
    const data = {
      startDate: this.state.bookingDetails.startDate,
      endDate: this.state.bookingDetails.endDate,
      num_dogs: this.state.bookingDetails.num_dogs,
      total: this.state.bookingDetails.total,
      services: this.state.bookingDetails.services,
      status: "cancelled",
      startTime: this.state.bookingDetails.startTime,
      endTime: this.state.bookingDetails.endTime,
      textArea: null,
      sitterID: this.state.bookingDetails.sitterID.id,
      ownerID: this.state.bookingDetails.ownerID.id,
      pets: this.state.bookingDetails.pets.map((d) => {
        return d.id;
      }),
    };

    fetch(`http://3.215.2.1:8000/api/booking/${this.state._id}`, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.handleCancelModal();
        this.getBookingDetails(this.state.bookingID);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  handleConfirm = () => {
    const data = {
      startDate: this.state.bookingDetails.startDate,
      endDate: this.state.bookingDetails.endDate,
      num_dogs: this.state.bookingDetails.num_dogs,
      total: this.state.bookingDetails.total,
      services: this.state.bookingDetails.services,
      status: "confirmed",
      startTime: this.state.bookingDetails.startTime,
      endTime: this.state.bookingDetails.endTime,
      textArea: null,
      sitterID: this.state.bookingDetails.sitterID.id,
      ownerID: this.state.bookingDetails.ownerID.id,
      pets: this.state.bookingDetails.pets.map((d) => {
        return d.id;
      }),
    };

    fetch(`http://3.215.2.1:8000/api/booking/${this.state._id}`, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.handleConfirmModal();
        this.getBookingDetails(this.state.bookingID);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  handleAccept = () => {
    const data = {
      startDate: this.state.bookingDetails.startDate,
      endDate: this.state.bookingDetails.endDate,
      num_dogs: this.state.bookingDetails.num_dogs,
      total: this.state.bookingDetails.total,
      services: this.state.bookingDetails.services,
      status: "accepted",
      startTime: this.state.bookingDetails.startTime,
      endTime: this.state.bookingDetails.endTime,
      textArea: null,
      sitterID: this.state.bookingDetails.sitterID.id,
      ownerID: this.state.bookingDetails.ownerID.id,
      pets: this.state.bookingDetails.pets.map((d) => {
        return d.id;
      }),
    };

    fetch(`http://3.215.2.1:8000/api/booking/${this.state._id}`, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.handleAcceptModal();
        this.getBookingDetails(this.state.bookingID);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  handleReject = () => {
    const data = {
      startDate: this.state.bookingDetails.startDate,
      endDate: this.state.bookingDetails.endDate,
      num_dogs: this.state.bookingDetails.num_dogs,
      total: this.state.bookingDetails.total,
      services: this.state.bookingDetails.services,
      status: "rejected",
      startTime: this.state.bookingDetails.startTime,
      endTime: this.state.bookingDetails.endTime,
      textArea: null,
      sitterID: this.state.bookingDetails.sitterID.id,
      ownerID: this.state.bookingDetails.ownerID.id,
      pets: this.state.bookingDetails.pets.map((d) => {
        return d.id;
      }),
    };

    fetch(`http://3.215.2.1:8000/api/booking/${this.state._id}`, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.handleModal();
        this.getBookingDetails(this.state.bookingID);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  render() {
    const user = JSON.parse(localStorage.getItem("user"));
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={7}>
            <div>
              <Typography color="primary" className={classes.breadcrumbs}>
                <Link
                  to={{ pathname: "/dashboard/bookings-meetings" }}
                  style={{ color: "#E8770E", textDecoration: "none" ,fontSize:"18px",fontWeight:"boldest" ,marginBottom:"3px"}}
                >
                  Booking & Meetings {" "}  >> Booking Requests
                </Link>
              
              </Typography>

              {user.user_type.is_sitter === true ? (
                <Typography color="primary" className={classes.homeHeading}>
                  {this.state.bookingDetails && this.state.owner_name} Would
                  love to book your services
                </Typography>
              ) : (
                <Typography color="primary" className={classes.homeHeading}>
                  Hi! {this.state.bookingDetails && this.state.sitter_name}{" "}
                  Would love to book your services
                </Typography>
              )}
              <div>
                <Typography color="primary" className={classes.subTopic}>
                  <span style={{ color: "#E8770E" }}>Status :</span>{" "}
                  <span style={{color:" #2ecc71"}}>{this.state.bookingDetails.status}</span>
                  
                </Typography>
              </div>
            </div>
            <div>
              <Typography color="primary" className={classes.TopicHeading}>
                Service Type
              </Typography>

              <div className={classes.serviceCard}>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <img src={DogIcon} style={{ height: 20, marginRight: 8 }} />
                  <h3
                    style={{
                      color: "#E8770E",
                      fontFamily: "Kanit",
                      fontSize: 18,
                      margin: 0,
                    }}
                  >
                    {this.state.bookingServices &&
                      this.state.bookingServices.core.core_service.service_id
                        .service_name}
                  </h3>
                </div>
                <p
                  style={{
                    color: "#979797",
                    fontFamily: "Kanit",
                    fontSize: 14,
                    margin: 0,
                    marginTop: 8,
                  }}
                >
                  {this.state.bookingServices &&
                    this.state.bookingServices.core.core_service.service_id
                      .service_description}
                </p>
              </div>
            </div>
            <Grid
              container
              style={{
                marginTop: 32,
              }}
            >
              <Grid item xs={3}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                  }}
                >
                  <DateRangeIcon
                    style={{
                      color: "#3EB3CD",
                      fontSize: 16,
                      marginTop: 4,
                    }}
                  />
                  <div>
                    <p
                      style={{
                        margin: 0,
                        color: "#3EB3CD",
                        fontSize: 16,
                        fontFamily: "Kanit",
                        marginLeft: 12,
                      }}
                    >
                      Start Date
                    </p>
                    <p
                      style={{
                        color: "#979797",
                        fontSize: 18,
                        fontFamily: "Kanit",
                        margin: 0,
                        marginLeft: 12,
                      }}
                    >
                      {moment(this.state.bookingDetails.startDate).format(
                        "DD/MM/YYYY"
                      )}
                    </p>
                  </div>
                </div>
              </Grid>
              <Grid item xs={3}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                  }}
                >
                  <DateRangeIcon
                    style={{
                      color: "#3EB3CD",
                      fontSize: 16,
                      marginTop: 4,
                    }}
                  />
                  <div>
                    <p
                      style={{
                        margin: 0,
                        color: "#3EB3CD",
                        fontSize: 16,
                        fontFamily: "Kanit",
                        marginLeft: 12,
                      }}
                    >
                      End Date
                    </p>
                    <p
                      style={{
                        color: "#979797",
                        fontSize: 18,
                        fontFamily: "Kanit",
                        margin: 0,
                        marginLeft: 12,
                      }}
                    >
                      {moment(this.state.bookingDetails.endDate).format(
                        "DD/MM/YYYY"
                      )}
                    </p>
                  </div>
                </div>
              </Grid>
              <Grid item xs={3}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                  }}
                >
                  <DateRangeIcon
                    style={{
                      color: "#3EB3CD",
                      fontSize: 16,
                      marginTop: 4,
                    }}
                  />
                  <div>
                    <p
                      style={{
                        margin: 0,
                        color: "#3EB3CD",
                        fontSize: 16,
                        fontFamily: "Kanit",
                        marginLeft: 12,
                      }}
                    >
                      Start Time
                    </p>
                    <p
                      style={{
                        color: "#979797",
                        fontSize: 18,
                        fontFamily: "Kanit",
                        margin: 0,
                        marginLeft: 12,
                      }}
                    >
                      {moment(this.state.bookingDetails.startTime).format(
                        "HH:mm"
                      )}
                    </p>
                  </div>
                </div>
              </Grid>
              <Grid item xs={3}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                  }}
                >
                  <DateRangeIcon
                    style={{
                      color: "#3EB3CD",
                      fontSize: 16,
                      marginTop: 4,
                    }}
                  />
                  <div>
                    <p
                      style={{
                        margin: 0,
                        color: "#3EB3CD",
                        fontSize: 16,
                        fontFamily: "Kanit",
                        marginLeft: 12,
                      }}
                    >
                      End Time
                    </p>
                    <p
                      style={{
                        color: "#979797",
                        fontSize: 18,
                        fontFamily: "Kanit",
                        margin: 0,
                        marginLeft: 12,
                      }}
                    >
                      {moment(this.state.bookingDetails.endTime).format(
                        "HH:mm"
                      )}
                    </p>
                  </div>
                </div>
              </Grid>
              <Grid item xs={12} style={{ marginTop: 24 }}>
                <Typography color="primary" className={classes.TopicHeading}>
                  Pets to Care
                </Typography>
                {this.state.petData &&
                  this.state.petData.map((pet, i) => {
                    return (
                      <div className={classes.petCard} key={i}>
                        <div
                          style={{
                            display: "flex",
                          }}
                        >
                          <div
                            style={{
                              width: 240,
                              backgroundImage: `linear-gradient(0deg, rgba(94, 90, 90, 0.3), rgba(94, 90, 90, 0.3)), linear-gradient(0deg, rgba(94, 90, 90, 0.6), rgba(94, 90, 90, 0.6)), url(${pet.profile_photo})`,
                              backgroundPosition: "center",
                              backgroundRepeat: "no-repeat",
                              backgroundSize: "cover",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "flex-end",
                            }}
                          >
                            <p style={{ color: "#ffffff", fontSize: 18 }}>
                              {pet.name}
                            </p>
                          </div>
                          <div style={{ padding: 18, paddingRight: 0 }}>
                            <div>
                              <h3
                                style={{
                                  color: "#979797",
                                  fontSize: 16,
                                  fontFamily: "Kanit",
                                  marginTop: 0,
                                }}
                              >
                                About {pet.name}
                              </h3>
                              <p
                                style={{
                                  color: "#5E5A5A",
                                  fontSize: 14,
                                  fontFamily: "Kanit",
                                }}
                              >
                                {pet.additional_info}
                              </p>
                            </div>
                            <Grid container>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>Nutered</p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.nutered === false ? "No" : "Yes"}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Dogs
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_dogs_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Strangers
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_strangers_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Chews Things
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.chews_things_choice}
                                </p>
                              </Grid>

                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Stay Home Alone
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.stay_home_alone_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Cats
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_cats_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Children
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_children_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Is Aggressive
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.is_aggressive_choice}
                                </p>
                              </Grid>
                            </Grid>
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </Grid>

              <Grid item xs={12} style={{ marginTop: 24 }}>
                <Typography color="primary" className={classes.TopicHeading}>
                  Add On Services Requested
                </Typography>
                <Grid container>
                  <Grid item xs={6}>
                    <div className={classes.serviceCard}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "center",
                        }}
                      >
                        <div>
                          <h3
                            style={{
                              color: "#E8770E",
                              fontFamily: "Kanit",
                              fontSize: 18,
                              margin: 0,
                            }}
                          >
                            {this.state.bookingServices &&
                            this.state.bookingServices.addOn !== {}
                              ? this.state.bookingServices.addOn.service_id
                                  .service_name
                              : null}
                          </h3>
                          <p
                            style={{
                              color: "#979797",
                              fontFamily: "Kanit",
                              fontSize: 14,
                              margin: 0,
                              marginTop: 8,
                            }}
                          >
                            {this.state.bookingServices &&
                            this.state.bookingServices.addOn !== {}
                              ? this.state.bookingServices.addOn.service_id
                                  .service_description
                              : null}
                          </p>
                        </div>
                        {this.state.bookingDetails.status === "Open" ? (
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "column",
                            }}
                          >
                            <button
                              style={{
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                                height: 28,
                                width: 28,
                                backgroundColor: "#40CF57",
                                borderRadius: 2,
                                border: "none",
                                margin: 4,
                                cursor: "pointer",
                              }}
                            >
                              <CheckIcon
                                style={{ color: "#ffffff", fontSize: 14 }}
                              />
                            </button>
                            <button
                              style={{
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center",
                                height: 28,
                                width: 28,
                                backgroundColor: "#D13C3C",
                                borderRadius: 2,
                                border: "none",
                                margin: 4,
                                cursor: "pointer",
                              }}
                            >
                              <CloseIcon
                                style={{ color: "#ffffff", fontSize: 14 }}
                              />
                            </button>
                          </div>
                        ) : null}
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid
            item
            xs={5}
            style={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div
              style={{
                width: "100%",
                textAlign: "center",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                color="primary"
                style={{
                  fontSize: 14,
                  fontFamily: "Kanit",
                  color: "#5E5A5A",
                  fontWeight: 500,
                  marginRight: 32,
                }}
              >
                Request sent on:{" "}
                <span style={{ color: "#979797" }}>
                  {this.state.bookingDetails &&
                    moment(this.state.bookingDetails.created_date).format(
                      "DD/MM/YYYY"
                    )}
                </span>
              </Typography>

              <Typography
                color="primary"
                style={{
                  fontSize: 14,
                  fontFamily: "Kanit",
                  color: "#5E5A5A",
                  fontWeight: 500,
                }}
              >
                Booking Id:{" "}
                <span style={{ color: "#979797" }}>
                  {this.state.bookingDetails.booking_id}
                </span>
              </Typography>
            </div>
            <div className={classes.priceSummary}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <h3
                  style={{
                    color: "#E8770E",
                    fontFamily: "Kanit",
                    fontSize: 20,
                    margin: 0,
                    textAlign: "center",
                  }}
                >
                  Payement Details
                </h3>
              </div>

              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <p
                  style={{
                    color: "#979797",
                    fontFamily: "Kanit",
                    fontSize: 18,
                  }}
                >
                  No. of days:
                </p>
                <p
                  style={{
                    color: "#979797",
                    fontFamily: "Kanit",
                    fontSize: 18,
                  }}
                >
                  {this.totalDays()}
                </p>
              </div>

              <div>
                <p
                  style={{
                    color: "#5E5A5A",
                    fontFamily: "Kanit",
                    fontSize: 18,
                  }}
                >
                  Chosen Services
                </p>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <p
                    style={{
                      color: "#979797",
                      fontFamily: "Kanit",
                      fontSize: 16,
                      maxWidth: 180,
                    }}
                  >
                    {this.state.bookingServices &&
                      this.state.bookingServices.core.core_service.service_id
                        .service_name}
                  </p>
                  <p
                    style={{
                      color: "#979797",
                      fontFamily: "Kanit",
                      fontSize: 16,
                    }}
                  >
                    €{" "}
                    {this.state.bookingServices &&
                      this.state.bookingServices.core.core_service.serviceRate}
                  </p>
                </div>
              </div>

              <div>
                <p
                  style={{
                    color: "#5E5A5A",
                    fontFamily: "Kanit",
                    fontSize: 18,
                  }}
                >
                  Add Ons
                </p>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <p
                    style={{
                      color: "#979797",
                      fontFamily: "Kanit",
                      fontSize: 16,
                    }}
                  >
                    {this.state.bookingServices &&
                    this.state.bookingServices.addOn !== {}
                      ? this.state.bookingServices.addOn.service_id.service_name
                      : null}
                  </p>
                  <p
                    style={{
                      color: "#979797",
                      fontFamily: "Kanit",
                      fontSize: 16,
                    }}
                  >
                    €{" "}
                    {this.state.bookingServices &&
                    this.state.bookingServices.addOn !== {}
                      ? this.state.bookingServices.addOn.serviceRate
                      : null}
                  </p>
                </div>
              </div>

              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <p
                  style={{
                    color: "#979797",
                    fontFamily: "Kanit",
                    fontSize: 16,
                  }}
                >
                  No. of dogs
                </p>
                <p
                  style={{
                    color: "#979797",
                    fontFamily: "Kanit",
                    fontSize: 16,
                  }}
                >
                  {this.state.bookingDetails.num_dogs}
                </p>
              </div>
              <div
                style={{
                  height: 2,
                  width: "100%",
                  backgroundColor: "#3EB3CD",
                  margin: "12px 0",
                }}
              />
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <p
                  style={{
                    color: "#5E5A5A",
                    fontFamily: "Kanit",
                    fontSize: 20,
                  }}
                >
                  {user.user_type.is_sitter === true ? "You Earn" : "You Pay"}
                </p>
                <p
                  style={{
                    color: "#F58220",
                    fontFamily: "Kanit",
                    fontSize: 20,
                  }}
                >
                  € {this.state.bookingDetails.total}
                </p>
              </div>
            </div>

            {this.state.bookingDetails &&
            this.state.bookingDetails.status === "accepted" &&
            user.user_type.is_sitter === false ? (
              <div
                style={{
                  maxWidth: "100%",
                  margin: "0px 72px",
                  textAlign: "center",
                  marginTop: 32,
                }}
              >
                <input
                  type="checkbox"
                  onChange={(e) => {
                    this.setState({
                      Terms: e.target.checked,
                    });
                  }}
                  style={{ color: "#E8770E" }}
                />
                <label style={{ color: "#E8770E" }}>
                  I agree to <a>Terms of Service</a> & <a>Privacy Policy</a>
                </label>
              </div>
            ) : this.state.bookingDetails &&
              this.state.bookingDetails.status === "open" &&
              user.user_type.is_sitter === true ? (
              <div
                style={{
                  maxWidth: "100%",
                  margin: "0px 72px",
                  textAlign: "center",
                  marginTop: 32,
                }}
              >
                <input
                  type="checkbox"
                  onChange={(e) => {
                    this.setState({
                      Terms: e.target.checked,
                    });
                  }}
                  style={{ color: "#E8770E" }}
                />
                <label style={{ color: "#E8770E" }}>
                  I agree to <a>Terms of Service</a> & <a>Privacy Policy</a>
                </label>
              </div>
            ) : null}
            <div
              style={{
                maxWidth: "100%",
                margin: "0px 72px",
                textAlign: "center",
                marginTop: 52,
                display: "flex",
                justifyContent: "center",
              }}
            >
              {this.state.bookingDetails &&
              this.state.bookingDetails.status === "open" &&
              user.user_type.is_sitter === true ? (
                <React.Fragment>
                  <button
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: 42,
                      width: "100%",
                      backgroundColor: "#40CF57",
                      borderRadius: 2,
                      border: "none",
                      margin: 4,
                      cursor:
                        this.state.Terms === false ? "not-allowed" : "pointer",
                      color: "#ffffff",
                    }}
                    disabled={this.state.Terms === false ? true : false}
                    onClick={this.handleAccept}
                  >
                    Accept
                  </button>
                  <button
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: 42,
                      width: "100%",
                      backgroundColor: "#D13C3C",
                      borderRadius: 2,
                      border: "none",
                      margin: 4,
                      cursor:
                        this.state.Terms === false ? "not-allowed" : "pointer",
                      color: "#ffffff",
                    }}
                    onClick={this.handleModal}
                    disabled={this.state.Terms === false ? true : false}
                  >
                    Reject
                  </button>
                </React.Fragment>
              ) : this.state.bookingDetails &&
                this.state.bookingDetails.status === "accepted" &&
                user.user_type.is_sitter === false ? (
                <React.Fragment>
                  <button
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: 42,
                      width: "100%",
                      backgroundColor: "#f6f6f6",
                      borderRadius: 2,
                      border: "none",
                      margin: 4,
                      cursor:
                        this.state.Terms === false ? "not-allowed" : "pointer",
                      color: "#000000",
                    }}
                    onClick={this.handleCancel}
                    disabled={this.state.Terms === false ? true : false}
                  >
                    Cancel
                  </button>
                  <StripeCheckout
                    name={"DogStays"}
                    description={"Booking Request"}
                    image={Logo}
                    token={(d) => {
                      this.setState({
                        stripeData: d,
                        stripeToken: d.id,
                      });
                    }}
                    amount={
                      this.state.bookingDetails &&
                      this.state.bookingDetails.total
                    }
                    currency={"EUR"}
                    // stripeKey={"pk_test_4RveNgOMiWDZXSLA8kB54A5N"}
                    stripeKey="pk_test_51HBxebFEnpBY1agIwplv8vJ8K3NpUjKLGDdR9sBH1R1k2WF6zLFzKgGZoHAGL9E6GvlCrJ2AeKD1PjgLExEVETlp00j5zaZJct"
                    email={
                      this.state.bookingDetails &&
                      this.state.bookingDetails.sitterID.user.email
                    }
                    allowRememberMe
                    closed={this.handlePayment}
                  >
                    <button
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        height: 42,
                        width: 200,
                        backgroundColor: "#40CF57",
                        borderRadius: 2,
                        border: "none",
                        margin: 4,
                        cursor:
                          this.state.Terms === false
                            ? "not-allowed"
                            : "pointer",
                        color: "#ffffff",
                      }}
                      disabled={this.state.Terms === false ? true : false}
                    >
                      Pay
                    </button>
                  </StripeCheckout>
                </React.Fragment>
              ) : null}
            </div>
            <div style={{ marginTop: 32, textAlign: "center" }}>
              <Typography color="primary" className={classes.breadcrumbs}>
                <Link
                  to={{ pathname: "/dashboard/bookings-meetings" }}
                  style={{ color: "#E8770E", textDecoration: "none" }}
                >
                  Return to Bookings & Meetings
                </Link>
              </Typography>
            </div>
          </Grid>

          {/* Reject */}

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalOpen}
            onClose={this.handleModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalOpen}>
              <div className={classes.paper}>
                <Typography
                  color="primary"
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 18,
                    color: "#E8770E",
                    textAlign: "left",
                  }}
                >
                  Reasons For Rejecting
                </Typography>
                <TextareaAutosize
                  aria-label="empty textarea"
                  placeholder="Reasons For Rejecting"
                  rowsMin={8}
                  style={{
                    width: 540,
                    border: "1px solid #E8770E",
                    borderRadius: 4,
                    marginTop: 28,
                    padding: 12,
                  }}
                />
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={this.handleReject}
                  >
                    Submit
                  </Button>
                </div>
              </div>
            </Fade>
          </Modal>

          {/* Accept */}

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalAcceptOpen}
            onClose={this.handleAcceptModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalAcceptOpen}>
              <div className={classes.paper}>
                <Typography
                  color="primary"
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 18,
                    color: "#E8770E",
                    textAlign: "center",
                  }}
                >
                  Woohooo! Booking accepted
                </Typography>

                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={this.handleAcceptModal}
                  >
                    Done
                  </Button>
                </div>
              </div>
            </Fade>
          </Modal>

          {/* Confirm */}

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalConfirmOpen}
            onClose={this.handleConfirmModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalConfirmOpen}>
              <div className={classes.paper}>
                <Typography
                  color="primary"
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 18,
                    color: "#E8770E",
                    textAlign: "center",
                  }}
                >
                  Woohooo! Booking Confirmed
                </Typography>

                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={this.handleConfirmModal}
                  >
                    Done
                  </Button>
                </div>
              </div>
            </Fade>
          </Modal>

          {/* Cancelled */}

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalCancelOpen}
            onClose={this.handleCancelModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalCancelOpen}>
              <div className={classes.paper}>
                <Typography
                  color="primary"
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 18,
                    color: "#E8770E",
                    textAlign: "center",
                  }}
                >
                  Booking Request Cancelled
                </Typography>

                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={this.handleCancelModal}
                  >
                    Done
                  </Button>
                </div>
              </div>
            </Fade>
          </Modal>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(BookingDetails);
