import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Divider,
  Button,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  TextareaAutosize,
} from "@material-ui/core";
import RoomIcon from "@material-ui/icons/Room";
import dogImage from "../../assets/sitterSearch/Vector.png";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import DeleteIcon from "@material-ui/icons/Delete";
import axios from "axios";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import DateRangeIcon from "@material-ui/icons/DateRange";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import DogIcon from "../../assets/common/dog_icon.svg";
import moment from "moment";
import {
  DatePicker,
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { Link, Link as RouterLink, withRouter } from "react-router-dom";

const user = JSON.parse(localStorage.getItem("user"));

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const styles = (theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
    minHeight: "80vh",
    backgroundColor: "#ffffff",
    padding: 24,
  },
  dogImage: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  breadcrumbs: {
    fontSize: 14,
    fontFamily: "Kanit",
    color: "#E8770E",
  },
  homeHeading: {
    fontSize: 30,
    fontFamily: "Fredoka One",
    color: "#3EB3CD",
  },
  subTopic: {
    color: "#979797",
    fontSize: 16,
    fontFamily: "Kanit",
    marginTop: 18,
  },
  subHeading: {
    color: "#5E5A5A",
    fontSize: 14,
    fontFamily: "Kanit",
    marginTop: 18,
  },
  TopicHeading: {
    color: "#3EB3CD",
    fontSize: 16,
    fontFamily: "Kanit",
    fontWeight: 500,
    marginTop: 18,
  },
  serviceCard: {
    border: "1px solid #E8770E",
    borderRadius: 4,
    padding: 18,
    maxWidth: 280,
    marginTop: 18,
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
  },
  priceSummary: {
    border: "1px solid #E8770E",
    borderRadius: 4,
    padding: 24,
    maxWidth: "100%",
    margin: "0px 72px",
    marginTop: 18,
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
  },
  petCard: {
    border: "1px solid #E8770E",
    borderRadius: 4,
    maxWidth: "100%",
    marginTop: 18,
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
  },
  editField: {
    width: 250,
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    padding: theme.spacing(0.5, 2),
  },
  orangeBtnOutlined: {
    color: "#F58220",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    border: "1px solid #F58220",
    width: "max-content",
    padding: theme.spacing(0.5, 2),
    backgroundColor: "#ffffff",
    marginRight: 18,
  },
  dogInfoWrapper: {
    backgroundColor: theme.palette.primary.main,
    textAlign: "center",
    color: "white",
    width: 294,
    marginTop: -5,
    margin: "0 auto",
    padding: theme.spacing(2, 0),
  },
  profileDivider: {
    backgroundColor: theme.palette.primary.main,
    width: 1.5,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  aboutPet: {
    marginTop: theme.spacing(4),
    color: "#979797",
    fontSize: 22,
    fontWeight: 500,
  },
  aboutPetDesc: {
    color: "#5E5A5A",
    fontSize: 16,
  },
  featureWrapper: {
    margin: theme.spacing(2, 1),
  },
  featureHeadings: {
    fontSize: 16,
    fontWeight: 500,
  },
  featureDesc: {
    color: "#5E5A5A",
    fontSize: 16,
    fontWeight: 500,
  },
  moreImageHeader: {
    paddingBottom: theme.spacing(0.5),
    borderBottom: "1px solid #E8770E",
    fontSize: 20,
    fontWeight: 500,
    width: "fit-content",
    margin: "0 auto",
  },
  moreImageWrapper: {
    display: "flex",
    justifyContent: "space-between",
    margin: theme.spacing(5, 0),
  },
  rightFeatureWrapper: {
    margin: theme.spacing(3, 0),
  },
  rightFeatureHeading: {
    fontSize: 22,
    fontWeight: 500,
  },
  rightFeatureContent: {
    fontSize: 16,
    color: "#5E5A5A",
  },
  dateTextField: {
    color: "#F58220 !important",
    "& .MuiInput-underline:before": {
      display: "none",
    },
    "& .MuiInput-underline:after": {
      display: "none",
    },
  },
  dateInput: {
    color: "#F58220 !important",
  },
  dateIcon: {
    "& button": {
      color: "#F58220 !important",
    },
  },
  timeIcon: {
    "& button": {
      paddingLeft: "0 !important",
      color: "#F58220 !important",
    },
  },
  whiteDivider: {
    backgroundColor: "#F58220",
    marginTop: 16,
    marginBottom: 8,
    height: 32,
    width: 1,
    marginRight: 48,
  },
  serviceWrapper: {
    marginRight: theme.spacing(6),
    border: "1px solid #F58220",
    padding: theme.spacing(1),
    width: "209px",
    borderRadius: 4,
  },
});

class MeetingDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      otherPet: [],
      editable: false,
      modalOpen: false,
      modalAcceptOpen: false,
      meetingDetails: {},
      _id: "",
      Terms: false,
      petData: null,
      openDateModal: false,
      startTime: "",
      startTime1: "",
      startTime2: "",
      endTime: "",
      endTime1: "",
      endTime2: "",
      selectDate1: "",
      selectDate2: "",
      selectDate3: "",
    };
  }

  componentDidMount() {
    const meetingID = localStorage.getItem("meetingID");
    this.setState(
      {
        meetingID: meetingID,
      },
      () => {
        this.getMeetingDetails(this.state.meetingID);
      }
    );
  }

  getMeetingDetails = (id) => {
    axios
      .get(`${config.apiUrl}/api/meetinglist/${id}`)
      .then((response) => {
        this.setState(
          {
            meetingDetails: response.data,
            petData: response.data.pets,
            sitter_name: response.data.sitterID.user.first_name,
            owner_name: response.data.ownerID.user.first_name,
            _id: id,
          },
          () => {
            const meetingServices = JSON.parse(
              this.state.meetingDetails.services
            );
            this.setState(
              {
                meetingServices: meetingServices,
              },
              () => {
                console.log(this.state.meetingServices);
              }
            );
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  deletePet = () => {
    const id = this.props.match.params.id;
    axios
      .delete(`${config.apiUrl}/api/petprofile/${id}`)
      .then((response) => {
        console.log("Add Pets response", response);
        window.location.href = "/dashboard/profile";
      })
      .catch((err) => {
        console.log(err);
      });
  };

  totalDays = () => {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(this.state.meetingDetails.endDate); // 29th of Feb at noon your timezone
    var secondDate = new Date(this.state.meetingDetails.startDate); // 2st of March at noon

    var diffDays = Math.round(
      Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay)
    );
    return diffDays;
  };

  openDateModal = () => {
    this.setState((prevState) => {
      return {
        openDateModal: !prevState.openDateModal,
      };
    });
  };

  handleModal = () => {
    this.setState((prevState) => {
      return {
        modalOpen: !prevState.modalOpen,
      };
    });
  };

  handleAcceptModal = () => {
    this.setState((prevState) => {
      return {
        modalAcceptOpen: !prevState.modalAcceptOpen,
      };
    });
  };

  handleAccept = () => {
    const data = {
      startDate: this.state.meetingDetails.startDate,
      endDate: this.state.meetingDetails.endDate,
      num_dogs: this.state.meetingDetails.num_dogs,
      total: this.state.meetingDetails.total,
      services: this.state.meetingDetails.services,
      status: "accepted",
      startTime: this.state.meetingDetails.startTime,
      endTime: this.state.meetingDetails.endTime,
      textArea: null,
      sitterID: this.state.meetingDetails.sitterID.id,
      ownerID: this.state.meetingDetails.ownerID.id,
      pets: this.state.meetingDetails.pets.map((d) => {
        return d.id;
      }),
    };

    fetch(`http://3.215.2.1:8000/api/meeting/${this.state._id}`, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.handleAcceptModal();
        this.componentDidMount();
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  handleReject = () => {
    const data = {
      startDate: this.state.meetingDetails.startDate,
      endDate: this.state.meetingDetails.endDate,
      num_dogs: this.state.meetingDetails.num_dogs,
      total: this.state.meetingDetails.total,
      services: this.state.meetingDetails.services,
      status: "rejected",
      startTime: this.state.meetingDetails.startTime,
      endTime: this.state.meetingDetails.endTime,
      textArea: null,
      sitterID: this.state.meetingDetails.sitterID.id,
      ownerID: this.state.meetingDetails.ownerID.id,
      pets: this.state.meetingDetails.pets.map((d) => {
        return d.id;
      }),
    };

    fetch(`http://3.215.2.1:8000/api/meeting/${this.state._id}`, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.handleModal();
        this.componentDidMount();
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  totalDays = () => {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(this.state.meetingDetails.endDate); // 29th of Feb at noon your timezone
    var secondDate = new Date(this.state.meetingDetails.startDate); // 2st of March at noon

    var diffDays = Math.round(
      Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay)
    );
    return diffDays;
  };

  // const handleStartTime = (time) => {
  //   const formatted = moment(time);
  //   setStartTime(formatted);
  // };
  // const handleStartTime1 = (time) => {
  //   const formatted = moment(time);
  //   setStartTime1(formatted);
  // };
  // const handleStartTime2 = (time) => {
  //   const formatted = moment(time);
  //   setStartTime2(formatted);
  // };

  // const handleEndTime = (time) => {
  //   const formatted = moment(time);
  //   setEndTime(formatted);
  // };
  // const handleEndTime1 = (time) => {
  //   const formatted = moment(time);
  //   setEndTime1(formatted);
  // };
  // const handleEndTime2 = (time) => {
  //   const formatted = moment(time);
  //   setEndTime2(formatted);
  // };

  // const handleSelectDate1 = (date) => {
  //   const formatted = moment(date).format("YYYY-MM-DD");
  //   setSelectDate1(formatted);
  // };
  // const handleSelectDate2 = (date) => {
  //   const formatted = moment(date).format("YYYY-MM-DD");
  //   setSelectDate2(formatted);
  // };
  // const handleSelectDate3 = (date) => {
  //   const formatted = moment(date).format("YYYY-MM-DD");
  //   setSelectDate3(formatted);
  // };

  navClick = () => {
    // window.location.href="/dashboard/bookings-meetings"
    window.history.back();
    //this.props.history.goBack();
  };
  render() {
    console.log("this.props in meeeting details", this.props);
    const user = JSON.parse(localStorage.getItem("user"));
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={7}>
            <div>
              <Typography color="primary" className={classes.breadcrumbs}>
                <Link
                  to={{ pathname: "/dashboard/bookings-meetings" }}
                  style={{ color: "#E8770E", textDecoration: "none",
                  color: "#E8770E", textDecoration: "none" ,fontSize:"18px",fontWeight:"boldest" ,marginBottom:"3px"
                }}
                >
                  Bookings & Meetings
                </Link>

                <Link
                  to={{ pathname: "/dashboard/bookings-meetings" }}
                  style={{ color: "#E8770E", textDecoration: "none" ,
                  fontWeight:"boldest",fontSize:"18px",marginTop:"6px",marginBottom:"6px"
                }}
                >
                  >> Meetings Requests
                </Link>
              </Typography>

              {user.user_type.is_sitter === true ? (
                <Typography color="primary" className={classes.homeHeading}>
                  {this.state.meetingDetails && this.state.owner_name} Would
                  love to meet you
                </Typography>
              ) : (
                <Typography color="primary" className={classes.homeHeading}>
                  Hi! {this.state.meetingDetails && this.state.sitter_name}{" "}
                  Would love to meet you
                </Typography>
              )}
              <div>
                <Typography color="primary" className={classes.subTopic}>
                  <span style={{ color: "#E8770E" }}>Status :</span>{" "}
                  
                  <span style={{color:" #2ecc71"}}>{this.state.meetingDetails.status}</span>
                </Typography>
              </div>
            </div>
            <div>
              <Typography color="primary" className={classes.TopicHeading}>
                Service Type
              </Typography>

              <div className={classes.serviceCard}>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <img src={DogIcon} style={{ height: 20, marginRight: 8 }} />
                  <h3
                    style={{
                      color: "#E8770E",
                      fontFamily: "Kanit",
                      fontSize: 18,
                      margin: 0,
                    }}
                  >
                    {this.state.meetingServices &&
                      this.state.meetingServices.core_service.service_id
                        .service_name}
                  </h3>
                </div>
                <p
                  style={{
                    color: "#979797",
                    fontFamily: "Kanit",
                    fontSize: 14,
                    margin: 0,
                    marginTop: 8,
                  }}
                >
                  {this.state.meetingServices &&
                    this.state.meetingServices.core_service.service_id
                      .service_description}
                </p>
              </div>
            </div>
            <Grid
              container
              style={{
                marginTop: 32,
              }}
            >
              <Grid item xs={3}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                  }}
                >
                  <DateRangeIcon
                    style={{
                      color: "#3EB3CD",
                      fontSize: 16,
                      marginTop: 4,
                    }}
                  />
                  <div>
                    <p
                      style={{
                        margin: 0,
                        color: "#3EB3CD",
                        fontSize: 16,
                        fontFamily: "Kanit",
                        marginLeft: 12,
                      }}
                    >
                      Start Date
                    </p>
                    <p
                      style={{
                        color: "#979797",
                        fontSize: 18,
                        fontFamily: "Kanit",
                        margin: 0,
                        marginLeft: 12,
                      }}
                    >
                      {moment(this.state.meetingDetails.startDate).format(
                        "DD/MM/YYYY"
                      )}
                    </p>
                  </div>
                </div>
              </Grid>
              <Grid item xs={3}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                  }}
                >
                  <DateRangeIcon
                    style={{
                      color: "#3EB3CD",
                      fontSize: 16,
                      marginTop: 4,
                    }}
                  />
                  <div>
                    <p
                      style={{
                        margin: 0,
                        color: "#3EB3CD",
                        fontSize: 16,
                        fontFamily: "Kanit",
                        marginLeft: 12,
                      }}
                    >
                      End Date
                    </p>
                    <p
                      style={{
                        color: "#979797",
                        fontSize: 18,
                        fontFamily: "Kanit",
                        margin: 0,
                        marginLeft: 12,
                      }}
                    >
                      {moment(this.state.meetingDetails.endDate).format(
                        "DD/MM/YYYY"
                      )}
                    </p>
                  </div>
                </div>
              </Grid>

              <Grid item xs={12} style={{ marginTop: 24 }}>
                <Typography color="primary" className={classes.TopicHeading}>
                  Pets to Care
                </Typography>
                {this.state.petData &&
                  this.state.petData.map((pet, i) => {
                    return (
                      <div className={classes.petCard} key={i}>
                        <div
                          style={{
                            display: "flex",
                          }}
                        >
                          <div
                            style={{
                              width: 240,
                              backgroundImage: `linear-gradient(0deg, rgba(94, 90, 90, 0.3), rgba(94, 90, 90, 0.3)), linear-gradient(0deg, rgba(94, 90, 90, 0.6), rgba(94, 90, 90, 0.6)), url(${pet.profile_photo})`,
                              backgroundPosition: "center",
                              backgroundRepeat: "no-repeat",
                              backgroundSize: "cover",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "flex-end",
                            }}
                          >
                            <p style={{ color: "#ffffff", fontSize: 18 }}>
                              {pet.name}
                            </p>
                          </div>
                          <div style={{ padding: 18, paddingRight: 0 }}>
                            <div>
                              <h3
                                style={{
                                  color: "#979797",
                                  fontSize: 16,
                                  fontFamily: "Kanit",
                                  marginTop: 0,
                                }}
                              >
                                About {pet.name}
                              </h3>
                              <p
                                style={{
                                  color: "#5E5A5A",
                                  fontSize: 14,
                                  fontFamily: "Kanit",
                                }}
                              >
                                {pet.additional_info}
                              </p>
                            </div>
                            <Grid container>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>Nutered</p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.nutered === false ? "No" : "Yes"}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Dogs
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_dogs_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Strangers
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_strangers_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Chews Things
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.chews_things_choice}
                                </p>
                              </Grid>

                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Stay Home Alone
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.stay_home_alone_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Cats
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_cats_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Friendly With Children
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.friendly_with_children_choice}
                                </p>
                              </Grid>
                              <Grid item xs={3}>
                                <p className={classes.TopicHeading}>
                                  Is Aggressive
                                </p>
                                <p
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    margin: 0,
                                  }}
                                >
                                  {pet.is_aggressive_choice}
                                </p>
                              </Grid>
                            </Grid>
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </Grid>

              <Grid item xs={12} style={{ marginTop: 24 }}>
                <div
                  style={{
                    width: "100%",
                    padding: 18,
                    border: "1px solid #E8770E",
                    borderRadius: 8,
                  }}
                >
                  <Grid container>
                    <Grid item xs={6}>
                      <div
                        style={{
                          width: "100%",
                        }}
                      >
                        <h3
                          style={{
                            margin: 0,
                            fontFamily: "Kanit",
                            fontSize: 22,
                            color: "#3EB3CD",
                          }}
                        >
                          Booking Estimate
                        </h3>
                        <p
                          style={{
                            margin: 0,
                            fontFamily: "Kanit",
                            fontSize: 18,
                            color: "#979797",
                          }}
                        >
                          No. of days: {this.totalDays()}
                        </p>
                        <h3
                          style={{
                            margin: 0,
                            fontFamily: "Kanit",
                            fontSize: 18,
                            color: "#3EB3CD",
                            marginTop: 18,
                          }}
                        >
                          Chosen Services
                        </h3>
                        <p
                          style={{
                            margin: 0,
                            fontFamily: "Kanit",
                            fontSize: 18,
                            color: "#979797",
                          }}
                        >
                          {this.state.meetingServices &&
                            this.state.meetingServices.core_service.service_id
                              .service_name}
                          : €{" "}
                          {this.state.meetingServices &&
                            this.state.meetingServices.core_service.serviceRate}
                        </p>
                      </div>
                    </Grid>
                    <Grid item xs={6}>
                      <div
                        style={{
                          paddingLeft: 18,
                          borderLeft: "1px solid #E8770E",
                        }}
                      >
                        <h2
                          style={{
                            fontFamily: "Kanit",
                            fontSize: 32,
                            color: "#E8770E",
                            marginTop: 0,
                          }}
                        >
                          Total: € {this.state.meetingDetails.total}
                        </h2>
                        <p
                          style={{
                            margin: 0,
                            fontFamily: "Kanit",
                            fontSize: 18,
                            color: "#979797",
                          }}
                        >
                          This is the estimated cost of your booking based on
                          the details above. It is only payable if your booking
                          is confirmed after you meet the sitter
                        </p>
                      </div>
                    </Grid>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid
            item
            xs={5}
            style={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div
              style={{
                width: "100%",
                textAlign: "left",
                margin: "0px 72px",
              }}
            >
              <div
                style={{
                  width: "100%",
                  textAlign: "center",
                  display: "flex",
                }}
              >
                <Typography
                  color="primary"
                  style={{
                    fontSize: 14,
                    fontFamily: "Kanit",
                    color: "#5E5A5A",
                    fontWeight: 500,
                    marginRight: 32,
                  }}
                >
                  Request sent on:{" "}
                  <span style={{ color: "#979797" }}>
                    {this.state.meetingDetails &&
                      moment(this.state.meetingDetails.created_date).format(
                        "DD/MM/YYYY"
                      )}
                  </span>
                </Typography>

                <Typography
                  color="primary"
                  style={{
                    fontSize: 14,
                    fontFamily: "Kanit",
                    color: "#5E5A5A",
                    fontWeight: 500,
                  }}
                >
                  Meeting Id:{" "}
                  <span style={{ color: "#979797" }}>
                    {this.state.meetingDetails.meeting_id}
                  </span>
                </Typography>
              </div>

              <Typography
                color="primary"
                style={{
                  fontSize: 14,
                  fontFamily: "Kanit",
                  color: "#3EB3CD",
                  fontWeight: 500,
                  marginTop: 28,
                }}
              >
                Mark Your Availability
              </Typography>
            </div>
            <div className={classes.priceSummary}>
              <RadioGroup
                aria-label="stay_home_alone_choice"
                name="stay_home_alone_choice"
                value={this.state.stay_home_alone_choice}
                onChange={this.handleStayHomeAloneChoiceChange}
                column
              >
                {this.state.meetingDetails.startDate2 && (
                  <div>
                    <FormControlLabel
                      value={this.state.meetingDetails.startDate2}
                      control={<Radio color="primary" />}
                      label={
                        <h3
                          style={{
                            color: "#E8770E",
                            fontFamily: "Kanit",
                            fontSize: 20,
                            margin: 0,
                            textAlign: "left",
                          }}
                        >
                          {moment(this.state.meetingDetails.startDate2).format(
                            "DD/MM/YYYY"
                          )}
                        </h3>
                      }
                    />
                    <p
                      style={{
                        marginLeft: 32,
                        marginTop: 4,
                        color: "#979797",
                      }}
                    >
                      {moment(this.state.meetingDetails.fromTime).format(
                        "HH:mm"
                      )}{" "}
                      -{" "}
                      {moment(this.state.meetingDetails.toTime).format("HH:mm")}
                    </p>
                  </div>
                )}
                {this.state.meetingDetails.startDate3 && (
                  <div>
                    <FormControlLabel
                      value={this.state.meetingDetails.startDate3}
                      control={<Radio color="primary" />}
                      label={
                        <h3
                          style={{
                            color: "#E8770E",
                            fontFamily: "Kanit",
                            fontSize: 20,
                            margin: 0,
                            textAlign: "left",
                          }}
                        >
                          {moment(this.state.meetingDetails.startDate3).format(
                            "DD/MM/YYYY"
                          )}
                        </h3>
                      }
                    />
                    <p
                      style={{
                        marginLeft: 32,
                        marginTop: 4,
                        color: "#979797",
                      }}
                    >
                      {moment(this.state.meetingDetails.fromTime2).format(
                        "HH:mm"
                      )}{" "}
                      -{" "}
                      {moment(this.state.meetingDetails.toTime2).format(
                        "HH:mm"
                      )}
                    </p>
                  </div>
                )}
                {this.state.meetingDetails.startDate4 && (
                  <div>
                    <FormControlLabel
                      value={this.state.meetingDetails.startDate4}
                      control={<Radio color="primary" />}
                      label={
                        <h3
                          style={{
                            color: "#E8770E",
                            fontFamily: "Kanit",
                            fontSize: 20,
                            margin: 0,
                            textAlign: "left",
                          }}
                        >
                          {moment(this.state.meetingDetails.startDate4).format(
                            "DD/MM/YYYY"
                          )}
                        </h3>
                      }
                    />
                    <p
                      style={{
                        marginLeft: 32,
                        marginTop: 4,
                        color: "#979797",
                      }}
                    >
                      {moment(this.state.meetingDetails.fromTime3).format(
                        "HH:mm"
                      )}{" "}
                      -{" "}
                      {moment(this.state.meetingDetails.toTime3).format(
                        "HH:mm"
                      )}
                    </p>
                  </div>
                )}
              </RadioGroup>
            </div>

            {  user.user_type.is_sitter &&( <div
              style={{
                maxWidth: "100%",
                margin: "0px 72px",
                textAlign: "center",
                fontFamily: "Kanit",
                marginTop: 32,
                color: "#E8770E",
                fontStyle: "italic",
              }}
            >
              - OR -
            </div>)
          }
          {/* {console.log(' user.user_type.is_sitter', user.user_type,user.user_type.is_sitter)} */}
          {user.user_type.is_sitter
            &&(<div
              style={{
                maxWidth: "100%",
                margin: "0px 72px",
                textAlign: "left",
                fontFamily: "Kanit",
                marginTop: 32,
                color: "#979797",
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={this.openDateModal}
            >
              Propose Alternate Meeting Schedule >>
            </div>)
            }
            <div
              style={{
                width: "100%",
                textAlign: "left",
                margin: "0px 72px",
              }}
            >
              <Typography
                color="primary"
                style={{
                  fontSize: 14,
                  fontFamily: "Kanit",
                  color: "#3EB3CD",
                  fontWeight: 500,
                  marginTop: 28,
                }}
              >
                Meeting Location
              </Typography>
            </div>

            <div className={classes.priceSummary}>
              {/* <h3
                style={{
                  color: "#E8770E",
                  fontFamily: "Kanit",
                  fontSize: 20,
                  margin: 0,
                  textAlign: "left",
                }}
              >
                Your house
              </h3> */}
              <div
                style={{
                  display: "flex",
                  marginTop: 24,
                  color: "#979797",
                  fontSize: 16,
                }}
              >
                <RoomIcon
                  style={{
                    color: "#979797",
                    marginRight: 18,
                  }}
                />
                {this.state.meetingDetails.location}
              </div>
            </div>
            {this.state.meetingDetails &&
            this.state.meetingDetails.status === "open" &&
            user.user_type.is_sitter === true ? (
              <div
                style={{
                  maxWidth: "100%",
                  margin: "0px 72px",
                  textAlign: "center",
                  marginTop: 32,
                }}
              >
                <input
                  type="checkbox"
                  onChange={(e) => {
                    this.setState({
                      Terms: e.target.checked,
                    });
                  }}
                  style={{ color: "#E8770E" }}
                />
                <label style={{ color: "#E8770E" }}>
                  I agree to <a>Terms of Service</a> & <a>Privacy Policy</a>
                </label>
              </div>
            ) : null}

            <div
              style={{
                maxWidth: "100%",
                margin: "0px 72px",
                textAlign: "center",
                marginTop: 52,
                display: "flex",
                justifyContent: "center",
              }}
            >
              {this.state.meetingDetails &&
              this.state.meetingDetails.status === "open" &&
              user.user_type.is_sitter === true ? (
                <React.Fragment>
                  <button
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: 42,
                      width: "100%",
                      backgroundColor: "#40CF57",
                      borderRadius: 2,
                      border: "none",
                      margin: 4,
                      cursor:
                        this.state.Terms === false ? "not-allowed" : "pointer",
                      color: "#ffffff",
                    }}
                    disabled={this.state.Terms === false ? true : false}
                    onClick={this.handleAccept}
                  >
                    Accept
                  </button>
                  <button
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: 42,
                      width: "100%",
                      backgroundColor: "#D13C3C",
                      borderRadius: 2,
                      border: "none",
                      margin: 4,
                      cursor:
                        this.state.Terms === false ? "not-allowed" : "pointer",
                      color: "#ffffff",
                    }}
                    onClick={this.handleModal}
                    disabled={this.state.Terms === false ? true : false}
                  >
                    Reject
                  </button>
                </React.Fragment>
              ) : null}
            </div>
            <div style={{ marginTop: 32, textAlign: "center" }}>
              <Typography color="primary" className={classes.breadcrumbs}>
                <Link
                  to={{ pathname: "/dashboard/bookings-meetings" }}
                  style={{ color: "#E8770E", textDecoration: "none" }}
                >
                  Return to Bookings & Meetings
                </Link>
              </Typography>
            </div>
          </Grid>

          {/* Reject */}
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalOpen}
            onClose={this.handleModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalOpen}>
              <div className={classes.paper}>
                <Typography
                  color="primary"
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 18,
                    color: "#E8770E",
                    textAlign: "left",
                  }}
                >
                  Reasons For Rejecting
                </Typography>
                <TextareaAutosize
                  aria-label="empty textarea"
                  placeholder="Reasons For Rejecting"
                  rowsMin={8}
                  style={{
                    width: 540,
                    border: "1px solid #E8770E",
                    borderRadius: 4,
                    marginTop: 28,
                    padding: 12,
                  }}
                />
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "flex-end",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={this.handleReject}
                  >
                    Submit
                  </Button>
                </div>
              </div>
            </Fade>
          </Modal>

          {/* Accept */}

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalAcceptOpen}
            onClose={this.handleAcceptModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalAcceptOpen}>
              <div className={classes.paper}>
                <Typography
                  color="primary"
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 18,
                    color: "#E8770E",
                    textAlign: "center",
                  }}
                >
                  Woohooo! Meeting accepted
                </Typography>

                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={this.handleAcceptModal}
                  >
                    Done
                  </Button>
                </div>
              </div>
            </Fade>
          </Modal>

          {/* Propose New Date Modal */}

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.openDateModal}
            onClose={this.openDateModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.openDateModal}>
              <div className={classes.paper}>
                <Typography
                  color="primary"
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 18,
                    color: "#3EB3CD",
                    textAlign: "center",
                  }}
                >
                  New Meeting Dates
                </Typography>

                <div>
                  <div
                    style={{
                      marginTop: "2rem",
                      display: "flex",
                    }}
                  >
                    <div
                      className={classes.serviceWrapper}
                      style={{ width: "270px", margin: "10px" }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            margin="normal"
                            format="dd/MM/yyyy"
                            id="date-picker-inline"
                            value={this.state.selectDate1}
                            onChange={this.handleSelectDate1}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.dateIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            invalidDateMessage=""
                            placeholder="Select Date"
                          />
                        </MuiPickersUtilsProvider>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <MuiPickersUtilsProvider
                          utils={DateFnsUtils}
                          style={{ marginLeft: 24 }}
                        >
                          <KeyboardTimePicker
                            variant="inline"
                            margin="normal"
                            id="time-picker1"
                            value={this.state.startTime}
                            onChange={this.handleStartTime}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.timeIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            placeholder="From"
                          />
                        </MuiPickersUtilsProvider>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardTimePicker
                            variant="inline"
                            margin="normal"
                            id="time-picker2"
                            value={this.state.endTime}
                            onChange={this.handleEndTime}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.timeIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            placeholder="To"
                          />
                        </MuiPickersUtilsProvider>
                      </div>
                    </div>
                    <div
                      className={classes.serviceWrapper}
                      style={{ width: "270px", margin: "10px" }}
                    >
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <Typography
                          style={{
                            fontSize: 18,
                            fontWeight: 500,
                            color: "#FFFFFF",
                            marginLeft: 16,
                          }}
                        >
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              disableToolbar
                              variant="inline"
                              margin="normal"
                              format="dd/MM/yyyy"
                              id="date-picker-inline"
                              value={this.state.selectDate2}
                              onChange={this.handleSelectDate2}
                              KeyboardButtonProps={{
                                "aria-label": "change date",
                              }}
                              inputProps={{ className: classes.dateInput }}
                              InputLabelProps={{ className: classes.dateInput }}
                              InputAdornmentProps={{
                                className: classes.dateIcon,
                                position: "start",
                              }}
                              className={classes.dateTextField}
                              invalidDateMessage=""
                              placeholder="Select Date"
                            />
                          </MuiPickersUtilsProvider>
                        </Typography>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardTimePicker
                            variant="inline"
                            margin="normal"
                            id="time-picker3"
                            value={this.state.startTime1}
                            onChange={this.handleStartTime1}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.timeIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            placeholder="From"
                          />
                        </MuiPickersUtilsProvider>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardTimePicker
                            variant="inline"
                            margin="normal"
                            id="time-picker4"
                            value={this.state.endTime1}
                            onChange={this.handleEndTime1}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.timeIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            placeholder="To"
                          />
                        </MuiPickersUtilsProvider>
                      </div>
                    </div>
                  </div>
                  <div
                    className={classes.serviceWrapper}
                    style={{ width: "270px", margin: "10px" }}
                  >
                    <div>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <Typography
                          style={{
                            fontSize: 18,
                            fontWeight: 500,
                            color: "#F58220",
                          }}
                        >
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              disableToolbar
                              variant="inline"
                              margin="normal"
                              format="dd/MM/yyyy"
                              id="date-picker-inline"
                              value={this.state.selectDate3}
                              onChange={this.handleSelectDate3}
                              KeyboardButtonProps={{
                                "aria-label": "change date",
                              }}
                              inputProps={{ className: classes.dateInput }}
                              InputLabelProps={{ className: classes.dateInput }}
                              InputAdornmentProps={{
                                className: classes.dateIcon,
                                position: "start",
                              }}
                              className={classes.dateTextField}
                              invalidDateMessage=""
                              placeholder="Select Date"
                            />
                          </MuiPickersUtilsProvider>
                          {/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      disableToolbar
                      variant="inline"
                      margin="normal"
                      format="dd/MM/yyyy"
                      id="date-picker-inline"
                      value={endDate}
                      onChange={handleEndDate}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                      inputProps={{ className: classes.dateInput }}
                      InputLabelProps={{ className: classes.dateInput }}
                      InputAdornmentProps={{
                        className: classes.dateIcon,
                        position: "start",
                      }}
                      className={classes.dateTextField}
                      invalidDateMessage=""
                      placeholder="End Date"
                    />
                  </MuiPickersUtilsProvider> */}
                        </Typography>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardTimePicker
                            variant="inline"
                            margin="normal"
                            id="time-picker5"
                            value={this.state.startTime2}
                            onChange={this.handleStartTime2}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.timeIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            placeholder="From"
                          />
                        </MuiPickersUtilsProvider>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardTimePicker
                            variant="inline"
                            margin="normal"
                            id="time-picker6"
                            value={this.state.endTime2}
                            onChange={this.handleEndTime2}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            inputProps={{ className: classes.dateInput }}
                            InputLabelProps={{ className: classes.dateInput }}
                            InputAdornmentProps={{
                              className: classes.timeIcon,
                              position: "start",
                            }}
                            className={classes.dateTextField}
                            placeholder="To"
                          />
                        </MuiPickersUtilsProvider>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    marginTop: 42,
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={this.openDateModal}
                  >
                    Send new dates
                  </Button>
                </div>
              </div>
            </Fade>
          </Modal>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(
  withRouter(MeetingDetails)
);
// Meeting details
