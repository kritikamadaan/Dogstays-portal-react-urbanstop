import React, { useState, useEffect, useSelector, Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { render } from "@testing-library/react";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import {
  CardActionArea,
  Grid,
  Card,
  CardMedia,
  CardContent,
  Typography,
} from "@material-ui/core";
import defaultPic from "../../assets/sitterSearch/defaultPic.png";
import { Link, Link as RouterLink } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    backgroundColor: "#FFFFFF",
    margin: theme.spacing(4),
    height: "100%",
  },

  sitterGrid: {
    display: "flex",
    padding: theme.spacing(5),

    borderRadius: 18,
    // boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.25)",
    //  background: "#FFFFFF",

    margin: theme.spacing(0, 8),
    justifyContent: "space-evenly",
    padding: theme.spacing(4, 3),
  },
  sitterCard: {
    display: "flex",
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
    opacity: 0.7,
    border: "1px solid #E8770E",
    width: 480,
  },
  sitterInfoCard: {
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
    border: "1px solid #E8770E",
  },
  sitterMedia: {
    width: 192,
  },
  sitterGrid: {
    padding: "10px",
  },
  container: {
    padding: 60,
    backgroundColor: "#FFFFFF",
  },
}));

const user = JSON.parse(localStorage.getItem("user"));

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

class DashboardFavourite extends Component {
  state = {
    sitterList: "",
    profile_photo: "",
    profile_name: "",
    loadingSitters: true,
  };

  componentDidMount() {
    this.getsitterProfileList();
  }
  getsitterProfileList = () => {
    console.log("User  *******", user);
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/`)
      .then((response) => {
        console.log("Response **********", response, response.data[0].id);
        this.setState({
          sitterList: response.data,
          loadingSitters: false,
          // profile_photo: response.data[0].profile_photo,
          // profile_name:response.data[0].profile_name
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    const { classes } = this.props;
    console.log("sitter profile list", this.state.sitterList);
    return (
      <Grid className={classes.root} style={{ backgroundColor: "white" }}>
        <Grid className={classes.sitterGrid}>
          {this.state.loadingSitters === true ? (
            <Grid style={{ height: "80vh", textAlign: "center" }}>
              <CircularProgress color="primary" centered />
            </Grid>
          ) : (
            <Grid xs={12} container className={classes.container}>
              {this.state.sitterList &&
                this.state.sitterList.map((sitter, index) => {
                  if (!sitter.vacation_mode) {
                    console.log("sitter single profile ********", sitter);
                    return (
                      <Grid item xs={4} spacing={4}>
                        <Link variant="h6" component={RouterLink}>
                          <Card
                            className={classes.sitterCard}
                            style={{
                              margin: 20,
                              marginbottom: 42,
                              borderRadius: 8,
                              backgroundColor: "white",
                            }}
                            key={index}
                            onClick={() => {
                              window.location.href = `/sitter-profile/${sitter.user.id}`;
                            }}
                          >
                            <CardActionArea>
                              <div style={{ display: "flex" }}>
                                <CardMedia
                                  component="img"
                                  classes={{ media: classes.sitterMedia }}
                                  width={140}
                                  height={140}
                                  image={
                                    sitter.profile_photo
                                      ? sitter.profile_photo
                                      : defaultPic
                                  }
                                  style={{ width: 140, height: 140 }}
                                />
                                <CardContent
                                  style={{
                                    position: "relative",
                                    width: "100%",
                                  }}
                                >
                                  <Typography
                                    variant="h5"
                                    color="secondary"
                                    style={{ textDecoration: "none" }}
                                  >
                                    {sitter.user.first_name}
                                  </Typography>
                                  <Typography
                                    variant="body1"
                                    style={{
                                      color: "#979797",
                                      marginTop: "8px",
                                      textDecoration: "none",
                                    }}
                                  >
                                    {sitter.profile_intro}
                                  </Typography>
                                </CardContent>
                              </div>
                            </CardActionArea>
                          </Card>
                        </Link>
                      </Grid>
                    );
                  }
                })}
            </Grid>
          )}
        </Grid>
      </Grid>
    );
  }
}
export default withStyles(styles, { withTheme: true })(DashboardFavourite);
