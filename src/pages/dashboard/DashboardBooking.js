import React, { useState, useEffect,useSelector } from "react";
// import { useDispatch, useSelector } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import { Typography, Tabs, Tab, Box, Divider, Button } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import bookingCardIcon from "../../assets/sitterDashboard/bookingCardIcon.png";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import moment from "moment";
import Logo from "../../assets/common/dogstays-logo.png";
import StripeCheckout from "react-stripe-checkout";
import axios from "axios";

const UserList = JSON.parse(localStorage.getItem("user"));
const config = {
  apiUrl: "http://3.215.2.1:8000",
};
const stripePromise = loadStripe("pk_test_JJ1eMdKN0Hp4UFJ6kWXWO4ix00jtXzq5XG");

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    backgroundColor: "white",
  },
  whiteBGWrapper: {
    padding: theme.spacing(5),
    backgroundColor: "#FFFFFF",
    borderRadius: 18,
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.25)",
  },
  profileTabs: {
    fontSize: 25,
    textTransform: "none",
    color: theme.palette.primary.main,
  },
  indicatorColor: {
    backgroundColor: "#F58220",
  },
  bookingCardsWrapper: {
    marginTop: theme.spacing(6),
  },
  bookingCard: {
    display: "flex",
    backgroundColor: "#F9F9F9",
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.25)",
    borderRadius: 5,
    width: "90%",
    // maxHeight: 170,
    padding: theme.spacing(0, 2),
    paddingTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    paddingRight: 0,
  },
  bookingCardLeft: {
    borderRight: "0.7px solid #F58220",
    paddingRight: theme.spacing(1),
    textAlign: "center",
  },
  bookingCardRight: {
    width: "100%",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    background: "#F58220",
    padding: theme.spacing(1, 4),
  },
  earnWrapper: {
    backgroundColor: theme.palette.primary.main,
    color: "#FFFFFF",
    textAlign: "center",
    padding: theme.spacing(0.5, 0),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
}));

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

const Payment = (props) => {
  return (
    <Elements stripe={stripePromise}>
      <form>
        <button type="submit" disabled={false}>
          Pay
        </button>
      </form>
    </Elements>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const DashboardBookings = () => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [responseId, setResponseId] = useState(null);
  const [tabValue, setTabValue] = useState(0);
  const [bookingListData, setBookingListData] = useState([]);
  const [meetingListData, setMeetingListData] = useState([]);
  const [open, setOpen] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [ModalOpen, setModalOpen] = React.useState(false);
  const [PaymentModalOpen, setPaymentModalOpen] = React.useState(false);
  const [BookingDetailsMapeed, setBookingDetailsMapeed] = React.useState(null);
  //Loading
  const [loadingMeeting, setLoadingMeeting] = React.useState(true);
  const [loadingBooking, setLoadingBooking] = React.useState(true);
  const [stripeData, setStripeData] = React.useState([]);
  const [stripeToken, setStripeToken] = React.useState("");
  const [CancelModalOpen, setCancelModalOpen] = React.useState(false);
  const [profilePhoto, setProfilePhoto] = useState("");
  const [userName,setUserName]=useState("")

  const user = JSON.parse(localStorage.getItem("user"));
  const getBookingList = () => {
    const userData = JSON.parse(localStorage.getItem("user"));
    const user_id = userData.user.id;

    fetch(`http://3.215.2.1:8000/api/bookinglist/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        setBookingListData(data);
        setLoadingBooking(false);
      });
  };

  const getMeetingList = () => {
    const userData = JSON.parse(localStorage.getItem("user"));
    const user_id = userData.user.id;

    fetch(`http://3.215.2.1:8000/api/meetinglist/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        setMeetingListData(data);
        setLoadingMeeting(false);
      });
  };

  function getModalStyle() {
    return {
      top: `50%`,
      left: `42%%`,
      transform: `translate(100%, 322%)`,
      backgroundColor: "#fff",
      width: 500,
    };
  }

  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };

  const handleOpen = (event, id) => {
    setResponseId(id);
    setOpen(true);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(false);
    setAnchorEl(null);
  };

  const handleModalOpen = () => {
    setModalOpen(true);
  };

  const handleModalClose = (props, context) => {
    getBookingList();
    getMeetingList();
    setModalOpen(false);
  };

  const handlePaymentModalOpen = () => {
    setPaymentModalOpen(true);
  };

  const handlePaymentModalOpenClose = (props, context) => {
    setPaymentModalOpen(false);
  };

  const handleCancelModalOpen = () => {
    setCancelModalOpen(true);
  };

  const handleCancelModalOpenClose = (props, context) => {
    setCancelModalOpen(false);
  };

  const handlePayment = (amount) => {
    // console.log("payment");
    const data = {
      amount: amount,
      currency: "EUR",
      description: "Booking Request",
      source: stripeToken,
    };
    axios
      .post(`${config.apiUrl}/api/payment/`, data)
      .then((response) => {
        console.log("payment response", response);
        if (response.status === 201) {
          handlePaymentModalOpen();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleAccept = (type) => {
    bookingListData &&
      bookingListData.map((list) => {
        if (responseId === list.id) {
          const data = {
            id: list.id,
            startDate: list.startDate,
            endDate: list.endDate,
            num_dogs: list.num_dogs,
            total: list.total,
            services: list.services,
            status: "accepted",
            startTime: list.startTime,
            endTime: list.endTime,
            textArea: list.textArea,
            sitterID: list.sitterID.id,
            ownerID: list.ownerID.id,
            pets: list.pets.map((d) => {
              return d.id;
            }),
          };

          fetch(`http://3.215.2.1:8000/api/${type}/${responseId}`, {
            method: "PUT", // or 'PUT'
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((response) => response.json())
            .then((data) => {
              handleClose();
              getBookingList();
              getMeetingList();
            })
            .catch((error) => {
              console.error("Error:", error);
            });
        }
      });
  };

  const handleCancel = (type) => {
    bookingListData &&
      bookingListData.map((list) => {
        if (responseId === list.id) {
          const data = {
            id: list.id,
            startDate: list.startDate,
            endDate: list.endDate,
            num_dogs: list.num_dogs,
            total: list.total,
            services: list.services,
            status: "cancelled",
            startTime: list.startTime,
            endTime: list.endTime,
            textArea: list.textArea,
            sitterID: list.sitterID.id,
            ownerID: list.ownerID.id,
            pets: list.pets.map((d) => {
              return d.id;
            }),
          };

          fetch(`http://3.215.2.1:8000/api/${type}/${responseId}`, {
            method: "PUT", // or 'PUT'
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((response) => response.json())
            .then((data) => {
              setCancelModalOpen(true);
              getBookingList();
              getMeetingList();
            })
            .catch((error) => {
              console.error("Error:", error);
            });
        }
      });
  };

  const handlePay = (type) => {
    handlePaymentModalOpen();
  };


  const getSitterDetails = () => {
    console.log("User", user);
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/?user=${user.user.id}`)
      .then((response) => {
        console.log("Response *** in bookings", response, response.data[0].id);
        setProfilePhoto(response.data[0].profile_photo);
        setUserName(response.data[0].user.first_name)
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const getOwnerDetails = () => {
    axios
      .get(`${config.apiUrl}/api/petownerprofile/list/?user=${user.user.id}`)
      .then((response) => {
        setProfilePhoto('response in booking**',response.data[0].profile_photo);
        setUserName(response.data[0].user.first_name)
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleReject = (type) => {
    bookingListData &&
      bookingListData.map((list) => {
        if (responseId === list.id) {
          const data = {
            id: list.id,
            startDate: list.startDate,
            endDate: list.endDate,
            num_dogs: list.num_dogs,
            total: list.total,
            services: list.services,
            status: "rejected",
            startTime: list.startTime,
            endTime: list.endTime,
            textArea: list.textArea,
            sitterID: list.sitterID.id,
            ownerID: list.ownerID.id,
            pets: list.pets.map((d) => {
              return d.id;
            }),
          };

          handleModalOpen();

          fetch(`http://3.215.2.1:8000/api/${type}/${responseId}`, {
            method: "PUT", // or 'PUT'
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          })
            .then((response) => response.json())
            .then((data) => {
              //
            })
            .catch((error) => {
              console.error("Error:", error);
            });
        }
      });
  };

  useEffect(() => {
    getBookingList();
    getMeetingList();
    if (user && user.user_type.is_sitter) {
      getSitterDetails();
    } else if (user && user.user_type.is_petowner) {
      getOwnerDetails();
    }
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.whiteBGWrapper}>
        <Tabs
          value={tabValue}
          onChange={handleTabChange}
          aria-label="simple tabs example"
          style={{ borderBottom: "0.7px solid rgba(245, 130, 32, 0.6)" }}
          variant="fullWidth"
          classes={{ indicator: classes.indicatorColor }}
        >
          <Tab
            label="Booking Requests"
            {...a11yProps(0)}
            className={classes.profileTabs}
          />
          <Tab
            label="Booking History"
            {...a11yProps(1)}
            className={classes.profileTabs}
          />
          <Tab
            label="Meeting Requests"
            {...a11yProps(2)}
            className={classes.profileTabs}
          />
          <Tab
            label="Meeting History"
            {...a11yProps(3)}
            className={classes.profileTabs}
          />
        </Tabs>
        <div className={classes.bookingCardsWrapper}>
          <TabPanel value={tabValue} index={2}>
            {loadingMeeting === true ? (
              <Grid style={{ height: "80vh", textAlign: "center" }}>
                <CircularProgress color="primary" centered />
              </Grid>
            ) : (
              <Grid container>
                {meetingListData &&
                  meetingListData.map((list) => {
                    const ServiceInfo = JSON.parse(list.services);
                    if (list.status === "open" || list.status === "accepted") {
                      console.log('the list is the',list)
                      return (
                        <Grid item xs={6}>
                          <div className={classes.bookingCard}>
                            {UserList.user_type.is_sitter === true ? (
                              <div className={classes.bookingCardLeft}>
                                {list.pets.map((pet) => {
                                  console.log()
                                  return (
                                    <div>
                                      <div
                                        className={classes.bookingCardLeft}
                                        style={{ borderRight: "none" }}
                                      >
                                        <img
                                       
                                          src={list.ownerID.profile_photo}
                                          alt="profile"
                                          style={{
                                            height: 32,
                                            width: 32,
                                            borderRadius: "50%",
                                          }}
                                        />
                                        <Typography style={{ fontSize: 12 }}>
                                     

                                          {list.ownerID.user.first_name}
                                        </Typography>
                                      </div>
                                    </div>
                                  );
                                })}
                              </div>
                            ) : (
                              <div className={classes.bookingCardLeft}>
                                <div>
                                  <div
                                    className={classes.bookingCardLeft}
                                    style={{ borderRight: "none" }}
                                  >
                                    <img
                                      src={list.sitterID.profile_photo}
                                      alt="profile"
                                      style={{
                                        height: 32,
                                        width: 32,
                                        borderRadius: "50%",
                                      }}
                                    />
                                    <Typography style={{ fontSize: 12 }}>
                                      {list.sitterID.user.first_name}
                                    </Typography>
                                  </div>
                                </div>
                              </div>
                            )}
                            <div className={classes.bookingCardRight}>
                              <div style={{ padding: "0 8px" }}>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                  }}
                                >
                                  <Typography
                                    style={{ fontSize: 14, fontWeight: 500 }}
                                  >
                                    <span style={{ color: "#979797" }}>ID</span>{" "}
                                    : #{list.meeting_id}
                                    <span style={{ color: "#21B9CC" }}>
                                      {UserList.user_type.is_sitter === true
                                        ? `(${list.ownerID.user.first_name})`
                                        : null}
                                    </span>
                                  </Typography>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      Meeting Dates
                                    </Typography>
                                  
                                        {console.log('momemt format',moment(list.startDate).format("DD/MM/YYYY"),list.status  )}
                                    
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >{list.status === "accepted"
                                    &&
                                      ( list.startDate === "" ||
                                      list.startDate === null
                                        ? "NA"
                                        : moment(list.startDate).format("DD/MM/YYYY")
                                      )}
                                    </Typography>
                                     
                                    { list.status === "open"  && (<div>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate2 === "" ||
                                      list.startDate2 === null
                                        ? "NA"
                                        : moment(list.startDate2).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate3 === "" ||
                                      list.startDate3 === null
                                        ? "NA"
                                        : moment(list.startDate3).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate4 === "" ||
                                      list.startDate4 === null
                                        ? "NA"
                                        : moment(list.startDate4).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography> 
                                    </div>
                                    )
                                    }
                                  </div>
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      Meeting Time
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {(list.fromTime === "" ||
                                      list.fromTime === null )&& list.status === "accepted"
                                        ? "NA"
                                        : moment(list.fromTime).format(
                                            "HH:MM"
                                          )}{" "}
                                      -{" "}
                                      {(list.toTime === "" ||
                                      list.toTime === null)  && list.status === "accepted"
                                        ? "NA"
                                        : moment(list.toTime).format("HH:MM")}
                                    </Typography>
                                    {list.status === "open" && ( 
                                      <div>
                                      <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.fromTime1 === "" ||
                                      list.fromTime1 === null
                                        ? "NA"
                                        : moment(list.fromTime1).format(
                                            "HH:MM"
                                          )}{" "}
                                      -{" "}
                                      {list.toTime1 === "" ||
                                      list.toTime1 === null
                                        ? "NA"
                                        : moment(list.toTime1).format("HH:MM")}
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.fromTime2 === "" ||
                                      list.fromTime2 === null
                                        ? "NA"
                                        : moment(list.fromTime2).format(
                                            "HH:MM"
                                          )}{" "}
                                      -{" "}
                                      {list.toTime2 === "" ||
                                      list.toTime2 === null
                                        ? "NA"
                                        : moment(list.toTime2).format("HH:MM")}
                                    </Typography> 
                                    </div>
                                    )}
                                  </div>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    marginTop: "1.5rem",
                                  }}
                                >
                                  <img
                                    src={bookingCardIcon}
                                    alt="booking card icon"
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 12,
                                      fontWeight: 500,
                                      color: "#5E5A5A",
                                    }}
                                  >
                                    {ServiceInfo && ServiceInfo.core_service &&
                                      ServiceInfo.core_service.service_id
                                        .service_name}
                                  </Typography>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <Typography
                                    color="primary"
                                    style={{ fontSize: 12, color:" #2ecc71",fontWeight: 500 }}
                                  >
                                    {list.status}
                                  </Typography>
                                </div>
                              </div>
                              <div
                                className={classes.earnWrapper}
                                style={{
                                  marginTop: "0.5rem",
                                  padding: 8,
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                }}
                              >
                                <Typography
                                  style={{ fontSize: 14, fontWeight: 500 }}
                                >
                                  {UserList.user_type.is_sitter === true
                                    ? `You Earn €${list.total}`
                                    : `View Details`}
                                </Typography>
                                <Link
                                  to={{
                                    pathname: `/dashboard/meetings/details/${list.id}`,
                                    customObject: { _id: list.id },
                                  }}
                                  style={{
                                    color: "#ffffff",
                                    textDecoration: "none",
                                  }}
                                  onClick={() => {
                                    localStorage.setItem("meetingID", list.id);
                                  }}
                                >
                                  <ArrowForwardIcon
                                    style={{ color: "#ffffff" }}
                                  />
                                </Link>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    }
                  })}
              </Grid>
            )}
          </TabPanel>
          <TabPanel value={tabValue} index={3}>
            {loadingMeeting === true ? (
              <Grid style={{ height: "80vh", textAlign: "center" }}>
                <CircularProgress color="primary" centered />
              </Grid>
            ) : (
              <Grid container>
                {meetingListData &&
                  meetingListData.map((list) => {
                    const ServiceInfo = JSON.parse(list.services);
                    if (list.status === "rejected") {
                      console.log('user List is ',UserList)
                      return (
                        <Grid item xs={6}>
                          <div className={classes.bookingCard}>
                            {UserList.user_type.is_sitter === true ? (
                              <div className={classes.bookingCardLeft}>
                                {list.pets.map((pet) => {
                                  console.log('the user list is',list)
                                  return (
                                    <div>
                                      <div
                                        className={classes.bookingCardLeft}
                                        style={{ borderRight: "none" }}
                                      >
                                        <img
                                          //src={pet.profile_photo}
                                          src={list.ownerID.profile_photo}
                                          alt="profile"
                                          style={{
                                            height: 32,
                                            width: 32,
                                            borderRadius: "50%",
                                          }}
                                        />
                                        <Typography style={{ fontSize: 12 }}>
                                          {list.ownerID.user.first_name}
                                        </Typography>
                                      </div>
                                    </div>
                                  );
                                })}
                              </div>
                            ) : (
                              <div className={classes.bookingCardLeft}>
                                <div>
                                  <div
                                    className={classes.bookingCardLeft}
                                    style={{ borderRight: "none" }}
                                  >
                                    <img
                                      src={list.sitterID.profile_photo}
                                      alt="profile"
                                      style={{
                                        height: 32,
                                        width: 32,
                                        borderRadius: "50%",
                                      }}
                                    />
                                    <Typography style={{ fontSize: 12 }}>
                                      {list.sitterID.user.first_name}
                                    </Typography>
                                  </div>
                                </div>
                              </div>
                            )}
                            <div className={classes.bookingCardRight}>
                              <div style={{ padding: "0 8px" }}>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                  }}
                                >
                                  <Typography
                                    style={{ fontSize: 14, fontWeight: 500 }}
                                  >
                                    <span style={{ color: "#979797" }}>ID</span>{" "}
                                    : #{list.meeting_id}
                                    <span style={{ color: "#21B9CC" }}>
                                      {UserList.user_type.is_sitter === true
                                        ? `(${list.ownerID.user.first_name})`
                                        : null}
                                    </span>
                                  </Typography>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      Meeting Dates
                                    </Typography>
                                    {console.log('Meeting dates ',list)}
                                    {list.startDate === "" ||
                                      list.startDate === null && list.status === "accepted"
                                        ? "NA"
                                        : moment(list.startDate).format(
                                            "DD/MM/YYYY"
                                          )}
                                        
                                    {list.status === "open" && ( 
                                      <div>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate2 === "" ||
                                      list.startDate2 === null
                                        ? "NA"
                                        : moment(list.startDate2).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate3 === "" ||
                                      list.startDate3 === null
                                        ? "NA"
                                        : moment(list.startDate3).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate4 === "" ||
                                      list.startDate4 === null
                                        ? "NA"
                                        : moment(list.startDate4).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                    </div>
                                    ) }
                                  </div>
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      Meeting Time
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.fromTime === "" ||
                                      list.fromTime === null   &&  list.status === "accepted"
                                        ? "NA"
                                        : moment(list.fromTime).format(
                                            "HH:MM"
                                          )}{" "}
                                      -{" "}
                                      {list.toTime === "" ||
                                      list.toTime === null
                                        ? "NA"
                                        : moment(list.toTime).format("HH:MM")}
                                    </Typography>
                                    {(list.status === "open")&&(<div>
                                       <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.fromTime1 === "" ||
                                      list.fromTime1 === null
                                        ? "NA"
                                        : moment(list.fromTime1).format(
                                            "HH:MM"
                                          )}{" "}
                                      -{" "}
                                      {list.toTime1 === "" ||
                                      list.toTime1 === null
                                        ? "NA"
                                        : moment(list.toTime1).format("HH:MM")}
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.fromTime2 === "" ||
                                      list.fromTime2 === null
                                        ? "NA"
                                        : moment(list.fromTime2).format(
                                            "HH:MM"
                                          )}{" "}
                                      -{" "}
                                      {list.toTime2 === "" ||
                                      list.toTime2 === null
                                        ? "NA"
                                        : moment(list.toTime2).format("HH:MM")}
                                    </Typography>
                                    </div>
                                    )}
                                  </div>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    marginTop: "1.5rem",
                                  }}
                                >
                                  <img
                                    src={bookingCardIcon}
                                    alt="booking card icon"
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 12,
                                      fontWeight: 500,
                                      color: "#5E5A5A",
                                    }}
                                  >
                                    {ServiceInfo &&
                                      ServiceInfo.core_service.service_id
                                        .service_name}
                                  </Typography>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <Typography
                                    color="primary"
                                    style={{ fontSize: 12, fontWeight: 500 }}
                                  >
                                    {list.status}
                                  </Typography>
                                </div>
                              </div>
                              <div
                                className={classes.earnWrapper}
                                style={{
                                  marginTop: "0.5rem",
                                  padding: 8,
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                }}
                              >
                                <Typography
                                  style={{ fontSize: 14, fontWeight: 500 }}
                                >
                                  {UserList.user_type.is_sitter === true
                                    ? `You Earn €${list.total}`
                                    : `View Details`}
                                </Typography>
                                <Link
                                  to={{
                                    pathname: `/dashboard/meetings/details/${list.id}`,
                                    customObject: { _id: list.id },
                                  }}
                                  style={{
                                    color: "#ffffff",
                                    textDecoration: "none",
                                  }}
                                  onClick={() => {
                                    localStorage.setItem("meetingID", list.id);
                                  }}
                                >
                                  <ArrowForwardIcon
                                    style={{ color: "#ffffff" }}
                                  />
                                </Link>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    }
                  })}
              </Grid>
            )}
          </TabPanel>
          <TabPanel value={tabValue} index={0}>
            {loadingBooking === true ? (
              <Grid style={{ height: "80vh", textAlign: "center" }}>
                <CircularProgress color="primary" centered />
              </Grid>
            ) : (
              <Grid container>
                {bookingListData &&
                  bookingListData.map((list) => {
                    const ServiceInfo = JSON.parse(list.services);
                    if (list.status === "open" || list.status === "accepted") {
                      return (
                        <Grid item xs={6}>
                          <div className={classes.bookingCard}>
                            {UserList.user_type.is_sitter === true ? (
                              <div className={classes.bookingCardLeft}>
                                {list.pets.map((pet) => {
                                  return (
                                    <div>
                                      <div
                                        className={classes.bookingCardLeft}
                                        style={{ borderRight: "none" }}
                                      >
                                        <img
                                          src={list.ownerID.profile_photo}
                                          alt="profile"
                                          style={{
                                            height: 32,
                                            width: 32,
                                            borderRadius: "50%",
                                          }}
                                        />
                                        <Typography style={{ fontSize: 12 }}>
                                          {/* {pet.name} */}
                                          {list.ownerID.user.first_name}
                                        </Typography>
                                      </div>
                                    </div>
                                  );
                                })}
                              </div>
                            ) : (
                              <div className={classes.bookingCardLeft}>
                                <div>
                                  <div
                                    className={classes.bookingCardLeft}
                                    style={{ borderRight: "none" }}
                                  >
                                    <img
                                      src={list.sitterID.profile_photo}
                                      alt="profile"
                                      style={{
                                        height: 32,
                                        width: 32,
                                        borderRadius: "50%",
                                      }}
                                    />
                                    <Typography style={{ fontSize: 12 }}>
                                      {list.sitterID.user.first_name}
                                    </Typography>
                                  </div>
                                </div>
                              </div>
                            )}
                            <div className={classes.bookingCardRight}>
                              <div style={{ padding: "0 8px" }}>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                  }}
                                >
                                  <Typography
                                    style={{ fontSize: 14, fontWeight: 500 }}
                                  >
                                    <span style={{ color: "#979797" }}>ID</span>{" "}
                                    : #{list.booking_id}
                                    <span style={{ color: "#21B9CC" }}>
                                      {UserList.user_type.is_sitter === true
                                        ? `(${list.ownerID.user.first_name})`
                                        : null}
                                    </span>
                                  </Typography>
                                  {list.status === "accepted" ? (
                                    <button
                                      onClick={(event) => {
                                        return handleOpen(event, list.id);
                                      }}
                                      style={{
                                        background: "transparent",
                                        border: "none",
                                      }}
                                    >
                                      <MoreVertIcon />
                                    </button>
                                  ) : null}
                                  {UserList.user_type.is_sitter === true ? (
                                    <Menu
                                      id="simple-menu"
                                      anchorEl={anchorEl}
                                      keepMounted
                                      open={open}
                                      onClose={handleClose}
                                    >
                                      <MenuItem
                                        onClick={() => {
                                          handleAccept(
                                            tabValue === 0
                                              ? "meeting"
                                              : "booking"
                                          );
                                        }}
                                      >
                                        Accept
                                      </MenuItem>
                                      <MenuItem
                                        onClick={() => {
                                          handleReject(
                                            tabValue === 0
                                              ? "meeting"
                                              : "booking"
                                          );
                                        }}
                                      >
                                        Reject
                                      </MenuItem>
                                    </Menu>
                                  ) : (
                                    <Menu
                                      id="simple-menu"
                                      anchorEl={anchorEl}
                                      keepMounted
                                      open={open}
                                      onClose={handleClose}
                                    >
                                      <StripeCheckout
                                        name={"DogStays"}
                                        description={"Booking Request"}
                                        image={Logo}
                                        token={(d) => {
                                          setStripeToken(d.id);
                                          setStripeData(d);
                                        }}
                                        amount={list.total}
                                        currency={"EUR"}
                                        // stripeKey={
                                        //   "pk_test_4RveNgOMiWDZXSLA8kB54A5N"
                                        // }
                                        stripeKey="pk_test_51HBxebFEnpBY1agIwplv8vJ8K3NpUjKLGDdR9sBH1R1k2WF6zLFzKgGZoHAGL9E6GvlCrJ2AeKD1PjgLExEVETlp00j5zaZJct"
                                        email={list.sitterID.user.email}
                                        allowRememberMe
                                        closed={() => {
                                          handlePayment(list.total);
                                        }}
                                      >
                                        <MenuItem>Pay Now</MenuItem>
                                      </StripeCheckout>
                                      <MenuItem onClick={handleCancel}>
                                        Cancel
                                      </MenuItem>
                                    </Menu>
                                  )}
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      Start Date
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate === "" ||
                                      list.startDate === null
                                        ? "NA"
                                        : moment(list.startDate).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                  </div>
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      End Date
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.endDate === "" ||
                                      list.endDate === null
                                        ? "NA"
                                        : moment(list.endDate).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                  </div>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    marginTop: "1.5rem",
                                  }}
                                >
                                  <img
                                    src={bookingCardIcon}
                                    alt="booking card icon"
                                    style={{
                                      height: 20,
                                      width: 20,
                                    }}
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 12,
                                      fontWeight: 500,
                                      color: "#5E5A5A",
                                    }}
                                  >
                                    {ServiceInfo && ServiceInfo.core &&
                                      ServiceInfo.core.core_service.service_id
                                        .service_name}
                                  </Typography>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <Typography
                                    color=" #2ecc71"
                                    style={{ fontSize: 12, color:" #2ecc71",fontWeight: 500 }}
                                  >
                                    {list.status}
                                  </Typography>
                                </div>
                              </div>
                              <div
                                className={classes.earnWrapper}
                                style={{
                                  marginTop: "0.5rem",
                                  padding: 8,
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                }}
                              >
                                <Typography
                                  style={{ fontSize: 14, fontWeight: 500 }}
                                >
                                  {UserList.user_type.is_sitter === true
                                    ? `You Earn €${list.total}`
                                    : `View Details`}
                                </Typography>
                                <Link
                                  // to={`/dashboard/bookings/details/${list.id}`}
                                  to={{
                                    pathname: `/dashboard/bookings/details/${list.id}`,
                                    customObject: { _id: list.id },
                                  }}
                                  style={{
                                    color: "#ffffff",
                                    textDecoration: "none",
                                  }}
                                  onClick={() => {
                                    localStorage.setItem("bookingID", list.id);
                                  }}
                                >
                                  <ArrowForwardIcon
                                    style={{ color: "#ffffff" }}
                                  />
                                </Link>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    }
                  })}
              </Grid>
            )}
          </TabPanel>
          <TabPanel value={tabValue} index={1}>
            {loadingBooking === true ? (
              <Grid style={{ height: "80vh", textAlign: "center" }}>
                <CircularProgress color="primary" centered />
              </Grid>
            ) : (
              <Grid container>
                {bookingListData &&
                  bookingListData.map((list) => {
                    const ServiceInfo = JSON.parse(list.services);
                    if (
                      list.status === "rejected" ||
                      list.status === "confirmed" ||
                      list.status === "cancelled"
                    ) {
                      return (
                        <Grid item xs={6}>
                          <div className={classes.bookingCard}>
                            {UserList.user_type.is_sitter === true ? (
                              <div className={classes.bookingCardLeft}>
                                {list.pets.map((pet) => {
                                  return (
                                    <div>
                                      <div
                                        className={classes.bookingCardLeft}
                                        style={{ borderRight: "none" }}
                                      >
                                        <img
                                          src={pet.profile_photo}
                                          alt="profile"
                                          style={{
                                            height: 32,
                                            width: 32,
                                            borderRadius: "50%",
                                          }}
                                        />
                                        <Typography style={{ fontSize: 12 }}>
                                          {pet.name}
                                        </Typography>
                                      </div>
                                    </div>
                                  );
                                })}
                              </div>
                            ) : (
                              <div className={classes.bookingCardLeft}>
                                <div>
                                  <div
                                    className={classes.bookingCardLeft}
                                    style={{ borderRight: "none" }}
                                  >
                                    <img
                                      src={list.sitterID.profile_photo}
                                      alt="profile"
                                      style={{
                                        height: 32,
                                        width: 32,
                                        borderRadius: "50%",
                                      }}
                                    />
                                    <Typography style={{ fontSize: 12 }}>
                                      {list.sitterID.user.first_name}
                                    </Typography>
                                  </div>
                                </div>
                              </div>
                            )}
                            <div className={classes.bookingCardRight}>
                              <div style={{ padding: "0 8px" }}>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                  }}
                                >
                                  <Typography
                                    style={{ fontSize: 14, fontWeight: 500 }}
                                  >
                                    <span style={{ color: "#979797" }}>ID</span>{" "}
                                    : #{list.booking_id}
                                    <span style={{ color: "#21B9CC" }}>
                                      {UserList.user_type.is_sitter === true
                                        ? `(${list.ownerID.user.first_name})`
                                        : null}
                                    </span>
                                  </Typography>
                                  {/* <button
                                  onClick={(event) => {
                                    return handleOpen(event, list.id);
                                  }}
                                  style={{
                                    background: "transparent",
                                    border: "none",
                                  }}
                                >
                                  <MoreVertIcon />
                                </button> */}
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      Start Date
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.startDate === "" ||
                                      list.startDate === null
                                        ? "NA"
                                        : moment(list.startDate).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                  </div>
                                  <div>
                                    <Typography
                                      style={{ fontSize: 10, color: "#979797" }}
                                    >
                                      End Date
                                    </Typography>
                                    <Typography
                                      style={{ fontSize: 12, color: "#5E5A5A" }}
                                    >
                                      {list.endDate === "" ||
                                      list.endDate === null
                                        ? "NA"
                                        : moment(list.endDate).format(
                                            "DD/MM/YYYY"
                                          )}
                                    </Typography>
                                  </div>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    marginTop: "1.5rem",
                                  }}
                                >
                                  <img
                                    src={bookingCardIcon}
                                    alt="booking card icon"
                                    style={{
                                      height: 20,
                                      width: 20,
                                    }}
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 12,
                                      fontWeight: 500,
                                      color: "#5E5A5A",
                                    }}
                                  >
                                    {ServiceInfo && ServiceInfo.core &&
                                      ServiceInfo.core.core_service.service_id
                                        .service_name}
                                  </Typography>
                                </div>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  <Typography
                                    color="primary"
                                    style={{ fontSize: 12, fontWeight: 500 }}
                                  >
                                    {list.status}
                                  </Typography>
                                </div>
                              </div>
                              <div
                                className={classes.earnWrapper}
                                style={{
                                  marginTop: "0.5rem",
                                  padding: 8,
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                }}
                              >
                                <Typography
                                  style={{ fontSize: 14, fontWeight: 500 }}
                                >
                                  {UserList.user_type.is_sitter === true
                                    ? `You Earn €${list.total}`
                                    : `View Details`}
                                </Typography>
                                <Link
                                  // to={`/dashboard/bookings/details/${list.id}`}
                                  to={{
                                    pathname: `/dashboard/bookings/details/${list.id}`,
                                    customObject: { _id: list.id },
                                  }}
                                  style={{
                                    color: "#ffffff",
                                    textDecoration: "none",
                                  }}
                                  onClick={() => {
                                    localStorage.setItem("bookingID", list.id);
                                  }}
                                >
                                  <ArrowForwardIcon
                                    style={{ color: "#ffffff" }}
                                  />
                                </Link>
                              </div>
                            </div>
                          </div>
                        </Grid>
                      );
                    }
                  })}
              </Grid>
            )}
          </TabPanel>
        </div>
      </div>

      {/* <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={PaymentModalOpen}
        onClose={handlePaymentModalOpenClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={PaymentModalOpen}>
          <div className={classes.paper}>
            <Payment />
          </div>
        </Fade>
      </Modal> */}

      {/* Confirm */}

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={PaymentModalOpen}
        onClose={handlePaymentModalOpenClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={PaymentModalOpen}>
          <div className={classes.paper}>
            <Typography
              color="primary"
              style={{
                fontFamily: "Kanit",
                fontSize: 18,
                color: "#E8770E",
                textAlign: "center",
              }}
            >
              Woohooo! Booking Confirmed
            </Typography>

            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                className={classes.orangeBtn}
                onClick={handlePaymentModalOpenClose}
              >
                Done
              </Button>
            </div>
          </div>
        </Fade>
      </Modal>

      {/* Cancel */}

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={CancelModalOpen}
        onClose={handleCancelModalOpenClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={PaymentModalOpen}>
          <div className={classes.paper}>
            <Typography
              color="primary"
              style={{
                fontFamily: "Kanit",
                fontSize: 18,
                color: "#E8770E",
                textAlign: "center",
              }}
            >
              Booking Request Cancelled
            </Typography>

            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                className={classes.orangeBtn}
                onClick={handleCancelModalOpenClose}
              >
                Done
              </Button>
            </div>
          </div>
        </Fade>
      </Modal>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={ModalOpen}
        onClose={handleModalClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={ModalOpen}>
          <div className={classes.paper}>
            <Typography
              color="primary"
              style={{
                fontFamily: "Kanit",
                fontSize: 18,
                color: "#E8770E",
                textAlign: "left",
              }}
            >
              Reasons For Rejecting
            </Typography>
            <TextareaAutosize
              aria-label="empty textarea"
              placeholder="Reasons For Rejecting"
              rowsMin={8}
              style={{
                width: 540,
                border: "1px solid #E8770E",
                borderRadius: 4,
                marginTop: 28,
              }}
            />
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                className={classes.orangeBtn}
                onClick={handleModalClose}
              >
                Submit
              </Button>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

// export default DashboardBookings;
export default withRouter(DashboardBookings);
