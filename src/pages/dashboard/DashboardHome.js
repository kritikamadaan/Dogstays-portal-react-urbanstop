import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid } from "@material-ui/core";
import interview from "../../assets/sitterDashboard/interview.png";
import Bell from "../../assets/sitterDashboard/bell.svg";
import Booking from "../../assets/sitterDashboard/booking.png";
import Calendar from "../../assets/sitterDashboard/calendar.png";
import Insights from "../../assets/sitterDashboard/insights.png";
import { Link } from "react-router-dom";
import axios from "axios";

const user = JSON.parse(localStorage.getItem("user"));

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    backgroundColor: theme.palette.primary.main,
  },
  whiteBGWrapper: {
    backgroundColor: "#FFFFFF",
    borderRadius: 18,
    // boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.25)",
    padding: theme.spacing(6),
  },
  homeHeading: {
    fontSize: 30,
    fontFamily: "Fredoka One",
  },
  homeLeftCard: {
    border: "0.5px solid #E8770E",
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.15)",
    borderRadius: 6,
    padding: 27,
  },
  homeRightCards: {
    border: "0.5px solid #E8770E",
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.15)",
    borderRadius: 6,
    padding: 17,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: theme.spacing(2, 5),
  },
  grid: {
    padding: theme.spacing(2, 4) + "!important",
  },
}));
const DashboardHome = () => {
  const [sitterID, setSitterID] = useState(null);
  const [ownerID, setOwnerID] = useState(null);
  const [dashbaordData, setDashbaordData] = useState([]);

  const classes = useStyles();

  const config = {
    apiUrl: "http://3.215.2.1:8000",
  };

  const goToCalendar = () => {
    window.location.href = "/calendar";
  };

  const goToInsights = () => {
    const self = this.props;
    self.history = "/dashboard/insights";
  };

  const goToMeeting = () => {
    // window.location.href = "/insights";
  };

  const goToBooking = () => {
    // window.location.href = "/insights";
  };

  const getOwnerDetails = () => {
    axios
      .get(`${config.apiUrl}/api/petownerprofile/list/?user=${user.user.id}`)
      .then((response) => {
        console.log("response in getownerprofile", response.data[0]);
        setOwnerID(response.data[0].id);
        getOwnerDashboardData(response.data[0].id);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getSitterDetails = () => {
    console.log("User", user);
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/?user=${user.user.id}`)
      .then((response) => {
        console.log("Response", response, response.data[0].id);
        setSitterID(response.data[0].id);
        getSitterDashboardData(response.data[0].id);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getOwnerDashboardData = (id) => {
    axios
      .get(`${config.apiUrl}/api/owner_dashboard_info/?owner_id=${id}`)
      .then((response) => {
        console.log("response in owner dashboard", response);
        setDashbaordData(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getSitterDashboardData = (id) => {
    axios
      .get(`${config.apiUrl}/api/sitter_dashboard_info/?sitter_id=${id}`)
      .then((response) => {
        console.log("response in sitter dashboard", response);
        setDashbaordData(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (user && user.user_type.is_sitter) {
      getSitterDetails();
    } else if (user && user.user_type.is_petowner) {
      getOwnerDetails();
    }
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.whiteBGWrapper}>
        <Typography color="primary" className={classes.homeHeading}>
          Welcome {user.user.first_name} !
        </Typography>
        <Typography style={{ fontSize: 20, color: "#979797" }}>
          Get to know more about your account here.......
        </Typography>
        <Grid container spacing={8} style={{ marginTop: "1rem" }}>
          <Grid item xs={6} className={classes.grid}>
            <div className={classes.homeLeftCard}>
              <div
                style={{
                  display: "flex",
                }}
              >
                <img
                  src={Bell}
                  style={{
                    height: 68,
                    width: 68,
                    marginRight: 32,
                  }}
                  alt=""
                />
                <div>
                  <Typography
                    color="secondary"
                    style={{ fontSize: 24, fontFamily: "Fredoka One" }}
                  >
                    Notification summary
                  </Typography>
                  <Typography
                    style={{
                      color: "#5E5A5A",
                      margin: "4px 0",
                      fontFamily: "Kanit",
                    }}
                  >
                    You have 3 new notifications
                  </Typography>
                </div>
              </div>

              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "32px 0",
                  marginRight: 64,
                }}
              >
                <Typography style={{ fontSize: 18, color: "#5E5A5A" }}>
                  Carl has requested you for a meeting
                </Typography>
                <Link
                  color="primary"
                  style={{ fontSize: 18, color: "#E8770E" }}
                >
                  View
                </Link>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "32px 0",
                  marginRight: 64,
                }}
              >
                <Typography style={{ fontSize: 18, color: "#5E5A5A" }}>
                  Carl has requested you for a meeting
                </Typography>
                <Link
                  color="primary"
                  style={{ fontSize: 18, color: "#E8770E" }}
                >
                  View
                </Link>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "32px 0",
                  marginRight: 64,
                }}
              >
                <Typography style={{ fontSize: 18, color: "#5E5A5A" }}>
                  Carl has requested you for a meeting
                </Typography>
                <Link
                  color="primary"
                  style={{ fontSize: 18, color: "#E8770E" }}
                >
                  View
                </Link>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "32px 0",
                  marginRight: 64,
                }}
              >
                <Typography style={{ fontSize: 18, color: "#5E5A5A" }}>
                  Carl has requested you for a meeting
                </Typography>
                <Link
                  color="primary"
                  style={{ fontSize: 18, color: "#E8770E" }}
                >
                  View
                </Link>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "32px 0",
                  marginRight: 64,
                }}
              >
                <Typography style={{ fontSize: 18, color: "#5E5A5A" }}>
                  Carl has requested you for a meeting
                </Typography>
                <Link
                  color="primary"
                  style={{ fontSize: 18, color: "#E8770E" }}
                >
                  View
                </Link>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "32px 0",
                  marginRight: 64,
                }}
              >
                <Typography style={{ fontSize: 18, color: "#5E5A5A" }}>
                  Carl has requested you for a meeting
                </Typography>
                <Link
                  color="primary"
                  style={{ fontSize: 18, color: "#E8770E" }}
                >
                  View
                </Link>
              </div>
            </div>
          </Grid>
          <Grid item xs={6} className={classes.grid}>
            <div
              className={classes.homeRightCards}
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                cursor: "pointer",
                marginTop: "0px",
              }}
              onClick={goToBooking}
            >
              <img
                src={interview}
                alt="interview"
                style={{
                  height: 72,
                  width: 72,
                }}
              />
              <div>
                <Link
                  to="/dashboard/bookings-meetings"
                  style={{ textDecoration: "none" }}
                >
                  <Typography color="secondary" style={{ fontSize: 24 }}>
                    Upcoming Meetings
                  </Typography>
                </Link>
                <Typography style={{ color: "#5E5A5A", fontSize: 16 }}>
                  You have 2 upcoming meetings
                </Typography>
              </div>
              <Typography style={{ fontSize: 20, color: "#E8770E" }}>
                &gt;
              </Typography>
            </div>
            <div
              className={classes.homeRightCards}
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                cursor: "pointer",
              }}
              onClick={goToMeeting}
            >
              <img
                src={Booking}
                alt="interview"
                style={{
                  height: 72,
                  width: 72,
                }}
              />
              <div>
                <Link
                  to="/dashboard/bookings-meetings"
                  style={{ textDecoration: "none" }}
                >
                  <Typography color="secondary" style={{ fontSize: 24 }}>
                    Upcoming Bookings
                  </Typography>
                </Link>
                <Typography style={{ color: "#5E5A5A", fontSize: 16 }}>
                  You have 2 upcoming bookings
                </Typography>
              </div>
              <Typography style={{ fontSize: 20, color: "#E8770E" }}>
                &gt;
              </Typography>
            </div>

            {JSON.parse(localStorage.getItem("user")).user_type.is_sitter ===
            true ? (
              <React.Fragment>
                <Link
                  to="/dashboard/calendar"
                  style={{ textDecoration: "none" }}
                >
                  <div
                    className={classes.homeRightCards}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      cursor: "pointer",
                    }}
                    // onClick={goToCalendar}
                  >
                    <img
                      src={Calendar}
                      alt="interview"
                      style={{
                        height: 72,
                        width: 72,
                      }}
                    />
                    <div>
                      <Typography color="secondary" style={{ fontSize: 24 }}>
                        Calendar
                      </Typography>
                      <Typography style={{ color: "#5E5A5A", fontSize: 16 }}>
                        Last updated on Jan 06 2020
                      </Typography>
                    </div>
                    <Typography style={{ fontSize: 20, color: "#E8770E" }}>
                      &gt;
                    </Typography>
                  </div>
                </Link>
                <Link
                  to="/dashboard/insights"
                  style={{ textDecoration: "none" }}
                >
                  <div
                    className={classes.homeRightCards}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      cursor: "pointer",
                    }}
                    // onClick={goToInsights}
                  >
                    <img
                      src={Insights}
                      alt="interview"
                      style={{
                        height: 72,
                        width: 72,
                      }}
                    />
                    <div>
                      <Typography color="secondary" style={{ fontSize: 24 }}>
                        Insights
                      </Typography>
                      <Typography
                        style={{
                          color: "#5E5A5A",
                          fontSize: 16,
                          maxWidth: 220,
                        }}
                      >
                        Get more insights on your account{" "}
                      </Typography>
                    </div>
                    <Typography style={{ fontSize: 20, color: "#E8770E" }}>
                      &gt;
                    </Typography>
                  </div>
                </Link>
              </React.Fragment>
            ) : null}
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default DashboardHome;
