import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import {
  Grid,
  Typography,
  Tabs,
  Tab,
  Box,
  Button,
  TextField,
  RadioGroup,
  Radio,
  IconButton,
  FormControlLabel,
  FormControl,
  Avatar,
  Menu,
  MenuItem,
  Select,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import MuiPhoneNumber from "react-phone-input-2";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Link as RouterLink } from "react-router-dom";
import axios from "axios";
import profileFallback from "../../assets/sitterDashboard/profileFallback.png";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import moment from "moment";
import AddIcon from "@material-ui/icons/Add";
import dogImage from "../../assets/sitterSearch/Vector.png";
import Rating from "@material-ui/lab/Rating";
import Dog from "../../assets/sitterSearch/Vector.png";
import Cat from "../../assets/sitterSearch/group.png";
import Other from "../../assets/sitterSearch/group_1.png";
import DogProfileDashboard from "./DogProfileDashboard";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import AddPetComponent from "./components/AddPetComponent";
import S3FileUpload from "react-s3";
import Slider from "@material-ui/core/Slider";
import Cropper from "react-easy-crop";
import Loader from "./components/LoaderComponent";
import DateRangeIcon from "@material-ui/icons/DateRange";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const user = JSON.parse(localStorage.getItem("user"));
var anchorId = " ";
const configBucket = user
  ? {
    bucketName: "dogstayss3",
    dirName:
      "user/" +
      (user !== null || user !== undefined ? user.user.id + "/" : "test/"),
    region: "us-east-1",
    accessKeyId: "AKIA5YN7PDU5YXKJ3BW2",
    secretAccessKey: "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",
  }
  : null;

const searchOptions = {
  types: ["(regions)"],
  // componentRestrictions: { regions: "country" },
};

const styles = (theme) => ({
  root: {
    padding: theme.spacing(3),
    backgroundColor: "white",
  },
  whiteBGWrapper: {
    backgroundColor: "#FFFFFF",
    borderRadius: 18,
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.25)",
  },
  myPets: {
    backgroundColor: "#FFFEFE",
    borderRadius: 10,
    border: "1px solid #F58220",
    height: "100%",
    textAlign: "center",
    paddingTop: theme.spacing(4),
  },
  profileImg: {
    border: "0.5px solid #F58220",
    borderRadius: 8,
    textAlign: "center",
    maxWidth: 200,
  },
  profileTabs: {
    fontSize: 24,
    textTransform: "none",
    color: theme.palette.primary.main,
  },
  indicatorColor: {
    backgroundColor: "#F58220",
  },
  leftGridScroll: {
    height: 384,
    overflow: "auto",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  leftGridContent: {
    fontSize: 18,
    color: "#5E5A5A",
  },
  capitalized: {
    textTransform: "capitalize",
    fontSize: 18,
    color: "#5E5A5A",
  },
  uppercase: {
    textTransform: "uppercase",
    fontSize: 18,
    color: "#5E5A5A",
  },
  rightGrid: {
    border: "1px solid rgba(232, 119, 14, 0.7)",
    borderRadius: 8,
    width: 308,
    margin: "0 auto",
    padding: theme.spacing(2),
    marginTop: "3rem",
  },
  orangeBtnOutlined: {
    padding: theme.spacing(1.5),
    fontSize: 20,
  },
  orangeBtnOutlinedCircle: {
    marginTop: 24,
    marginBottom: 12,
    padding: theme.spacing(1.5),
    fontSize: 24,
    borderRadius: "50%",
    height: 62,
    width: 42,
    border: "1px solid #F58220",
  },
  experienceImg: {
    width: 200,
    height: 200,
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 8,
    objectFit: "cover",
  },
  reviewCard: {
    borderBottom: "2px solid rgba(232, 119, 14, 0.7)",
    marginTop: theme.spacing(4),
  },
  reviewerName: {
    fontSize: 22,
    fontWeight: 600,
  },
  orangeRating: {
    color: theme.palette.primary.main,
    marginTop: theme.spacing(0.5),
  },
  reviewCardLeft: {
    textAlign: "center",
    paddingTop: theme.spacing(1),
  },
  editField: {
    width: 250,
  },
  maxSittingCapacityBtnGroup: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  paper_pet: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  center: {
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    width: "60%",
    margin: "auto",
  },

  cropperPaper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  cropContainer: {
    minHeight: 500,
  },
  sliderControls: {
    position: "absolute",
    bottom: 0,
    left: "50%",
    width: "50%",
    transform: "translateX(-50%)",
    height: 80,
    display: "flex",
    alignItems: "center",
  },
  placeholderText: {
    color: "#979797",
  },
});

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const CssTextField = withStyles({
  root: {
    color: "#E8770E",
    "& label": {
      color: "rgba(94, 90, 90, 0.7)",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "1px solid #E8770E",
    },
    "& label.Mui-focused": {
      color: "#3EB3CD",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#3EB3CD",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "2px solid #3EB3CD",
    },
  },
})(TextField);

class DashboardOwnerProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      loadingProfile: true,
      editable: false,
      postal_code: "",
      tabValue: 0,
      address: "",
      id_type: null,
      id_number: null,
      owner: [],
      ownerPets: [],
      profileFileName: "",
      idFileName: "",
      profile_photo: "",
      petModal: false,
      border1: false,
      border2: false,
      border3: false,
      AddPetStep: false,
      type_of_pet: "",
      handleStayHomeAloneChoiceChange: false,
      toilet_trained_choice: false,
      confirmModalOpen: false,
      isConfirm: false,
      openPetView: false,
      petViewId: null,
      id_image: null,
      idFileName: "",
      imageCropperModal: false,
      imageSrc:
        "https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000",
      crop: { x: 0, y: 0 },
      zoom: 1,
      aspect: 1 / 1,
      radio1: "",
      anchorEl: null,
      anchorId: "",
      open: "false",
      have_insurance: "no",
      insurance_privider: "",
      other: "",
      googleAddress: "",
      house_no: "",
    };
  }

  componentDidMount() {
    this.getOwnerDetails();
    this.getOwnerPets();
    this.handleIdUpload = this.handleIdUpload.bind(this);
    this.handleProfileUpload = this.handleProfileUpload.bind(this);
  }

  handleHaveInsurance = (event) => {
    this.setState({
      have_insurance: event.target.value,
    });
  };

  handleInsuranceMenuChange = (event) => {
    this.setState({
      insurance_privider: event.target.value,
    });
  };

  handleInsuranceOtherChange = (event) => {
    console.log("the insurance provider other field ", event.target.value);
    this.setState({
      other: event.target.value,
    });
  };

  handleAnchorClick = (event) => {
    console.log("handle Anchor click", event.target.value);
    this.setState({
      anchorEl: event.currentTarget,
    });
  };

  handleAnchorClose = () => {
    this.setState({
      anchorEl: null,
    });
  };

  handleClickById = (id) => {
    console.log("handle click id", id);
    anchorId = id;
    console.log("anchor Id ", anchorId);
  };

  handleIdUpload(e) {
    this.refs.fileUploader.click();
  }

  handleGoogleAddressSelect = (googleAddress) => {
    geocodeByAddress(googleAddress)
      .then((results) => {
        console.log(results);
        getLatLng(results[0]);
        this.setState({
          city: results[0].address_components[0].long_name,
        });
      })
      .then((latLng) => console.log("Success", latLng))
      .catch((error) => console.error("Error", error));
  };

  handleGoogleCountrySelect = (googleAddress) => {
    geocodeByAddress(googleAddress)
      .then((results) => {
        console.log(results);
        getLatLng(results[0]);
        this.setState({
          country: results[0].address_components[0].long_name,
        });
      })
      .then((latLng) => console.log("Success", latLng))
      .catch((error) => console.error("Error", error));
  };

  handleGoogleCitySelect = (googleAddress) => {
    geocodeByAddress(googleAddress)
      .then((results) => {
        console.log(results);
        getLatLng(results[0]);
        this.setState({
          city: results[0].address_components[0].long_name,
        });
      })
      .then((latLng) => console.log("Success", latLng))
      .catch((error) => console.error("Error", error));
  };

  handleGoogleAddressChange = (googleAddress) => {
    this.setState({ googleAddress });
  };

  handleGoogleCityChange = (city) => {
    this.setState({ city });
  };

  handleGoogleCountryChange = (country) => {
    this.setState({ country });
  };

  handleGooglePostalCodeSelect = (googleAddress) => {
    geocodeByAddress(googleAddress)
      .then((results) => {
        console.log(results);
        getLatLng(results[0]);
        this.setState({
          postal_code: results[0].address_components[0].long_name,
        });
      })
      .then((latLng) => console.log("Success", latLng))
      .catch((error) => console.error("Error", error));
  };

  handleGooglePostalCodeChange = (postal_code) => {
    this.setState({ postal_code });
  };

  handleGoogleStreetSelect = (googleAddress) => {
    geocodeByAddress(googleAddress)
      .then((results) => {
        console.log(results);
        getLatLng(results[0]);
        this.setState({
          street_address: results[0].formatted_address,
        });
      })
      .then((latLng) => console.log("Success", latLng))
      .catch((error) => console.error("Error", error));
  };

  handleGoogleStreetChange = (street_address) => {
    this.setState({ street_address });
  };

  handleProfileUpload = (e) => {
    this.refs.imageUploader.click();
  };
  fileUploader = (e) => {
    e.persist();
    const file = e.target.files[0];
    let formData = new FormData();
    formData.append("file", file);
    configBucket.dirName = configBucket.dirName + "profilePhotos/";
    console.log(configBucket);
    S3FileUpload.uploadFile(file, configBucket)
      .then((data) => {
        console.log("bucket_data", data);
        this.setState({
          profile_photo: data.location,
          loadingProfile: false,
        });
      })
      .catch((err) => console.error(err));
    this.setState({
      profileFileName: e.target.files[0].name,
    });
  };
  handleIdUpload(e) {
    this.refs.idImageUploader.click();
  }
  idFileUploader = (e) => {
    e.persist();
    const file = e.target.files[0];
    let formData = new FormData();
    formData.append("file", file);
    configBucket.dirName = configBucket.dirName + "idPhoto";
    console.log(configBucket);
    S3FileUpload.uploadFile(file, configBucket)
      .then((data) => {
        console.log("bucket_data", data);
        this.setState({
          id_image: data.location,
          idFileName: e.target.files[0].name,
        });
        console.log("bucket_data", this.state.idFileName);
      })
      .catch((err) => console.error(err));
  };

  getOwnerDetails = () => {
    axios
      .get(`${config.apiUrl}/api/petownerprofile/list/?user=${user.user.id}`)
      .then((response) => {
        console.log("response in getownerprofile", response.data[0]);
        this.setState({
          owner: response.data,

          ownerID: response.data[0].id ? response.data[0].id : null,
          postal_code: response.data[0].postal_code,
          address: response.data[0].address,
          dob: response.data[0].dob,
          mobile: response.data[0].mobile,
          id_number: response.data[0].id_number,
          id_type: response.data[0].id_type,
          insurance_details: response.data[0].insurance_details,
          insurance_proof: response.data[0].insurance_proof_no,
          insurance_provider: response.data[0].insurance_provider,
          street_address: response.data[0].street_address,
          house_no: response.data[0].house_no,
          city: response.data[0].city,
          state: response.data[0].state,
          profile_photo: response.data[0].profile_photo,
          id_image: response.data[0].id_image,
          have_insurance: response.data[0].have_insurance,
          insurance_privider: response.data[0].insurance_provider,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getOwnerPets = () => {
    axios
      .get(`${config.apiUrl}/api/petprofile/list?user=${user.user.id}`)
      .then((response) => {
        this.setState({
          ownerPets: response.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleEditable = () => {
    this.setState((prevState) => {
      return {
        editable: !prevState.editable,
      };
    });
  };

  handleTextChange = (event) => {
    console.log(event.target.name);
    console.log(event.target.value);
    this.setState({ [event.target.name]: event.target.value });
  };

  handlePetTextChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleNuteredChange = (event) => {
    this.setState({
      nutered: event.target.value,
    });
  };

  handleStayHomeAloneChoiceChange = (event) => {
    this.setState({
      stay_home_alone_choice: event.target.value,
    });
  };

  handleToiletTrainedChoiceChange = (event) => {
    this.setState({
      toilet_trained_choice: event.target.value,
    });
  };

  handleFriendlyWithDogsChoiceChange = (event) => {
    this.setState({
      friendly_with_dogs_choice: event.target.value,
    });
  };

  handleFriendlyWithCatsChoiceChange = (event) => {
    this.setState({
      friendly_with_cats_choice: event.target.value,
    });
  };

  handleFriendlyWithStrangersChoiceChange = (event) => {
    this.setState({
      friendly_with_strangers_choice: event.target.value,
    });
  };

  handleFriendlyWithChildrensChoiceChange = (event) => {
    this.setState({
      friendly_with_children_choice: event.target.value,
    });
  };

  handleChewsThingsChoiceChange = (event) => {
    this.setState({
      chews_things_choice: event.target.value,
    });
  };

  handleAggressiveChoiceChange = (event) => {
    this.setState({
      is_aggressive_choice: event.target.value,
    });
  };

  handleIDTypeChange = (event) => {
    this.setState({
      id_type: event.target.value,
    });
  };
  handleRadioChange = (event) => {
    console.log("test", event.target.name);
    this.setState({
      insurance_provider: event.target.value,
    });
  };

  handleTabChange = (event, value) => {
    this.setState({
      tabValue: value,
    });
  };

  handlePetModal = () => {
    this.setState((prevState) => {
      return {
        petModal: !prevState.petModal,
      };
    });
    this.getOwnerPets();
  };
  handleCancel = () => {
    this.setState({
      editable: false,
    });
  };
  handleConfirmModal = () => {
    this.setState({
      confirmModalOpen: !this.state.confirmModalOpen,
      isConfirm: false,
    });
  };
  handlePetViewModal = (pet) => {
    this.setState((prevState) => {
      return {
        openPetView: !prevState.openPetView,
        petViewId: pet,
      };
    });
  };
  onCropChange = (crop) => {
    this.setState({ crop });
  };

  onCropComplete = (croppedArea, croppedAreaPixels) => {
    const self = this;
    console.log(croppedArea, croppedAreaPixels);
    this.croppedImage(this.state.imageDataUrl, croppedAreaPixels, 0);
  };

  handleChangeMobileCode = (value) => {
    this.setState({ mobile: value });
  };

  async croppedImage(imageDataUrl, croppedAreaPixels, rotation) {
    const croppedImage = await this.getCroppedImg(
      imageDataUrl,
      croppedAreaPixels,
      rotation
    );
    this.croppedImageData(croppedImage);
  }

  croppedImageData = (croppedImage) => {
    this.setState({
      croppedImage: croppedImage,
    });
  };

  async getCroppedImg(imageSrc, pixelCrop, rotation = 0) {
    const image = await this.createImage(imageSrc);
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    const maxSize = Math.max(image.width, image.height);
    const safeArea = 2 * ((maxSize / 2) * Math.sqrt(2));

    // set each dimensions to double largest dimension to allow for a safe area for the
    // image to rotate in without being clipped by canvas context
    canvas.width = safeArea;
    canvas.height = safeArea;

    // translate canvas context to a central location on image to allow rotating around the center.
    ctx.translate(safeArea / 2, safeArea / 2);
    ctx.rotate(rotation);
    ctx.translate(-safeArea / 2, -safeArea / 2);

    // draw rotated image and store data.
    ctx.drawImage(
      image,
      safeArea / 2 - image.width * 0.5,
      safeArea / 2 - image.height * 0.5
    );
    const data = ctx.getImageData(0, 0, safeArea, safeArea);

    // set canvas width to final desired crop size - this will clear existing context
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;

    // paste generated rotate image with correct offsets for x,y crop values.
    ctx.putImageData(
      data,
      Math.round(0 - safeArea / 2 + image.width * 0.5 - pixelCrop.x),
      Math.round(0 - safeArea / 2 + image.height * 0.5 - pixelCrop.y)
    );

    // As Base64 string
    return canvas.toDataURL("image/jpeg");

    // As a blob
    // return new Promise((resolve) => {
    //   canvas.toBlob((file) => {
    //     resolve(URL.createObjectURL(file));
    //   }, "image/jpeg");
    // });
  }

  createImage = (url) =>
    new Promise((resolve, reject) => {
      const image = new Image();
      image.addEventListener("load", () => resolve(image));
      image.addEventListener("error", (error) => reject(error));
      image.setAttribute("crossOrigin", "anonymous"); // needed to avoid cross-origin issues on CodeSandbox
      image.src = url;
    });

  onZoomChange = (zoom) => {
    this.setState({ zoom });
  };

  handleImageCropperModal = () => {
    this.setState((prevState) => {
      return {
        imageCropperModal: !prevState.imageCropperModal,
      };
    });
  };
  fileSelector = (event) => {
    this.setState({
      profileFileName: event.target.files[0].name,
    });
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        this.setState(
          {
            imageDataUrl: e.target.result,
          },
          () => {
            this.handleImageCropperModal();
          }
        );
      };
    }
  };

  handleChangeRadio1 = (event) => {
    this.setState({ radio1: event.target.value });
  };

  onImageCropDone = (e) => {
    e.persist();
    // const file = this.state.croppedImage + "name.png";
    // let formData = new FormData();
    // formData.append("file", file);
    // configBucket.dirName = configBucket.dirName + "profilePhotos";

    // S3FileUpload.uploadFile(formData, configBucket)
    //   .then((data) => {
    //     this.setState(
    //       {
    //         profile_photo: data.location,
    //       },
    //       () => {
    //         console.log(this.state.profile_photo);
    //       }
    //     );
    //   })
    //   .catch((err) => console.error(err));
    this.handleImageCropperModal();
  };
  // getSitterPets = () => {
  //   axios
  //     .get(`${config.apiUrl}/api/petprofile/?user=${user.user.id}`)
  //     .then((response) => {
  //       this.setState({
  //         sitterPets: response.data,
  //       });
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };

  handleSubmit = () => {
    console.log("insurance provider submit handler ****", this.state.other);
    const data = {
      // address: this.state.address,
      id_image: this.state.id_image,
      id_number: this.state.id_number,
      id_type: this.state.id_type,
      have_insurance: this.state.have_insurance,
      insurance_details: this.state.insurance_details,
      insurance_proof_no: this.state.insurance_proof,
      // insurance_provider: this.state.insurance_provider,
      mobile: this.state.mobile,
      email: this.state.email,
      postal_code: this.state.postal_code,
      profile_photo: this.state.croppedImage,
      dob: this.state.dob,
      user: user.user.id,
      street_address: this.state.street_address,
      house_no: this.state.house_no,
      city: this.state.city,
      state: this.state.state,
      country: this.state.country,
      have_insurance: this.state.have_insurance,
      insurance_provider: this.state.insurance_privider,
      other_insurance: this.state.other,
    };

    console.log(" submitting data **********", data);
    axios
      .put(`${config.apiUrl}/api/petownerprofile/${this.state.ownerID}`, data)
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          this.handleEditable();
          this.componentDidMount();
          // window.location.reload(true);
          this.setState({ isConfirm: true });
          this.getOwnerPets();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  addPets = () => {
    const data = {
      user: user.user.id,
      cust_id: this.state.cust_id,
      pet_id: `${this.state.cust_id}_1`,
      pet_choice: this.state.type_of_pet,
      type_of_pet: this.state.type_of_pet,
      name: this.state.name,
      breed: this.state.breed,
      // profile_photo: this.state.profile_photo,
      dob: this.state.dob,
      nutered: this.state.nutered,
      stay_home_alone_choice: this.state.stay_home_alone_choice,
      stay_home_alone_detail: this.state.stay_home_alone_detail,
      toilet_trained_choice: this.state.toilet_trained_choice,
      toilet_trained_detail: this.state.toilet_trained_detail,
      friendly_with_dogs_choice: this.state.friendly_with_dogs_choice,
      friendly_with_dogs_detail: this.state.friendly_with_dogs_detail,
      friendly_with_cats_choice: this.state.friendly_with_cats_choice,
      friendly_with_cats_detail: this.state.friendly_with_cats_detail,
      friendly_with_strangers_choice: this.state.friendly_with_strangers_choice,
      friendly_with_strangers_detail: this.state.friendly_with_strangers_detail,
      friendly_with_children_choice: this.state.friendly_with_children_choice,
      friendly_with_children_detail: this.state.friendly_with_children_detail,
      chews_things_choice: this.state.chews_things_choice,
      chews_things_detail: this.state.chews_things_detail,
      is_aggressive_choice: this.state.is_aggressive_choice,
      is_aggressive_detail: this.state.is_aggressive_detail,
      additional_info: this.state.additional_info,
    };

    console.log(data);

    axios
      .post(`${config.apiUrl}/api/petprofile/?user=${user.user.id}`, data)
      .then((response) => {
        console.log("Add Pets response", response);
        this.getOwnerPets();
        this.handlePetModal();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>

        <div className={classes.whiteBGWrapper}>
          {this.state.owner &&
            this.state.owner.map((list) => {
              return (
                <Grid container>
                  <Grid item xs={10} style={{ padding: 24 }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <div style={{ display: "flex" }}>
                        {!this.state.editable && (
                          <div
                            style={{
                              padding: list.profile_photo ? 0 : 32,
                              height: 180,
                              width: 180,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            className={classes.profileImg}
                          >
                            <img
                              src={
                                list.profile_photo
                                  ? list.profile_photo
                                  : profileFallback
                              }
                              alt="profile-fallback"
                              style={{
                                height: list.profile_photo ? 180 : 48,
                                width: list.profile_photo ? 180 : 48,
                                borderRadius: 8,
                              }}
                            />
                          </div>
                        )}
                        {/* {props.editable && (
                          <div
                            style={{
                              padding:
                                props.profile_photo === "" ||
                                props.profile_photo === null
                                  ? 32
                                  : 0,
                              height: 180,
                              width: 200,
                              cursor: "pointer",
                            }}
                            onClick={props.uploadProfileImage}
                          >
                            {props.profile_photo === "" ||
                            props.profile_photo === null ||
                            props.profileFileName ? (
                              <img
                                src={profileFallback}
                                alt="profile-fallback"
                                style={{
                                  height: 48,
                                  width: 48,
                                }}
                              />
                            ) : (
                              <img
                                src={props.sitter.profile_photo}
                                alt="profile-fallback"
                                style={{
                                  height: props.sitter.profile_photo ? 180 : 48,
                                  width: props.sitter.profile_photo ? 180 : 48,
                                  borderRadius: 8,
                                }}
                              />
                            )}

                            <Typography
                              color="primary"
                              style={{ fontSize: 20, fontWeight: 500 }}
                            >
                              {props.profileFileName
                                ? props.profileFileName
                                : props.profile_photo === "" ||
                                  props.profile_photo === null
                                ? "Upload"
                                : "Click to Upload"}
                            </Typography>
                            <input
                              type="file"
                              id="imageFile"
                              ref={(input) => {
                                imageUploader = input;
                              }}
                              name="profile_photo"
                              style={{ display: "none" }}
                              onChange={props.fileUploader}
                            />
                          </div>
                        )} */}
                        {this.state.editable && (
                          <div
                            className={classes.profileImg}
                            style={
                              this.profile_photo === ""
                                ? {
                                  height: 180,
                                  width: 180,
                                  cursor: "pointer",
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                  flexDirection: "column",
                                }
                                : this.profile_photo === null
                                  ? {
                                    height: 180,
                                    width: 180,
                                    cursor: "pointer",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    flexDirection: "column",
                                  }
                                  : !this.profile_photo
                                    ? {
                                      height: 180,
                                      width: 180,
                                      cursor: "pointer",
                                      display: "flex",
                                      alignItems: "center",
                                      justifyContent: "center",
                                      flexDirection: "column",
                                    }
                                    : this.profile_photo
                                      ? {
                                        height: 180,
                                        width: 180,
                                        cursor: "pointer",
                                      }
                                      : {
                                        height: 180,
                                        width: 180,
                                        cursor: "pointer",
                                      }
                            }
                            onClick={this.handleProfileUpload}
                          >
                            <div style={{ padding: 12, overflow: "hidden" }}>
                              {this.state.profile_photo === "" ||
                                this.state.profile_photo === null ||
                                this.state.loadingProfile === true ||
                                this.state.profileFileName ? (
                                  <div>
                                    <img
                                      src={profileFallback}
                                      alt="profile-fallback"
                                      style={{
                                        height: 48,
                                        width: 48,
                                      }}
                                    />
                                    <CircularProgress color="primary" centered />
                                  </div>
                                ) : (
                                  <div
                                    alt="profile-fallback"
                                    style={{
                                      height: this.state.profile_photo ? 180 : 48,
                                      width: this.state.profile_photo ? 180 : 48,
                                      borderRadius: 8,
                                      backgroundSize: "cover",
                                      backgroundRepeat: "no-repeat",
                                      backgroundImage: this.state.profile_photo
                                        ? "url(" + this.state.profile_photo + ")"
                                        : profileFallback,
                                    }}
                                  />
                                  // <img
                                  //   src={this.state.profile_photo}
                                  //   alt="profile-fallback"
                                  //   style={{
                                  //     height: this.state.profile_photo ? 180 : 48,
                                  //     width: this.state.profile_photo ? 180 : 48,
                                  //     borderRadius: 8,
                                  //   }}
                                  // />
                                )}
                              <Typography
                                color="primary"
                                style={{
                                  fontSize: 20,
                                  fontWeight: 500,
                                }}
                              >
                                {this.state.profileFileName
                                  ? this.state.profileFileName
                                  : this.state.profile_photo === "" ||
                                    this.state.profile_photo === null
                                    ? "Click to Upload"
                                    : "Click to Upload"}
                              </Typography>
                            </div>
                            <input
                              type="file"
                              id="imageFile"
                              ref="imageUploader"
                              name="profile_photo"
                              style={{ display: "none" }}
                              onChange={this.fileSelector}
                            />
                          </div>
                        )}
                        <div style={{ marginLeft: "1rem" }}>
                          {
                            // !editable &&
                            <Typography
                              color="secondary"
                              style={{
                                fontSize: "1.5rem",
                                fontFamily: "Fredoka One",
                              }}
                            >
                              {list.user.first_name} {list.user.last_name}
                            </Typography>
                          }

                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              marginTop: "8px",
                              color: "#5E5A5A",
                            }}
                          >
                            {!this.state.editable && (
                              <div>
                                <LocationOnIcon />
                                <Typography style={{ fontSize: "1.2rem" }}>
                                  {list.postal_code}
                                </Typography>
                              </div>
                            )}
                            {this.state.editable && (
                              <PlacesAutocomplete
                                value={this.state.postal_code}
                                onChange={this.handleGooglePostalCodeChange}
                                onSelect={this.handleGooglePostalCodeSelect}
                                searchOptions={searchOptions}
                                name="postal_code"
                                type="number"
                              >
                                {({
                                  getInputProps,
                                  suggestions,
                                  getSuggestionItemProps,
                                  loading,
                                }) => (
                                    <div>
                                      {/* <TextField
                                      name="postal_code"
                                      type="number"
                                      {...getInputProps({
                                        placeholder: "Search for Zip Codes ...",
                                        className: "location-search-input",
                                      })}
                                    /> */}

                                      <TextField
                                        label="Postal Code"
                                        name="postal_code"
                                        {...getInputProps({
                                          placeholder: "Search for Zip Codes ...",
                                          className: "location-search-input",
                                          type: "number"
                                        })}
                                      />

                                      <div className="autocomplete-dropdown-container">
                                        {loading && <div>Loading...</div>}
                                        {suggestions.map((suggestion) => {
                                          const className = suggestion.active
                                            ? "suggestion-item--active"
                                            : "suggestion-item";
                                          // inline style for demonstration purpose
                                          const style = suggestion.active
                                            ? {
                                              backgroundColor: "#fafafa",
                                              cursor: "pointer",
                                            }
                                            : {
                                              backgroundColor: "#ffffff",
                                              cursor: "pointer",
                                            };
                                          return (
                                            <div
                                              {...getSuggestionItemProps(
                                                suggestion,
                                                {
                                                  className,
                                                  style,
                                                }
                                              )}
                                            >
                                              <span>
                                                {suggestion.description}
                                              </span>
                                            </div>
                                          );
                                        })}
                                      </div>
                                    </div>
                                  )}
                              </PlacesAutocomplete>
                            )}
                          </div>
                        </div>
                      </div>
                      {!this.state.editable && (
                        <EditIcon
                          color="primary"
                          fontSize="large"
                          onClick={this.handleEditable}
                          style={{ cursor: "pointer" }}
                        />
                      )}
                      {this.state.editable && (
                        <div>
                          <SaveIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleConfirmModal}
                            style={{ cursor: "pointer" }}
                          />
                          <CancelIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleCancel}
                            style={{ cursor: "pointer" }}
                          />
                        </div>
                      )}
                    </div>
                    <Tabs
                      value={this.state.tabValue}
                      onChange={this.handleTabChange}
                      aria-label="simple tabs example"
                      style={{
                        marginTop: 32,
                        borderBottom: "0.7px solid rgba(245, 130, 32, 0.6)",
                      }}
                      variant="fullWidth"
                      classes={{ indicator: classes.indicatorColor }}
                    >
                      <Tab
                        label="Personal Info is this"
                        {...a11yProps(0)}
                        className={classes.profileTabs}
                      />
                      {/* <Tab
                        label="Reviews"
                        {...a11yProps(1)}
                        className={classes.profileTabs}
                      /> */}
                    </Tabs>
                    <Grid container spacing={4} style={{ display: "flex" }}>
                      <Grid item xs={12}>
                        <TabPanel value={this.state.tabValue} index={0}>
                          <div className={classes.leftGridScroll}>
                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Date of Birth
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.dob ? (
                                    moment(list.dob, "YYYY-MM-DD").format(
                                      "DD/MM/YYYY"
                                    )
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        Ex. 19/10/2023
                                      </span>
                                    )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                <TextField
                                  name="dob"
                                  type="date"
                                  value={this.state.dob}
                                  // defaultValue="2017-05-24"
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />
                              )}
                            </div>
                            {/* {!this.state.editable && (
                              <div style={{ margin: "8px 0" }}>
                                <Typography
                                  color="secondary"
                                >
                                  Date of Birth
                                </Typography>
                                <Typography className={classes.leftGridContent}>
                                  {list.dob ? list.dob : "DD/MM/YYYY"}
                                </Typography>
                              </div>
                            )}
                            {this.state.editable && (
                              <TextField
                                label="Date of Birth"
                                placeholder="DD/MM/YYYY"
                                name="dob"
                                value={this.state.dob}
                                onChange={this.handleTextChange}
                                className={classes.editField}
                              />
                            )} */}

                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Email
                              </Typography>
                              <Typography className={classes.leftGridContent}>
                                {list.user.email
                                  ? list.user.email
                                  : user.user.email}
                              </Typography>
                              {/* {!this.state.editable && (
                                
                              )} */}
                              {/* {this.state.editable && (
                                <TextField
                                  label="Email"
                                  name="email"
                                  value={this.state.email}
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />
                              )} */}
                            </div>
                            {/* <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Address
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.address}
                                </Typography>
                              )}
                              {this.state.editable && (
                                <TextField
                                  name="address"
                                  value={this.state.address}
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />
                              )}
                            </div> */}
                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.sitterProfileHeadings}
                                color="secondary"
                              >
                                Country
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.country ? (
                                    list.country
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        Ex. France
                                      </span>
                                    )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                // <TextField
                                //   name="country"
                                //   value={this.state.country}
                                //   onChange={this.handleTextChange}
                                //   className={classes.editField}
                                // />
                                <PlacesAutocomplete
                                  value={this.state.country}
                                  onChange={this.handleGoogleCountryChange}
                                  onSelect={this.handleGoogleCountrySelect}
                                  searchOptions={searchOptions}
                                >
                                  {({
                                    getInputProps,
                                    suggestions,
                                    getSuggestionItemProps,
                                    loading,
                                  }) => (
                                      <div>
                                        <TextField
                                          name="country"
                                          {...getInputProps({
                                            placeholder: "Search Country ...",
                                            className: "location-search-input",
                                          })}
                                        />

                                        <div className="autocomplete-dropdown-container">
                                          {loading && <div>Loading...</div>}
                                          {suggestions.map((suggestion) => {
                                            const className = suggestion.active
                                              ? "suggestion-item--active"
                                              : "suggestion-item";
                                            // inline style for demonstration purpose
                                            const style = suggestion.active
                                              ? {
                                                backgroundColor: "#fafafa",
                                                cursor: "pointer",
                                              }
                                              : {
                                                backgroundColor: "#ffffff",
                                                cursor: "pointer",
                                              };
                                            return (
                                              <div
                                                {...getSuggestionItemProps(
                                                  suggestion,
                                                  {
                                                    className,
                                                    style,
                                                  }
                                                )}
                                              >
                                                <span>
                                                  {suggestion.description}
                                                </span>
                                              </div>
                                            );
                                          })}
                                        </div>
                                      </div>
                                    )}
                                </PlacesAutocomplete>
                              )}
                            </div>


                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Street Address
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {/* {list.street_address ? (
                                    list.street_address
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        Ex. Rue du Diamant
                                      </span>
                                    )} */}

                                  {list.country && (
                                    <>{list.country} {","}</>
                                  )}
                                  {list.address && (
                                    <>{list.address}{","}</>
                                  )}

                                  {list.street_address && (

                                    <>{list.street_address} {","}</>
                                  )}
                                  {list.house_no && (
                                    <>{list.house_no} {","}</>
                                  )}

                                  {list.city && (

                                    <>{list.city} {","}</>
                                  )}
                                  {list.postal_code && (
                                    <>{list.postal_code}{","}</>
                                  )
                                  }
                                  {list.mobile && (
                                    <>{list.mobile}{","}</>

                                  )}
                                  {console.log('list country*', list)}
                                  {console.log('list address*', list.address)}
                                  {console.log('list  street_address*', list.street_address)}
                                  {console.log('list house no*', list.house_no)}
                                  {console.log('list  city*', list.city)}
                                  {console.log('list postal_code*', list.postal_code)}
                                  {console.log('list mobile*', list.mobile)}


                                </Typography>
                              )}
                              <div style={{ margin: "8px 0" }}>
                                <Typography
                                  className={classes.leftGridHeaders}
                                  color="secondary"
                                >
                                  House No
                              </Typography>
                                {!this.state.editable && (
                                  <Typography className={classes.leftGridContent}>
                                    {list.house_no ? (
                                      list.house_no
                                    ) : (
                                        <span className={classes.placeholderText}>
                                          Ex. #01
                                        </span>
                                      )}
                                  </Typography>
                                )}
                                {this.state.editable && (
                                  <TextField
                                    name="house_no"
                                    value={this.state.house_no}
                                    onChange={this.handleTextChange}
                                    className={classes.editField}
                                  />
                                )}
                              </div>
                              <div style={{ margin: "8px 0" }}>
                                <Typography
                                  className={classes.leftGridHeaders}
                                  color="secondary"
                                >
                                  City
                              </Typography>
                                {!this.state.editable && (
                                  <Typography className={classes.leftGridContent}>
                                    {list.city ? (
                                      list.city
                                    ) : (
                                        <span className={classes.placeholderText}>
                                          Ex. New York
                                        </span>
                                      )}
                                  </Typography>
                                )}
                                {this.state.editable && (
                                  //   <TextField
                                  // name="city"
                                  // value={this.state.city}
                                  // onChange={this.handleTextChange}
                                  //   className={classes.editField}
                                  // />
                                  <PlacesAutocomplete
                                    value={this.state.city}
                                    onChange={this.handleGoogleCityChange}
                                    onSelect={this.handleGoogleCitySelect}
                                    searchOptions={{
                                      types: ["(cities)"],
                                    }}
                                    name="city"
                                  >
                                    {({
                                      getInputProps,
                                      suggestions,
                                      getSuggestionItemProps,
                                      loading,
                                    }) => (
                                        <div>
                                          <TextField
                                            name="city"
                                            {...getInputProps({
                                              placeholder: "Search for City ...",
                                              className: "location-search-input",
                                            })}
                                          />

                                          <div className="autocomplete-dropdown-container">
                                            {loading && <div>Loading...</div>}
                                            {suggestions.map((suggestion) => {
                                              const className = suggestion.active
                                                ? "suggestion-item--active"
                                                : "suggestion-item";
                                              // inline style for demonstration purpose
                                              const style = suggestion.active
                                                ? {
                                                  backgroundColor: "#fafafa",
                                                  cursor: "pointer",
                                                }
                                                : {
                                                  backgroundColor: "#ffffff",
                                                  cursor: "pointer",
                                                };
                                              return (
                                                <div
                                                  {...getSuggestionItemProps(
                                                    suggestion,
                                                    {
                                                      className,
                                                      style,
                                                    }
                                                  )}
                                                >
                                                  <span>
                                                    {suggestion.description}
                                                  </span>
                                                </div>
                                              );
                                            })}
                                          </div>
                                        </div>
                                      )}
                                  </PlacesAutocomplete>
                                )}
                              </div>
                              {this.state.editable && (
                                <TextField
                                  name="street_address"
                                  value={this.state.street_address}
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />

                                // <PlacesAutocomplete
                                //   value={this.state.street_address}
                                //   onChange={this.handleGoogleStreetChange}
                                //   onSelect={this.handleGoogleStreetSelect}
                                //   name="street_address"
                                // >
                                //   {({
                                //     getInputProps,
                                //     suggestions,
                                //     getSuggestionItemProps,
                                //     loading,
                                //   }) => (
                                //     <div>
                                //       <TextField
                                //         name="street_address"
                                //         {...getInputProps({
                                //           placeholder: "Search for Street ...",
                                //           className: "location-search-input",
                                //         })}
                                //       />

                                //       <div className="autocomplete-dropdown-container">
                                //         {loading && <div>Loading...</div>}
                                //         {suggestions.map((suggestion) => {
                                //           const className = suggestion.active
                                //             ? "suggestion-item--active"
                                //             : "suggestion-item";
                                //           // inline style for demonstration purpose
                                //           const style = suggestion.active
                                //             ? {
                                //                 backgroundColor: "#fafafa",
                                //                 cursor: "pointer",
                                //               }
                                //             : {
                                //                 backgroundColor: "#ffffff",
                                //                 cursor: "pointer",
                                //               };
                                //           return (
                                //             <div
                                //               {...getSuggestionItemProps(
                                //                 suggestion,
                                //                 {
                                //                   className,
                                //                   style,
                                //                 }
                                //               )}
                                //             >
                                //               <span>
                                //                 {suggestion.description}
                                //               </span>
                                //             </div>
                                //           );
                                //         })}
                                //       </div>
                                //     </div>
                                //   )}
                                // </PlacesAutocomplete>
                              )}
                            </div>

                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                County (If Applicable)
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.state ? (
                                    list.state
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        Ex. Alabama
                                      </span>
                                    )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                <TextField
                                  name="state"
                                  value={this.state.state}
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />
                              )}
                            </div>
                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Postal Code
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.postal_code ? (
                                    list.postal_code
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        Ex. 100890
                                      </span>
                                    )}
                                </Typography>
                              )}






                              {this.state.editable && (
                                // <TextField
                                //   name="postal_code"
                                //   value={this.state.postal_code}
                                //   onChange={this.handleTextChange}
                                //   className={classes.editField}
                                // />
                                <PlacesAutocomplete
                                  value={this.state.postal_code}
                                  onChange={this.handleGooglePostalCodeChange}
                                  onSelect={this.handleGooglePostalCodeSelect}
                                  searchOptions={searchOptions}
                                  name="postal_code"
                                  type="number"
                                >
                                  {({
                                    getInputProps,
                                    suggestions,
                                    getSuggestionItemProps,
                                    loading,
                                  }) => (
                                      <div>
                                        <TextField
                                          name="postal_code"
                                          type="number"
                                          {...getInputProps({
                                            placeholder:
                                              "Search for Zip Codes ...",
                                            className: "location-search-input",
                                            type: "number"
                                          })}
                                        />

                                        <div className="autocomplete-dropdown-container">
                                          {loading && <div>Loading...</div>}
                                          {suggestions.map((suggestion) => {
                                            const className = suggestion.active
                                              ? "suggestion-item--active"
                                              : "suggestion-item";
                                            // inline style for demonstration purpose
                                            const style = suggestion.active
                                              ? {
                                                backgroundColor: "#fafafa",
                                                cursor: "pointer",
                                              }
                                              : {
                                                backgroundColor: "#ffffff",
                                                cursor: "pointer",
                                              };
                                            return (
                                              <div
                                                {...getSuggestionItemProps(
                                                  suggestion,
                                                  {
                                                    className,
                                                    style,
                                                  }
                                                )}
                                              >
                                                <span>
                                                  {suggestion.description}
                                                </span>
                                              </div>
                                            );
                                          })}
                                        </div>
                                      </div>
                                    )}
                                </PlacesAutocomplete>
                              )}
                            </div>
                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Mobile Number
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.mobile ? (
                                    list.mobile
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        +xx xxxxxxxxxx
                                      </span>
                                    )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                // <TextField
                                //   name="mobile"
                                //   value={this.state.mobile}
                                //   onChange={this.handleTextChange}
                                //   className={classes.editField}
                                // />
                                <MuiPhoneNumber
                                  preferredCountries={[
                                    "lu",
                                    "fr",
                                    "be",
                                    "de",
                                    "at",
                                  ]}
                                  value={this.state.mobile}
                                  onChange={this.handleChangeMobileCode}
                                  style={{ margin: "10px 0px" }}
                                  name="mobile"
                                  inputClass={classes.mobileNumberInput}
                                />
                              )}
                            </div>
                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                ID Type
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.id_type ? (
                                    list.id_type
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        Ex. EU ID
                                      </span>
                                    )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                <RadioGroup
                                  aria-label="id_type"
                                  name="id_type"
                                  value={this.state.id_type}
                                  onChange={this.handleIDTypeChange}
                                  row
                                >
                                  <FormControlLabel
                                    value="eu id"
                                    control={<Radio color="primary" />}
                                    label="EU ID"
                                  />
                                  <FormControlLabel
                                    value="passport"
                                    control={<Radio color="primary" />}
                                    label="Passport"
                                  />
                                </RadioGroup>
                              )}
                            </div>
                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                ID Number
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.id_number ? (
                                    list.id_number
                                  ) : (
                                      <span className={classes.placeholderText}>
                                        Ex. ID009098767
                                      </span>
                                    )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                <TextField
                                  name="id_number"
                                  value={this.state.id_number}
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />
                              )}
                            </div>
                            <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                ID Image
                              </Typography>
                              {!this.state.editable && (
                                <div
                                  style={{
                                    padding: this.state.id_image ? 0 : 32,
                                    height: 180,
                                    width: 180,
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center",
                                  }}
                                >
                                  <img
                                    src={
                                      this.state.id_image
                                        ? this.state.id_image
                                        : profileFallback
                                    }
                                    alt="profile-fallback"
                                    style={{
                                      height: this.state.id_image ? 180 : 48,
                                      width: this.state.id_image ? 180 : 48,
                                      borderRadius: 8,
                                    }}
                                  />
                                </div>
                              )}
                              {this.state.editable && (
                                <div
                                  style={{
                                    padding:
                                      this.state.id_image === "" ||
                                        this.state.id_image === null
                                        ? 32
                                        : 0,
                                    height: 180,
                                    width: 200,
                                    cursor: "pointer",
                                  }}
                                  onClick={this.handleIdUpload}
                                >
                                  {this.state.id_image === "" ||
                                    this.state.id_image === null ||
                                    this.state.idFileName ? (
                                      <img
                                        src={profileFallback}
                                        alt="profile-fallback"
                                        style={{
                                          height: 48,
                                          width: 48,
                                        }}
                                      />
                                    ) : (
                                      <img
                                        src={this.state.id_image}
                                        alt="profile-fallback"
                                        style={{
                                          height: this.state.id_image ? 180 : 48,
                                          width: this.state.id_image ? 180 : 48,
                                          borderRadius: 8,
                                        }}
                                      />
                                    )}

                                  <Typography
                                    color="primary"
                                    style={{ fontSize: 20, fontWeight: 500 }}
                                  >
                                    {this.state.idFileName
                                      ? this.state.idFileName
                                      : this.state.id_image === "" ||
                                        this.state.id_image === null
                                        ? "Upload"
                                        : "Click to Upload"}
                                  </Typography>
                                  <input
                                    type="file"
                                    id="idFile"
                                    ref="idImageUploader"
                                    name="id_image"
                                    style={{ display: "none" }}
                                    onChange={this.idFileUploader}
                                  />
                                </div>
                              )}
                            </div>
                            {/* {this.state.editable && (
                              <RadioGroup
                                aria-label="have_insurance"
                                name="have_insurance"
                                value={this.state.have_insurance}
                                onChange={this.handleRadioChange}
                                row
                              >
                                <FormControlLabel
                                  value="yes"
                                  control={<Radio color="primary" />}
                                  label="Yes"
                                />
                                <FormControlLabel
                                  value="no"
                                  control={<Radio color="primary" />}
                                  label="No"
                                />
                              </RadioGroup>
                            )} */}
                            {/* <div
                              style={{ margin: "8px 0", paddingTop: "20px" }}
                            >
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Insurance Details
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.insurance_details ? (
                                    list.insurance_details
                                  ) : (
                                    <span className={classes.placeholderText}>
                                      Ex. Term life insurance
                                    </span>
                                  )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                <TextField
                                  name="insurance_details"
                                  value={this.state.insurance_details}
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />
                              )}
                            </div> */}
                            {/* <div style={{ margin: "8px 0" }}>
                              <Typography
                                className={classes.leftGridHeaders}
                                color="secondary"
                              >
                                Insurance Proof No
                              </Typography>
                              {!this.state.editable && (
                                <Typography className={classes.leftGridContent}>
                                  {list.insurance_proof_no ? (
                                    list.insurance_proof_no
                                  ) : (
                                    <span className={classes.placeholderText}>
                                      ID009098767
                                    </span>
                                  )}
                                </Typography>
                              )}
                              {this.state.editable && (
                                <TextField
                                  name="insurance_proof"
                                  value={this.state.insurance_proof}
                                  onChange={this.handleTextChange}
                                  className={classes.editField}
                                />
                              )}
                            </div> */}
                            {/* {console.log('the state in insurance provider',this.state)}
                            {console.log('the insurance provider is',this.state.radio1)}
                           */}

                            {!this.state.editable && (
                              <div style={{ margin: "8px 0" }}>
                                <Typography
                                  className={classes.leftGridHeaders}
                                  color="secondary"
                                  style={{
                                    maxWidth: 380,
                                  }}
                                >
                                  Do you have private insurance (civil
                                  liability/ home insurance) that covers your
                                  dog?
                                </Typography>
                                <Typography
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 17,
                                  }}
                                >
                                  {this.state.have_insurance}
                                </Typography>

                                <Typography
                                  className={classes.sitterProfileHeadings}
                                  color="secondary"
                                  style={{
                                    marginTop: 24,
                                  }}
                                >
                                  Your Insurance Provider is:
                                </Typography>

                                <Typography
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 17,
                                  }}
                                >
                                  {this.state.insurance_provider === 'other' &&
                                    this.state.insurance_provider
                                  }
                                </Typography>

                                {this.state.insurance_privider === "other" &&
                                  !this.state.editable &&
                                  this.state.have_insurance === "yes" &&
                                  (<Typography
                                    className={classes.sitterProfileHeadings}
                                    color="secondary"
                                    style={{
                                      marginTop: 24,
                                    }}
                                  >
                                    Your  Other Insurance Provider is:
                                  </Typography>)}

                                <Typography
                                  style={{
                                    color: "#5E5A5A",
                                    fontSize: 17,
                                  }}
                                >  {this.state.insurance_privider === "other" &&
                                  !this.state.editable &&
                                  this.state.have_insurance === "yes" && (
                                    this.state.other)}
                                </Typography>
                              </div>
                            )}

                            {this.state.editable && (
                              <div
                                style={{ margin: "8px 0", marginTop: "60px" }}
                              >
                                <Typography
                                  className={classes.leftGridHeaders}
                                  color="secondary"
                                >
                                  Do you have private insurance (civil
                                  liability/ home insurance) that covers your
                                  dog?
                                </Typography>
                                <FormControl component="fieldset">
                                  <RadioGroup
                                    name="gender1"
                                    value={this.state.have_insurance}
                                    onChange={this.handleHaveInsurance}
                                    style={{ fontSize: "17px" }}
                                    row
                                  >
                                    <FormControlLabel
                                      value="yes"
                                      control={<Radio />}
                                      label="Yes"
                                    />
                                    <FormControlLabel
                                      value="no"
                                      control={<Radio />}
                                      label="NO"
                                    />
                                  </RadioGroup>
                                </FormControl>
                              </div>
                            )}
                            <div>
                              {this.state.have_insurance === "yes" &&
                                this.state.editable ? (
                                  <div>
                                    <Typography
                                      className={classes.leftGridHeaders}
                                      color="secondary"
                                    >
                                      {" "}
                                    Select your insurance provider
                                  </Typography>
                                    {/* <label 
                                   className={classes.sitterProfileHeadings}
                                   color="secondary"
                                   style={{
                                     marginTop: 24,
                                   }}
                                  >
                                    Select your insurance provider
                                  </label> */}
                                    <Select
                                      labelId="demo-simple-select-label"
                                      id="simple-select"
                                      value={this.state.insurance_privider}
                                      placeholder="Select your insurance provider"
                                      // defaultValue={props.insurance_privider}
                                      onChange={this.handleInsuranceMenuChange}
                                      style={{ margin: "10px", width: 180 }}
                                    >
                                      <MenuItem value={"axa"}>Axa</MenuItem>
                                      <MenuItem value={"foyer"}>Foyer</MenuItem>
                                      <MenuItem value={"baloise"}>
                                        Baloise
                                    </MenuItem>
                                      <MenuItem value={"lalux"}>Lalux</MenuItem>
                                      <MenuItem value={"other"}>Other</MenuItem>
                                    </Select>
                                  </div>
                                ) : null}

                              {this.state.insurance_privider === "other" &&
                                this.state.editable &&
                                this.state.have_insurance === "yes" && (
                                  <div>
                                    <Typography
                                      className={classes.leftGridHeaders}
                                      color="secondary"
                                    >
                                      {" "}
                                      Other Insurance Provider
                                    </Typography>
                                    {/* <label 
                                      className={classes.sitterProfileHeadings}
                                      color="secondary"
                                      style={{
                                        marginTop: 24,
                                      }}
                                     >
                                      Other Insurance Provider
                                   </label> */}
                                    <TextField
                                      name="other"
                                      value={this.state.other}
                                      onChange={this.handleInsuranceOtherChange}
                                      className={classes.editField}
                                    />
                                  </div>
                                )}
                              {console.log(
                                "********* insurance ",
                                this.state.insurance_privider === "other"
                                  ? "yes"
                                  : "no"
                              )}

                              {/* {this.state.editable && (
                                <RadioGroup
                                  aria-label="insurance_provider"
                                  name="insurance_provider"
                                  value={this.state.insurance_provider}
                                  onChange={this.handleRadioChange}
                                  row
                                >
                                  <FormControlLabel
                                    value="axa"
                                    control={<Radio color="primary" />}
                                    label="Axa"
                                  />
                                  <FormControlLabel
                                    value="floyer"
                                    control={<Radio color="primary" />}
                                    label="Floyer"
                                  />
                                  <FormControlLabel
                                    value="baloise"
                                    control={<Radio color="primary" />}
                                    label="Baloise"
                                  />
                                  <FormControlLabel
                                    value="lalux"
                                    control={<Radio color="primary" />}
                                    label="Lalux"
                                  />
                                  <FormControlLabel
                                    value="other"
                                    control={<Radio color="primary" />}
                                    label="other"
                                  />
                                </RadioGroup>
                              )}  */}
                              {console.log(
                                "this state insurance provider",
                                this.state.insurance_provider
                              )}
                            </div>
                          </div>
                        </TabPanel>
                        <TabPanel value={this.state.tabValue} index={1}>
                          <div className={classes.leftGridScroll}>
                            <Typography
                              style={{
                                fontSize: 18,
                                fontWeight: 500,
                                color: "#979797",
                              }}
                            >
                              10 Comments
                            </Typography>
                            <div className={classes.reviewCard}>
                              <Grid style={{ display: "flex" }}>
                                <Grid
                                  item
                                  xs={2}
                                  className={classes.reviewCardLeft}
                                >
                                  <img
                                    src="https://cdn.quasar.dev/img/avatar.png"
                                    style={{
                                      borderRadius: "50%",
                                      width: "50px",
                                    }}
                                    alt=""
                                  />
                                </Grid>
                                <Grid item xs={10}>
                                  <Typography
                                    color="secondary"
                                    className={classes.reviewerName}
                                  >
                                    Angel D'Souza{" "}
                                    <span
                                      style={{
                                        color: "#979797",
                                        fontWeight: "lighter",
                                        paddingLeft: 16,
                                        fontSize: 18,
                                      }}
                                    >
                                      Mar 20th
                                    </span>
                                  </Typography>
                                  <Rating
                                    name="read-only"
                                    value={4}
                                    readOnly
                                    classes={{ root: classes.orangeRating }}
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 18,
                                      fontWeight: 500,
                                      color: "#5E5A5A",
                                      marginTop: "1rem",
                                    }}
                                  >
                                    Great Experience
                                  </Typography>
                                  <Typography
                                    style={{
                                      color: "#979797",
                                      fontSize: 18,
                                      margin: "1rem 0",
                                    }}
                                  >
                                    Lorem occaecat nostrud reprehenderit nisi
                                    fugiat veniam mollit incididunt commodo.
                                    Commodo sit tempor excepteur sint. Amet
                                    proident irure cupidatat eu Lorem ullamco
                                    deserunt ea anim ipsum laboris sit ea
                                    fugiat. Aliquip minim minim cillum
                                    reprehenderit deserunt et culpa aliqua
                                    incididunt aute tempor
                                  </Typography>
                                </Grid>
                              </Grid>
                            </div>
                            <div className={classes.reviewCard}>
                              <Grid style={{ display: "flex" }}>
                                <Grid
                                  item
                                  xs={2}
                                  className={classes.reviewCardLeft}
                                >
                                  <img
                                    src="https://cdn.quasar.dev/img/avatar.png"
                                    style={{
                                      borderRadius: "50%",
                                      width: "50px",
                                    }}
                                    alt=""
                                  />
                                </Grid>
                                <Grid item xs={10}>
                                  <Typography
                                    color="secondary"
                                    className={classes.reviewerName}
                                  >
                                    Angel D'Souza{" "}
                                    <span
                                      style={{
                                        color: "#979797",
                                        fontWeight: "lighter",
                                        paddingLeft: 16,
                                        fontSize: 18,
                                      }}
                                    >
                                      Mar 20th
                                    </span>
                                  </Typography>
                                  <Rating
                                    name="read-only"
                                    value={4}
                                    readOnly
                                    classes={{ root: classes.orangeRating }}
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 18,
                                      fontWeight: 500,
                                      color: "#5E5A5A",
                                      marginTop: "1rem",
                                    }}
                                  >
                                    Great Experience
                                  </Typography>
                                  <Typography
                                    style={{
                                      color: "#979797",
                                      fontSize: 18,
                                      margin: "1rem 0",
                                    }}
                                  >
                                    Lorem occaecat nostrud reprehenderit nisi
                                    fugiat veniam mollit incididunt commodo.
                                    Commodo sit tempor excepteur sint. Amet
                                    proident irure cupidatat eu Lorem ullamco
                                    deserunt ea anim ipsum laboris sit ea
                                    fugiat. Aliquip minim minim cillum
                                    reprehenderit deserunt et culpa aliqua
                                    incididunt aute tempor
                                  </Typography>
                                </Grid>
                              </Grid>
                            </div>
                            <div className={classes.reviewCard}>
                              <Grid style={{ display: "flex" }}>
                                <Grid
                                  item
                                  xs={2}
                                  className={classes.reviewCardLeft}
                                >
                                  <img
                                    src="https://cdn.quasar.dev/img/avatar.png"
                                    style={{
                                      borderRadius: "50%",
                                      width: "50px",
                                    }}
                                    alt=""
                                  />
                                </Grid>
                                <Grid item xs={10}>
                                  <Typography
                                    color="secondary"
                                    className={classes.reviewerName}
                                  >
                                    Angel D'Souza{" "}
                                    <span
                                      style={{
                                        color: "#979797",
                                        fontWeight: "lighter",
                                        paddingLeft: 16,
                                        fontSize: 18,
                                      }}
                                    >
                                      Mar 20th
                                    </span>
                                  </Typography>
                                  <Rating
                                    name="read-only"
                                    value={4}
                                    readOnly
                                    classes={{ root: classes.orangeRating }}
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 18,
                                      fontWeight: 500,
                                      color: "#5E5A5A",
                                      marginTop: "1rem",
                                    }}
                                  >
                                    Great Experience
                                  </Typography>
                                  <Typography
                                    style={{
                                      color: "#979797",
                                      fontSize: 18,
                                      margin: "1rem 0",
                                    }}
                                  >
                                    Lorem occaecat nostrud reprehenderit nisi
                                    fugiat veniam mollit incididunt commodo.
                                    Commodo sit tempor excepteur sint. Amet
                                    proident irure cupidatat eu Lorem ullamco
                                    deserunt ea anim ipsum laboris sit ea
                                    fugiat. Aliquip minim minim cillum
                                    reprehenderit deserunt et culpa aliqua
                                    incididunt aute tempor
                                  </Typography>
                                </Grid>
                              </Grid>
                            </div>
                          </div>
                        </TabPanel>
                      </Grid>
                      <Grid item xs={4}></Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={2}>
                    <div className={classes.myPets}>
                      <Typography
                        color="secondary"
                        style={{ fontFamily: "Fredoka One", fontSize: 20 }}
                      >
                        My Pets
                      </Typography>
                      {this.state.ownerPets &&
                        this.state.ownerPets.map((pet, index) => {
                          return (
                            <div
                              style={{
                                width: "100%",
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                                marginBottom: 18,
                              }}
                            >
                              <Avatar
                                alt="Pet"
                                src={
                                  pet.profile_photo
                                    ? pet.profile_photo
                                    : dogImage
                                }
                                style={{ width: 72, height: 72 }}
                                onClick={() => this.handlePetViewModal(pet.id)}
                                component={RouterLink}
                              // to={
                              //   pet.type_of_pet === "Dog"
                              //     ? `/dog-dashboard/${pet.id}`
                              //     : pet.type_of_pet === "Cat"
                              //     ? `/cat-dashboard/${pet.id}`
                              //     : `/otherpet-dashboard/${pet.id}`
                              // }
                              />
                              <p
                                style={{
                                  color: "#F58220",
                                  fontSize: 18,
                                  fontFamily: "Kanit",
                                  textAlign: "center",
                                }}
                              >
                                {pet.name}
                              </p>
                            </div>
                          );
                        })}

                      <div>
                        <Button
                          color="primary"
                          className={classes.orangeBtnOutlinedCircle}
                          onClick={this.handlePetModal}
                        >
                          <AddIcon />
                        </Button>
                        <p
                          style={{
                            fontFamily: "Kanit",
                            fontSize: 20,
                            textAlign: "center",
                            color: "#F58220",
                            margin: 0,
                            marginBottom: 24,
                          }}
                        >
                          Create New
                        </p>
                      </div>
                    </div>
                  </Grid>
                  {/* Add Pet Modal */}
                  <Dialog
                    onClose={this.handlePetModal}
                    aria-labelledby="customized-dialog-title"
                    open={this.state.petModal}
                    // className={classes.petViewModal}
                    fullWidth={true}
                    maxWidth={"lg"}
                  >
                    <DialogTitle
                      id="customized-dialog-title"
                      onClose={this.handlePetModal}
                    >
                      Add Pet{" "}
                      {this.state.type_of_pet ? this.state.type_of_pet : ""}
                      <IconButton
                        aria-label="close"
                        className={classes.closeButton}
                        onClick={this.handlePetModal}
                      >
                        <CloseIcon />
                      </IconButton>
                    </DialogTitle>
                    <DialogContent dividers>
                      {this.state.AddPetStep !== true ? (
                        <div className={classes.center}>
                          <Grid container>
                            <Grid item xs={4}>
                              <div
                                style={{
                                  border:
                                    this.state.border1 === false
                                      ? "1px solid #c4c4c4"
                                      : "1px solid #E8770E",
                                  borderRadius: 4,
                                  padding: 18,
                                  margin: 8,
                                  minHeight: 102,
                                  cursor: "pointer",
                                }}
                                onClick={() => {
                                  this.setState({
                                    type_of_pet: "dog",
                                    border1: !this.state.border1,
                                    border2: false,
                                    border3: false,
                                  });
                                }}
                              >
                                <img src={Dog} />
                              </div>
                            </Grid>
                            <Grid item xs={4}>
                              <div
                                style={{
                                  border:
                                    this.state.border2 === false
                                      ? "1px solid #c4c4c4"
                                      : "1px solid #E8770E",
                                  borderRadius: 4,
                                  padding: 18,
                                  margin: 8,
                                  minHeight: 102,
                                  cursor: "pointer",
                                }}
                                onClick={() => {
                                  this.setState({
                                    type_of_pet: "cat",
                                    border2: !this.state.border2,
                                    border1: false,
                                    border3: false,
                                  });
                                }}
                              >
                                <img src={Cat} />
                              </div>
                            </Grid>
                            <Grid item xs={4}>
                              <div
                                style={{
                                  border:
                                    this.state.border3 === false
                                      ? "1px solid #c4c4c4"
                                      : "1px solid #E8770E",
                                  borderRadius: 4,
                                  padding: 18,
                                  margin: 8,
                                  minHeight: 102,
                                  cursor: "pointer",
                                }}
                                onClick={() => {
                                  this.setState({
                                    type_of_pet: "other animal",
                                    border3: !this.state.border3,
                                    border1: false,
                                    border2: false,
                                  });
                                }}
                              >
                                <img src={Other} />
                              </div>
                            </Grid>
                            <Grid item xs={12} style={{ marginTop: 24 }}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  flexDirection: "column",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                {/* <Button
                            variant="contained"
                            color="primary"
                            className={classes.orangeBtn}
                            onClick={() => {
                              if (this.state.type_of_pet === "") {
                                alert(
                                  "Please Select type of pet you want to add."
                                );
                              } else {
                                this.setState({
                                  AddPetStep: true,
                                });
                              }
                            }}
                          >
                            Add Your Pet
                          </Button> */}
                              </div>
                            </Grid>
                          </Grid>
                        </div>
                      ) : (
                          <div>
                            <Grid container>
                              <Grid item xs={12}>
                                <div>
                                  {/* <div
                          style={{
                            display: "flex",
                          }}
                        >
                          <CssTextField
                            id="cur-name"
                            label="Pet Name"
                            type="text"
                            name="name"
                            style={{
                              width: "100%",
                              margin: "0 8px",
                            }}
                            onChange={this.handlePetTextChange}
                          />
                          <CssTextField
                            id="cur-name"
                            label="Pet breed"
                            type="text"
                            name="breed"
                            style={{
                              width: "100%",
                              margin: "0 8px",
                            }}
                            onChange={this.handlePetTextChange}
                          />
                          <CssTextField
                            id="cur-name"
                            label="Pet Date of Birth"
                            type="text"
                            name="pet_dob"
                            style={{
                              width: "100%",
                              margin: "0 8px",
                            }}
                            onChange={this.handlePetTextChange}
                          />
                        </div>
                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Nutered
                          </Typography>
                          <RadioGroup
                            aria-label="nutered"
                            name="nutered"
                            value={this.state.nutered}
                            onChange={this.handleNuteredChange}
                            row
                          >
                            <FormControlLabel
                              value={true}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={false}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                        </div>
                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Stay at home alone
                          </Typography>
                          <RadioGroup
                            aria-label="stay_home_alone_choice"
                            name="stay_home_alone_choice"
                            value={this.state.stay_home_alone_choice}
                            onChange={this.handleStayHomeAloneChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="stay_home_alone_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Toilet trained
                          </Typography>
                          <RadioGroup
                            aria-label="toilet_trained_choice"
                            name="toilet_trained_choice"
                            value={this.state.toilet_trained_choice}
                            onChange={this.handleToiletTrainedChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="other"
                              control={<Radio color="primary" />}
                              label="Other"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="toilet_trained_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with dogs
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_dogs_choice"
                            name="friendly_with_dogs_choice"
                            value={this.state.friendly_with_dogs_choice}
                            onChange={this.handleFriendlyWithDogsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_dogs_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with cats
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_cats_choice"
                            name="friendly_with_cats_choice"
                            value={this.state.friendly_with_cats_choice}
                            onChange={this.handleFriendlyWithCatsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_cats_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with Strangers
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_strangers_choice"
                            name="friendly_with_strangers_choice"
                            value={this.state.friendly_with_strangers_choice}
                            onChange={
                              this.handleFriendlyWithStrangersChoiceChange
                            }
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_strangers_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with Children
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_children_choice"
                            name="friendly_with_children_choice"
                            value={this.state.friendly_with_children_choice}
                            onChange={
                              this.handleFriendlyWithChildrensChoiceChange
                            }
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_children_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Chews Things
                          </Typography>
                          <RadioGroup
                            aria-label="chews_things_choice"
                            name="chews_things_choice"
                            value={this.state.chews_things_choice}
                            onChange={this.handleChewsThingsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="1"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="2"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="3"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="chews_things_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Aggressive
                          </Typography>
                          <RadioGroup
                            aria-label="is_aggressive_choice"
                            name="is_aggressive_choice"
                            value={this.state.is_aggressive_choice}
                            onChange={this.handleAggressiveChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="is_aggressive_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Additional Info
                          </Typography>

                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Additional Info"
                              type="text"
                              name="additional_info"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div> */}
                                </div>
                                <AddPetComponent
                                  handleModal={this.handlePetModal}
                                  getSitterPets={this.getOwnerPets}
                                  type_of_pet={this.state.type_of_pet}
                                  getAllData={this.componentDidMount}
                                />
                              </Grid>
                              <Grid item xs={12} style={{ marginTop: 24 }}>
                                <div
                                  style={{
                                    width: "100%",
                                    display: "flex",
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    alignItems: "center",
                                  }}
                                ></div>
                              </Grid>
                            </Grid>
                          </div>
                        )}
                    </DialogContent>
                    <DialogActions>
                      {this.state.AddPetStep !== true ? (
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.orangeBtn}
                          onClick={() => {
                            if (
                              this.state.type_of_pet === "" ||
                              this.state.type_of_pet === null
                            ) {
                              alert(
                                "Please Select type of pet you want to add."
                              );
                            } else {
                              this.setState({
                                AddPetStep: true,
                              });
                            }
                          }}
                        >
                          Next
                        </Button>
                      ) : (
                          <Button
                            color="primary"
                            onClick={() => {
                              this.setState({
                                AddPetStep: false,
                              });
                            }}
                          >
                            Back
                          </Button>
                        )}
                      <Button onClick={this.handlePetModal} color="primary">
                        Close
                      </Button>
                    </DialogActions>
                  </Dialog>
                </Grid>
              );
            })}
        </div>
        <Dialog
          open={this.state.confirmModalOpen}
          onClose={this.handleConfirmModal}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {!this.state.isConfirm ? "Are you sure?" : "Sucessfull"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {!this.state.isConfirm ? (
                <div>Are you sure you want to make changes?</div>
              ) : (
                  <div>Profile updated Successfully</div>
                )}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleConfirmModal} color="primary">
              {!this.state.isConfirm ? "No" : "Close"}
            </Button>
            {!this.state.isConfirm ? (
              <Button onClick={this.handleSubmit} color="primary" autoFocus>
                Yes
              </Button>
            ) : (
                ""
              )}
          </DialogActions>
        </Dialog>
        {/* View Pet Modal */}
        <Dialog
          onClose={this.handlePetViewModal}
          aria-labelledby="customized-dialog-title"
          open={this.state.openPetView}
          fullWidth={true}
          maxWidth={"lg"}
        // className={classes.petViewModal}
        >
          <DialogTitle
            id="customized-dialog-title"
            onClose={this.handlePetViewModal}
          >
            View Pet
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={this.handlePetViewModal}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <DogProfileDashboard
              petId={this.state.petViewId}
              getSitterPets={this.getOwnerPets}
              editable={true}
            />
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={this.handlePetViewModal} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>

        {/* Image Cropper Modal */}

        <Dialog
          onClose={this.handleImageCropperModal}
          aria-labelledby="customized-dialog-title"
          open={this.state.imageCropperModal}
          fullWidth={false}
          maxWidth={"lg"}
          className={classes.petViewModal}
        >
          <DialogTitle
            id="customized-dialog-title"
            onClose={this.handleImageCropperModal}
          >
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={this.handleImageCropperModal}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <div className={classes.cropperPaper}>
              <div className={classes.cropContainer}>
                <Cropper
                  image={this.state.imageDataUrl}
                  crop={this.state.crop}
                  zoom={this.state.zoom}
                  aspect={this.state.aspect}
                  onCropChange={this.onCropChange}
                  onCropComplete={this.onCropComplete}
                  onZoomChange={this.onZoomChange}
                />
              </div>
            </div>
          </DialogContent>
          <DialogActions>
            <div className={classes.sliderControls}>
              <Slider
                value={this.state.zoom}
                min={1}
                max={3}
                step={0.1}
                aria-labelledby="Zoom"
                onChange={(e, zoom) => this.onZoomChange(zoom)}
                classes={{ container: "slider" }}
              />
            </div>
            <Button autoFocus onClick={this.onImageCropDone} color="primary">
              Done
            </Button>
            <Button
              autoFocus
              onClick={this.handleImageCropperModal}
              color="primary"
            >
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(DashboardOwnerProfile);
