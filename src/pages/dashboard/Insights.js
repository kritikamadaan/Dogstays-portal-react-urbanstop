import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { Typography, Grid, Link } from "@material-ui/core";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import interview from "../../assets/sitterDashboard/interview.png";
import Bell from "../../assets/sitterDashboard/bell.svg";
import Booking from "../../assets/sitterDashboard/booking.png";
import Calendar from "../../assets/sitterDashboard/calendar.png";
import Insights from "../../assets/sitterDashboard/insights.png";
import axios from "axios";

import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const data = [
  {
    name: "Jan 20",
    amount: 4000,
  },
  {
    name: "Feb 20",
    amount: 3000,
  },
  {
    name: "Mar 20",
    amount: 2000,
  },
  {
    name: "Apr 20",
    amount: 2780,
  },
  {
    name: "May 20",
    amount: 1890,
  },
  {
    name: "Jun 20",
    amount: 2390,
  },
  {
    name: "Jul 20",
    amount: 3490,
  },
  {
    name: "Aug 20",
    amount: 3490,
  },
  {
    name: "Sep 20",
    amount: 3490,
  },
  {
    name: "Oct 20",
    amount: 3490,
  },
  {
    name: "Nov 20",
    amount: 3490,
  },
  {
    name: "Dec 20",
    amount: 3490,
  },
];

const styles = (theme) => ({
  root: {
    padding: theme.spacing(4),
    maxWidth: 1200,
  },
  whiteBGWrapper: {
    backgroundColor: "#FFFFFF",
    borderRadius: 18,
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.25)",
    padding: theme.spacing(4),
  },
  breadcrumbs: {
    fontSize: 14,
    fontFamily: "Kanit",
    color: "#E8770E",
  },
  homeHeading: {
    fontSize: 30,
    fontFamily: "Fredoka One",
    color: "#3EB3CD",
  },
  homeLeftCard: {
    border: "0.5px solid #E8770E",
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.15)",
    borderRadius: 6,
    padding: 27,
    maxWidth: "100%",
  },
  sliderBody: {
    maxWidth: "100%",
  },
});

// const user = JSON.parse(localStorage.getItem("user")).user;

class DashboardInsights extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      insightData: [],
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  componentDidMount() {
    this.getInsightData();
  }

  getInsightData = () => {
    axios
      //   .get(`${config.apiUrl}/api/message/?${data.type}=${data._id}`)
      .get(`${config.apiUrl}/api/insights/`)
      .then((response) => {
        this.setState({
          insightData: response.data[0],
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  next = () => {
    console.log("next");
    this.slider.slickNext();
  };

  previous = () => {
    console.log("previous");
    this.slider.slickPrev();
  };

  render() {
    const { classes } = this.props;

    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 5000,
      arrows: false,
      className: classes.slickMain,
      dotsClass: classes.button__bar,
    };

    return (
      <div className={classes.root}>
        <div className={classes.whiteBGWrapper}>
          <Grid container>
            <Grid item xs={12} style={{ marginBottom: 32 }}>
              <Typography color="primary" className={classes.breadcrumbs}>
                Home >> Insights
              </Typography>
              <Typography color="primary" className={classes.homeHeading}>
                Insights
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <div className={classes.homeLeftCard}>
                <div className={classes.sliderContainer}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      position: "relative",
                      top: 6,
                    }}
                  >
                    <ArrowBackIosIcon
                      style={{
                        position: "absolute",
                        left: 360,
                        fontSize: 18,
                        cursor: "pointer",
                        zIndex: 1000,
                      }}
                      onClick={this.previous}
                    />
                    <ArrowForwardIosIcon
                      style={{
                        position: "absolute",
                        right: 360,
                        fontSize: 18,
                        cursor: "pointer",
                        zIndex: 1000,
                      }}
                      onClick={this.next}
                    />
                  </div>
                  <Slider ref={(c) => (this.slider = c)} {...settings}>
                    <div className={classes.sliderBody}>
                      <Grid container justify="center">
                        <span
                          style={{
                            fontFamily: "Kanit",
                            fontSize: 22,
                            color: "#5E5A5A",
                          }}
                        >
                          Other Details
                        </span>
                      </Grid>
                      <Grid container>
                        <Grid item xs={6}>
                          <div
                            style={{
                              border: "1px solid #F58220",
                              padding: "4px 12px",
                              display: "flex",
                              alignItems: "center",
                              margin: 18,
                              borderRadius: 8,
                            }}
                          >
                            <div>
                              <h3
                                style={{
                                  fontSize: 42,
                                  color: "#F58220",
                                  fontFamily: "Kanit",
                                  margin: 0,
                                  marginRight: 24,
                                }}
                              >
                                {this.state.insightData.meeting_request}
                              </h3>
                            </div>
                            <p
                              style={{
                                fontSize: 24,
                                color: "#3EB3CD",
                                fontFamily: "Fredoka One",
                                margin: 0,
                              }}
                            >
                              Meeting requests
                            </p>
                          </div>
                        </Grid>

                        <Grid item xs={6}>
                          <div
                            style={{
                              border: "1px solid #F58220",
                              padding: "4px 12px",
                              display: "flex",
                              alignItems: "center",
                              margin: 18,
                              borderRadius: 8,
                            }}
                          >
                            <div>
                              <h3
                                style={{
                                  fontSize: 42,
                                  color: "#F58220",
                                  fontFamily: "Kanit",
                                  margin: 0,
                                  marginRight: 24,
                                }}
                              >
                                {this.state.insightData.completed_meeting}
                              </h3>
                            </div>
                            <p
                              style={{
                                fontSize: 24,
                                color: "#3EB3CD",
                                fontFamily: "Fredoka One",
                                margin: 0,
                              }}
                            >
                              Completed Meeting
                            </p>
                          </div>
                        </Grid>

                        <Grid item xs={6}>
                          <div
                            style={{
                              border: "1px solid #F58220",
                              padding: "4px 12px",
                              display: "flex",
                              alignItems: "center",
                              margin: 18,
                              borderRadius: 8,
                            }}
                          >
                            <div>
                              <h3
                                style={{
                                  fontSize: 42,
                                  color: "#F58220",
                                  fontFamily: "Kanit",
                                  margin: 0,
                                  marginRight: 24,
                                }}
                              >
                                {this.state.insightData.booking_request}
                              </h3>
                            </div>
                            <p
                              style={{
                                fontSize: 24,
                                color: "#3EB3CD",
                                fontFamily: "Fredoka One",
                                margin: 0,
                              }}
                            >
                              Booking requests
                            </p>
                          </div>
                        </Grid>

                        <Grid item xs={6}>
                          <div
                            style={{
                              border: "1px solid #F58220",
                              padding: "4px 12px",
                              display: "flex",
                              alignItems: "center",
                              margin: 18,
                              borderRadius: 8,
                            }}
                          >
                            <div>
                              <h3
                                style={{
                                  fontSize: 42,
                                  color: "#F58220",
                                  fontFamily: "Kanit",
                                  margin: 0,
                                  marginRight: 24,
                                }}
                              >
                                {this.state.insightData.completed_booking}
                              </h3>
                            </div>
                            <p
                              style={{
                                fontSize: 24,
                                color: "#3EB3CD",
                                fontFamily: "Fredoka One",
                                margin: 0,
                              }}
                            >
                              Completed Booking
                            </p>
                          </div>
                        </Grid>
                      </Grid>
                    </div>
                    <div className={classes.sliderBody}>
                      <Grid container justify="center">
                        <span
                          style={{
                            fontFamily: "Kanit",
                            fontSize: 22,
                            color: "#5E5A5A",
                          }}
                        >
                          Search appearance
                        </span>
                      </Grid>
                      <Grid container>
                        <Grid item xs={12}>
                          <ResponsiveContainer width="100%" height={300}>
                            <BarChart
                              data={data}
                              margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                              }}
                            >
                              <XAxis dataKey="name" />
                              <YAxis />
                              <Tooltip />
                              <Legend />
                              <Bar dataKey="amount" fill="#F58220" />
                            </BarChart>
                          </ResponsiveContainer>
                        </Grid>
                      </Grid>
                    </div>
                  </Slider>
                </div>
              </div>
            </Grid>
            <Grid item xs={4} style={{ marginTop: 24 }}>
              <div className={classes.homeLeftCard} style={{ marginRight: 12 }}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    borderRadius: 8,
                  }}
                >
                  <div>
                    <h3
                      style={{
                        fontSize: 42,
                        color: "#F58220",
                        fontFamily: "Kanit",
                        margin: 0,
                        marginRight: 24,
                      }}
                    >
                      {this.state.insightData.pending_payments}
                    </h3>
                  </div>
                  <div>
                    <p
                      style={{
                        fontSize: 24,
                        color: "#3EB3CD",
                        fontFamily: "Fredoka One",
                        margin: 0,
                      }}
                    >
                      Pending Payments
                    </p>
                    <span>For upcoming bookings</span>
                  </div>
                </div>
              </div>
            </Grid>
            <Grid item xs={8} style={{ marginTop: 24 }}>
              <div className={classes.homeLeftCard} style={{ marginLeft: 12 }}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                    borderRadius: 8,
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <div>
                      <h3
                        style={{
                          fontSize: 42,
                          color: "#F58220",
                          fontFamily: "Kanit",
                          margin: 0,
                          marginRight: 24,
                        }}
                      >
                        5
                      </h3>
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: 24,
                          color: "#3EB3CD",
                          fontFamily: "Fredoka One",
                          margin: 0,
                        }}
                      >
                        Total amount earned
                      </p>
                      <span>Per Year</span>
                    </div>
                  </div>
                  <div>
                    <h3
                      style={{
                        fontSize: 42,
                        color: "#F58220",
                        fontFamily: "Kanit",
                        margin: 0,
                        marginRight: 24,
                      }}
                    >
                      {this.state.insightData.total_earn}
                    </h3>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(DashboardInsights);
