import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Drawer,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import HomeIcon from "@material-ui/icons/Home";
import PersonIcon from "@material-ui/icons/Person";
import ChatIcon from "@material-ui/icons/Chat";
import HistoryIcon from "@material-ui/icons/History";
import DateRangeIcon from "@material-ui/icons/DateRange";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import DashboardHome from "./DashboardHome";
import DashboardBookings from "./DashboardBooking";
import DashboardInsights from "./Insights";
import DashboardProfile from "./DashboardProfile";
import DashboardOwnerProfile from "./DashboardOwnerProfile";
import CalendarView from "./Calendar";
import BookingDetails from "./BookingDetails";
import MeetingDetails from "./MeetingDetails";
import DashboardFavourite from "./DashboardFavourite";
import Message from "./Message";
import { Link as RouterLink } from "react-router-dom";

const drawerWidth = 250;
const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 90,
    flexGrow: 1,
    backgroundColor: theme.palette.primary.main,
    display: "flex",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    position: "initial",
    // marginTop: 92,
    // height: "73%",
    backgroundColor: theme.palette.primary.main,
    paddingTop: theme.spacing(5),
    color: "white",
    // color: "#ffffffbd",
    marginBottom: 115,
  },
  drawerHeaders: {
    "& span": {
      fontSize: 20,
      fontWeight: 500,
    },
  },
}));
const user =
  localStorage.getItem("user") &&
  JSON.parse(localStorage.getItem("user")).user

const routes = [
  {
    name: "Home",
    path: "/dashboard",
    icon: <HomeIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <DashboardHome />,
  },
  {
    name: "Insights",
    path: "/dashboard/insights",
    icon: null,
    exact: true,
    main: () => <DashboardInsights />,
  },
  {
    name: "Meeting Details",
    path: "/dashboard/bookings/details/:id",
    icon: null,
    exact: true,
    main: () => <BookingDetails />,
  },
  {
    name: "Booking Details",
    path: "/dashboard/meetings/details/:id",
    icon: null,
    exact: true,
    main: () => <MeetingDetails />,
  },
  {
    name: "Profile",
    path: "/dashboard/profile",
    icon: <PersonIcon style={{ color: "white" }} />,
    main: () => <DashboardProfile />,
  },
  {
    name: "Meetings & Bookings",
    icon: <HistoryIcon style={{ color: "white" }} />,
    path: "/dashboard/bookings-meetings",
    main: () => <DashboardBookings />,
  },
  {
    name: "Calendar",
    path: "/dashboard/calendar",
    icon: <DateRangeIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <CalendarView />,
  },
  {
    name: "Reviews for me",
    path: "/dashboard/reviews",
    icon: <ChatIcon style={{ color: "white" }} />,
    main: () => <div />,
  },
  {
    name: "Messages",
    path: "/dashboard/messages",
    icon: <ChatIcon style={{ color: "white" }} />,
    main: () => <Message />,
  },
  user && user.user !== undefined && user.user.is_sitter &&
  {

    name: "My Favourite Sitters",
    path: "/dashboard/favourite-sitters",
    icon: <PersonIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <DashboardFavourite />,
  },
];

const OwnerRoutes = [
  {
    name: "Home",
    path: "/dashboard",
    icon: <HomeIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <DashboardHome />,
  },
  {

    name: "Profile",
    path: "/dashboard/profile",
    icon: <PersonIcon style={{ color: "white" }} />,
    main: () => <DashboardOwnerProfile />,
  },
  {
    name: "My Favourite Sitters",
    path: "/dashboard/favourite-sitters",
    icon: <PersonIcon style={{ color: "white" }} />,
    main: () => <DashboardFavourite />,
  },
  {
    name: "Meetings & Bookings",
    path: "/dashboard/bookings-meetings",
    icon: <HistoryIcon style={{ color: "white" }} />,
    main: () => <DashboardBookings />,
  },
  {
    name: "Meeting Details",
    path: "/dashboard/bookings/details/:id",
    icon: null,
    exact: true,
    main: () => <BookingDetails />,
  },
  {
    name: "Booking Details",
    path: "/dashboard/meetings/details/:id",
    icon: null,
    exact: true,
    main: () => <MeetingDetails />,
  },
  {
    name: "Messages",
    path: "/dashboard/messages",
    icon: <ChatIcon style={{ color: "white" }} />,
    main: () => <Message />,
  },
  {
    name: "Reviews by me",
    path: "/dashboard/reviews",
    icon: <ChatIcon style={{ color: "white" }} />,
    main: () => <div />,
  },
];
const isActivePage = () => {
  let currentUrl = window.location.href;
  var activePage = console.log("Current", activePage);
};
const getLastUrl = (url) => {
  url = url.split("/");
  return url[url.length - 1];
};
const DashboardWrap = (props) => {
  // const [activePage, setActivePage] = useState(null);
  // isActivePage();
  const classes = useStyles();
  console.log("all routes  in dashboard wrap", props);

  const user =
    localStorage.getItem("user") &&
    JSON.parse(localStorage.getItem("user")).user;
  const isSitter = user && user.is_sitter;
  console.log("I am in dashboard wrap");
  return (
    <Router>
      <div className={classes.root}>
        <CssBaseline />
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
          anchor="left"
        >
          {console.log("is sitter******", isSitter)}
          {console.log("owner routes*****", OwnerRoutes)}
          {console.log("all routes******", routes)}
          {isSitter
            ? routes.map((route) => {
              return route.name !== "Insights" &&
                // route.name !== "Calendar View" &&
                route.name !== "Meeting Details" &&
                route.name !== "Booking Details" ? (
                  <ListItem
                    button
                    key={route.name}
                    style={{ margin: "1.5rem 0" }}
                    component={RouterLink}
                    to={route.path}
                  >
                    <ListItemIcon>{route.icon}</ListItemIcon>
                    <ListItemText
                      primary={route.name}
                      className={classes.drawerHeaders}
                    />
                    <ListItemIcon>{route.activeIcon}</ListItemIcon>
                  </ListItem>
                ) : null;
            })
            : OwnerRoutes.map((route) => {
              return route.name !== "Calendar View" &&
                route.name !== "Meeting Details" &&
                route.name !== "Booking Details" ? (
                  <ListItem
                    button
                    key={route.name}
                    style={{ margin: "1.5rem 0" }}
                    component={RouterLink}
                    to={route.path}
                  >
                    <ListItemIcon>{route.icon}</ListItemIcon>
                    <ListItemText
                      primary={route.name}
                      className={classes.drawerHeaders}
                    />
                  </ListItem>
                ) : null;
            })}
        </Drawer>
        <div style={{ width: "100%" }}>{props.children}</div>
      </div>
    </Router>
  );
};

export default DashboardWrap;
