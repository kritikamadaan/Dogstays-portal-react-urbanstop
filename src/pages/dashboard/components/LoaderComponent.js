import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = (theme) => ({
  loader: {
    position: "absolute",
    backgroundColor: "#ffffff6b",
    width: "100%",
    height: "100%",
    textAlign: "center",
    zIndex: 1,
  },
});
LoaderComponent.propTypes = {};

function LoaderComponent(props) {
    const { classes } = props;
  return (
    <div className={classes.loader} style={props.display?{display:"block"}:{display:"none"}} >
      <CircularProgress  />
    </div>
  );
}

export default withStyles(styles, { withTheme: true })(LoaderComponent);
