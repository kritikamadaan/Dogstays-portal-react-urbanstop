import React, { useState, useEffect } from "react";
import {
  Button,
  TextField,
  Typography,
  IconButton,
  Avatar,
} from "@material-ui/core";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import CloseIcon from "@material-ui/icons/Close";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import axios from "axios";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};
let chatSocket;
const user = JSON.parse(localStorage.getItem("user"));

const styles = (theme) => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const ContactComponent = (props) => {
  const [message, setmessage] = useState("");
  const [success, setSuccess] = useState(false);
  const { classes } = props;

  const createRoom = () => {
    axios.post(`${config.apiUrl}/api/chat/create_room/`).then((response) => {
      connectToSocket(response.data.data.chat_room_uri);
    });
  };

  const connectToSocket = (id) => {
    chatSocket = new WebSocket(`ws://3.215.2.1:8000/ws/chat/${id}`);
    chatSocket.onopen = () => {
      console.log("OPEN");
      chatSocket.send(
        JSON.stringify({
          command: "join",
          user_id: user.user.id,
          other_user: props.userID,
        })
      );
      sendMessage();
    };

    chatSocket.onmessage = (val) => {
      let messJson = JSON.parse(val.data);
      console.log(messJson);
      return false;
    };

    chatSocket.onclose = (m) => {
      console.log("DEAD ", m);
    };

    chatSocket.onerror = (val) => {
      console.log("ERROR", val);
    };
  };

  const sendMessage = () => {
    if (message === "" || message === null) {
      alert("Please enter a message to submit");
    } else {
      chatSocket.send(
        JSON.stringify({
          command: "send",
          user_id: user.user.id,
          other_user: props.userID,
          message: message,
        })
      );
      setSuccess(true);
    }
  };

  return (
    <Dialog
      open={props.modalOpen}
      onClose={(e) => {
        e.preventDefault();
        props.handleModal();
        setmessage("");
        setSuccess(false);
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      style={{ minHeight: 200 }}
    >
      <DialogTitle id="alert-dialog-title">
        {success ? (
          <div>
            <Typography
              color="primary"
              variant="h4"
              style={{ fontSize: 20, textAlign: "center", fontWeight: "bold" }}
            >
              Message Sent
            </Typography>
            <Typography style={{ textAlign: "center" }}>
              Your message has been sent succesfully !
            </Typography>
          </div>
        ) : (
          <div>
            <Typography
              color="secondary"
              variant="h4"
              style={{ fontFamily: "Fredoka One", fontSize: 20 }}
            >
              Create New Message
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={props.handleModal}
            >
              <CloseIcon />
            </IconButton>
            <Typography
              style={{ display: "flex", alignItems: "center", padding: 10 }}
            >
              To:&nbsp;&nbsp;{" "}
              <Avatar
                alt="Sitter"
                src={
                  props.profile_photo
                    ? props.profile_photo
                    : "https://cdn.quasar.dev/img/avatar.png"
                }
                style={{ width: 40, height: 40 }}
              />{" "}
              &nbsp;
              {props.name}
            </Typography>
          </div>
        )}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {success ? (
            <div style={{ textAlign: "center" }}>
              <CheckCircleOutlineIcon color="primary" fontSize="large" />
            </div>
          ) : (
            <TextField
              name="message"
              value={message}
              onChange={(event) => {
                setmessage(event.target.value);
              }}
              multiline={true}
              style={{ width: "90%" }}
            />
          )}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        {success ? (
          <div />
        ) : (
          <Button onClick={createRoom} color="primary">
            Send Message
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default withStyles(styles, { withTheme: true })(ContactComponent);
