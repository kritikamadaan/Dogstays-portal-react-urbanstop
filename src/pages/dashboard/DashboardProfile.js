import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import {
  Grid,
  Typography,
  Tabs,
  Tab,
  Box,
  Button,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  IconButton,
  Avatar,
  ButtonGroup,
  FormGroup,
  Checkbox,
  FormControl,
  Divider,
  Menu,
  MenuItem,
  Select,
} from "@material-ui/core";

import CircularProgress from "@material-ui/core/CircularProgress";
import CloseIcon from "@material-ui/icons/Close";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import axios from "axios";
import MuiPhoneNumber from "react-phone-input-2";
import profileFallback from "../../assets/sitterDashboard/profileFallback.png";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import galleryImg1 from "../../assets/home/gallery_1.jpg";
import galleryImg2 from "../../assets/home/gallery_2.jpg";
import galleryImg3 from "../../assets/home/gallery_3.jpg";
import dogImage from "../../assets/sitterSearch/Vector.png";
import Rating from "@material-ui/lab/Rating";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import DogProfileDashboard from "./DogProfileDashboard";
import CatProfileDashboard from "./CatProfileDashboard";
import { Link as RouterLink } from "react-router-dom";
import Dog from "../../assets/sitterSearch/Vector.png";
import Cat from "../../assets/sitterSearch/group.png";
import Other from "../../assets/sitterSearch/group_1.png";
import moment from "moment";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import AddPetComponent from "./components/AddPetComponent";
import S3FileUpload from "react-s3";
import Slider from "@material-ui/core/Slider";
import Cropper from "react-easy-crop";
import Loader from "./components/LoaderComponent";
import { DropzoneDialog } from "material-ui-dropzone";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import DateRangeIcon from "@material-ui/icons/DateRange";
import { id } from "date-fns/locale";
// import MUIPlacesAutocomplete from "mui-places-autocomplete";

const user = JSON.parse(localStorage.getItem("user"));

var anchorId = "";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const configBucket =
  user && user.user !== undefined
    ? {
      bucketName: "dogstayss3",
      dirName:
        "user/" +
        (user !== null || user !== undefined ? user.user.id + "/" : "test/"),
      region: "us-east-1",
      accessKeyId: "AKIA5YN7PDU5YXKJ3BW2",
      secretAccessKey: "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",
    }
    : null;

const styles = (theme) => ({
  root: {
    padding: theme.spacing(3),
    backgroundColor: "white",
  },
  whiteBGWrapper: {
    backgroundColor: "#FFFFFF",
    borderRadius: 18,
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.25)",
  },
  myPets: {
    backgroundColor: "#FFFEFE",
    borderRadius: 10,
    border: "1px solid #F58220",
    height: "100%",
    textAlign: "center",
    paddingTop: theme.spacing(4),
  },
  profileImg: {
    border: "0.5px solid #F58220",
    borderRadius: 8,
    textAlign: "center",
    maxWidth: 200,
  },
  id_Img: {
    padding: 32,
    border: "0.5px solid #F58220",
    borderRadius: 8,
    textAlign: "center",
    width: 200,
  },
  profileTabs: {
    fontSize: 18,
    textTransform: "none",
    color: theme.palette.primary.main,
  },
  indicatorColor: {
    backgroundColor: "#F58220",
  },
  leftGridScroll: {
    height: 384,
    overflow: "auto",
  },
  servicesHeader: {
    fontSize: 22,
    fontWeight: 500,
    paddingRight: "3rem",
    color: theme.palette.primary.main,
    textAlign: "left",
    paddingLeft: "1rem",
  },
  leftGridHeaders: {
    fontSize: 22,
    fontWeight: 500,
    paddingRight: "3rem",
  },
  leftGridContent: {
    fontSize: 17,
    color: "#5E5A5A",
  },
  rightGrid: {
    border: "1px solid rgba(232, 119, 14, 0.7)",
    borderRadius: 8,
    width: 350,
    margin: "0 auto",
    padding: theme.spacing(2),
    marginTop: "3rem",
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    padding: theme.spacing(1, 4),
  },
  orangeBtnOutlined: {
    padding: theme.spacing(1.5),
    fontSize: 20,
  },
  orangeBtnOutlinedCircle: {
    marginTop: 24,
    marginBottom: 12,
    padding: theme.spacing(1.5),
    fontSize: 24,
    borderRadius: "50%",
    height: 62,
    width: 42,
    border: "1px solid #F58220",
  },
  experienceImg: {
    width: 200,
    height: 200,
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 8,
    objectFit: "cover",
  },
  multiplePhotos: {
    width: 200,
    height: 120,
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 8,
    objectFit: "cover",
  },
  reviewCard: {
    borderBottom: "2px solid rgba(232, 119, 14, 0.7)",
    marginTop: theme.spacing(4),
  },
  reviewerName: {
    fontSize: 22,
    fontWeight: 600,
  },
  orangeRating: {
    color: theme.palette.primary.main,
    marginTop: theme.spacing(0.5),
  },
  reviewCardLeft: {
    textAlign: "center",
    paddingTop: theme.spacing(1),
  },
  editField: {
    width: 250,
  },
  maxSittingCapacityBtnGroup: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  petViewModal: {
    // backgroundColor: "white",
    // maxWidth: "80vw",
    minHeight: 500,
  },
  dropzoneStyles: {
    padding: 10,
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  cropperPaper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  cropContainer: {
    minHeight: 500,
  },
  sliderControls: {
    position: "absolute",
    bottom: 0,
    left: "50%",
    width: "50%",
    transform: "translateX(-50%)",
    height: 80,
    display: "flex",
    alignItems: "center",
  },

  paper_pet: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  center: {
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    width: "60%",
    margin: "auto",
  },
  placeholderText: {
    color: "#979797",
  },
});

let imageUploader = null;
let idImageUploader = null;

let openSpaceArray = [];
let languagesSpokeArray = [];
let SpaceOutsideArray = [];
let selectedServiceArray = [];
// let selectedServiceRate
let DogAgeArray = [];
let DogSizeArray = [];
let BookingOverlapArray = [];
let multiplePetImageUrl = [];
let multipleHomeImageUrl = [];

const CssTextField = withStyles({
  root: {
    color: "#E8770E",
    "& label": {
      color: "rgba(94, 90, 90, 0.7)",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "1px solid #E8770E",
    },
    "& label.Mui-focused": {
      color: "#3EB3CD",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#3EB3CD",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "2px solid #3EB3CD",
    },
  },
})(TextField);

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};




const MainProfile = (props) => {
  const classes = props.classes;
  const loadingFiles = props.loadingFiles;
  const other_privider = props.other_privider;

  const [radio1, setRadio1] = useState(0);
  const [open, setOpen] = React.useState(false);
  const [isDeleteConfirm, setIsDelete] = useState(false);
  const [servicesLoading, setServicesLoading] = useState(true);

  const handleClickOpen = () => {
    setOpen(true);
    setIsDelete(false);
  };

  const handleClose = () => {
    setOpen(false);
    setIsDelete(true);
  };

  return (
    <div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div style={{ display: "flex" }}>
          <div className={classes.profileImg}>
            {!props.editable && (
              <div
                style={{
                  padding: 0,
                  height: 180,
                  width: 180,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {/* <div
                  alt="profile-fallback"
                  style={{
                    height: props.sitter.profile_photo ? 180 : 48,
                    width: props.sitter.profile_photo ? 180 : 48,
                    borderRadius: 8,
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    backgroundImage: props.sitter.profile_photo
                      ? "url(" + props.sitter.profile_photo + ")"
                      : profileFallback,
                  }}
                /> */}
                {props.profile_photo === "" ||
                  props.profile_photo === null ||
                  props.profileFileName ? (
                    <img
                      src={profileFallback}
                      alt="profile-fallback"
                      style={{
                        height: 180,
                        width: 180,
                      }}
                    />
                  ) : (
                    <img
                      src={
                        props.sitter.profile_photo
                          ? props.sitter.profile_photo
                          : profileFallback
                      }
                      alt="profile-fallback"
                      style={{
                        height: props.sitter.profile_photo ? 180 : 48,
                        width: props.sitter.profile_photo ? 180 : 48,
                        borderRadius: 8,
                      }}
                    />
                  )}
              </div>
            )}
            {props.editable && (
              <div
                style={
                  props.sitter.profile_photo === ""
                    ? {
                      height: 180,
                      width: 180,
                      cursor: "pointer",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      flexDirection: "column",
                    }
                    : props.sitter.profile_photo === null
                      ? {
                        height: 180,
                        width: 180,
                        cursor: "pointer",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                      }
                      : !props.sitter.profile_photo
                        ? {
                          height: 180,
                          width: 180,
                          cursor: "pointer",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "column",
                        }
                        : props.sitter.profile_photo
                          ? {
                            height: 180,
                            width: 180,
                            cursor: "pointer",
                          }
                          : {
                            height: 180,
                            width: 180,
                            cursor: "pointer",
                          }
                }
                onClick={props.uploadProfileImage}
              >
                {props.sitter.profile_photo === "" ||
                  props.sitter.profile_photo === null ||
                  props.loadingFiles === true ||
                  props.profileFileName ? (
                    <div>
                      <img
                        src={profileFallback}
                        alt="profile-fallback"
                        style={{
                          height: 48,
                          width: 48,
                        }}
                      />
                      <CircularProgress color="primary" centered />
                    </div>
                  )

                  : (
                    // <img
                    //   src={profileFallback}
                    //   alt="profile-fallback"
                    //   style={{
                    //     height: 48,
                    //     width: 48,
                    //   }}
                    // />
                    <img
                      src={
                        props.sitter.profile_photo
                          ? props.sitter.profile_photo
                          : profileFallback
                      }
                      alt="profile-fallback"
                      style={{
                        height: props.sitter.profile_photo ? 180 : 48,
                        width: props.sitter.profile_photo ? 180 : 48,
                        borderRadius: 8,
                      }}
                    />
                  )}

                <Typography
                  color="primary"
                  style={{ fontSize: 20, fontWeight: 500 }}
                >
                  {props.profileFileName
                    ? props.profileFileName
                    : props.profile_photo === "" || props.profile_photo === null
                      ? "Click to Upload"
                      : "Click to Upload"}
                </Typography>
                <input
                  type="file"
                  id="imageFile"
                  ref={(input) => {
                    imageUploader = input;
                  }}
                  name="profile_photo"
                  style={{ display: "none" }}
                  onChange={props.fileUploader}
                />
              </div>
            )}
          </div>
          <div style={{ marginLeft: "1rem" }}>
            {
              <Typography
                color="secondary"
                style={{ fontSize: "1.5rem", fontFamily: "Fredoka One" }}
              >
                {user.user.first_name} {user.user.last_name}
              </Typography>
            }

            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginTop: "8px",
                color: "#5E5A5A",
              }}
            >
              {!props.editable && (
                <div style={{ display: "flex", alignItems: "center" }}>
                  <LocationOnIcon />
                  <Typography style={{ fontSize: "1.20rem", marginLeft: 14 }}>
                    {props.sitter.postal_code}
                  </Typography>
                </div>
              )}
              {props.editable && (
                <TextField
                  label="Postal Code"
                  name="postal_code"
                  value={props.postal_code}
                  onChange={props.handleTextChange}
                  style={{ fontSize: "1.20rem" }}
                  className={classes.editField}
                />
              )}
            </div>
            {!props.editable && (
              <Typography
                style={{ fontSize: "1.2rem", color: "#979797", marginTop: 8 }}
              >
                {props.sitter.profile_intro}
              </Typography>
            )}
            {props.editable && (
              <TextField
                label="Profile Intro"
                name="profile_intro"
                value={props.profile_intro}
                onChange={props.handleTextChange}
                className={classes.editField}
              />
            )}
          </div>
        </div>
        {!props.editable && (
          <EditIcon
            color="primary"
            fontSize="large"
            onClick={props.handleEditable}
            style={{ cursor: "pointer" }}
          />
        )}
        {props.editable && (
          <div>
            <SaveIcon
              color="primary"
              fontSize="large"
              onClick={props.handleConfirmModal}
              style={{ cursor: "pointer" }}
            />
            <CancelIcon
              color="primary"
              fontSize="large"
              onClick={props.handleCancel}
              style={{ cursor: "pointer" }}
            />
          </div>
        )}
      </div>
      <Tabs
        value={props.tabValue}
        onChange={props.handleTabChange}
        aria-label="simple tabs example"
        style={{
          marginTop: 32,
          borderBottom: "0.7px solid rgba(245, 130, 32, 0.6)",
        }}
        variant="fullWidth"
        classes={{ indicator: classes.indicatorColor }}
      >
        <Tab
          label="Personal Info "
          {...a11yProps(0)}
          className={classes.profileTabs}
        />
        <Tab label="About" {...a11yProps(1)} className={classes.profileTabs} />
        <Tab
          label="Experience"
          {...a11yProps(2)}
          className={classes.profileTabs}
        />
        <Tab
          label="Home & Family"
          {...a11yProps(3)}
          className={classes.profileTabs}
        />
        {/* <Tab
          label="Other Details"
          {...a11yProps(4)}
          className={classes.profileTabs}
        /> */}
        {/* <Tab
          label="Reviews"
          {...a11yProps(5)}
          className={classes.profileTabs}
        /> */}
      </Tabs>
      <Grid container spacing={4} style={{ display: "flex" }}>
        <Grid item xs={7}>
          <TabPanel value={props.tabValue} index={0}>
            <div className={classes.leftGridScroll}>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  Date of Birth
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.dob ? (
                      moment(props.dob, "YYYY-MM-DD").format("DD/MM/YYYY")
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. 19/10/2023
                        </span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="dob"
                    type="date"
                    value={props.dob}
                    // defaultValue="2017-05-24"
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>

              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  Email
                </Typography>
                <Typography className={classes.leftGridContent}>
                  {props.sitter.email ? props.sitter.email : user.user.email}
                </Typography>
              </div>
              <div style={{ margin: "8px 0" }}>
                {/* <MUIPlacesAutocomplete
                  // onSuggestionSelected={this.onSuggestionSelected}
                  // renderTarget={() => <SomeCoolComponent />}
                /> */}
              </div>





              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  Country
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.sitter.country ? (
                      props.sitter.country
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. France
                        </span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="country"
                    value={props.country}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  Street Address
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {/* {props.sitter.street_address ? (
                      props.sitter.street_address
                    ) : (
                      <span className={classes.placeholderText}>
                        Ex. Chruch Strret
                      </span>
                    )} */}

                    {props.sitter.country && (
                      props.sitter.country
                    )},
                    {props.sitter.address && (
                      props.sitter.address
                    )},

                    {props.sitter.street_address && (
                      props.sitter.street_address
                    )}

                    {props.sitter.house_no && (
                      props.sitter.house_no
                    )}
                    {props.sitter.city && (
                      props.sitter.city
                    )}
                    {props.sitter.postal_code && (
                      props.sitter.postal_code
                    )}
                    {props.mobile && (
                      props.mobile
                    )}
                    {console.log('list country*', props.sitter.country)}
                    {console.log('list address*', props.sitter.address)}
                    {console.log('list  street_address*', props.sitter.street_address)}
                    {console.log('list house no*', props.sitter.house_no)}
                    {console.log('list  city*', props.sitter.postal_code)}
                    {console.log('list postcode*', props.postal_code)}
                    {console.log('list mobile*', props.mobile)}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="street_address"
                    value={props.street_address}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  House No
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.sitter.house_no ? (
                      props.sitter.house_no
                    ) : (
                        <span className={classes.placeholderText}>Ex. #01</span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="house_no"
                    value={props.house_no}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  City
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.sitter.city ? (
                      props.sitter.city
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. New York
                        </span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="city"
                    value={props.city}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  State
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.sitter.state ? (
                      props.sitter.state
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. Alabama
                        </span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="state"
                    value={props.state}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  Postal Code
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.sitter.postal_code ? (
                      props.sitter.postal_code
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. 100890
                        </span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="postal_code"
                    value={props.postal_code}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>

              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  Mobile Number
                </Typography>

                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.mobile ? (
                      props.mobile
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. 111 000 2222
                        </span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  // <TextField
                  //   name="mobile"
                  //   value={props.mobile}
                  //   onChange={props.handleTextChange}
                  //   className={classes.editField}
                  // />
                  <MuiPhoneNumber
                    preferredCountries={["lu", "fr", "be", "de", "at"]}
                    value={props.mobile}
                    onChange={props.handleChangeMobileCode}
                    style={{ margin: "10px 0px" }}
                    name="mobile"
                    inputClass={classes.mobileNumberInput}
                  />
                )}
              </div>



              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  ID Type
                </Typography>
                {!props.editable && (
                  <Typography
                    style={{ textTransform: "capitalize" }}
                    className={classes.leftGridContent}
                  >
                    {props.sitter.id_type ? (
                      props.sitter.id_type
                    ) : (
                        <span className={classes.placeholderText}>Ex. EU ID</span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <RadioGroup
                    aria-label="id_type"
                    name="id_type"
                    value={props.id_type}
                    onChange={props.handleIDTypeChange}
                    row
                  >
                    <FormControlLabel
                      value="eu id"
                      control={<Radio color="primary" />}
                      label="EU ID"
                    />
                    <FormControlLabel
                      value="passport"
                      control={<Radio color="primary" />}
                      label="Passport"
                    />
                  </RadioGroup>
                )}
              </div>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  ID Number
                </Typography>
                {!props.editable && (
                  <Typography className={classes.leftGridContent}>
                    {props.sitter.id_number ? (
                      props.sitter.id_number
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. ID009098767
                        </span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <TextField
                    name="id_number"
                    value={props.id_number}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                )}
              </div>
              <div style={{ margin: "8px 0" }}>
                <Typography
                  className={classes.sitterProfileHeadings}
                  color="secondary"
                >
                  ID Image
                </Typography>
                {!props.editable && (
                  <div
                    style={{
                      padding: props.sitter.id_image ? 0 : 32,
                      height: 180,
                      width: 180,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      src={
                        props.sitter.id_image
                          ? props.sitter.id_image
                          : profileFallback
                      }
                      alt="profile-fallback"
                      style={{
                        height: props.sitter.id_image ? 180 : 48,
                        width: props.sitter.id_image ? 180 : 48,
                        borderRadius: 8,
                      }}
                    />
                  </div>
                )}

                {props.editable && (
                  <div
                    style={{
                      padding:
                        props.id_image === "" || props.id_image === null
                          ? 32
                          : 0,
                      height: 180,
                      width: 200,
                      cursor: "pointer",
                    }}
                    onClick={props.handleIdUpload}
                  >
                    {props.id_image === "" ||
                      props.id_image === null ||
                      props.idFileName ? (
                        <img
                          src={profileFallback}
                          alt="profile-fallback"
                          style={{
                            height: 48,
                            width: 48,
                          }}
                        />
                      ) : (
                        <img
                          src={props.sitter.id_image}
                          alt="profile-fallback"
                          style={{
                            height: props.sitter.id_image ? 180 : 48,
                            width: props.sitter.id_image ? 180 : 48,
                            borderRadius: 8,
                          }}
                        />
                      )}

                    <Typography
                      color="primary"
                      style={{ fontSize: 20, fontWeight: 500 }}
                    >
                      {props.idFileName
                        ? props.idFileName
                        : props.id_image === "" || props.id_image === null
                          ? "Upload"
                          : "Click to Upload"}
                    </Typography>
                    <input
                      type="file"
                      id="idFile"
                      ref={(input) => {
                        idImageUploader = input;
                      }}
                      name="id_image"
                      style={{ display: "none" }}
                      onChange={props.idFileUploader}
                    />
                  </div>
                )}

                {!props.editable && (
                  <React.Fragment>
                    <Typography
                      className={classes.sitterProfileHeadings}
                      color="secondary"
                      style={{
                        marginTop: 24,
                        maxWidth: 380,
                      }}
                    >
                      Do you have private insurance (civil liability/ home
                      insurance) that covers your dog?
                    </Typography>

                    <Typography
                      style={{
                        color: "#5E5A5A",
                        fontSize: 17,
                      }}
                    >
                      {props.have_insurance}
                    </Typography>

                    <Typography
                      className={classes.sitterProfileHeadings}
                      color="secondary"
                      style={{
                        marginTop: 24,
                      }}
                    >
                      Your Insurance Provider is:
                    </Typography>

                    <Typography
                      style={{
                        color: "#5E5A5A",
                        fontSize: 17,
                      }}
                    >
                      {props.insurance_privider}
                      {console.log('props insurance provider', props.insurance_privider)}
                    </Typography>

                    {!props.editable && props.insurance_privider === "other" &&
                      (<Typography
                        className={classes.sitterProfileHeadings}
                        color="secondary"
                        style={{
                          marginTop: 24,
                        }}
                      >
                        Your Other  Insurance Provider is:
                      </Typography>
                      )}

                    <Typography
                      style={{
                        color: "#5E5A5A",
                        fontSize: 17,
                      }}
                    >
                      {!props.editable && props.insurance_privider === "other" && (
                        props.other_privider
                      )}
                    </Typography>
                  </React.Fragment>
                )}

                {props.editable && (
                  <div style={{ margin: "8px 0", marginTop: "50px" }}>
                    <Typography
                      className={classes.sitterProfileHeadings}
                      color="secondary"
                    >
                      Do you have private insurance (civil liability/ home
                      insurance) that covers your dog?
                    </Typography>
                    <FormControl component="fieldset">
                      <RadioGroup
                        name="gender1"
                        value={props.have_insurance}
                        onChange={props.handleHaveInsurance}
                        style={{ fontSize: "17px" }}
                        row
                      >
                        <FormControlLabel
                          value="yes"
                          control={<Radio />}
                          label="Yes"
                        />
                        <FormControlLabel
                          value="no"
                          control={<Radio />}
                          label="NO"
                        />
                      </RadioGroup>
                    </FormControl>
                  </div>
                )}
                <div style={{ margin: "8px 0" }}>
                  {props.have_insurance === "yes" && props.editable ? (
                    <div>
                      <Typography
                        className={classes.sitterProfileHeadings}
                        color="secondary"
                      >
                        Select your insurance provider
                    </Typography>
                      {/* <label style={{ color: "#5E5A5A" }}>
                        Select your insurance provider
                      </label> */}
                      <Select
                        labelId="demo-simple-select-label"
                        id="simple-select"
                        value={props.insurance_privider}
                        placeholder="Select your insurance provider"
                        // defaultValue={props.insurance_privider}
                        onChange={props.handleInsuranceMenuChange}
                        style={{ margin: "10px", width: 180 }}
                      >
                        <MenuItem value={"axa"}>Axa</MenuItem>
                        <MenuItem value={"foyer"}>Foyer</MenuItem>
                        <MenuItem value={"baloise"}>Baloise</MenuItem>
                        <MenuItem value={"lalux"}>Lalux</MenuItem>
                        <MenuItem value={"other"}>Other</MenuItem>
                      </Select>
                    </div>
                  ) : null}
                </div>
                {props.editable && props.insurance_privider === "other" && (
                  <div>
                    <Typography
                      className={classes.sitterProfileHeadings}
                      color="secondary"
                    >
                      Other Insurance Provider:
                    </Typography>

                    <TextField
                      name="profile_bio"
                      value={props.other_privider}
                      onChange={props.handleOtherInsuranceChange}
                      multiline={true}
                      style={{ width: "50%" }}
                    />
                  </div>
                )}

              </div>
            </div>
          </TabPanel>
          <TabPanel value={props.tabValue} index={1}>
            <div className={classes.leftGridScroll}>
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Profile Bio
              </Typography>
              {!props.editable && (
                <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                  {props.sitter.profile_bio ? (
                    props.sitter.profile_bio
                  ) : (
                      <span className={classes.placeholderText}>Your Bio</span>
                    )}
                </Typography>
              )}
              {props.editable && (
                <TextField
                  name="profile_bio"
                  value={props.profile_bio}
                  onChange={props.handleTextChange}
                  multiline={true}
                  style={{ width: "90%" }}
                />
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Languages spoken
              </Typography>

              {!props.editable &&
                props.languages_spoke.map((space, i) => {
                  return space === 1 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      English
                    </Typography>
                  ) : space === 2 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Spanish
                    </Typography>
                  ) : space === 3 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      French
                    </Typography>
                  ) : space === 4 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Italian
                    </Typography>
                  ) : space === 5 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Portuguese
                    </Typography>
                  ) : space === 6 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Luxembourgish
                    </Typography>
                  ) : space === 7 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      German
                    </Typography>
                  ) : (
                                  <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                                    No
                                  </Typography>
                                );
                })}
              {props.editable && (
                <FormGroup row>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.languages_spoke.indexOf(1) === -1 ? false : true
                        }
                        onChange={props.handleLanguageChange}
                        name="checkedA"
                        value={1}
                      />
                    }
                    label="English"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.languages_spoke.indexOf(2) === -1 ? false : true
                        }
                        onChange={props.handleLanguageChange}
                        name="checkedB"
                        value={2}
                      />
                    }
                    label="Spanish"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.languages_spoke.indexOf(3) === -1 ? false : true
                        }
                        onChange={props.handleLanguageChange}
                        name="checkedC"
                        value={3}
                      />
                    }
                    label="French"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.languages_spoke.indexOf(4) === -1 ? false : true
                        }
                        onChange={props.handleLanguageChange}
                        name="checkedD"
                        value={4}
                      />
                    }
                    label="Italian"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.languages_spoke.indexOf(5) === -1 ? false : true
                        }
                        onChange={props.handleLanguageChange}
                        name="checkedE"
                        value={5}
                      />
                    }
                    label="Portuguese"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.languages_spoke.indexOf(6) === -1 ? false : true
                        }
                        onChange={props.handleLanguageChange}
                        name="checkedF"
                        value={6}
                      />
                    }
                    label="Luxembourgish"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.languages_spoke.indexOf(7) === -1 ? false : true
                        }
                        onChange={props.handleLanguageChange}
                        name="checkedG"
                        value={7}
                      />
                    }
                    label="German"
                  />
                </FormGroup>
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Reasons For Joining
              </Typography>
              {!props.editable && (
                <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                  {props.sitter.reason_for_joining ? (
                    props.sitter.reason_for_joining
                  ) : (
                      <span className={classes.placeholderText}>
                        Ex. Lorem occaecat nostrud reprehenderit nisi fugiat
                        veniam
                      </span>
                    )}
                </Typography>
              )}
              {props.editable && (
                <TextField
                  label="Reasons For Joining"
                  name="reasons_for_joining"
                  value={props.reasons_for_joining}
                  onChange={props.handleTextChange}
                  className={classes.editField}
                />
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Work Schedule
              </Typography>
              {!props.editable && (
                <div>
                  <Typography
                    style={{
                      fontSize: 17,
                      color: "#5E5A5A",
                      textTransform: "capitalize",
                    }}
                  >
                    {props.sitter.work_schedule ? (
                      props.sitter.work_schedule
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. Flexible Schedule
                        </span>
                      )}
                  </Typography>
                  <Typography
                    className={classes.sitterProfileHeadings}
                    color="secondary"
                  //  style={{ marginTop: "2rem" }}
                  >
                    Describe your work schedule
                  </Typography>
                  <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                    {props.work_schedule_detail ? (
                      props.work_schedule_detail
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. Lorem occaecat nostrud reprehenderit nisi fugiat
                          veniam
                        </span>
                      )}
                  </Typography>
                </div>
              )}
              {/*  props.sitter.work_schedule === "don't work"
                        ? 1
                        : props.sitter.work_schedule === "work full time"
                        ? 2
                        : props.sitter.work_schedule === "work part time"
                        ? 3
                        : props.sitter.work_schedule === "flexible work"
                        ? 4
                        : props.sitter.work_schedule === "work from home"
                        ? 5
                        : props.sitter.work_schedule === "available mostly on weekends"
                        ? 6
                        : props.sitter.work_schedule === "others"
                        ? 7
                        : 0 */}
              {props.editable && (
                <div>
                  <RadioGroup
                    aria-label="workSchedule"
                    name="workSchedule"
                    value={props.workSchedule}
                    onChange={props.handleWorkScheduleChange}
                    row
                  >
                    <FormControlLabel
                      value="1"
                      control={<Radio color="primary" />}
                      label="Don't work"
                    />
                    <FormControlLabel
                      value="2"
                      control={<Radio color="primary" />}
                      label="Work full time"
                    />
                    <FormControlLabel
                      value="3"
                      control={<Radio color="primary" />}
                      label="Work part time"
                    />
                    <FormControlLabel
                      value="4"
                      control={<Radio color="primary" />}
                      label="Flexible work"
                    />
                    <FormControlLabel
                      value="5"
                      control={<Radio color="primary" />}
                      label="Work from home"
                    />
                    <FormControlLabel
                      value="6"
                      control={<Radio color="primary" />}
                      label="Available mostly on weekends"
                    />
                    <FormControlLabel
                      value="7"
                      control={<Radio color="primary" />}
                      label="Other"
                    />
                  </RadioGroup>
                  <TextField
                    name="work_schedule_detail"
                    label="Work Schedule Detail"
                    value={props.work_schedule_detail}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                </div>
              )}
              <Typography
                color="secondary"
                className={classes.sitterProfileHeadings}
                style={{ marginTop: "1rem" }}
              >
                Dog Size Preference
              </Typography>
              <div>
                {!props.editable &&
                  props.dog_size_preference.map((space, i) => {
                    return space === 1 ? (
                      <Typography
                        style={{
                          fontSize: 17,
                          color: "#5E5A5A",
                        }}
                      >
                        Small
                      </Typography>
                    ) : space === 2 ? (
                      <Typography
                        style={{
                          fontSize: 17,
                          color: "#5E5A5A",
                        }}
                      >
                        Medium
                      </Typography>
                    ) : space === 3 ? (
                      <Typography
                        style={{
                          fontSize: 17,
                          color: "#5E5A5A",
                        }}
                      >
                        Large
                      </Typography>
                    ) : space === 4 ? (
                      <Typography
                        style={{
                          fontSize: 17,
                          color: "#5E5A5A",
                        }}
                      >
                        No Preference
                      </Typography>
                    ) : (
                              <Typography
                                style={{
                                  fontSize: 17,
                                  color: "#5E5A5A",
                                }}
                              >
                                No Preference
                              </Typography>
                            );
                  })}
                {props.editable && (
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_size_preference.indexOf(1) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogSizePreferences}
                          name="checkedA"
                          value={1}
                        />
                      }
                      label="Small"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_size_preference.indexOf(2) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogSizePreferences}
                          name="checkedB"
                          value={2}
                        />
                      }
                      label="Medium"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_size_preference.indexOf(3) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogSizePreferences}
                          name="checkedC"
                          value={3}
                        />
                      }
                      label="Large"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_size_preference.indexOf(4) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogSizePreferences}
                          name="checkedC"
                          value={4}
                        />
                      }
                      label="No Preference"
                    />
                  </FormGroup>
                )}
              </div>
              <div style={{ margin: "2rem 0" }}>
                <Typography
                  color="secondary"
                  className={classes.sitterProfileHeadings}
                >
                  Dog Age Preference
                </Typography>
                {!props.editable &&
                  props.dog_age_preference.map((space, i) => {
                    return space === 1 ? (
                      <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                        Puppy: 2-6 Months
                      </Typography>
                    ) : space === 2 ? (
                      <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                        Puppy: 6-24 Months
                      </Typography>
                    ) : space === 3 ? (
                      <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                        Adult Dog: 2 Years+
                      </Typography>
                    ) : space === 4 ? (
                      <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                        Elderly Dog: 10Years+
                      </Typography>
                    ) : space === 5 ? (
                      <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                        Any Age
                      </Typography>
                    ) : (
                                <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                                  No
                                </Typography>
                              );
                  })}
                {props.editable && (
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_age_preference.indexOf(1) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogAgePreference}
                          name="checkedA"
                          value={1}
                        />
                      }
                      label="Puppy: 2-6 Months"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_age_preference.indexOf(2) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogAgePreference}
                          name="checkedB"
                          value={2}
                        />
                      }
                      label="Puppy: 6-24 Months"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_age_preference.indexOf(3) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogAgePreference}
                          name="checkedC"
                          value={3}
                        />
                      }
                      label="Adult Dog: 2 Years+"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_age_preference.indexOf(4) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogAgePreference}
                          name="checkedC"
                          value={4}
                        />
                      }
                      label="Elderly Dog: 10Years++"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={
                            props.dog_age_preference.indexOf(5) === -1
                              ? false
                              : true
                          }
                          onChange={props.handleDogAgePreference}
                          name="checkedC"
                          value={5}
                        />
                      }
                      label="Any Age"
                    />
                  </FormGroup>
                )}
              </div>
              <div style={{ margin: "2rem 0" }}>
                <Typography
                  color="secondary"
                  className={classes.sitterProfileHeadings}
                >
                  Maximum Sitting Capacity
                </Typography>
                {!props.editable && (
                  <Typography style={{ color: "#5E5A5A", fontSize: 20 }}>
                    {props.max_sitting_capacity ? (
                      props.max_sitting_capacity
                    ) : (
                        <span className={classes.placeholderText}>Ex. 2</span>
                      )}
                  </Typography>
                )}
                {props.editable && (
                  <div className={classes.maxSittingCapacityBtnGroup}>
                    <ButtonGroup
                      size="small"
                      aria-label="small outlined button group"
                    >
                      <Button
                        name="decrement"
                        onClick={props.handleMaxSittingDecrement}
                      >
                        -
                      </Button>
                      <Button>{props.max_sitting_capacity}</Button>
                      <Button
                        name="increment"
                        onClick={props.handleMaxSittingIncrement}
                      >
                        +
                      </Button>
                    </ButtonGroup>
                  </div>
                )}
              </div>
              <div style={{ margin: "2rem 0" }}>
                <Typography
                  color="secondary"
                  className={classes.sitterProfileHeadings}
                >
                  Booking Overlap
                </Typography>
                {!props.editable && (
                  <div>
                    <Typography style={{ color: "#5E5A5A", fontSize: 20 }}>
                      {props.sitter.booking_overlap}
                    </Typography>
                    <Typography style={{ color: "#5E5A5A", fontSize: 20 }}>
                      {props.sitter.booking_overlap_parameter}
                    </Typography>
                  </div>
                )}
                {props.editable && (
                  <div>
                    <RadioGroup
                      aria-label="workSchedule"
                      name="booking_overlap"
                      value={props.booking_overlap}
                      onChange={props.handleRadioChange}
                      row
                    >
                      <FormControlLabel
                        value="yes"
                        control={<Radio color="primary" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        value="no"
                        control={<Radio color="primary" />}
                        label="No"
                      />
                      <FormControlLabel
                        value="sometimes"
                        control={<Radio color="primary" />}
                        label="Sometimes"
                      />
                    </RadioGroup>
                  </div>
                )}
                {props.editable && props.booking_overlap === "yes" && (
                  <div>
                    <RadioGroup
                      aria-label="workSchedule"
                      name="booking_overlap_parameter"
                      value={props.booking_overlap_parameter}
                      onChange={props.handleRadioChange}
                      row
                    >
                      <FormControlLabel
                        value="1-2 days"
                        control={<Radio color="primary" />}
                        label="1-2 Days"
                      />
                      <FormControlLabel
                        value="2-4 days"
                        control={<Radio color="primary" />}
                        label="2-4 Days"
                      />
                      <FormControlLabel
                        value="upto a week"
                        control={<Radio color="primary" />}
                        label="Upto a week"
                      />
                      <FormControlLabel
                        value="more"
                        control={<Radio color="primary" />}
                        label="More"
                      />
                    </RadioGroup>
                  </div>
                )}
              </div>
            </div>
          </TabPanel>
          <TabPanel value={props.tabValue} index={2}>
            <div className={classes.leftGridScroll}>
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Experience With Dogs
              </Typography>
              {!props.editable && (
                <div>
                  <Typography
                    style={{
                      fontSize: 17,
                      color: "#979797",
                    }}
                  >
                    {props.experience_with_dogs_choice ? (
                      props.experience_with_dogs_choice
                    ) : (
                        <span className={classes.placeholderText}>Ex. Yes</span>
                      )}
                  </Typography>
                  <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                    {props.experience_with_dogs ? (
                      props.experience_with_dogs
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. Lorem occaecat nostrud reprehenderit nisi fugiat
                          veniam
                        </span>
                      )}
                  </Typography>
                </div>
              )}
              {props.editable && (
                <React.Fragment>
                  <div>
                    <RadioGroup
                      aria-label="workSchedule"
                      name="experience_with_dogs_choice"
                      value={props.experience_with_dogs_choice}
                      onChange={props.handleRadioChange}
                      row
                    >
                      <FormControlLabel
                        value="Yes"
                        control={<Radio color="primary" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        value="No"
                        control={<Radio color="primary" />}
                        label="No"
                      />
                    </RadioGroup>
                  </div>
                  <TextField
                    label="Experience With Dogs"
                    name="experience_with_dogs"
                    value={props.experience_with_dogs}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                </React.Fragment>
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Experience with other animals
              </Typography>
              {!props.editable && (
                <div>
                  <Typography
                    style={{
                      fontSize: 17,
                      color: "#5E5A5A",
                    }}
                  >
                    {props.experience_with_pets_choice ? (
                      props.experience_with_pets_choice
                    ) : (
                        <span className={classes.placeholderText}>Ex. Yes</span>
                      )}
                  </Typography>
                  <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                    {props.experience_with_pets ? (
                      props.experience_with_pets
                    ) : (
                        <span className={classes.placeholderText}>
                          Ex. Lorem occaecat nostrud reprehenderit nisi fugiat
                          veniam
                        </span>
                      )}
                  </Typography>
                </div>
              )}
              {props.editable && (
                <React.Fragment>
                  <div>
                    <RadioGroup
                      aria-label=""
                      name="experience_with_pets_choice"
                      value={props.experience_with_pets_choice}
                      onChange={props.handleRadioChange}
                      row
                    >
                      <FormControlLabel
                        value="Yes"
                        control={<Radio color="primary" />}
                        label="Yes"
                      />
                      <FormControlLabel
                        value="No"
                        control={<Radio color="primary" />}
                        label="No"
                      />
                    </RadioGroup>
                  </div>
                  <TextField
                    label="Experience With Pets"
                    name="experience_with_pets"
                    value={props.experience_with_pets}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                </React.Fragment>
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem", marginBottom: "18px" }}
              >
                Photos of my pets
              </Typography>
              <div>
                {!props.editable && props.PetImageFiles
                  ? props.PetImageFiles.map((file) => {
                    return (
                      <img
                        src={file.location}
                        alt="Pet 1"
                        width="200"
                        className={classes.multiplePhotos}
                        style={{ marginRight: 12 }}
                      />
                    );
                  })
                  : null}
              </div>
              {props.editable && (
                <React.Fragment>
                  <Button
                    name="PetImageFilesOpen"
                    onClick={props.handlePetMultipleOpen}
                    color="primary"
                  >
                    Upload
                  </Button>
                  <DropzoneDialog
                    name="PetImageFilesOpen"
                    cancelButtonText={"cancel"}
                    submitButtonText={"save"}
                    open={props.PetImageFilesOpen}
                    onSave={props.handlePetImageUpload}
                    acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
                    showPreviews={true}
                    maxFileSize={5000000}
                    onClose={props.handlePetMultipleOpen}
                    className={classes.dropzoneStyles}
                  />
                </React.Fragment>
              )}
            </div>
          </TabPanel>
          <TabPanel value={props.tabValue} index={3}>
            <div className={classes.leftGridScroll}>
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Type Of Home
              </Typography>
              {!props.editable && (
                <Typography
                  style={{
                    fontSize: 17,
                    color: "#5E5A5A",
                    textTransform: "capitalize",
                  }}
                >
                  {props.type_of_home ? (
                    props.type_of_home
                  ) : (
                      <span className={classes.placeholderText}>Ex. House</span>
                    )}
                </Typography>
              )}
              {props.editable && (
                // <TextField
                //   label="Type Of Home"
                //   name="type_of_home"
                //   value={props.type_of_home}
                //   onChange={props.handleTextChange}
                //   className={classes.editField}
                // />
                <React.Fragment>
                  <div>
                    <RadioGroup
                      aria-label="Type Of Home"
                      name="type_of_home"
                      value={props.type_of_home}
                      onChange={props.handleRadioChange}
                      row
                    >
                      <FormControlLabel
                        value="house"
                        control={<Radio color="primary" />}
                        label="House"
                      />
                      <FormControlLabel
                        value="Apartment"
                        control={<Radio color="primary" />}
                        label="Apartment"
                      />
                    </RadioGroup>
                  </div>
                </React.Fragment>
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Home Description
              </Typography>
              {!props.editable && (
                <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                  {props.home_type_description ? (
                    props.home_type_description
                  ) : (
                      <span className={classes.placeholderText}>
                        Ex. Lorem occaecat nostrud reprehenderit nisi fugiat
                        veniam
                      </span>
                    )}
                </Typography>
              )}
              {props.editable && (
                <TextField
                  label="Describe your home"
                  name="home_type_description"
                  value={props.home_type_description}
                  onChange={props.handleTextChange}
                  className={classes.editField}
                />
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Size Of Home
              </Typography>
              {!props.editable && (
                <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                  {props.home_type_area ? (
                    props.home_type_area + " " + props.home_type_area_unit
                  ) : (
                      <span className={classes.placeholderText}>
                        Ex. 2500 Sq. Ft.
                      </span>
                    )}
                </Typography>
              )}
              {props.editable && (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <TextField
                    label="Size Of Home"
                    name="home_type_area"
                    value={props.home_type_area}
                    onChange={props.handleTextChange}
                    className={classes.editField}
                  />
                  <div>
                    <RadioGroup
                      aria-label=""
                      name="home_type_area_unit"
                      value={props.home_type_area_unit}
                      onChange={props.handleRadioChange}
                      row
                    >
                      <FormControlLabel
                        value="Sq Ft"
                        control={<Radio color="primary" />}
                        label="Sq Ft"
                      />
                      <FormControlLabel
                        value="Sq Mt"
                        control={<Radio color="primary" />}
                        label="Sq Mt"
                      />
                    </RadioGroup>
                  </div>
                </div>
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Open spaces in my home
              </Typography>
              {!props.editable &&
                props.open_spaces_available.map((space, i) => {
                  return space === 1 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Yes
                    </Typography>
                  ) : space === 2 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Balcony
                    </Typography>
                  ) : space === 3 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Patio
                    </Typography>
                  ) : space === 4 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Terrace
                    </Typography>
                  ) : space === 5 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Garden
                    </Typography>
                  ) : (
                              <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                                No
                              </Typography>
                            );
                })}
              {props.editable && (
                <FormGroup row>
                  {/* <FormControlLabel
                    control={
                      <Checkbox
                        checked={props.checkedA}
                        onChange={props.handleOpenSpaceChange}
                        name="checkedA"
                        value={1}
                      />
                    }
                    label="Yes"
                  /> */}
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.open_spaces_available.indexOf(2) === -1
                            ? false
                            : true
                        }
                        onChange={props.handleOpenSpaceChange}
                        name="checkedB"
                        value={2}
                      />
                    }
                    label="Balcony"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.open_spaces_available.indexOf(3) === -1
                            ? false
                            : true
                        }
                        onChange={props.handleOpenSpaceChange}
                        name="checkedC"
                        value={3}
                      />
                    }
                    label="Patio"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.open_spaces_available.indexOf(4) === -1
                            ? false
                            : true
                        }
                        onChange={props.handleOpenSpaceChange}
                        name="checkedD"
                        value={4}
                      />
                    }
                    label="Terrace"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.open_spaces_available.indexOf(5) === -1
                            ? false
                            : true
                        }
                        onChange={props.handleOpenSpaceChange}
                        name="checkedE"
                        value={5}
                      />
                    }
                    label="Garden"
                  />
                </FormGroup>
              )}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Open spaces outside
              </Typography>
              {!props.editable &&
                props.access_to_green_spaces_outside.map((space, i) => {
                  return space === 1 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Dog Park
                    </Typography>
                  ) : space === 2 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Forest
                    </Typography>
                  ) : space === 3 ? (
                    <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                      Open Area or Ground
                    </Typography>
                  ) : (
                          <Typography style={{ fontSize: 17, color: "#5E5A5A" }}>
                            No
                          </Typography>
                        );
                })}
              {props.editable && (
                <FormGroup row>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.access_to_green_spaces_outside.indexOf(1) === -1
                            ? false
                            : true
                        }
                        onChange={props.handleSpaceOutsideChange}
                        name="checkedA"
                        value={1}
                      />
                    }
                    label="Dog Park"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.access_to_green_spaces_outside.indexOf(2) === -1
                            ? false
                            : true
                        }
                        onChange={props.handleSpaceOutsideChange}
                        name="checkedB"
                        value={2}
                      />
                    }
                    label="Forest"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          props.access_to_green_spaces_outside.indexOf(3) === -1
                            ? false
                            : true
                        }
                        onChange={props.handleSpaceOutsideChange}
                        name="checkedC"
                        value={3}
                      />
                    }
                    label="Open area or ground"
                  />
                </FormGroup>
              )}

              {/*  */}

              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem" }}
              >
                Children at Home
              </Typography>
              <Typography
                style={{
                  fontSize: 17,
                  color: "#5E5A5A",
                  marginTop: "1rem",
                  textTransform: "capitalize",
                }}
              >
                {props.children_at_home ? (
                  props.children_at_home
                ) : (
                    <span className={classes.placeholderText}>Ex. No</span>
                  )}
              </Typography>
              {!props.editable && props.children_at_home && (
                <div>
                  {props.children
                    ? props.children.map((child, index) => {
                      return (
                        <div>
                          <Typography
                            style={{
                              fontSize: 17,
                              color: "#5E5A5A",
                              marginTop: "1rem",
                            }}
                          >
                            Child {index + 1}:&nbsp; Age: {child.age} ,
                              Gender: {child.gender}
                          </Typography>
                          {/* <Typography
                            style={{
                              fontSize: 18,
                              color: "#979797",
                              marginTop: "1rem",
                            }}
                          >
                            
                          </Typography> */}
                        </div>
                      );
                    })
                    : null}

                  {/* <Typography style={{fontSize:18, color:'#5E5A5A'}}>Lorem occaecat Lorem occaecat Lorem occaecat Lorem occaecat Lorem occaecat</Typography> */}
                </div>
              )}
              {props.editable && (
                <React.Fragment>
                  <div>
                    <RadioGroup
                      name="children_at_home"
                      value={props.children_at_home}
                      onChange={props.handleRadioChange}
                      row
                    >
                      <FormControlLabel
                        value="yes"
                        control={<Radio color="primary" />}
                        label="Yes"
                      // onClick={this.radioButtonClicked}
                      />
                      <FormControlLabel
                        value="no"
                        control={<Radio color="primary" />}
                        label="No"
                      />
                    </RadioGroup>
                  </div>
                  {console.log("the children at home", props.children_at_home)}

                  {props.editable && (
                    <div>
                      {props.children_at_home === "yes" ? (
                        <div className={classes.maxSittingCapacityBtnGroup}>
                          <ButtonGroup
                            size="small"
                            aria-label="small outlined button group"
                          >
                            <Typography
                              style={{
                                fontSize: 17,
                                color: "#5E5A5A",
                                marginTop: "0.5rem",
                              }}
                            >
                              Number of Children: &nbsp;
                            </Typography>
                            <Button
                              name="decrement"
                              onClick={
                                props.children_at_home === "yes"
                                  ? props.handleChildrenDecrement
                                  : null
                              }
                            >
                              -
                            </Button>
                            <Button>{props.children.length}</Button>
                            <Button
                              name="increment"
                              onClick={
                                props.children_at_home === "yes"
                                  ? props.handleChildrenIncrement
                                  : null
                              }
                            >
                              +
                            </Button>
                          </ButtonGroup>
                        </div>
                      ) : null}
                      <div>
                        {props.children && props.children_at_home === "yes"
                          ? props.children.map((child, index) => {
                            return (
                              <div>
                                <Typography
                                  style={{
                                    fontSize: 17,
                                    color: "#5E5A5A",
                                    marginTop: "0.5rem",
                                  }}
                                >
                                  Child {index + 1}:&nbsp;
                                  </Typography>

                                <RadioGroup
                                  name={index}
                                  value={props.children[index].gender}
                                  onChange={props.handleGenderChange}
                                  row
                                >
                                  <TextField
                                    label="Age"
                                    name={index}
                                    value={props.children[index].age}
                                    onChange={props.handleAgeChange}
                                    className={classes.editField}
                                    style={{
                                      width: "40px",
                                      marginRight: "10px",
                                    }}
                                  />
                                  <Typography
                                    style={{
                                      fontSize: 17,
                                      color: "#5E5A5A",
                                      marginTop: "0.5rem",
                                    }}
                                  >
                                    Gender:&nbsp;
                                    </Typography>
                                  <FormControlLabel
                                    value="male"
                                    control={<Radio color="primary" />}
                                    label="Male"
                                  />
                                  <FormControlLabel
                                    value="female"
                                    control={<Radio color="primary" />}
                                    label="Female"
                                  />
                                </RadioGroup>
                              </div>
                            );
                          })
                          : null}
                      </div>
                    </div>
                  )}
                </React.Fragment>
              )}
              {/*  */}
              <Typography
                className={classes.sitterProfileHeadings}
                color="secondary"
                style={{ marginTop: "2rem", marginBottom: "18px" }}
              >
                Photos Of My Home
              </Typography>
              <div style={{ justifyContent: "space-around" }}>
                {!props.editable && props.HomeImageFiles
                  ? props.HomeImageFiles.map((file) => {
                    return (
                      <img
                        src={file.location}
                        alt="Home 1"
                        width="200"
                        className={classes.multiplePhotos}
                        style={{ marginRight: 12 }}
                      />
                    );
                  })
                  : null}
              </div>
              {props.editable && (
                <React.Fragment>
                  <Button
                    name="HomeImageFilesOpen"
                    onClick={props.handleHomeMultipleOpen}
                    color="primary"
                  >
                    Upload
                  </Button>
                  <DropzoneDialog
                    name="HomeImageFilesOpen"
                    cancelButtonText={"cancel"}
                    submitButtonText={"save"}
                    open={props.HomeImageFilesOpen}
                    onSave={props.handleHomeImageUpload}
                    acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
                    showPreviews={true}
                    maxFileSize={5000000}
                    onClose={props.handleHomeMultipleOpen}
                    className={classes.dropzoneStyles}
                  />
                </React.Fragment>
              )}
              {/* <div style={{ display: "flex", justifyContent: "space-around" }}>
                {props.HomeImageFiles &&
                  props.HomeImageFiles.map((file) => {
                    return (
                      <img
                        src={file}
                        alt="Pet 1"
                        width="200"
                        className={classes.experienceImg}
                        style={{ marginRight: 12 }}
                      />
                    );
                  })}
              </div>
              {props.editable && (
                <React.Fragment>
                  <Button
                    name="HomeImageFilsesOpen"
                    onClick={props.handleHomeMultipleOpen}
                    color="primary"
                  >
                    Upload
                  </Button>
                  <DropzoneDialog
                    name="HomeImageFilesOpen"
                    cancelButtonText={"cancel"}
                    submitButtonText={"save"}
                    open={props.HomeImageFilesOpen}
                    onSave={props.handleHomeImageUpload}
                    acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
                    showPreviews={true}
                    maxFileSize={5000000}
                    onClose={props.handleHomeMultipleOpen}
                    className={classes.dropzoneStyles}
                  />
                </React.Fragment>
              )} */}
            </div>
          </TabPanel>
          {/* <TabPanel value={props.tabValue} index={4}>
            <div className={classes.leftGridScroll}>
              
            </div>
          </TabPanel> */}
          <TabPanel value={props.tabValue} index={5}>
            <div className={classes.leftGridScroll}>
              <Typography
                style={{ fontSize: 17, fontWeight: 500, color: "#979797" }}
              >
                10 Comments
              </Typography>
              <div className={classes.reviewCard}>
                <Grid style={{ display: "flex" }}>
                  <Grid item xs={2} className={classes.reviewCardLeft}>
                    <img
                      src="https://cdn.quasar.dev/img/avatar.png"
                      style={{ borderRadius: "50%", width: "50px" }}
                      alt=""
                    />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography
                      color="secondary"
                      className={classes.reviewerName}
                    >
                      Angel D'Souza{" "}
                      <span
                        style={{
                          color: "#979797",
                          fontWeight: "lighter",
                          paddingLeft: 16,
                          fontSize: 17,
                        }}
                      >
                        Mar 20th
                      </span>
                    </Typography>
                    <Rating
                      name="read-only"
                      value={4}
                      readOnly
                      classes={{ root: classes.orangeRating }}
                    />
                    <Typography
                      style={{
                        fontSize: 17,
                        fontWeight: 500,
                        color: "#5E5A5A",
                        marginTop: "1rem",
                      }}
                    >
                      Great Experience
                    </Typography>
                    <Typography
                      style={{
                        color: "#979797",
                        fontSize: 17,
                        margin: "1rem 0",
                      }}
                    >
                      Lorem occaecat nostrud reprehenderit nisi fugiat veniam
                      mollit incididunt commodo. Commodo sit tempor excepteur
                      sint. Amet proident irure cupidatat eu Lorem ullamco
                      deserunt ea anim ipsum laboris sit ea fugiat. Aliquip
                      minim minim cillum reprehenderit deserunt et culpa aliqua
                      incididunt aute tempor
                    </Typography>
                  </Grid>
                </Grid>
              </div>
              <div className={classes.reviewCard}>
                <Grid style={{ display: "flex" }}>
                  <Grid item xs={2} className={classes.reviewCardLeft}>
                    <img
                      src="https://cdn.quasar.dev/img/avatar.png"
                      style={{ borderRadius: "50%", width: "50px" }}
                      alt=""
                    />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography
                      color="secondary"
                      className={classes.reviewerName}
                    >
                      Angel D'Souza{" "}
                      <span
                        style={{
                          color: "#979797",
                          fontWeight: "lighter",
                          paddingLeft: 16,
                          fontSize: 17,
                        }}
                      >
                        Mar 20th
                      </span>
                    </Typography>
                    <Rating
                      name="read-only"
                      value={4}
                      readOnly
                      classes={{ root: classes.orangeRating }}
                    />
                    <Typography
                      style={{
                        fontSize: 17,
                        fontWeight: 500,
                        color: "#5E5A5A",
                        marginTop: "1rem",
                      }}
                    >
                      Great Experience
                    </Typography>
                    <Typography
                      style={{
                        color: "#979797",
                        fontSize: 17,
                        margin: "1rem 0",
                      }}
                    >
                      Lorem occaecat nostrud reprehenderit nisi fugiat veniam
                      mollit incididunt commodo. Commodo sit tempor excepteur
                      sint. Amet proident irure cupidatat eu Lorem ullamco
                      deserunt ea anim ipsum laboris sit ea fugiat. Aliquip
                      minim minim cillum reprehenderit deserunt et culpa aliqua
                      incididunt aute tempor
                    </Typography>
                  </Grid>
                </Grid>
              </div>
              <div className={classes.reviewCard}>
                <Grid style={{ display: "flex" }}>
                  <Grid item xs={2} className={classes.reviewCardLeft}>
                    <img
                      src="https://cdn.quasar.dev/img/avatar.png"
                      style={{ borderRadius: "50%", width: "50px" }}
                      alt=""
                    />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography
                      color="secondary"
                      className={classes.reviewerName}
                    >
                      Angel D'Souza{" "}
                      <span
                        style={{
                          color: "#979797",
                          fontWeight: "lighter",
                          paddingLeft: 16,
                          fontSize: 18,
                        }}
                      >
                        Mar 20th
                      </span>
                    </Typography>
                    <Rating
                      name="read-only"
                      value={4}
                      readOnly
                      classes={{ root: classes.orangeRating }}
                    />
                    <Typography
                      style={{
                        fontSize: 18,
                        fontWeight: 500,
                        color: "#5E5A5A",
                        marginTop: "1rem",
                      }}
                    >
                      Great Experience
                    </Typography>
                    <Typography
                      style={{
                        color: "#979797",
                        fontSize: 18,
                        margin: "1rem 0",
                      }}
                    >
                      Lorem occaecat nostrud reprehenderit nisi fugiat veniam
                      mollit incididunt commodo. Commodo sit tempor excepteur
                      sint. Amet proident irure cupidatat eu Lorem ullamco
                      deserunt ea anim ipsum laboris sit ea fugiat. Aliquip
                      minim minim cillum reprehenderit deserunt et culpa aliqua
                      incididunt aute tempor
                    </Typography>
                  </Grid>
                </Grid>
              </div>
            </div>
          </TabPanel>
          <Dialog
            open={props.confirmModalOpen}
            onClose={props.handleConfirmModal}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              {!props.isConfirm ? "Are you sure?" : "Sucessfull"}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {!props.isConfirm ? (
                  <div>Are you sure you want to make changes?</div>
                ) : (
                    <div>Profile updated Successfully</div>
                  )}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={props.handleConfirmModal} color="primary">
                {!props.isConfirm ? "No" : "Close"}
              </Button>
              {!props.isConfirm ? (
                <Button onClick={props.handleSubmit} color="primary" autoFocus>
                  Yes
                </Button>
              ) : (
                  ""
                )}
            </DialogActions>
          </Dialog>
        </Grid>
        <Grid item xs={5}>
          <div className={classes.rightGrid}>
            <Typography
              color="primary"
              style={{ fontSize: 25, fontWeight: 500 }}
            >
              Services and Rates
            </Typography>
            <Tabs
              value={props.serviceListTabValue}
              onChange={props.handleServiceListTabChange}
              aria-label="Core & Add-on tabs"
              classes={{ indicator: classes.indicatorTab }}
            >
              <Tab
                label="Core Services"
                {...a11yProps(0)}
                className={classes.profileTabs}
              />
              <Tab
                label="Add-on Services"
                {...a11yProps(1)}
                className={classes.profileTabs}
              />
            </Tabs>
            {/* core service */}
            {console.log("props.services loading", props.servicesLoading)}
            <TabPanel value={props.serviceListTabValue} index={0}>
              {props.servicesLoading === true ? (
                <Grid style={{ height: "80vh", textAlign: "center" }}>
                  <CircularProgress color="primary" centered />
                </Grid>
              ) : (
                  <div>
                    {props.selectedSitterServices &&
                      props.selectedSitterServices.map((service, index) => {
                        console.log("@@@@@@$$$$$$$$$$$$$$$$$", service);
                        if (
                          !props.editable &&
                          service.service_id.service_type === "core services"
                        )
                          return (
                            <div>
                              <div
                                key={service.id}
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                  margin: "1rem 0",
                                }}
                              >
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontSize: 16,
                                    fontWeight: 500,
                                    maxWidth: "50%",
                                  }}
                                >
                                  {service.service_id
                                    ? service.service_id.service_name
                                    : ""}
                                </Typography>
                                <Typography
                                  color="secondary"
                                  style={{ fontSize: 16, fontWeight: 500 }}
                                >
                                  {service.serviceRate}&nbsp;
                                {service.service_id
                                    ? service.service_id.service_name ===
                                      "Day Care at Sitter's Home"
                                      ? "EUR / Day"
                                      : service.service_id.service_name ===
                                        "Day Care at Dog Sitter's Home"
                                        ? "EUR / Day"

                                        : service.service_id.service_name ===
                                          "Day Care at Dog Parent's Home"
                                          ? "EUR / Day"
                                          : service.service_id.service_name ===
                                            "Overnight Boarding at Sitter's Home"
                                            ? "EUR / Night"
                                            : service.service_id.service_name ===
                                              "Overnight Boarding at Dog Sitter's Home"
                                              ? "EUR / Night"
                                              : service.service_id.service_name ===
                                                "Overnight Boarding at Dog Parent's Home"
                                                ? "EUR / Night"
                                                : service.service_id.service_name ===
                                                  "Pick Up & Drop"
                                                  ? "EUR / Booking"
                                                  : service.service_id.service_name ===
                                                    "Vet Visit"
                                                    ? "EUR / Visit"
                                                    : service.service_id.service_name ===
                                                      "Dog Walking"
                                                      ? "EUR / Visit"
                                                      : ""
                                    : ""}
                                </Typography>
                              </div>
                              <Divider
                                style={{ backgroundColor: "#F58220" }}
                                light={true}
                              />
                            </div>
                          );
                        else if (
                          props.editable &&
                          service.service_id.service_type === "core services"
                        )
                          return (
                            <div
                              key={service.id}
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                alignItems: "center",
                                margin: "1rem 0",
                              }}
                            >
                              <Typography
                                color="secondary"
                                style={{
                                  fontSize: 16,
                                  fontWeight: 500,
                                  maxWidth: 100,
                                  marginRight: 10,
                                }}
                              >
                                {service.service_id
                                  ? service.service_id.service_name
                                  : ""}
                              </Typography>
                              <div
                                style={{
                                  display: "flex",
                                }}
                              >
                                <TextField
                                  name={index}
                                  value={
                                    props.selectedSitterServices[index]
                                      .serviceRate
                                  }
                                  onChange={props.handleRateChange}
                                  className={classes.editField}
                                  style={{ width: 60 }}
                                />
                                <Typography
                                  color="secondary"
                                  style={{ fontSize: 16, fontWeight: 500 }}
                                >
                                  &nbsp; EUR / Night
                              </Typography>
                                <DeleteIcon
                                  name={service.id}
                                  color="primary"
                                  fontSize="large"
                                  onClick={handleClickOpen}
                                  style={{ cursor: "pointer", marginLeft: 5 }}
                                />
                                {/* Delete Pet Modal */}
                                {open ? (
                                  <Dialog
                                    open={open}
                                    onClose={handleClose}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                  >
                                    <DialogTitle id="alert-dialog-title">
                                      {!isDeleteConfirm
                                        ? "Are you sure?"
                                        : "Sucessfull"}
                                    </DialogTitle>
                                    <DialogContent>
                                      <DialogContentText id="alert-dialog-description">
                                        {!isDeleteConfirm ? (
                                          <div>
                                            Are you sure you want To Delete
                                            Service?
                                          </div>
                                        ) : (
                                            <div>Service Deleted Successfully</div>
                                          )}
                                      </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                      <Button
                                        onClick={handleClose}
                                        color="primary"
                                      >
                                        {!isDeleteConfirm ? "No" : "Close"}
                                      </Button>
                                      {!isDeleteConfirm ? (
                                        <Button
                                          onClick={props.handleServiceDelete(
                                            service,
                                            handleClose
                                          )}
                                          color="primary"
                                          autoFocus
                                        >
                                          Yes
                                        </Button>
                                      ) : (
                                          ""
                                        )}
                                    </DialogActions>
                                  </Dialog>
                                ) : null}

                                {/* Delete Pet modal */}
                              </div>
                            </div>
                          );
                      })}
                  </div>
                )}
            </TabPanel>

            {/* Add On services */}

            <TabPanel value={props.serviceListTabValue} index={1}>
              {props.servicesLoading === true ? (
                <Grid style={{ height: "80vh", textAlign: "center" }}>
                  <CircularProgress color="primary" centered />
                </Grid>
              ) : (
                  <div>
                    {props.selectedSitterServices &&
                      props.selectedSitterServices.map((service, index) => {
                        if (
                          !props.editable &&
                          service.service_id.service_type === "add-on Services"
                        )
                          return (
                            <div>
                              <div
                                key={service.id}
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                  margin: "1rem 0",
                                }}
                              >
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontSize: 16,
                                    fontWeight: 500,
                                    maxWidth: "50%",
                                  }}
                                >
                                  {service.service_id
                                    ? service.service_id.service_name
                                    : ""}
                                </Typography>
                                <Typography
                                  color="secondary"
                                  style={{ fontSize: 16, fontWeight: 500 }}
                                >
                                  {service.serviceRate}&nbsp;
                                {service.service_id
                                    ? service.service_id.service_name ===
                                      "Day Care at Sitter's Home"
                                      ? "EUR / Day"
                                      : service.service_id.service_name ===
                                        "Day Care at Dog Parent's Home"
                                        ? "EUR / Day"
                                        : service.service_id.service_name ===
                                          "Overnight Boarding at Sitter's Home"
                                          ? "EUR / Night"
                                          : service.service_id.service_name ===
                                            "Overnight Boarding at Dog Parent's Home"
                                            ? "EUR / Night"
                                            : service.service_id.service_name ===
                                              "Pick Up & Drop"
                                              ? "EUR / Booking"
                                              : service.service_id.service_name ===
                                                "Vet Visit"
                                                ? "EUR / Visit"
                                                : service.service_id.service_name ===
                                                  "Dog Walking"
                                                  ? "EUR / Visit"
                                                  : ""
                                    : ""}
                                </Typography>
                              </div>
                              <Divider
                                style={{ backgroundColor: "#F58220" }}
                                light={true}
                              />
                            </div>
                          );
                        else if (
                          props.editable &&
                          service.service_id.service_type === "add-on Services"
                        )
                          return (
                            <div
                              key={service.id}
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                alignItems: "center",
                                margin: "1rem 0",
                              }}
                            >
                              <Typography
                                color="secondary"
                                style={{
                                  fontSize: 16,
                                  fontWeight: 500,
                                  maxWidth: 100,
                                  marginRight: 10,
                                }}
                              >
                                {service.service_id
                                  ? service.service_id.service_name
                                  : ""}
                              </Typography>
                              <div
                                style={{
                                  display: "flex",
                                }}
                              >
                                <TextField
                                  name={index}
                                  value={
                                    props.selectedSitterServices[index]
                                      .serviceRate
                                  }
                                  onChange={props.handleRateChange}
                                  className={classes.editField}
                                  style={{ width: 60 }}
                                />
                                <Typography
                                  color="secondary"
                                  style={{ fontSize: 16, fontWeight: 500 }}
                                >
                                  &nbsp; EUR / Night
                              </Typography>
                                <DeleteIcon
                                  name={service.id}
                                  color="primary"
                                  fontSize="large"
                                  onClick={handleClickOpen}
                                  style={{ cursor: "pointer", marginLeft: 5 }}
                                />
                                {/* Delete Pet Modal */}
                                {open ? (
                                  <Dialog
                                    open={open}
                                    onClose={handleClose}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                  >
                                    <DialogTitle id="alert-dialog-title">
                                      {!isDeleteConfirm
                                        ? "Are you sure?"
                                        : "Sucessfull"}
                                    </DialogTitle>
                                    <DialogContent>
                                      <DialogContentText id="alert-dialog-description">
                                        {!isDeleteConfirm ? (
                                          <div>
                                            Are you sure you want To Delete
                                            Service?
                                          </div>
                                        ) : (
                                            <div>Service Deleted Successfully</div>
                                          )}
                                      </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                      <Button
                                        onClick={handleClose}
                                        color="primary"
                                      >
                                        {!isDeleteConfirm ? "No" : "Close"}
                                      </Button>
                                      {!isDeleteConfirm ? (
                                        <Button
                                          onClick={props.handleServiceDelete(
                                            service,
                                            handleClose
                                          )}
                                          color="primary"
                                          autoFocus
                                        >
                                          Yes
                                        </Button>
                                      ) : (
                                          ""
                                        )}
                                    </DialogActions>
                                  </Dialog>
                                ) : null}

                                {/* Delete Pet modal */}
                              </div>
                            </div>
                          );
                      })}
                  </div>
                )}
            </TabPanel>

            <div style={{ display: "flex", justifyContent: "center" }}>
              <Button
                color="primary"
                className={classes.orangeBtnOutlined}
                startIcon={<AddIcon />}
                onClick={props.handleModal}
              >
                Add Service
              </Button>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

class DashboardProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loadingFiles: false,
      tabValue: 0,
      serviceTabValue: 0,
      editable: false,
      fullName: "",
      postal_code: "",
      country: "",
      profile_intro: "",
      profile_bio: "",
      address: "",
      id_number: "",
      dob: "",
      mobile: "",
      dog_age_preference: [],
      dog_size_preference: [],
      max_sitting_capacity: 1,
      experience_with_dogs: "",
      experience_with_dogs_choice: null,
      experience_with_pets: "",
      experience_with_pets_choice: null,
      workSchedule: "",
      work_schedule_detail: "",
      type_of_home: "",
      home_type_description: "",
      home_type_area: "",
      home_type_area_unit: "",
      reasons_for_joining: "",
      confirmModalOpen: false,
      isConfirm: false,
      sitter: [],
      booking_overlap: null,
      booking_overlap_parameter: null,
      selectedSitterServices: [],
      modalOpen: false,
      petModal: false,
      openPetView: false,
      petViewId: null,
      pet_dob: "",
      AllServices: [],
      selectedServiceID: null,
      radioChecked: false,
      selectedServiceRate: null,
      open_spaces_available: [],
      languages_spoke: [],
      access_to_green_spaces_outside: [],
      border1: false,
      border2: false,
      border3: false,
      AddPetStep: false,
      type_of_pet: "",
      handleStayHomeAloneChoiceChange: false,
      toilet_trained_choice: false,
      selectedServiceArray: [],
      profileFileName: "",
      idFileName: "",
      children: [],
      imageCropperModal: false,
      imageSrc:
        "https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000",
      crop: { x: 0, y: 0 },
      zoom: 1,
      aspect: 1 / 1,
      PetImageFiles: [],
      HomeImageFiles: [],
      HomeImageFilesOpen: false,
      PetImageFilesOpen: false,
      radioButton: false,
      anchorId: "",
      have_insurance: "no",
      insurance_privider: "",
      serviceListTabValue: 0,
      servicesLoading: true,
      other_privider: "",
    };
  }

  componentDidMount() {
    this.getSitterDetails();
    this.getSitterPets();
    this.getAllServicesList();
  }

  radioButtonClicked = () => {
    this.setState({
      radioButton: true,
    });
  };

  handleHaveInsurance = (event) => {
    this.setState({
      have_insurance: event.target.value,
    });
  };

  handleInsuranceMenuChange = (event) => {
    this.setState({
      insurance_privider: event.target.value,
    });
  };
  handleOtherInsuranceChange = (event) => {
    this.setState({
      other_privider: event.target.value,
    });
  };

  handleModal = () => {
    this.setState(
      (prevState) => {
        return {
          modalOpen: !prevState.modalOpen,
        };
      },
      () => {
        selectedServiceArray = [];
      }
    );
  };

  handlePetModal = () => {
    this.setState((prevState) => {
      return {
        petModal: !prevState.petModal,
      };
    });
    if (!this.state.petModal) {
      this.setState({
        AddPetStep: false,
        type_of_pet: "",
        border1: false,
        border2: false,
        border3: false,
      });
    }
  };

  onCropChange = (crop) => {
    this.setState({ crop });
  };

  onCropComplete = (croppedArea, croppedAreaPixels) => {
    const self = this;
    console.log(croppedArea, croppedAreaPixels);
    this.croppedImage(this.state.imageDataUrl, croppedAreaPixels, 0);
  };

  async croppedImage(imageDataUrl, croppedAreaPixels, rotation) {
    const croppedImage = await this.getCroppedImg(
      imageDataUrl,
      croppedAreaPixels,
      rotation
    );
    this.croppedImageData(croppedImage);
  }

  croppedImageData = (croppedImage) => {
    this.setState({
      croppedImage: croppedImage,
    });
  };

  async getCroppedImg(imageSrc, pixelCrop, rotation = 0) {
    const image = await this.createImage(imageSrc);
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    const maxSize = Math.max(image.width, image.height);
    const safeArea = 2 * ((maxSize / 2) * Math.sqrt(2));

    // set each dimensions to double largest dimension to allow for a safe area for the
    // image to rotate in without being clipped by canvas context
    canvas.width = safeArea;
    canvas.height = safeArea;

    // translate canvas context to a central location on image to allow rotating around the center.
    ctx.translate(safeArea / 2, safeArea / 2);
    ctx.rotate(rotation);
    ctx.translate(-safeArea / 2, -safeArea / 2);

    // draw rotated image and store data.
    ctx.drawImage(
      image,
      safeArea / 2 - image.width * 0.5,
      safeArea / 2 - image.height * 0.5
    );
    const data = ctx.getImageData(0, 0, safeArea, safeArea);

    // set canvas width to final desired crop size - this will clear existing context
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;

    // paste generated rotate image with correct offsets for x,y crop values.
    ctx.putImageData(
      data,
      Math.round(0 - safeArea / 2 + image.width * 0.5 - pixelCrop.x),
      Math.round(0 - safeArea / 2 + image.height * 0.5 - pixelCrop.y)
    );

    // As Base64 string
    return canvas.toDataURL("image/jpeg");

    // As a blob
    // return new Promise((resolve) => {

    //   canvas.toBlob((file) => {
    //     resolve(URL.createObjectURL(file));
    //   }, "image/jpeg");
    // });
  }

  createImage = (url) =>
    new Promise((resolve, reject) => {
      const image = new Image();
      image.addEventListener("load", () => resolve(image));
      image.addEventListener("error", (error) => reject(error));
      image.setAttribute("crossOrigin", "anonymous"); // needed to avoid cross-origin issues on CodeSandbox
      image.src = url;
    });

  onZoomChange = (zoom) => {
    this.setState({ zoom });
  };

  handleImageCropperModal = () => {
    this.setState((prevState) => {
      return {
        imageCropperModal: !prevState.imageCropperModal,
      };
    });
  };

  handlePetViewModal = (pet) => {
    this.setState((prevState) => {
      return {
        openPetView: !prevState.openPetView,
        petViewId: pet,
      };
    });
  };
  handleChange = (event) => {
    // setState({ ...state, [event.target.name]: event.target.checked });
  };

  handleOpenSpaceChange = (event) => {
    if (event.target.checked === true) {
      openSpaceArray.push(parseInt(event.target.value));
    } else {
      openSpaceArray.splice(
        openSpaceArray.indexOf(parseInt(event.target.value)),
        1
      );
    }
    this.setState({ open_spaces_available: openSpaceArray });
  };
  handleLanguageChange = (event) => {
    if (event.target.checked === true) {
      languagesSpokeArray.push(parseInt(event.target.value));
    } else {
      languagesSpokeArray.splice(
        languagesSpokeArray.indexOf(parseInt(event.target.value)),
        1
      );
    }
    this.setState({ languages_spoke: languagesSpokeArray });
  };

  handleSpaceOutsideChange = (event) => {
    if (event.target.checked === true) {
      SpaceOutsideArray.push(parseInt(event.target.value));
    } else {
      SpaceOutsideArray.splice(
        SpaceOutsideArray.indexOf(parseInt(event.target.value)),
        1
      );
    }
    this.setState({ access_to_green_spaces_outside: SpaceOutsideArray });
  };
  handleDogAgePreference = (event) => {
    if (event.target.checked === true) {
      if (event.target.value !== "5") {
        if (DogAgeArray.indexOf(parseInt(5)) !== -1) {
          DogAgeArray.splice(DogAgeArray.indexOf(parseInt(5)), 1);
        }
        DogAgeArray.push(parseInt(event.target.value));
      } else if (event.target.value === "5") {
        DogAgeArray = [];
        DogAgeArray.push(parseInt(event.target.value));
      }
    } else {
      DogAgeArray.splice(DogAgeArray.indexOf(parseInt(event.target.value)), 1);
    }
    this.setState({ dog_age_preference: DogAgeArray });
  };
  handleDogSizePreferences = (event) => {
    if (event.target.checked === true) {
      if (event.target.value !== "4") {
        if (DogSizeArray.indexOf(parseInt(4)) !== -1) {
          DogSizeArray.splice(DogSizeArray.indexOf(parseInt(4)), 1);
        }
        DogSizeArray.push(parseInt(event.target.value));
      } else if (event.target.value === "4") {
        DogSizeArray = [];
        DogSizeArray.push(parseInt(event.target.value));
      }
    } else {
      DogSizeArray.splice(
        DogSizeArray.indexOf(parseInt(event.target.value)),
        1
      );
    }
    this.setState({ dog_size_preference: DogSizeArray });
  };

  handleBookingOverlapParam = (event) => {
    if (event.target.checked === true) {
      BookingOverlapArray.push(parseInt(event.target.value));
    } else {
      BookingOverlapArray.splice(
        BookingOverlapArray.indexOf(parseInt(event.target.value)),
        1
      );
    }
    this.setState({ booking_overlap_parameter: BookingOverlapArray });
  };
  handleChildrenDecrement = () => {
    let array = this.state.children;
    if (this.state.children.length > 0) {
      array.pop();
      this.setState({
        children: array,
      });
      console.log("children", this.state.children);
    }
  };
  handleChildrenIncrement = () => {
    let array = this.state.children;
    if (this.state.children.length < 3) {
      array.push({ age: null, gender: null });
      this.setState({
        children: array,
      });
      console.log("children", this.state.children);
    }
  };
  handleMaxSittingDecrement = () => {
    if (this.state.max_sitting_capacity > 1) {
      this.setState({
        max_sitting_capacity: parseInt(this.state.max_sitting_capacity) - 1,
      });
    }
  };
  handleMaxSittingIncrement = () => {
    if (this.state.max_sitting_capacity < 3) {
      this.setState({
        max_sitting_capacity: parseInt(this.state.max_sitting_capacity) + 1,
      });
    }
  };
  handleConfirmModal = () => {
    this.setState({
      confirmModalOpen: !this.state.confirmModalOpen,
      isConfirm: false,
    });
  };
  handleTabChange = (event, value) => {
    this.setState({
      tabValue: value,
    });
  };
  handleServiceTabChange = (event, value) => {
    this.setState({
      serviceTabValue: value,
    });
  };

  handleServiceListTabChange = (event, value) => {
    this.setState({
      serviceListTabValue: value,
    });
  };

  handleChangeMobileCode = (value) => {
    this.setState({ mobile: value });
  };

  addService = () => {
    console.log("selected data", selectedServiceArray);
    selectedServiceArray.map((d) => {
      axios
        .post(`${config.apiUrl}/api/servicesprice/`, d)
        .then((response) => {
          console.log("response", response);
          if (response.status === 201) {
            this.handleModal();
            this.componentDidMount();
            this.getSelectedSitterServices(this.state.sitterID);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  addPets = () => {
    console.log("add pets clicked", anchorId);
    const data = {
      user: user.user.id,
      cust_id: this.state.cust_id,
      pet_id: `${this.state.cust_id}_1`,
      pet_choice: this.state.type_of_pet,
      name: this.state.name,
      breed: this.state.breed,
      profile_photo: this.state.profile_photo,
      dob: this.state.pet_dob,
      nutered: this.state.nutered,
      stay_home_alone_choice: this.state.stay_home_alone_choice,
      stay_home_alone_detail: this.state.stay_home_alone_detail,
      toilet_trained_choice: this.state.toilet_trained_choice,
      toilet_trained_detail: this.state.toilet_trained_detail,
      friendly_with_dogs_choice: this.state.friendly_with_dogs_choice,
      friendly_with_dogs_detail: this.state.friendly_with_dogs_detail,
      friendly_with_cats_choice: this.state.friendly_with_cats_choice,
      friendly_with_cats_detail: this.state.friendly_with_cats_detail,
      friendly_with_strangers_choice: this.state.friendly_with_strangers_choice,
      friendly_with_strangers_detail: this.state.friendly_with_strangers_detail,
      friendly_with_children_choice: this.state.friendly_with_children_choice,
      friendly_with_children_detail: this.state.friendly_with_children_detail,
      chews_things_choice: this.state.chews_things_choice,
      chews_things_detail: this.state.chews_things_detail,
      is_aggressive_choice: this.state.is_aggressive_choice,
      is_aggressive_detail: this.state.is_aggressive_detail,
      additional_info: this.state.additional_info,
      type_of_pet: this.state.type_of_pet,
      insurance_provider: anchorId,
    };
    console.log("data in the backend ", data);
    axios
      .post(`${config.apiUrl}/api/petprofile/?user=${user.user.id}`, data)
      .then((response) => {
        console.log("Add Pets response", response);
        this.getSitterPets();
        this.handlePetModal();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  anchorId = "";
  getSitterDetails = () => {
    console.log("User", user);
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/?user=${user.user.id}`)
      .then((response) => {
        console.log("Response get sitter details &***", response, response.data[0].id);
        this.setState(
          {
            sitter: response.data[0],
            sitterID: response.data[0].id,
            open_spaces_available: response.data[0].open_spaces_available,
            languages_spoke: response.data[0].languages_spoke
              ? response.data[0].languages_spoke
              : [],
            access_to_green_spaces_outside:
              response.data[0].access_to_green_spaces_outside,
            postal_code: response.data[0].postal_code,
            country: response.data[0].country,
            profile_intro: response.data[0].profile_intro,
            profile_bio: response.data[0].profile_bio,
            address: response.data[0].address,
            dob: response.data[0].dob,
            mobile: response.data[0].mobile,
            id_number: response.data[0].id_number,
            id_type: response.data[0].id_type,
            dog_age_preference: response.data[0].dog_age_preference,
            booking_overlap: response.data[0].booking_overlap,
            booking_overlap_parameter:
              response.data[0].booking_overlap_parameter,
            dog_size_preference: response.data[0].dog_size_preference,
            max_sitting_capacity: response.data[0].max_sitting_capacity
              ? parseInt(response.data[0].max_sitting_capacity)
              : 0,
            experience_with_dogs: response.data[0].experience_with_dogs,
            experience_with_dogs_choice:
              response.data[0].experience_with_dogs_choice,
            experience_with_pets: response.data[0].experience_with_pets,
            experience_with_pets_choice:
              response.data[0].experience_with_pets_choice,
            workSchedule:
              response.data[0].work_schedule === "don't work"
                ? "1"
                : response.data[0].work_schedule === "work full time"
                  ? "2"
                  : response.data[0].work_schedule === "work part time"
                    ? "3"
                    : response.data[0].work_schedule === "flexible work"
                      ? "4"
                      : response.data[0].work_schedule === "work from home"
                        ? "5"
                        : response.data[0].work_schedule ===
                          "available mostly on weekends"
                          ? "6"
                          : response.data[0].work_schedule === "others"
                            ? "7"
                            : "0",
            work_schedule_detail: response.data[0].work_schedule_detail,
            type_of_home: response.data[0].home_type,
            home_type_description: response.data[0].home_type_description,
            home_type_area: response.data[0].home_type_area,
            home_type_area_unit: response.data[0].home_type_area_unit,
            reasons_for_joining: response.data[0].reason_for_joining,
            children:
              response.data[0].children === "" ||
                response.data[0].children === null ||
                response.data[0].children === "[]"
                ? []
                : JSON.parse(response.data[0].children),
            children_at_home: response.data[0].children_at_home,
            street_address: response.data[0].street_address,
            house_no: response.data[0].house_no,
            city: response.data[0].city,
            state: response.data[0].state,
            profile_photo: response.data[0].profile_photo,
            id_image: response.data[0].id_image,
            have_insurance: response.data[0].have_insurance,
            insurance_privider: response.data[0].insurance_privider,
            other_privider: response.data[0].other_insurance,

            PetImageFiles: response.data[0].my_photos_with_pets
              ? JSON.parse(response.data[0].my_photos_with_pets)
              : [],
            HomeImageFiles:
              response.data[0].home_images &&
                response.data[0].home_images !== response.data[0].home_type
                ? JSON.parse(response.data[0].home_images)
                : [],
          },
          () => {
            this.getSelectedSitterServices(response.data[0].id);
            openSpaceArray = response.data[0].open_spaces_available;
            languagesSpokeArray = response.data[0].languages_spoke
              ? response.data[0].languages_spoke
              : [];
            // languagesSpokeArray = [];
            SpaceOutsideArray = response.data[0].access_to_green_spaces_outside;
            DogAgeArray = response.data[0].dog_age_preference;
            // BookingOverlapArray = response.data[0].booking_overlap_parameter;
            DogSizeArray = response.data[0].dog_size_preference;
            multiplePetImageUrl =
              response.data[0].my_photos_with_pets !== null ||
                response.data[0].my_photos_with_pets !== "" ||
                response.data[0].my_photos_with_pets !== []
                ? this.state.PetImageFiles
                : [];
            multipleHomeImageUrl =
              response.data[0].home_images !== null ||
                response.data[0].home_images !== "" ||
                response.data[0].home_images !== []
                ? this.state.HomeImageFiles
                : [];
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getSitterPets = () => {
    axios
      .get(`${config.apiUrl}/api/petprofile/?user=${user.user.id}`)
      .then((response) => {
        this.setState({
          sitterPets: response.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getSelectedSitterServices = (id) => {
    fetch(`http://3.215.2.1:8000/api/servicesprice/list?sitters=${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log("service list data", data);
        this.setState({
          selectedSitterServices: data,
          servicesLoading: false,
        });
      });
  };

  getAllServicesList = () => {
    fetch(`http://3.215.2.1:8000/api/serviceslist/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        this.setState({
          AllServices: data,
          servicesLoading: false,
        });
      });
  };

  handleTextChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleRateChange = (event) => {
    let array = this.state.selectedSitterServices;
    array[event.target.name].serviceRate = event.target.value;
    this.setState({
      selectedSitterServices: array,
    });
    console.log("Hanlde gender", this.state.selectedSitterServices);
  };

  handlePetTextChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleNuteredChange = (event) => {
    this.setState({
      nutered: event.target.value,
    });
  };

  handleStayHomeAloneChoiceChange = (event) => {
    this.setState({
      stay_home_alone_choice: event.target.value,
    });
  };

  handleToiletTrainedChoiceChange = (event) => {
    this.setState({
      toilet_trained_choice: event.target.value,
    });
  };

  handleFriendlyWithDogsChoiceChange = (event) => {
    this.setState({
      friendly_with_dogs_choice: event.target.value,
    });
  };

  handleFriendlyWithCatsChoiceChange = (event) => {
    this.setState({
      friendly_with_cats_choice: event.target.value,
    });
  };

  handleFriendlyWithStrangersChoiceChange = (event) => {
    this.setState({
      friendly_with_strangers_choice: event.target.value,
    });
  };

  handleFriendlyWithChildrensChoiceChange = (event) => {
    this.setState({
      friendly_with_children_choice: event.target.value,
    });
  };

  handleChewsThingsChoiceChange = (event) => {
    this.setState({
      chews_things_choice: event.target.value,
    });
  };

  handleAggressiveChoiceChange = (event) => {
    this.setState({
      is_aggressive_choice: event.target.value,
    });
  };

  handleIDTypeChange = (event) => {
    this.setState({
      id_type: event.target.value,
    });
  };

  handleWorkScheduleChange = (event) => {
    this.setState({
      workSchedule: event.target.value,
    });
  };
  handleRadioChange = (event) => {
    console.log("radoio button  children at home", event.target.name);
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleAgeChange = (event) => {
    let array = this.state.children;
    array[event.target.name].age = event.target.value;
    this.setState({
      children: array,
    });
    console.log("Hanlde gender", this.state.children);
  };
  handleGenderChange = (event) => {
    // console.log("gender", index, event.target.name);
    let array = this.state.children;
    array[event.target.name].gender = event.target.value;
    this.setState({
      children: array,
    });
    console.log("Hanlde gender", this.state.children);
  };
  handleEditable = () => {
    this.setState((prevState) => {
      return {
        editable: !prevState.editable,
      };
    });
  };

  handleIdUpload(e) {
    idImageUploader.click();
  }

  handleProfileUpload(e) {
    imageUploader.click();
  }

  fileSelector = (event) => {
    this.setState({
      profileFileName: event.target.files[0].name,
    });
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        this.setState(
          {
            imageDataUrl: e.target.result,
          },
          () => {
            this.handleImageCropperModal();
          }
        );
      };
    }
  };

  fileUploader = (e) => {
    e.persist();
    const file = e.target.files[0];
    console.log("file profile", file);
    let formData = new FormData();
    formData.append("file", file);
    configBucket.dirName = configBucket.dirName + "profilePhotos";

    S3FileUpload.uploadFile(file, configBucket)
      .then((data) => {
        this.setState(
          {
            profile_photo: data.location,
          },
          () => {
            console.log(this.state.profile_photo);
          }
        );
      })
      .catch((err) => console.error(err));
    this.setState({
      profileFileName: e.target.files[0].name,
    });
  };

  onImageCropDone = (e) => {
    e.persist();
    // const file = this.state.croppedImage + "name.png";
    // let formData = new FormData();
    // formData.append("file", file);
    // configBucket.dirName = configBucket.dirName + "profilePhotos";

    // S3FileUpload.uploadFile(formData, configBucket)
    //   .then((data) => {
    //     this.setState(
    //       {
    //         profile_photo: data.location,
    //       },
    //       () => {
    //         console.log(this.state.profile_photo);
    //       }
    //     );
    //   })
    //   .catch((err) => console.error(err));
    this.handleImageCropperModal();
  };

  idFileUploader = (e) => {
    e.persist();
    const file = e.target.files[0];
    let formData = new FormData();
    formData.append("file", file);
    configBucket.dirName = configBucket.dirName + "idPhoto";
    console.log(configBucket);
    S3FileUpload.uploadFile(file, configBucket)
      .then((data) => {
        console.log("bucket_data", data);
        this.setState({
          id_image: data.location,
          idFileName: e.target.files[0].name,
          loadingFiles: false
        });
        console.log("bucket_data", this.state.idFileName);
      })
      .catch((err) => console.error(err));
  };

  handlePetImageUpload = (files) => {
    this.setState({
      PetImageFilesOpen: false,
    });
    files.map((file) => {
      let formData = new FormData();
      formData.append("file", file);
      configBucket.dirName = configBucket.dirName + "photosWithPets";
      S3FileUpload.uploadFile(file, configBucket)
        .then((data) => {
          const obj = {
            location: data.location,
          };
          console.log(obj);
          multiplePetImageUrl.push(obj);
          console.log(multiplePetImageUrl);
          this.setState({
            PetImageFiles: multiplePetImageUrl,
          });
        })
        .catch((err) => console.error(err));
    });

    console.log("files", files, this.state.PetImageFiles);
  };

  handleHomeImageUpload = (files) => {
    this.setState({
      HomeImageFilesOpen: false,
    });
    files.map((file) => {
      let formData = new FormData();
      formData.append("file", file);
      configBucket.dirName = configBucket.dirName + "homePhotos";
      console.log(configBucket);
      S3FileUpload.uploadFile(file, configBucket)
        .then((data) => {
          const obj = {
            location: data.location,
          };
          console.log(obj);
          multipleHomeImageUrl.push(obj);
          this.setState({
            HomeImageFiles: multipleHomeImageUrl,
          });
        })
        .catch((err) => console.error(err));
    });
    console.log("files", files, this.state.HomeImageFiles);
  };

  handlePetMultipleOpen = (event) => {
    this.setState((prevState) => {
      return {
        PetImageFilesOpen: !prevState.PetImageFilesOpen,
      };
    });
  };

  handleHomeMultipleOpen = (event) => {
    this.setState((prevState) => {
      return {
        HomeImageFilesOpen: !prevState.HomeImageFilesOpen,
      };
    });
  };

  multipleS3uploade = () => { };

  handleCancel = () => {
    this.setState({
      editable: false,
    });
  };
  // handleConfirm =()=>{

  // }
  handleSubmit = () => {
    this.handleServicesSave();
    // this.uploadMultipleImage();
    const data = {
      user: user.user.id,
      profile_intro: this.state.profile_intro,
      profile_bio: this.state.profile_bio,
      address: this.state.address,
      postal_code: this.state.postal_code,
      country: this.state.country,
      reasons_for_joining: this.state.reasons_for_joining,
      dob: this.state.dob ? moment(this.state.dob).format("YYYY-MM-DD") : null,
      mobile: this.state.mobile,
      id_type: this.state.id_type,
      id_number: this.state.id_number,
      experience_with_dogs: this.state.experience_with_dogs,
      experience_with_dogs_choice: this.state.experience_with_dogs_choice,
      experience_with_pets: this.state.experience_with_pets,
      experience_with_pets_choice: this.state.experience_with_pets_choice,
      home_type: this.state.type_of_home,
      work_schedule:
        this.state.workSchedule === 1 || this.state.workSchedule === "1"
          ? "don't work"
          : this.state.workSchedule === 2 || this.state.workSchedule === "2"
            ? "work full time"
            : this.state.workSchedule === 3 || this.state.workSchedule === "3"
              ? "work part time"
              : this.state.workSchedule === 4 || this.state.workSchedule === "4"
                ? "flexible work"
                : this.state.workSchedule === 4 || this.state.workSchedule === "5"
                  ? "work from home"
                  : this.state.workSchedule === 4 || this.state.workSchedule === "6"
                    ? "available mostly on weekends"
                    : this.state.workSchedule === 4 || this.state.workSchedule === "7"
                      ? "other"
                      : null,
      work_schedule_detail: this.state.work_schedule_detail,
      type_of_home: this.state.type_of_home,
      children_at_home: this.state.children_at_home,
      children: JSON.stringify(this.state.children),
      home_type_description: this.state.home_type_description,
      home_type_area: this.state.home_type_area,
      home_type_area_unit: this.state.home_type_area_unit,
      open_spaces_available: this.state.open_spaces_available,
      languages_spoke: this.state.languages_spoke,
      access_to_green_spaces_outside: this.state.access_to_green_spaces_outside,
      reason_for_joining: this.state.reasons_for_joining,
      dog_age_preference: this.state.dog_age_preference,
      dog_size_preference: this.state.dog_size_preference,
      max_sitting_capacity:
        this.state.max_sitting_capacity > 0
          ? this.state.max_sitting_capacity
          : null,
      booking_overlap: this.state.booking_overlap,
      booking_overlap_parameter: this.state.booking_overlap_parameter,
      street_address: this.state.street_address,
      house_no: this.state.house_no,
      city: this.state.city,
      state: this.state.state,
      profile_photo: this.state.croppedImage,
      id_image: this.state.id_image,
      my_photos_with_pets: JSON.stringify(this.state.PetImageFiles),
      home_images: JSON.stringify(this.state.HomeImageFiles),
      have_insurance: this.state.have_insurance,
      insurance_privider: this.state.insurance_privider,
      other_insurance: this.state.other_privider
    };
    console.log('insurance provider values is submitting ', data)
    axios
      .put(`${config.apiUrl}/api/sitterprofile/${this.state.sitterID}`, data)
      .then((response) => {
        console.log("response submitting ***********", response);
        if (response.status === 200) {
          this.handleEditable();
          this.componentDidMount();
          this.setState({ isConfirm: true });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  handleServicesSave = () => {
    this.state.selectedSitterServices.map((d) => {
      axios
        .put(`${config.apiUrl}/api/servicesprice/${d.id}`, {
          serviceRate: d.serviceRate,
          sitters: d.sitters.id,
          service_id: d.service_id.id,
        })
        .then((response) => {
          console.log(response);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };
  // uploadMultipleImage = () => {
  //   this.state.PetImageFiles.map((file) => {
  //     let formData = new FormData();
  //     formData.append("file", file);
  //     configBucket.dirName = configBucket.dirName + "photosWithPets";
  //     console.log(configBucket);
  //     S3FileUpload.uploadFile(file, configBucket)
  //       .then((data) => {
  //         console.log("bucket_data", data);
  //         this.setState({
  //           id_image: data.location,
  //           idFileName: e.target.files[0].name,
  //         });
  //         console.log("bucket_data", this.state.idFileName);
  //       })
  //       .catch((err) => console.error(err));
  //   });
  // };
  handleServiceDelete = (params, close) => (e) => {
    console.log("Delete", params.id);
    axios
      .delete(`${config.apiUrl}/api/servicesprice/${params.id}`)
      .then((response) => {
        console.log(response);
        close();
        this.getSelectedSitterServices(this.state.sitterID);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  selectedSitterServices = () => {
    // return (
    //   sitterServices &&
    //   sitterServices.filter((service) => {
    //     if (service.sitters && service.sitters.user === sitter.user) {
    //       return service;
    //     }
    //   })
    // );
  };

  getSitterWorkSchedule = () => {
    // if (sitter.work_schedule === 1) return "Full Time Work";
    // else if (sitter.work_schedule === 2) return "Part Time Work";
    // else if (sitter.work_schedule === 3) return "Flexible Work";
    // else return "Available only on weekends";
  };

  getDogAgePreference = () => {
    // var preference = "";
    // if (sitter && sitter.dog_age_preference) {
    //   sitter.dog_age_preference.map((age) => {
    //     if (age === 1) {
    //       preference = preference + "Puppy (2 - 6months)";
    //     } else if (age === 2) {
    //       preference = preference + "Puppy (6 - 24months)";
    //     } else if (age === 3) {
    //       preference = preference + "Adult Dog (2years +)";
    //     } else {
    //       preference = preference + "Elderly Dog (10 years +)";
    //     }
    //   });
    //   return preference;
    // } else {
    //   return "-";
    // }
  };

  goToDogProfile = (pet) => {
    window.location.href =
      pet.type_of_pet === "Dog"
        ? `/dog-dashboard/${pet.id}`
        : pet.type_of_pet === "Cat"
          ? `/cat-dashboard/${pet.id}`
          : `/otherpet-dashboard/${pet.id}`;
  };


  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Loader display={false} />
        <div className={classes.whiteBGWrapper}>
          <Grid container>
            <Grid item xs={10}>
              <MainProfile
                other_privider={this.state.other_privider}
                handleOtherInsuranceChange={this.handleOtherInsuranceChange}
                loadingFiles={this.state.loadingFiles}
                servicesLoading={this.state.servicesLoading}
                handleChangeMobileCode={this.handleChangeMobileCode}
                handleServiceListTabChange={this.handleServiceListTabChange}
                serviceListTabValue={this.state.serviceListTabValue}
                handleHaveInsurance={this.handleHaveInsurance}
                have_insurance={this.state.have_insurance}
                handleInsuranceMenuChange={this.handleInsuranceMenuChange}
                insurance_privider={this.state.insurance_privider}
                classes={classes}
                tabValue={this.state.tabValue}
                serviceTabValue={this.state.serviceTabValue}
                sitter={this.state.sitter}
                editable={this.state.editable}
                handleTabChange={this.handleTabChange}
                selectedSitterServices={this.state.selectedSitterServices}
                handleModal={this.handleModal}
                handleEditable={this.handleEditable}
                handleSubmit={this.handleSubmit}
                handleCancel={this.handleCancel}
                handleTextChange={this.handleTextChange}
                profile_intro={this.state.profile_intro}
                profile_bio={this.state.profile_bio}
                address={this.state.address}
                dob={this.state.dob}
                mobile={this.state.mobile}
                postal_code={this.state.postal_code}
                country={this.state.country}
                reasons_for_joining={this.state.reasons_for_joining}
                id_type={this.state.id_type}
                id_number={this.state.id_number}
                experience_with_dogs={this.state.experience_with_dogs}
                experience_with_dogs_choice={
                  this.state.experience_with_dogs_choice
                }
                experience_with_pets={this.state.experience_with_pets}
                experience_with_pets_choice={
                  this.state.experience_with_pets_choice
                }
                workSchedule={this.state.workSchedule}
                work_schedule_detail={this.state.work_schedule_detail}
                type_of_home={this.state.type_of_home}
                children={this.state.children}
                children_at_home={this.state.children_at_home}
                home_type_description={this.state.home_type_description}
                home_type_area={this.state.home_type_area}
                home_type_area_unit={this.state.home_type_area_unit}
                open_spaces_available={this.state.open_spaces_available}
                languages_spoke={this.state.languages_spoke}
                access_to_green_spaces_outside={
                  this.state.access_to_green_spaces_outside
                }
                street_address={this.state.street_address}
                house_no={this.state.house_no}
                city={this.state.city}
                state={this.state.state}
                reason_for_joining={this.state.reasons_for_joining}
                dog_age_preference={this.state.dog_age_preference}
                dog_size_preference={this.state.dog_size_preference}
                max_sitting_capacity={this.state.max_sitting_capacity}
                confirmModalOpen={this.state.confirmModalOpen}
                isConfirm={this.state.isConfirm}
                booking_overlap={this.state.booking_overlap}
                booking_overlap_parameter={this.state.booking_overlap_parameter}
                profileFileName={this.state.profileFileName}
                idFileName={this.state.idFileName}
                id_image={this.state.id_image}
                PetImageFiles={this.state.PetImageFiles}
                HomeImageFiles={this.state.HomeImageFiles}
                PetImageFilesOpen={this.state.PetImageFilesOpen}
                HomeImageFilesOpen={this.state.HomeImageFilesOpen}
                handleIDTypeChange={this.handleIDTypeChange}
                fileUploader={this.fileSelector}
                handleIdUpload={this.handleIdUpload}
                idFileUploader={this.idFileUploader}
                handlePetImageUpload={this.handlePetImageUpload}
                handleHomeImageUpload={this.handleHomeImageUpload}
                handlePetMultipleOpen={this.handlePetMultipleOpen}
                handleHomeMultipleOpen={this.handleHomeMultipleOpen}
                uploadProfileImage={this.handleProfileUpload}
                handleWorkScheduleChange={this.handleWorkScheduleChange}
                handleRadioChange={this.handleRadioChange}
                handleOpenSpaceChange={this.handleOpenSpaceChange}
                handleLanguageChange={this.handleLanguageChange}
                handleSpaceOutsideChange={this.handleSpaceOutsideChange}
                handleDogAgePreference={this.handleDogAgePreference}
                handleMaxSittingDecrement={this.handleMaxSittingDecrement}
                handleMaxSittingIncrement={this.handleMaxSittingIncrement}
                handleChildrenDecrement={this.handleChildrenDecrement}
                handleChildrenIncrement={this.handleChildrenIncrement}
                handleConfirmModal={this.handleConfirmModal}
                handleDogSizePreferences={this.handleDogSizePreferences}
                handleGenderChange={this.handleGenderChange}
                handleAgeChange={this.handleAgeChange}
                handleRateChange={this.handleRateChange}
                handleServiceDelete={this.handleServiceDelete}
                handleBookingOverlapParam={this.handleBookingOverlapParam}
              />
            </Grid>
            <Grid item xs={2}>
              <div className={classes.myPets}>
                <Typography
                  color="secondary"
                  style={{
                    fontFamily: "Fredoka One",
                    fontSize: 20,
                    marginBottom: 24,
                  }}
                >
                  My Pets
                </Typography>
                {this.state.sitterPets &&
                  this.state.sitterPets.map((pet, index, props) => {
                    return (
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                          marginBottom: 18,
                        }}
                      >
                        <Avatar
                          alt="Pet"
                          src={pet.profile_photo ? pet.profile_photo : dogImage}
                          style={{ width: 72, height: 72 }}
                          onClick={() => this.handlePetViewModal(pet.id)}
                          component={RouterLink}
                        // to={
                        //   pet.type_of_pet === "Dog"
                        //     ? `/dog-dashboard/${pet.id}`
                        //     : pet.type_of_pet === "Cat"
                        //     ? `/cat-dashboard/${pet.id}`
                        //     : `/otherpet-dashboard/${pet.id}`
                        // }
                        />
                        <p
                          style={{
                            color: "#F58220",
                            fontSize: 18,
                            fontFamily: "Kanit",
                            textAlign: "center",
                          }}
                        >
                          {pet.name}
                        </p>
                      </div>
                    );
                  })}

                <div>
                  <Button
                    color="primary"
                    className={classes.orangeBtnOutlinedCircle}
                    onClick={this.handlePetModal}
                  >
                    <AddIcon />
                  </Button>
                  <p
                    style={{
                      fontFamily: "Kanit",
                      fontSize: 20,
                      textAlign: "center",
                      color: "#F58220",
                      margin: 0,
                      marginBottom: 24,
                    }}
                  >
                    Create New
                  </p>
                </div>
              </div>
            </Grid>
            {/* Service Modal */}
            <Dialog
              aria-labelledby="customized-dialog-title"
              className={classes.modal}
              open={this.state.modalOpen}
              onClose={this.handleModal}
              fullWidth={true}
              maxWidth={"md"}
              style={{ minWidth: 600 }}
            >
              <DialogTitle
                id="customized-dialog-title"
                onClose={this.handleModal}
              >
                Add Services
                <IconButton
                  aria-label="close"
                  className={classes.closeButton}
                  onClick={this.handleModal}
                >
                  <CloseIcon />
                </IconButton>
              </DialogTitle>
              <DialogContent dividers>
                <Grid container>
                  {/* {console.log("Services",this.state.AllServices)} */}
                  <Grid item lg={12}>
                    <Tabs
                      value={this.state.serviceTabValue}
                      onChange={this.handleServiceTabChange}
                      aria-label="login & register tabs"
                      classes={{ indicator: classes.indicatorTab }}
                    >
                      <Tab
                        label="Core Services"
                        {...a11yProps(0)}
                        className={classes.profileTabs}
                      />
                      <Tab
                        label="Add-on Services"
                        {...a11yProps(1)}
                        className={classes.profileTabs}
                      />
                    </Tabs>
                  </Grid>
                </Grid>
                <TabPanel value={this.state.serviceTabValue} index={0}>
                  <Grid container style={{ width: 700 }}>
                    {this.state.AllServices.map((services) => {
                      if (
                        services.service_type === "core services" &&
                        !this.state.selectedSitterServices.some(
                          (el) => el.service_id.id === services.id
                        )
                      ) {
                        // console.log("State", this.state.AllServices);
                        return (
                          <Grid item lg={4}>
                            <div
                              style={{
                                border: "1px solid #E8770E",
                                borderRadius: 4,
                                height: 220,
                                padding: 18,
                                margin: 8,
                                // opacity: `${
                                //   (
                                //     this.state.selectedSitterServices
                                //       ? this.state.selectedSitterServices.some(
                                //           (el) =>
                                //             el.service_id.id === services.id
                                //         )
                                //       : false
                                //   )
                                //     ? "0.5"
                                //     : "1"
                                // }`,
                              }}
                            >
                              <div style={{ display: "flex" }}>
                                <input
                                  type="checkbox"
                                  value={services.id}
                                  name={services.id}
                                  style={{ margin: 0, marginRight: 8 }}
                                  defaultChecked={
                                    this.state.selectedSitterServices
                                      ? this.state.selectedSitterServices.some(
                                        (el) =>
                                          el.service_id.id === services.id
                                      )
                                      : false
                                  }
                                  onChange={(e) => {
                                    if (e.target.checked === true) {
                                      let data = {
                                        serviceRate: this.state
                                          .selectedServiceRate,
                                        sitters: this.state.sitterID,
                                        service_id: parseInt(e.target.value),
                                      };
                                      selectedServiceArray.push(data);
                                    } else {
                                      selectedServiceArray = selectedServiceArray.filter(
                                        (item) =>
                                          item.service_id !== e.target.value
                                      );
                                      // selectedServiceArray.pop(e.target.value);
                                    }
                                    this.setState({
                                      selectedServiceArray: selectedServiceArray,
                                    });
                                  }}
                                />

                                <p
                                  style={{
                                    color: "#3EB3CD",
                                    fontSize: 16,
                                    fontFamily: "Fredoka One",
                                    margin: 0,
                                    textAlign: "left",
                                  }}
                                >
                                  {services.service_name}
                                </p>
                              </div>
                              <div
                                style={{
                                  marginTop: 18,
                                  marginBottom: 18,
                                }}
                              >
                                <p
                                  style={{
                                    color: "#979797",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    textAlign: "left",
                                  }}
                                >
                                  {services.service_description}
                                </p>
                              </div>
                              <div>
                                <CssTextField
                                  id={services.id}
                                  label="Service Rate"
                                  type="number"
                                  name="selectedServiceRate"
                                  style={{
                                    width: "100%",
                                  }}
                                  onChange={(e) => {
                                    console.log(
                                      "Array",
                                      this.state.selectedServiceArray
                                    );
                                    if (selectedServiceArray) {
                                      selectedServiceArray = this.state.selectedServiceArray.map(
                                        (service) => {
                                          if (
                                            service.service_id &&
                                            service.service_id ===
                                            parseInt(e.target.id)
                                          ) {
                                            service.serviceRate =
                                              e.target.value;
                                            return service;
                                          } else return service;
                                        }
                                      );
                                    }
                                    // selectedServiceRate;
                                    this.setState({
                                      selectedServiceRate: e.target.value,
                                    });
                                  }}
                                />
                              </div>
                            </div>
                          </Grid>
                        );
                      } else return <div />;
                    })}
                  </Grid>
                </TabPanel>
                <TabPanel value={this.state.serviceTabValue} index={1}>
                  <Grid container style={{ width: 700 }}>
                    {this.state.AllServices.map((services) => {
                      if (
                        services.service_type === "add-on Services" &&
                        !this.state.selectedSitterServices.some(
                          (el) => el.service_id.id === services.id
                        )
                      )
                        return (
                          <Grid item lg={4}>
                            <div
                              style={{
                                border: "1px solid #E8770E",
                                borderRadius: 4,
                                height: 220,
                                padding: 18,
                                margin: 8,
                                // opacity: `${
                                //   (
                                //     this.state.selectedSitterServices
                                //       ? this.state.selectedSitterServices.some(
                                //           (el) =>
                                //             el.service_id.id === services.id
                                //         )
                                //       : false
                                //   )
                                //     ? "0.5"
                                //     : "1"
                                // }`,
                              }}
                            >
                              <div style={{ display: "flex" }}>
                                <input
                                  type="checkbox"
                                  value={services.id}
                                  name={services.id}
                                  defaultChecked={
                                    this.state.selectedSitterServices
                                      ? this.state.selectedSitterServices.some(
                                        (el) =>
                                          el.service_id.id === services.id
                                      )
                                      : false
                                  }
                                  style={{ margin: 0, marginRight: 8 }}
                                  onChange={(e) => {
                                    if (e.target.checked === true) {
                                      let data = {
                                        serviceRate: this.state
                                          .selectedServiceRate,
                                        sitters: this.state.sitterID,
                                        service_id: parseInt(e.target.value),
                                      };
                                      selectedServiceArray.push(data);
                                    } else {
                                      selectedServiceArray = selectedServiceArray.filter(
                                        (item) =>
                                          item.service_id !== e.target.value
                                      );
                                      // selectedServiceArray.pop(e.target.value);
                                    }
                                    this.setState({
                                      selectedServiceArray: selectedServiceArray,
                                    });
                                  }}
                                />

                                <p
                                  style={{
                                    color: "#3EB3CD",
                                    fontSize: 16,
                                    fontFamily: "Fredoka One",
                                    margin: 0,
                                    textAlign: "left",
                                  }}
                                >
                                  {services.service_name}
                                </p>
                              </div>
                              <div
                                style={{
                                  marginTop: 18,
                                  marginBottom: 18,
                                }}
                              >
                                <p
                                  style={{
                                    color: "#979797",
                                    fontSize: 14,
                                    fontFamily: "Kanit",
                                    textAlign: "left",
                                  }}
                                >
                                  {services.service_description}
                                </p>
                              </div>
                              <div>
                                <CssTextField
                                  id={services.id}
                                  label="Service Rate"
                                  type="number"
                                  name="selectedServiceRate"
                                  style={{
                                    width: "100%",
                                  }}
                                  onChange={(e) => {
                                    console.log(
                                      "Array",
                                      this.state.selectedServiceArray,
                                      e.target
                                    );
                                    if (selectedServiceArray) {
                                      selectedServiceArray = this.state.selectedServiceArray.map(
                                        (service) => {
                                          if (
                                            service.service_id &&
                                            service.service_id ===
                                            parseInt(e.target.id)
                                          ) {
                                            service.serviceRate =
                                              e.target.value;
                                            console.log(
                                              "target",
                                              e.target.value
                                            );
                                            return service;
                                          } else return service;
                                        }
                                      );
                                      this.setState({
                                        selectedServiceRate: e.target.value,
                                      });
                                    }
                                  }}
                                />
                              </div>
                            </div>
                          </Grid>
                        );
                      else return <div />;
                    })}
                  </Grid>
                </TabPanel>
                {/* </Grid> */}
                <Grid container>
                  <Grid item xs={12} style={{ marginTop: 24 }}>
                    <div
                      style={{
                        width: "100%",
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      {/* <p
                        style={{
                          fontFamily: "Kanit",
                          color: "#E8770E",
                          textAlign: "center",
                          fontSize: 14,
                        }}
                      >
                        *required for you to appear in search results
                      </p> */}
                    </div>
                  </Grid>
                </Grid>
              </DialogContent>
              <DialogActions style={{ margin: "auto" }}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.orangeBtn}
                  onClick={this.addService}
                >
                  Add Service
                </Button>
                {/* <Button autoFocus onClick={this.handleModal} color="primary">
                  Close
                </Button> */}
              </DialogActions>
            </Dialog>
            {/* <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={this.state.modalOpen}
              onClose={this.handleModal}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
              className={classes.modal}
            >
              <Fade in={this.state.modalOpen}>
                
              </Fade>
            </Modal> */}

            {/* View Pet Modal */}
            <Dialog
              onClose={this.handlePetViewModal}
              aria-labelledby="customized-dialog-title"
              open={this.state.openPetView}
              fullWidth={true}
              maxWidth={"lg"}
            // className={classes.petViewModal}
            >
              <DialogTitle
                id="customized-dialog-title"
                onClose={this.handlePetViewModal}
              >
                View Pet
                <IconButton
                  aria-label="close"
                  className={classes.closeButton}
                  onClick={this.handlePetViewModal}
                >
                  <CloseIcon />
                </IconButton>
              </DialogTitle>
              <DialogContent dividers>
                <DogProfileDashboard
                  petId={this.state.petViewId}
                  getSitterPets={this.getSitterPets}
                  editable={true}
                />
              </DialogContent>
              <DialogActions>
                <Button
                  autoFocus
                  onClick={this.handlePetViewModal}
                  color="primary"
                >
                  Close
                </Button>
              </DialogActions>
            </Dialog>

            {/* Add Pet Modal */}
            <Dialog
              onClose={this.handlePetModal}
              aria-labelledby="customized-dialog-title"
              open={this.state.petModal}
              // className={classes.petViewModal}
              fullWidth={true}
              maxWidth={"md"}
            >
              <DialogTitle
                id="customized-dialog-title"
                onClose={this.handlePetModal}
              >
                Add Pet {this.state.type_of_pet ? this.state.type_of_pet : ""}
                <IconButton
                  aria-label="close"
                  className={classes.closeButton}
                  onClick={this.handlePetModal}
                >
                  <CloseIcon />
                </IconButton>
              </DialogTitle>
              <DialogContent dividers>
                {this.state.AddPetStep !== true ? (
                  <div className={classes.center}>
                    <Grid container>
                      <Grid item xs={4}>
                        <div
                          style={{
                            border:
                              this.state.border1 === false
                                ? "1px solid #c4c4c4"
                                : "1px solid #E8770E",
                            borderRadius: 4,
                            padding: 18,
                            margin: 8,
                            minHeight: 102,
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            this.setState({
                              type_of_pet: "dog",
                              border1: !this.state.border1,
                              border2: false,
                              border3: false,
                            });
                          }}
                        >
                          <img src={Dog} />
                        </div>
                      </Grid>
                      <Grid item xs={4}>
                        <div
                          style={{
                            border:
                              this.state.border2 === false
                                ? "1px solid #c4c4c4"
                                : "1px solid #E8770E",
                            borderRadius: 4,
                            padding: 18,
                            margin: 8,
                            minHeight: 102,
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            this.setState({
                              type_of_pet: "cat",
                              border2: !this.state.border2,
                              border1: false,
                              border3: false,
                            });
                          }}
                        >
                          <img src={Cat} />
                        </div>
                      </Grid>
                      <Grid item xs={4}>
                        <div
                          style={{
                            border:
                              this.state.border3 === false
                                ? "1px solid #c4c4c4"
                                : "1px solid #E8770E",
                            borderRadius: 4,
                            padding: 18,
                            margin: 8,
                            minHeight: 102,
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            this.setState({
                              type_of_pet: "other animal",
                              border3: !this.state.border3,
                              border1: false,
                              border2: false,
                            });
                          }}
                        >
                          <img src={Other} />
                        </div>
                      </Grid>
                      <Grid item xs={12} style={{ marginTop: 24 }}>
                        <div
                          style={{
                            width: "100%",
                            display: "flex",
                            flexDirection: "column",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          {/* <Button
                            variant="contained"
                            color="primary"
                            className={classes.orangeBtn}
                            onClick={() => {
                              if (this.state.type_of_pet === "") {
                                alert(
                                  "Please Select type of pet you want to add."
                                );
                              } else {
                                this.setState({
                                  AddPetStep: true,
                                });
                              }
                            }}
                          >
                            Add Your Pet
                          </Button> */}
                        </div>
                      </Grid>
                    </Grid>
                  </div>
                ) : (
                    <div>
                      <Grid container>
                        <Grid item xs={12}>
                          <div>
                            {/* <div
                          style={{
                            display: "flex",
                          }}
                        >
                          <CssTextField
                            id="cur-name"
                            label="Pet Name"
                            type="text"
                            name="name"
                            style={{
                              width: "100%",
                              margin: "0 8px",
                            }}
                            onChange={this.handlePetTextChange}
                          />
                          <CssTextField
                            id="cur-name"
                            label="Pet breed"
                            type="text"
                            name="breed"
                            style={{
                              width: "100%",
                              margin: "0 8px",
                            }}
                            onChange={this.handlePetTextChange}
                          />
                          <CssTextField
                            id="cur-name"
                            label="Pet 
                            Date of Birth"
                            type="text"
                            name="pet_dob"
                            style={{
                              width: "100%",
                              margin: "0 8px",
                            }}
                            onChange={this.handlePetTextChange}
                          />
                        </div>
                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Nutered
                          </Typography>
                          <RadioGroup
                            aria-label="nutered"
                            name="nutered"
                            value={this.state.nutered}
                            onChange={this.handleNuteredChange}
                            row
                          >
                            <FormControlLabel
                              value={true}
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value={false}
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                        </div>
                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Stay at home alone
                          </Typography>
                          <RadioGroup
                            aria-label="stay_home_alone_choice"
                            name="stay_home_alone_choice"
                            value={this.state.stay_home_alone_choice}
                            onChange={this.handleStayHomeAloneChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="stay_home_alone_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Toilet trained
                          </Typography>
                          <RadioGroup
                            aria-label="toilet_trained_choice"
                            name="toilet_trained_choice"
                            value={this.state.toilet_trained_choice}
                            onChange={this.handleToiletTrainedChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="other"
                              control={<Radio color="primary" />}
                              label="Other"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="toilet_trained_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with dogs
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_dogs_choice"
                            name="friendly_with_dogs_choice"
                            value={this.state.friendly_with_dogs_choice}
                            onChange={this.handleFriendlyWithDogsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_dogs_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with cats
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_cats_choice"
                            name="friendly_with_cats_choice"
                            value={this.state.friendly_with_cats_choice}
                            onChange={this.handleFriendlyWithCatsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_cats_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with Strangers
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_strangers_choice"
                            name="friendly_with_strangers_choice"
                            value={this.state.friendly_with_strangers_choice}
                            onChange={
                              this.handleFriendlyWithStrangersChoiceChange
                            }
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_strangers_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Friendly with Children
                          </Typography>
                          <RadioGroup
                            aria-label="friendly_with_children_choice"
                            name="friendly_with_children_choice"
                            value={this.state.friendly_with_children_choice}
                            onChange={
                              this.handleFriendlyWithChildrensChoiceChange
                            }
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="friendly_with_children_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Chews Things
                          </Typography>
                          <RadioGroup
                            aria-label="chews_things_choice"
                            name="chews_things_choice"
                            value={this.state.chews_things_choice}
                            onChange={this.handleChewsThingsChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="1"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="2"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="3"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="chews_things_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Aggressive
                          </Typography>
                          <RadioGroup
                            aria-label="is_aggressive_choice"
                            name="is_aggressive_choice"
                            value={this.state.is_aggressive_choice}
                            onChange={this.handleAggressiveChoiceChange}
                            row
                          >
                            <FormControlLabel
                              value="yes"
                              control={<Radio color="primary" />}
                              label="Yes"
                            />
                            <FormControlLabel
                              value="no"
                              control={<Radio color="primary" />}
                              label="No"
                            />
                            <FormControlLabel
                              value="sometimes"
                              control={<Radio color="primary" />}
                              label="Sometimes"
                            />
                          </RadioGroup>
                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Details"
                              type="text"
                              name="is_aggressive_detail"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div>

                        <div>
                          <Typography
                            style={{
                              fontSize: 22,
                              fontWeight: 500,
                              paddingRight: "3rem",
                              textAlign: "left",
                              marginTop: 18,
                            }}
                            color="secondary"
                          >
                            Additional Info
                          </Typography>

                          <div style={{ marginTop: 18 }}>
                            <CssTextField
                              id="cur-name"
                              label="Additional Info"
                              type="text"
                              name="additional_info"
                              style={{
                                width: "100%",
                                margin: "0 8px",
                              }}
                              onChange={this.handlePetTextChange}
                            />
                          </div>
                        </div> */}
                          </div>
                          <AddPetComponent
                            handleModal={this.handlePetModal}
                            getSitterPets={this.getSitterPets}
                            type_of_pet={this.state.type_of_pet}
                            getAllData={this.componentDidMount}
                          />
                        </Grid>
                        <Grid item xs={12} style={{ marginTop: 24 }}>
                          <div
                            style={{
                              width: "100%",
                              display: "flex",
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          ></div>
                        </Grid>
                      </Grid>
                    </div>
                  )}
              </DialogContent>
              <DialogActions>
                {this.state.AddPetStep !== true ? (
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtn}
                    onClick={() => {
                      if (this.state.type_of_pet === "") {
                        alert("Please Select type of pet you want to add.");
                      } else {
                        this.setState({
                          AddPetStep: true,
                        });
                      }
                    }}
                  >
                    Next
                  </Button>
                ) : (
                    <Button
                      color="primary"
                      onClick={() => {
                        this.setState({
                          AddPetStep: false,
                        });
                      }}
                    >
                      Back
                    </Button>
                  )}
                <Button onClick={this.handlePetModal} color="primary">
                  Close
                </Button>
              </DialogActions>
            </Dialog>

            {/* Image Cropper Modal */}

            {/* Image Cropper Modal */}

            <Dialog
              onClose={this.handleImageCropperModal}
              aria-labelledby="customized-dialog-title"
              open={this.state.imageCropperModal}
              fullWidth={false}
              maxWidth={"lg"}
              className={classes.petViewModal}
            >
              <DialogTitle
                id="customized-dialog-title"
                onClose={this.handleImageCropperModal}
              >
                <IconButton
                  aria-label="close"
                  className={classes.closeButton}
                  onClick={this.handleImageCropperModal}
                >
                  <CloseIcon />
                </IconButton>
              </DialogTitle>
              <DialogContent dividers>
                <div className={classes.cropperPaper}>
                  <div className={classes.cropContainer}>
                    <Cropper
                      image={this.state.imageDataUrl}
                      crop={this.state.crop}
                      zoom={this.state.zoom}
                      aspect={this.state.aspect}
                      onCropChange={this.onCropChange}
                      onCropComplete={this.onCropComplete}
                      onZoomChange={this.onZoomChange}
                    />
                  </div>
                </div>
              </DialogContent>
              <DialogActions>
                <div className={classes.sliderControls}>
                  <Slider
                    value={this.state.zoom}
                    min={1}
                    max={3}
                    step={0.1}
                    aria-labelledby="Zoom"
                    onChange={(e, zoom) => this.onZoomChange(zoom)}
                    classes={{ container: "slider" }}
                  />
                </div>
                <Button
                  autoFocus
                  onClick={this.onImageCropDone}
                  color="primary"
                >
                  Done
                </Button>
                <Button
                  autoFocus
                  onClick={this.handleImageCropperModal}
                  color="primary"
                >
                  Close
                </Button>
              </DialogActions>
            </Dialog>
          </Grid>
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(DashboardProfile);
