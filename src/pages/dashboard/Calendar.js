import React, { Children } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import {
  Typography,
  Box,
  Grid,
  Button,
  RadioGroup,
  Radio,
  FormControlLabel,
} from "@material-ui/core";
import { Calendar, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Toggle from "react-toggle";
import "react-toggle/style.css"; // for ES6 modules
import moment from "moment";
import axios from "axios";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const localizer = momentLocalizer(moment);
let calendarEvent = [];

const styles = (theme) => ({
  root: {
    marginTop: 62,
    flexGrow: 1,
    minHeight: "80vh",
    backgroundColor: "#ffffff",
    padding: 24,
  },
  calendar: {
    width: "100%",
    height: "100%",
    backgroundColor: "white",
    padding: 54,
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    padding: theme.spacing(0.5, 2),
    float: "right",
  },
  orangeBtnOutlined: {
    color: "#F58220",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    border: "1px solid #F58220",
    width: "max-content",
    padding: theme.spacing(0.5, 2),
    backgroundColor: "#ffffff",
    marginRight: 18,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  toggleStyle: {
    "& .react-toggle--checked .react-toggle-track": {
      backgroundColor: "#E8770E !important",
    },
  },
});

const date = new Date();

class CalendarView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      selectedDate: date,
      selectedVacationEndDate: null,
      selectedVacationStartDate: null,
      modalVacationOpen: false,
      toggle: false,
      events: [
        {
          start: moment(date).toDate(),
          end: moment(date).toDate(),
          title: "",
        },
      ],
      calenderId: null,
      vacationEndDate: null,
    };
  }

  componentDidMount = () => {
    this.getSitterProfile();
  };

  onChangeDate = (e) => {
    this.setState({
      selectedDate: e,
    });
  };

  removeDate = () => {
    console.log(this.state.events);
    let ev = this.state.events;
    const index = ev
      .map((x) => {
        return x.id;
      })
      .indexOf(this.state.calenderId);
    ev.splice(index, 1);

    this.setState(
      {
        events: ev,
      },
      () => {
        console.log(ev);
        this.handleSubmit();
        this.handleModal();
      }
    );
  };

  handleToggleChange = (event) => {
    this.setState(
      {
        toggle: !this.state.toggle,
        vacationMode: !this.state.toggle,
      },
      () => {
        if (this.state.toggle === true) {
          this.handleVacationModal();
        } else return null;
      }
    );
  };

  getSitterProfile = () => {
    const user = JSON.parse(localStorage.getItem("user"));
    const id = user.user.id;

    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/?user=${id}`)
      .then((response) => {
        console.log("api res", response.data);
        this.setState(
          {
            sitterData: response.data[0],
            sitterID: response.data[0].id,
            vacationMode:
              response.data[0].vacation_mode === null
                ? false
                : response.data[0].vacation_mode.onVacation,
            vacationEndDate:
              response.data[0].vacation_mode === null
                ? null
                : response.data[0].vacation_mode.endDate,
            toggle:
              response.data[0].vacation_mode === null
                ? false
                : response.data[0].vacation_mode.onVacation,
            events:
              response.data[0].events === null ? [] : response.data[0].events,
          },
          () => {
            calendarEvent = [];
            if (this.state.events !== "" || this.state.events !== null) {
              let count = 0;
              calendarEvent = this.state.events;
              calendarEvent = calendarEvent.map((event) => {
                event.id = count++;
                return event;
              });
              console.log("Hey api", calendarEvent);
            }
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleSubmit = () => {
    console.log(this.state.events);
    const data = {
      user: this.state.sitterData.user.id,
      cust_id: this.state.sitterData.cust_id,
      dob: this.state.sitterData.dob,
      mobile: this.state.sitterData.mobile,
      profile_photo: this.state.sitterData.profile_photo,
      profile_intro: this.state.sitterData.profile_intro,
      profile_bio: this.state.sitterData.profile_bio,
      address: this.state.sitterData.address,
      postal_code: this.state.sitterData.postal_code,
      id_type: this.state.sitterData.id_type,
      id_number: this.state.sitterData.id_number,
      id_image: this.state.sitterData.id_image,
      experience_with_dogs: this.state.sitterData.experience_with_dogs,
      experience_with_pets: this.state.sitterData.experience_with_pets,
      work_schedule: this.state.sitterData.work_schedule,
      work_schedule_detail: this.state.sitterData.work_schedule_detail,
      home_type: this.state.sitterData.home_type,
      home_images: this.state.sitterData.home_images,
      home_type_description: this.state.sitterData.home_type_description,
      home_type_area: this.state.sitterData.home_type_area,
      home_proof_of_residence: this.state.sitterData.home_proof_of_residence,
      family_description: this.state.sitterData.family_description,
      children_at_home: this.state.sitterData.children_at_home,
      max_sitting_capacity: this.state.sitterData.max_sitting_capacity,
      booking_overlap: this.state.sitterData.booking_overlap,
      booking_overlap_parameter: this.state.sitterData
        .booking_overlap_parameter,
      home_insurance: this.state.sitterData.home_insurance,
      third_party_liability_available: this.state.sitterData
        .third_party_liability_available,
      insurance_provider: this.state.sitterData.insurance_provider,
      insurance_details: this.state.sitterData.insurance_details,
      insurance_proof: this.state.sitterData.insurance_proof,
      reason_for_joining: this.state.sitterData.reason_for_joining,
      experience_with_dogs_choice: this.state.sitterData
        .experience_with_dogs_choice,
      experience_with_pets_choice: this.state.sitterData
        .experience_with_dogs_choice,
      home_type_area_unit: this.state.sitterData.home_type_area_unit,
      children: this.state.sitterData.children,
      availability: this.state.sitterData.availability,
      events: this.state.events.length === 0 ? null : this.state.events,
      vacation_mode: {
        onVacation: this.state.vacationMode,
        startDate:
          this.state.vacationMode === true
            ? this.state.selectedVacationStartDate
            : null,
        endDate:
          this.state.vacationMode === true
            ? this.state.selectedVacationEndDate
            : null,
      },
      has_pet_choice: this.state.sitterData.has_pet_choice,
      open_spaces_available: this.state.sitterData.open_spaces_available,
      access_to_green_spaces_outside: this.state.sitterData
        .access_to_green_spaces_outside,
      dog_size_preference: this.state.sitterData.dog_size_preference,
      dog_age_preference: this.state.sitterData.dog_age_preference,
    };
    console.log("API data", data.events);
    const user = JSON.parse(localStorage.getItem("user"));
    const id = user.user.id;

    axios
      .put(`${config.apiUrl}/api/sitterprofile/${this.state.sitterID}`, data)
      .then((response) => {
        this.getSitterProfile();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleSelect = (event) => {
    // const title = window.prompt("New Event name");
    var DateTime = new Date(event.end);
    DateTime.setHours(23, 59, 59, 0);
    console.log(DateTime);
    this.setState({
      selectedStartDate: event.start,
      selectedEndDate: DateTime,
      availability: event.title === "Available" ? "yes" : "no",
      calenderId: event.id,
    });
    this.handleModal();
    // this.setState({
    //   events: [
    //     ...this.state.events,
    //     {
    //       start,
    //       end,
    //       title,
    //     },
    //   ],
    // });
  };

  handleModal = () => {
    this.setState((prevState) => {
      return {
        modalOpen: !prevState.modalOpen,
      };
    });
  };

  handleVacationModal = () => {
    this.setState(
      (prevState) => {
        return {
          modalVacationOpen: !prevState.modalVacationOpen,
        };
      },
      () => {
        this.handleSubmit();
      }
    );
  };

  handleDateChangeModal = (event) => {
    console.log(event);
    this.setState({
      selectedStartDate: event.start,
      selectedEndDate: event.end,
      availability: event.title === "Available" ? "yes" : "no",
      calenderId: event.id,
    });
    this.handleModal();
    // console.log("Date",event)
  };

  handleStartDateChange = (date) => {
    this.setState({
      selectedStartDate: date,
    });
  };

  handleEndDateChange = (date) => {
    var DateTime = new Date(date);
    DateTime.setHours(23, 59, 59, 0);
    console.log(DateTime);
    this.setState({
      selectedEndDate: DateTime,
    });
  };

  handleVacationEndDateChange = (date) => {
    var today = new Date();
    this.setState({
      selectedVacationStartDate: today,
      selectedVacationEndDate: date,
    });
  };

  handleAvailabilityChange = (event) => {
    console.log(event.target.value);
    this.setState({
      availability: event.target.value,
    });
  };

  confirmDates = () => {
    // if(this.state.calendarEvent)
    let event = calendarEvent.find(
      (event) => event.id === this.state.calenderId
    );

    if (event) {
      let index = calendarEvent.indexOf(event);
      calendarEvent[index].start = this.state.selectedStartDate;
      calendarEvent[index].end = this.state.selectedEndDate;
      calendarEvent[index].title =
        this.state.availability === "yes" ? "Available" : "Unavailable";
    } else {
      calendarEvent.push({
        start: this.state.selectedStartDate,
        end: this.state.selectedEndDate,
        title: this.state.availability === "yes" ? "Available" : "Unavailable",
      });
    }

    this.setState(
      {
        events: calendarEvent,
      },
      () => {
        this.handleSubmit();
        this.handleModal();
      }
    );
  };

  CustomEvent = (event) => {
    return (
      <div
        style={
          event.title === "Available"
            ? { backgroundColor: "#397d49", paddingLeft: 8, borderRadius: 2 }
            : event.title === "Unavailable"
            ? { backgroundColor: "#ef5533", paddingLeft: 8, borderRadius: 2 }
            : null
        }
      >
        <span style={{ color: "#ffffff" }}>{event.title}</span>
      </div>
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid
            item
            xs={12}
            style={{ marginTop: "2rem", backgroundColor: "#ffffff" }}
          >
            <div className={classes.calendar}>
              <Calendar
                selectable
                localizer={localizer}
                defaultDate={new Date()}
                defaultView="month"
                views={["month"]}
                events={this.state.events}
                onSelectEvent={this.handleDateChangeModal}
                style={{ height: 360 }}
                onSelectSlot={this.handleSelect}
                onSelecting={(slot) => {
                  console.log(slot);
                  return false;
                }}
                timeslots={8}
                components={{
                  agenda: {
                    dateHeader: ({ date, label }) => {
                      console.log(date, label);
                      return <span style={{ color: "#333333" }}>{label}</span>;
                    },
                  },
                  event: this.CustomEvent,
                }}
              />
            </div>
          </Grid>

          <Grid
            item
            xs={12}
            style={{ marginTop: "2rem", backgroundColor: "#ffffff" }}
          >
            <div style={{ padding: 54 }}>
              <div
                style={{
                  float: "left",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <label
                  htmlFor="vacation-mode"
                  style={{
                    color: "#3EB3CD",
                    fontSize: 18,
                    fontFamily: "Kanit",
                    marginRight: 24,
                  }}
                >
                  Vacation mode
                </label>
                <Toggle
                  id="vacation-mode"
                  className={classes.toggleStyle}
                  checked={this.state.toggle}
                  onChange={this.handleToggleChange}
                />
                {this.state.vacationEndDate === null ||
                this.state.vacationEndDate === "" ||
                this.state.vacationEndDate === undefined ? null : (
                  <span
                    style={{
                      marginLeft: 24,
                      color: "#999999",
                    }}
                  >
                    You're on vacation till:{" "}
                    <span style={{ fontWeight: "bold" }}>
                      {moment(this.state.vacationEndDate).format("DD/MM/YYYY")}
                    </span>
                  </span>
                )}
              </div>
              {/* <Button
                variant="contained"
                color="primary"
                className={classes.orangeBtn}
                onClick={this.handleSubmit}
              >
                Done
              </Button> */}
            </div>
            <span
              style={{
                marginLeft: 56,
                color: "#121212",
              }}
            >
              <strong>Note</strong>: When vacation mode is on, you won't recieve
              booking or meeting request till end date selected by you.
            </span>
          </Grid>
          {/* Calendar Unavailable Status Modal */}
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalOpen}
            onClose={this.handleModal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalOpen}>
              <div className={classes.paper}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <Grid container justify="space-around">
                    <KeyboardDatePicker
                      disableToolbar
                      variant="inline"
                      format="MM/dd/yyyy"
                      margin="normal"
                      id="date-picker-inline"
                      label="Start Date"
                      value={this.state.selectedStartDate}
                      onChange={this.handleStartDateChange}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                      style={{ margin: 8 }}
                    />
                    <KeyboardDatePicker
                      disableToolbar
                      variant="inline"
                      format="MM/dd/yyyy"
                      margin="normal"
                      id="date-picker-inline"
                      label="End Date"
                      value={this.state.selectedEndDate}
                      onChange={this.handleEndDateChange}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                      style={{ margin: 8 }}
                    />
                  </Grid>
                </MuiPickersUtilsProvider>
                <Grid
                  container
                  justify="space-around"
                  style={{ marginTop: 12 }}
                >
                  <RadioGroup
                    aria-label="Availability"
                    name="availability"
                    p
                    value={this.state.availability}
                    onChange={this.handleAvailabilityChange}
                    row
                  >
                    {/* <FormControlLabel
                      value={"yes"}
                      control={<Radio color="primary" />}
                      label="Available"
                    /> */}
                    <FormControlLabel
                      value={"no"}
                      control={<Radio color="primary" />}
                      label="Unavailable"
                    />
                  </RadioGroup>
                </Grid>
                <Grid
                  container
                  justify="space-around"
                  style={{ marginTop: 18 }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtnOutlined}
                    onClick={this.confirmDates}
                  >
                    Done
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtnOutlined}
                    onClick={this.removeDate}
                  >
                    Remove
                  </Button>
                </Grid>
              </div>
            </Fade>
          </Modal>

          {/* Vacation mode date picker modal */}

          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={this.state.modalVacationOpen}
            onClose={() => {
              if (this.state.selectedVacationEndDate === null) {
                this.handleToggleChange();
              }
              this.handleVacationModal();
            }}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={this.state.modalVacationOpen}>
              <div className={classes.paper}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <Grid container justify="space-around">
                    <KeyboardDatePicker
                      disableToolbar
                      variant="inline"
                      format="MM/dd/yyyy"
                      margin="normal"
                      id="date-picker-inline"
                      label="End Date"
                      value={this.state.selectedVacationEndDate}
                      onChange={this.handleVacationEndDateChange}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                      style={{ margin: 8 }}
                    />
                  </Grid>
                </MuiPickersUtilsProvider>

                <Grid
                  container
                  justify="space-around"
                  style={{ marginTop: 18 }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.orangeBtnOutlined}
                    onClick={this.handleVacationModal}
                  >
                    Save
                  </Button>
                </Grid>
              </div>
            </Fade>
          </Modal>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(CalendarView);
