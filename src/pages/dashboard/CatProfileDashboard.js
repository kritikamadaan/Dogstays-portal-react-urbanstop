import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Divider,
  Button,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  TextareaAutosize,
} from "@material-ui/core";
import dogImage from "../../assets/sitterSearch/Vector.png";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import DeleteIcon from "@material-ui/icons/Delete";
import axios from "axios";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const user = JSON.parse(localStorage.getItem("user"));
const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const styles = (theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
    minHeight: "80vh",
  },
  dogImage: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  editField: {
    width: 250,
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    padding: theme.spacing(0.5, 2),
  },
  orangeBtnOutlined: {
    color: "#F58220",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    border: "1px solid #F58220",
    width: "max-content",
    padding: theme.spacing(0.5, 2),
    backgroundColor: "#ffffff",
    marginRight: 18,
  },
  dogInfoWrapper: {
    backgroundColor: theme.palette.primary.main,
    textAlign: "center",
    color: "white",
    width: 294,
    marginTop: -5,
    margin: "0 auto",
    padding: theme.spacing(2, 0),
  },
  profileDivider: {
    backgroundColor: theme.palette.primary.main,
    width: 1.5,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxWidth: 970,
    maxHeight: 720,
    overflowY: "auto",
  },
  aboutPet: {
    marginTop: theme.spacing(4),
    color: "#979797",
    fontSize: 22,
    fontWeight: 500,
  },
  aboutPetDesc: {
    color: "#5E5A5A",
    fontSize: 16,
  },
  featureWrapper: {
    margin: theme.spacing(2, 1),
  },
  featureHeadings: {
    fontSize: 16,
    fontWeight: 500,
  },
  featureDesc: {
    color: "#5E5A5A",
    fontSize: 16,
    fontWeight: 500,
  },
  moreImageHeader: {
    paddingBottom: theme.spacing(0.5),
    borderBottom: "1px solid #E8770E",
    fontSize: 20,
    fontWeight: 500,
    width: "fit-content",
    margin: "0 auto",
  },
  moreImageWrapper: {
    display: "flex",
    justifyContent: "space-between",
    margin: theme.spacing(5, 0),
  },
  rightFeatureWrapper: {
    margin: theme.spacing(3, 0),
  },
  rightFeatureHeading: {
    fontSize: 22,
    fontWeight: 500,
  },
  rightFeatureContent: {
    fontSize: 16,
    color: "#5E5A5A",
  },
});

// const DogProfileDashboard = (props) => {
//   const classes = useStyles();
//   const [dog, setDog] = useState(null);

//   useEffect(() => {
//     getDogProfile(props.id);
//   }, [props.id]);

// //   const getDogProfile = (id) => {
// //     fetch(`http://3.215.2.1:8000/api/petprofile/${id}`, {
// //       method: "GET",
// //       headers: {
// //         "Content-Type": "application/json",
// //       },
// //     })
// //       .then((res) => {
// //         return res.json();
// //       })
// //       .then((data) => {
// //         setDog(data);
// //       });
// //   };
// // };

class CatProfileDashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      cat: [],
      editable: false,
      modalOpen: false,
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.getDogProfile(id);
  }

  handleEditable = () => {
    this.setState((prevState) => {
      return {
        editable: !prevState.editable,
      };
    });
  };

  handleTextChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleNuteredChange = (event) => {
    this.setState({
      nutered: event.target.value,
    });
  };

  handleStayHomeAloneChoiceChange = (event) => {
    this.setState({
      stay_home_alone_choice: event.target.value,
    });
  };

  handleToiletTrainedChoiceChange = (event) => {
    this.setState({
      toilet_trained_choice: event.target.value,
    });
  };

  handleFriendlyWithDogsChoiceChange = (event) => {
    this.setState({
      friendly_with_dogs_choice: event.target.value,
    });
  };

  handleFriendlyWithCatsChoiceChange = (event) => {
    this.setState({
      friendly_with_cats_choice: event.target.value,
    });
  };

  handleFriendlyWithStrangersChoiceChange = (event) => {
    this.setState({
      friendly_with_strangers_choice: event.target.value,
    });
  };

  handleFriendlyWithChildrensChoiceChange = (event) => {
    this.setState({
      friendly_with_children_choice: event.target.value,
    });
  };

  handleChewsThingsChoiceChange = (event) => {
    this.setState({
      chews_things_choice: event.target.value,
    });
  };

  handleAggressiveChoiceChange = (event) => {
    this.setState({
      is_aggressive_choice: event.target.value,
    });
  };

  handleSave = () => {
    const id = this.props.match.params.id;
    const data = {
      user: user.user.id,
      id: id,
      cust_id: this.state.cust_id,
      pet_id: `${this.state.cust_id}_1`,
      pet_choice: this.state.type_of_pet,
      name: this.state.name,
      breed: this.state.breed,
      profile_photo: this.state.profile_photo,
      dob: this.state.dob,
      nutered: this.state.nutered,
      stay_home_alone_choice: this.state.stay_home_alone_choice,
      stay_home_alone_detail: this.state.stay_home_alone_detail,
      toilet_trained_choice: this.state.toilet_trained_choice,
      toilet_trained_detail: this.state.toilet_trained_detail,
      friendly_with_dogs_choice: this.state.friendly_with_dogs_choice,
      friendly_with_dogs_detail: this.state.friendly_with_dogs_detail,
      friendly_with_cats_choice: this.state.friendly_with_cats_choice,
      friendly_with_cats_detail: this.state.friendly_with_cats_detail,
      friendly_with_strangers_choice: this.state.friendly_with_strangers_choice,
      friendly_with_strangers_detail: this.state.friendly_with_strangers_detail,
      friendly_with_children_choice: this.state.friendly_with_children_choice,
      friendly_with_children_detail: this.state.friendly_with_children_detail,
      chews_things_choice: this.state.chews_things_choice,
      chews_things_detail: this.state.chews_things_detail,
      is_aggressive_choice: this.state.is_aggressive_choice,
      is_aggressive_detail: this.state.is_aggressive_detail,
      additional_info: this.state.additional_info,
      type_of_pet: this.state.type_of_pet,
    };

    axios
      .put(`${config.apiUrl}/api/petprofile/${id}`, data)
      .then((response) => {
        console.log("Add Pets response", response);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getDogProfile = (id) => {
    fetch(`http://3.215.2.1:8000/api/petprofile/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({
          cat: data,
        });
      });
  };

  deletePet = () => {
    const id = this.props.match.params.id;

    axios
      .delete(`${config.apiUrl}/api/petprofile/${id}`)
      .then((response) => {
        console.log("Add Pets response", response);
        window.location.href = "/dashboard";
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleModal = () => {
    this.setState((prevState) => {
      return {
        modalOpen: !prevState.modalOpen,
      };
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.state.cat && (
          <Grid container style={{ padding: 48 }}>
            <Grid item xs={5} style={{ paddingRight: "1rem" }}>
              <div className={classes.dogImage}>
                <img
                  src={
                    this.state.cat.profile_photo === null ||
                    this.state.cat.profile_photo === ""
                      ? dogImage
                      : this.state.cat.profile_photo
                  }
                  alt="dog profile"
                  width="390px"
                  height="240px"
                />
                <div className={classes.dogInfoWrapper}>
                  {!this.state.editable && (
                    <Typography style={{ fontSize: 30 }}>
                      {this.state.cat.name}
                    </Typography>
                  )}
                  {this.state.editable && (
                    <TextField
                      label="name"
                      name="name"
                      value={this.state.name}
                      onChange={this.handleTextChange}
                      className={classes.editField}
                    />
                  )}

                  {!this.state.editable && (
                    <Typography style={{ fontSize: 18 }}>
                      {this.state.cat.breed}
                    </Typography>
                  )}
                  {this.state.editable && (
                    <TextField
                      label="breed"
                      name="breed"
                      value={this.state.breed}
                      onChange={this.handleTextChange}
                      className={classes.editField}
                    />
                  )}

                  {!this.state.editable && (
                    <Typography
                      style={{ fontSize: 18 }}
                    >{`Born on ${this.state.cat.dob}`}</Typography>
                  )}
                  {this.state.editable && (
                    <TextField
                      label="Date of Birth"
                      name="dob"
                      value={this.state.dob}
                      onChange={this.handleTextChange}
                      className={classes.editField}
                    />
                  )}
                </div>
              </div>
              <Typography className={classes.aboutPet}>
                About {this.state.cat.name}
              </Typography>

              {!this.state.editable && (
                <Typography className={classes.aboutPetDesc}>
                  {this.state.cat.additional_info}
                </Typography>
              )}
              {this.state.editable && (
                <TextField
                  label="Additional Info"
                  name="additional_info"
                  value={this.state.additional_info}
                  onChange={this.handleTextChange}
                  className={classes.editField}
                />
              )}
              <div style={{ margin: "2rem 0" }}>
                <Grid container>
                  <Grid item xs={5}>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Neutered
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.neutered ? "Yes" : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="neutered"
                          name="neutered"
                          value={this.state.neutered}
                          onChange={this.handleNuteredChange}
                          row
                        >
                          <FormControlLabel
                            value={true}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={false}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Stay Home Alone
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.stay_home_alone_choice ? "Yes" : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="stay_home_alone_choice"
                          name="stay_home_alone_choice"
                          value={this.state.stay_home_alone_choice}
                          onChange={this.handleStayHomeAloneChoiceChange}
                          row
                        >
                          <FormControlLabel
                            value={"yes"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"no"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Friendly With Dogs
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.friendly_with_dogs_choice
                            ? "Yes"
                            : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="friendly_with_dogs_choice"
                          name="friendly_with_dogs_choice"
                          value={this.state.friendly_with_dogs_choice}
                          onChange={this.handleFriendlyWithDogsChoiceChange}
                          row
                        >
                          <FormControlLabel
                            value={"yes"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"no"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Friendly With Cats
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.friendly_with_cats_choice
                            ? "Yes"
                            : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="friendly_with_cats_choice"
                          name="friendly_with_cats_choice"
                          value={this.state.friendly_with_cats_choice}
                          onChange={this.handleFriendlyWithCatsChoiceChange}
                          row
                        >
                          <FormControlLabel
                            value={"yes"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"no"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={2}></Grid>
                  <Grid item xs={5}>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Friendly With Strangers
                      </Typography>
                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.friendly_with_strangers_choice
                            ? "Yes"
                            : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="friendly_with_strangers_choice"
                          name="friendly_with_strangers_choice"
                          value={this.state.friendly_with_strangers_choice}
                          onChange={
                            this.handleFriendlyWithStrangersChoiceChange
                          }
                          row
                        >
                          <FormControlLabel
                            value={"yes"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"no"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Friendly With Children
                      </Typography>
                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.friendly_with_children_choice
                            ? "Yes"
                            : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="friendly_with_children_choice"
                          name="friendly_with_children_choice"
                          value={this.state.friendly_with_children_choice}
                          onChange={
                            this.handleFriendlyWithChildrensChoiceChange
                          }
                          row
                        >
                          <FormControlLabel
                            value={true}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={false}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Chews Things
                      </Typography>
                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.chews_things_choice === "1"
                            ? "Yes"
                            : this.state.cat.chews_things_choice === "2"
                            ? "No"
                            : this.state.cat.chews_things_choice === "3"
                            ? "Sometimes"
                            : null}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="chews_things_choice"
                          name="chews_things_choice"
                          value={this.state.chews_things_choice}
                          onChange={this.handleChewsThingsChoiceChange}
                          row
                        >
                          <FormControlLabel
                            value={"1"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"2"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                          <FormControlLabel
                            value={"3"}
                            control={<Radio color="primary" />}
                            label="Sometimes"
                          />
                        </RadioGroup>
                      )}
                    </div>
                    <div className={classes.featureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.featureHeadings}
                      >
                        Is Aggressive
                      </Typography>
                      {!this.state.editable && (
                        <Typography className={classes.featureDesc}>
                          {this.state.cat.is_aggressive_choice ? "Yes" : "No"}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <RadioGroup
                          aria-label="is_aggressive_choice"
                          name="is_aggressive_choice"
                          value={this.state.is_aggressive_choice}
                          onChange={this.handleAggressiveChoiceChange}
                          row
                        >
                          <FormControlLabel
                            value={"yes"}
                            control={<Radio color="primary" />}
                            label="Yes"
                          />
                          <FormControlLabel
                            value={"no"}
                            control={<Radio color="primary" />}
                            label="No"
                          />
                        </RadioGroup>
                      )}
                    </div>
                  </Grid>
                </Grid>
              </div>
              <div>
                <Typography className={classes.moreImageHeader} color="primary">
                  More Images
                </Typography>
                <div className={classes.moreImageWrapper}>
                  <img
                    src={this.state.cat.profile_photo}
                    alt="dog profile"
                    width="200px"
                  />
                  <img
                    src={this.state.cat.profile_photo}
                    alt="dog profile"
                    width="200px"
                  />
                  <img
                    src={this.state.cat.profile_photo}
                    alt="dog profile"
                    width="200px"
                  />
                </div>
              </div>
            </Grid>
            <Grid
              item
              xs={1}
              style={{ display: "flex", justifyContent: "center" }}
            >
              <Divider
                orientation="vertical"
                className={classes.profileDivider}
              />
            </Grid>
            <Grid item xs={6}>
              <div>
                <Grid container justify="space-between">
                  <Grid item xs={12}>
                    <div
                      style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "flex-end",
                      }}
                    >
                      {!this.state.editable && (
                        <div>
                          <EditIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleEditable}
                            style={{ cursor: "pointer" }}
                          />

                          <DeleteIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleModal}
                            style={{ cursor: "pointer", marginLeft: 18 }}
                          />
                        </div>
                      )}
                      {this.state.editable && (
                        <div>
                          <SaveIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleSave}
                            style={{ cursor: "pointer" }}
                          />

                          <CancelIcon
                            color="primary"
                            fontSize="large"
                            onClick={this.handleEditable}
                            style={{ cursor: "pointer", marginLeft: 18 }}
                          />
                        </div>
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={5}>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Stay Home Alone
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.stay_home_alone_detail}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Stay Home Alone Detail"
                          name="stay_home_alone_detail"
                          value={this.state.stay_home_alone_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Friendly With Dogs
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.friendly_with_dogs_detail}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Friendly With Dogs Detail"
                          name="friendly_with_dogs_detail"
                          value={this.state.friendly_with_dogs_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Friendly With Cats
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.friendly_with_cats_detail}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Friendly With Cats Detail"
                          name="friendly_with_cats_detail"
                          value={this.state.friendly_with_cats_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Additional Info
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.additional_info}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Additional Info"
                          name="additional_info"
                          value={this.state.additional_info}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={5}>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Friendly With Strangers
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.friendly_with_strangers_detail}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Friendly with strangers details"
                          name="friendly_with_strangers_detail"
                          value={this.state.friendly_with_strangers_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Friendly With Children
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.friendly_with_children_detail}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Friendly with childrens details"
                          name="friendly_with_children_detail"
                          value={this.state.friendly_with_children_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Chews Things
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.chews_things_detail}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Chews Things Details"
                          name="chews_things_detail"
                          value={this.state.chews_things_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                    <div className={classes.rightFeatureWrapper}>
                      <Typography
                        color="secondary"
                        className={classes.rightFeatureHeading}
                      >
                        Is Agrressive
                      </Typography>

                      {!this.state.editable && (
                        <Typography className={classes.rightFeatureContent}>
                          {this.state.cat.is_aggressive_detail}
                        </Typography>
                      )}
                      {this.state.editable && (
                        <TextField
                          label="Is Agrressive Details"
                          name="is_aggressive_detail"
                          value={this.state.is_aggressive_detail}
                          onChange={this.handleTextChange}
                          className={classes.editField}
                        />
                      )}
                    </div>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={this.state.modalOpen}
              onClose={this.handleModal}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
              className={classes.modal}
            >
              <Fade in={this.state.modalOpen}>
                <div className={classes.paper}>
                  <Grid container>
                    <Grid item xs={12}>
                      <p>Are you sure you want to delete your pet?</p>
                    </Grid>
                    <Grid item xs={12}>
                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.orangeBtnOutlined}
                        onClick={this.handleModal}
                      >
                        Cancel
                      </Button>

                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.orangeBtn}
                        onClick={this.deletePet}
                      >
                        Confirm
                      </Button>
                    </Grid>
                  </Grid>
                </div>
              </Fade>
            </Modal>
          </Grid>
        )}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(CatProfileDashboard);
