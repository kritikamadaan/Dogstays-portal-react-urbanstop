import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Divider,
  Button,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  TextareaAutosize,
} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import SendIcon from "@material-ui/icons/Send";
import InputBase from "@material-ui/core/InputBase";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import CircularProgress from "@material-ui/core/CircularProgress";
import moment from "moment";

const user = JSON.parse(localStorage.getItem("user"));
const config = {
  apiUrl: "http://3.215.2.1:8000",
};

let chatSocket;

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    minHeight: "80vh",
    backgroundColor: "#ffffff",
    padding: 40,
  },
  chatWindowRoot: {
    flexGrow: 1,
    backgroundColor: "#ffffff",
    padding: 32,
    paddingTop: 0,
  },
  paper: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: 42,
    borderRadius: 24,
    border: "1px solid #979797",
    marginBottom: 18,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  messageCard: {
    padding: 8,
    backgroundColor: "#ffffff",
    border: "1px solid #979797",
    borderRadius: 12,
    marginTop: 18,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 4,
    width: 42,
    height: 42,
    padding: 4,
    marginLeft: 18,
  },
  messageCardActive: {
    padding: 8,
    backgroundColor: "#F58220",
    borderRadius: 12,
    marginTop: 18,
    cursor: "pointer",
  },
  activeChatWindowCard: {
    padding: 8,
    backgroundColor: "#ffffff",
    border: "1px solid #979797",
    borderRadius: 12,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  profilePic: {
    height: 62,
    width: 62,
    borderRadius: "50%",
    marginRight: 24,
  },
  PersonName: {
    color: "#3EB3CD",
    fontFamily: "Kanit",
    margin: 0,
    fontSize: 20,
  },
  chatContainer: {
    overflowY: "auto",
    minHeight: 420,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
  },
  recieverText: {
    float: "left",
    border: "1px solid #F58220",
    borderRadius: 12,
    padding: 12,
    maxWidth: 520,
  },
  senderText: {
    float: "right",
    backgroundColor: "#F58220",
    borderRadius: 12,
    padding: 12,
    color: "#ffffff",
    maxWidth: 520,
  },
});

let chatUsers = [];
let resArr = [];

class Message extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      message: "",
      chatData: [],
      chatList: [],
      chatUsers: [],
      resetChatData: [],
      chatWindow: false,
      other_user: null,
      profile_photo: "",
      chatLoading: false,
    };

    this.handleKey = this.handleKey.bind(this);
  }

  componentDidMount() {
    this.getChatroomList();
  }

  scrollToBottom = () => {
    setTimeout(() => {
      var target = document.getElementById("bottomDiv");
      target.parentNode.scrollTop = target.offsetTop;
    }, 500);
  };

  getSitterDetails = (id, obj) => {
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/?user=${id}`)
      .then((response) => {
        console.log("Sitter", response, response.data[0].id);
        this.setState(
          {
            chatLoading: false,
            profile_photo: response.data[0].profile_photo,
          },
          () => {
            const userData = {
              ...obj,
              profile_photo: this.state.profile_photo,
            };
            chatUsers.push(userData);
            // chatUsers.reverse();
            chatUsers.sort((a, b) => {
              return a.chat_last_updated_time.localeCompare(
                b.chat_last_updated_time
              );
            });

            chatUsers.forEach(function (item) {
              var i = resArr.findIndex((x) => {
                return x.user_data.id === item.user_data.id;
              });
              if (i <= -1) {
                resArr.push(item);
              }
            });

            this.setState({
              chatList: response.data.chat_list,
              chatUsers: resArr,
              resetChatData: resArr,
            });
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getOwnerDetails = (id, obj) => {
    axios
      .get(`${config.apiUrl}/api/petownerprofile/list/?user=${id}`)
      .then((response) => {
        console.log("Owner", response, response.data[0].id);
        this.setState(
          {
            profile_photo: response.data[0].profile_photo,
            chatLoading: false,
          },
          () => {
            const userData = {
              ...obj,
              profile_photo: this.state.profile_photo,
            };
            chatUsers.push(userData);
            // chatUsers.reverse();

            chatUsers.sort(
              (a, b) => b.chat_last_updated_time - a.chat_last_updated_time
            );

            chatUsers.forEach(function (item) {
              var i = resArr.findIndex((x) => {
                return x.user_data.id === item.user_data.id;
              });
              if (i <= -1) {
                resArr.push(item);
              }
            });

            console.log(resArr);

            this.setState({
              chatList: response.data.chat_list,
              chatUsers: resArr,
            });
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  createRoom = () => {
    axios.post(`${config.apiUrl}/api/chat/create_room/`).then((response) => {
      console.log("room", response);
      this.connectToSocket(response.data.data.chat_room_uri);
    });
  };

  connectToSocket = (id, other_user) => {
    this.setState({
      other_user: other_user,
    });
    chatSocket = new WebSocket(`ws://3.215.2.1:8000/ws/chat/${id}`);
    chatSocket.onopen = () => {
      console.log("OPEN");
      chatSocket.send(
        JSON.stringify({
          command: "join",
          user_id: user.user.id,
          other_user: other_user,
        })
      );
    };

    chatSocket.onmessage = (val) => {
      // getMessages()
      let messJson = JSON.parse(val.data);

      return false;
    };

    chatSocket.onclose = (m) => {
      console.log("DEAD ", m);
    };

    chatSocket.onerror = (val) => {
      console.log("ERROR", val);
    };
  };

  getChatroomList = () => {
    axios
      .get(`${config.apiUrl}/api/chat/get-rooms/${user.user.id}/`)
      .then((response) => {
        response.data.chat_list.length > 0 &&
          response.data.chat_list.map((userList) => {
            userList.chat_room_users.map((u) => {
              if (u.id !== user.user.id) {
                if (user.user_type.is_petowner === true) {
                  const obj = {
                    chat_room_uri: userList.chat_room_uri,
                    chat_room_users: u.id,
                    chat_last_updated_time: userList.chat_last_updated_time,
                    last_msgs: userList.last_msgs,
                    user_data: u,
                  };
                  this.getSitterDetails(u.id, obj);
                } else {
                  const obj = {
                    chat_room_uri: userList.chat_room_uri,
                    chat_room_users: u.id,
                    chat_last_updated_time: userList.chat_last_updated_time,
                    last_msgs: userList.last_msgs,
                    user_data: u,
                  };
                  this.getOwnerDetails(u.id, obj);
                }
              }
            });
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getMessages = (room_id) => {
    axios
      .get(`${config.apiUrl}/api/chat/preload/?uri=${room_id}`)
      .then((response) => {
        console.log("msg", response);

        this.setState({
          chatData: response.data.data,
        });
        response.data.data.forEach((message) => {
          console.log(message);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleKey(e) {
    if (e.key == "Enter") {
      e.preventDefault();
      this.handleSubmit();
    } else {
      return null;
    }
  }

  handleSubmit = (e) => {
    const data = {
      sitter_messages: this.state.message,
      owner_messages: null,
      sitter: user.user.id,
      owner: 6,
    };

    chatSocket.send(
      JSON.stringify({
        command: "send",
        user_id: user.user.id,
        other_user: this.state.other_user,
        message: this.state.message,
      })
    );

    this.setState(
      {
        message: "",
      },
      () => {
        this.getMessages(this.state.chat_room_uri);
        this.scrollToBottom();
      }
    );
  };

  searchFilterFunction = (text) => {
    if (text !== "") {
      const newData = this.state.chatUsers.filter((item) => {
        const itemData = `${item.user_data.first_name.toUpperCase()}`;
        const textData = `${text.toUpperCase()}`;
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        chatUsers: newData,
      });
    } else {
      this.setState({
        chatUsers: this.state.resetChatData,
      });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={4}>
            <Grid container>
              <Grid item xs={12}>
                <Paper component="form" className={classes.paper}>
                  <InputBase
                    className={classes.input}
                    placeholder="Chat Search"
                    inputProps={{ "aria-label": "search google maps" }}
                    onChange={(e) => {
                      this.searchFilterFunction(e.target.value);
                    }}
                  />
                  <IconButton
                    type="submit"
                    className={classes.iconButton}
                    aria-label="search"
                  >
                    <SearchIcon />
                  </IconButton>
                </Paper>
              </Grid>

              {this.state.chatUsers &&
                this.state.chatUsers.map((messages, i) => {
                  const time = moment(messages.chat_last_updated_time).format(
                    "HH:mm"
                  );
                  return (
                    <Grid item xs={12} key={i}>
                      <div
                        className={classes.messageCard}
                        onClick={() => {
                          this.connectToSocket(
                            messages.chat_room_uri,
                            messages.chat_room_users
                          );
                          this.getMessages(messages.chat_room_uri);
                          this.scrollToBottom();
                          this.setState({
                            chatWindow: true,
                            chat_room_uri: messages.chat_room_uri,
                            window_profile_photo: messages.profile_photo,
                            chatWindowUserName: `${
                              messages.user_data.first_name
                            }${" "}${messages.user_data.last_name}`,
                          });
                        }}
                      >
                        <img
                          src={messages.profile_photo}
                          alt="profile image"
                          className={classes.profilePic}
                        />
                        <div style={{ width: "68%" }}>
                          <h3 className={classes.PersonName}>
                            {messages.user_data.first_name}{" "}
                            {messages.user_data.last_name}
                          </h3>
                          <p
                            style={{
                              color: "#979797",
                              fontFamily: "Kanit",
                              margin: 0,
                              marginTop: 4,
                              fontSize: 16,
                              whiteSpace: "nowrap",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              maxWidth: 200,
                            }}
                          >
                            {messages.last_msgs.content}
                          </p>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            justifyContent: "space-between",
                            alignItems: "center",
                          }}
                        >
                          {/* <div
                            style={{
                              backgroundColor: "#299F35",
                              height: 24,
                              width: 24,
                              borderRadius: "50%",
                              textAlign: "center",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <span
                              style={{
                                fontSize: 14,
                                fontFamily: "Kanit",
                                textAlign: "center",
                                color: "#ffffff",
                              }}
                            >
                              2
                            </span>
                          </div> */}
                          <p
                            style={{
                              color: "#979797",
                              fontFamily: "Kanit",
                              margin: 0,
                              marginTop: 8,
                              fontSize: 14,
                            }}
                          >
                            {time}
                          </p>
                        </div>
                      </div>
                    </Grid>
                  );
                })}
            </Grid>
          </Grid>
          {this.state.chatWindow === true ? (
            <Grid item xs={8}>
              <div className={classes.chatWindowRoot}>
                <Grid container>
                  <Grid item xs={12}>
                    <div className={classes.activeChatWindowCard}>
                      <img
                        src={this.state.window_profile_photo}
                        alt="profile image"
                        className={classes.profilePic}
                      />
                      <div style={{ width: "68%" }}>
                        <h3 className={classes.PersonName}>
                          {this.state.chatWindowUserName}
                        </h3>
                      </div>
                    </div>
                  </Grid>
                  <Grid item xs={12}>
                    <div className={classes.chatContainer}>
                      <Grid
                        container
                        style={{
                          height: "100%",
                          maxHeight: 420,
                          overflowY: "auto",
                        }}
                      >
                        {this.state.chatData.map((chats, i) => {
                          if (user.user.id !== chats.sender_id) {
                            return (
                              <React.Fragment key={i}>
                                <Grid
                                  item
                                  xs={12}
                                  style={{ marginTop: 24 }}
                                  key={i}
                                >
                                  <div className={classes.recieverText}>
                                    <span>{chats.message}</span>
                                  </div>
                                </Grid>
                              </React.Fragment>
                            );
                          } else {
                            return (
                              <React.Fragment key={i}>
                                <Grid
                                  item
                                  xs={12}
                                  style={{ marginTop: 24 }}
                                  key={i}
                                >
                                  <div className={classes.senderText}>
                                    <span>{chats.message}</span>
                                  </div>
                                </Grid>
                              </React.Fragment>
                            );
                          }
                        })}
                        <div
                          style={{ float: "left", clear: "both" }}
                          id="bottomDiv"
                          ref={(el) => {
                            this.messagesEnd = el;
                          }}
                        ></div>
                      </Grid>
                    </div>
                  </Grid>

                  <Grid item xs={12}>
                    <div
                      style={{
                        display: "flex",
                        marginTop: 18,
                      }}
                    >
                      <Paper component="form" className={classes.paper}>
                        <InputBase
                          className={classes.input}
                          value={this.state.message}
                          onKeyPress={this.handleKey}
                          placeholder="Type to chat..."
                          inputProps={{ "aria-label": "search google maps" }}
                          onChange={(e) => {
                            this.setState({
                              message: e.target.value,
                            });
                          }}
                        />
                      </Paper>
                      <Button
                        variant="contained"
                        color="primary"
                        className={classes.orangeBtn}
                        onClick={this.handleSubmit}
                      >
                        <SendIcon style={{ fontSize: 18 }} />
                      </Button>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          ) : (
            <Grid item xs={8}>
              <div
                style={{
                  display: "flex",
                  height: "100%",
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <h3
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 24,
                    color: "#979797",
                    textAlign: "center",
                  }}
                >
                  Click on Chats to start chatting
                </h3>
              </div>
              {this.state.chatLoading === true ? (
                <Grid style={{ height: "10vh", textAlign: "center" }}>
                  <CircularProgress color="primary" centered />
                  "No Chats found"
                </Grid>
              ) : (
               <div/>
              )}
            </Grid>
          )}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Message);
