import React from "react";
import { withStyles } from "@material-ui/core/styles";
import homeBanner from "../assets/home/home-banner.jpg";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import moment from "moment";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Icon from "@material-ui/core/Icon";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CardActions from "@material-ui/core/CardActions";
import Rating from "@material-ui/lab/Rating";
import DoubleArrowIcon from "@material-ui/icons/DoubleArrow";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";

// assets

import pawIcon from "../assets/home/pawIcon.svg";
import featureIcon1 from "../assets/home/feature-icon1.svg";
import featureIcon2 from "../assets/home/feature-icon2.svg";
import featureIcon3 from "../assets/home/feature-icon3.svg";
import featureIcon4 from "../assets/home/feature-icon4.svg";
import illustration1 from "../assets/home/illustration_1.svg";
import illustration2 from "../assets/home/illustration_2.svg";
import illustration3 from "../assets/home/illustration_3.svg";
import illustration4 from "../assets/home/illustration_4.svg";
import testimonialImg from "../assets/home/testimonial_img.jpg";
import galleryImg1 from "../assets/home/gallery_1.jpg";
import galleryImg2 from "../assets/home/gallery_2.jpg";
import galleryImg3 from "../assets/home/gallery_3.jpg";
import galleryImg4 from "../assets/home/gallery_4.jpg";

const styles = (theme) => ({
  homeBanner: {
    background: `url(${homeBanner})`,
    backgroundSize: "cover",
    backgroundPosition: "top",
    minHeight: "100vh",
  },
  bannerOverlay: {
    background: "rgba(245, 130, 32, 0.7)",
    width: 540,
    height: 540,
    position: "relative",
    left: 55,
    top: 240,
    padding: theme.spacing(7),
    color: "#F9F3F3",
  },
  welcomeHeader: {
    fontFamily: "'Fredoka One', cursive",
    fontSize: 38,
  },
  bannerTagline: {
    fontSize: 25,
    fontFamily: "'Fredoka One', cursive",
    width: 377,
    marginTop: theme.spacing(4),
  },
  dateTextField: {
    color: "white !important",
    "& .MuiInput-underline:before": {
      borderColor: "white !important",
    },
    "& .MuiInput-underline:after": {
      borderColor: "white !important",
    },
  },
  dateInput: {
    color: "white !important",
  },
  dateIcon: {
    "& button": {
      color: "white !important",
    },
  },
  margin: {
    margin: theme.spacing(1),
  },
  pawIconRoot: {
    width: 55,
    height: "100%",
    position: "relative",
    left: "48%",
    bottom: "-340px",
  },
  featureRow: {
    margin: theme.spacing(4),
  },
  innerGridWrapper: {
    textAlign: "center",
  },
  featureButton: {
    height: 47,
    textTransform: "none",
    color: theme.palette.common.gray,
    fontSize: 18,
    borderRadius: 4,
  },
  sectionClass: {
    margin: theme.spacing(10, 0),
  },
  bookingHeader: {
    textAlign: "center",
    fontSize: 35,
    fontWeight: 600,
  },
  bookingSubHeader: {
    fontSize: 20,
    color: theme.palette.common.black,
    textAlign: "center",
  },
  orangeBtn: {
    color: "white",
    fontSize: 25,
    textTransform: "none",
  },
  carouselWrapper: {
    marginTop: theme.spacing(10),
  },
  testimonialCard: {
    padding: theme.spacing(6),
    borderRadius: 8,
    boxShadow: "10px 10px 10px rgba(128, 128, 128, 0.15)",
    border: "1.5px solid #3EB3CD",
  },
  blogCard: {
    borderRadius: 8,
    boxShadow: "10px 10px 10px rgba(128, 128, 128, 0.15)",
    width: 375,
    "& .MuiCardContent-root": {
      padding: theme.spacing(4),
    },
    backgroundColor: "#E8770E !important",
    color: "#F9F3F3",
    "& .MuiCardActions-root": {
      padding: theme.spacing(0, 4),
      paddingBottom: theme.spacing(4),
    },
    margin: "0 auto",
  },
  blogTitle: {
    fontSize: 22,
    fontWeight: 600,
  },
  blogSubtitle: {
    fontSize: 18,
    borderBottom: "1px solid",
    width: "fit-content",
    fontWeight: 500,
  },
  blogButton: {
    marginLeft: -6,
    color: "#F9F3F3",
    fontSize: 18,
    textTransform: "none",
  },
  instaMosaic: {
    padding: theme.spacing(0, 11),
    margin: theme.spacing(5, 0),
    marginTop: theme.spacing(9),
  },
  gridImg: {
    "& div": {
      borderRadius: 8,
    },
  },
});

const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.common.white,
    borderColor: theme.palette.common.white,
    padding: theme.spacing(1, 5),
    borderRadius: 2,
    textTransform: "none",
    fontSize: 25,
  },
}))(Button);

const FeatureIcon = (props) => {
  return (
    <Icon>
      <img alt="edit" src={props.src} width="100%" />
    </Icon>
  );
};

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      startDate: null,
      endDate: null,
      startTime: null,
      endTIme: null,
    };
  }

  handleStartDate = (date) => {
    console.log(date);
    const Formatted = moment(date).format("YYYY-MM-DD");
    this.setState({
      startDate: Formatted,
    });
  };

  handleEndDate = (date) => {
    console.log(date);
    const Formatted = moment(date).format("YYYY-MM-DD");
    this.setState({
      endDate: Formatted,
    });
  };

  handleStartTime = (time) => {
    console.log(time);
    const Formatted = moment(time).format("h:mm A");
    console.log(Formatted);

    this.setState({
      startTime: Formatted,
    });
  };

  handleEndTime = (time) => {
    console.log(time);
    const Formatted = moment(time).format("h:mm A");
    this.setState({
      endTIme: Formatted,
    });
  };

  render() {
    const { classes } = this.props;
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: 0,
    };
    return (
      <div className={classes.home}>
        <section className={classes.homeBanner}>
          <div className={classes.bannerOverlay}>
            <Typography
              variant="h4"
              gutterBottom
              className={classes.welcomeHeader}
            >
              Welcome to DogStays
            </Typography>
            <Typography
              variant="h5"
              gutterBottom
              className={classes.bannerTagline}
            >
              We’ll connect you with just the right dog person to look after
              your dog.
            </Typography>
            <Grid container justify="space-around" spacing={4}>
              <Grid item xs={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="MM/dd/yyyy"
                    margin="normal"
                    id="date-picker-inline"
                    label="Start Date"
                    value={this.state.startDate}
                    onChange={this.handleStartDate}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage=""
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{ className: classes.dateIcon }}
                    className={classes.dateTextField}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardTimePicker
                    variant="inline"
                    margin="normal"
                    id="time-picker"
                    label="Start Time"
                    value={this.state.startTime}
                    onChange={this.handleStartTime}
                    KeyboardButtonProps={{
                      "aria-label": "change time",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{ className: classes.dateIcon }}
                    className={classes.dateTextField}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
            </Grid>
            <Grid container justify="space-around" spacing={4}>
              <Grid item xs={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format="MM/dd/yyyy"
                    margin="normal"
                    id="date-picker-inline"
                    label="End Date"
                    value={this.state.endDate}
                    onChange={this.handleEndDate}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    invalidDateMessage=""
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{ className: classes.dateIcon }}
                    className={classes.dateTextField}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardTimePicker
                    variant="inline"
                    margin="normal"
                    id="time-picker"
                    label="End Time"
                    // value={this.state.endTime}
                    onChange={this.handleEndTime}
                    KeyboardButtonProps={{
                      "aria-label": "change time",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{ className: classes.dateIcon }}
                    className={classes.dateTextField}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
            </Grid>
            <Grid container justify="center" style={{ marginTop: "1.5rem" }}>
              <ColorButton variant="outlined" className={classes.margin}>
                Submit
              </ColorButton>
            </Grid>
          </div>
          <Icon classes={{ root: classes.pawIconRoot }}>
            <img className={classes.imageIcon} src={pawIcon} alt="Paw Icon" />
          </Icon>
        </section>
        <section className={classes.featureRow}>
          <Grid container justify="space-between" spacing={6}>
            <Grid item xs={6}>
              <Grid container justify="space-evenly">
                <Grid item xs={6} className={classes.innerGridWrapper}>
                  <Button
                    variant="outlined"
                    color="primary"
                    className={classes.featureButton}
                    startIcon={<FeatureIcon src={featureIcon1} />}
                  >
                    Choice of Multiple Sitters
                  </Button>
                </Grid>
                <Grid item xs={6} className={classes.innerGridWrapper}>
                  <Button
                    variant="outlined"
                    color="primary"
                    className={classes.featureButton}
                    startIcon={<FeatureIcon src={featureIcon2} />}
                  >
                    Reviews & Feedback
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <Grid container justify="space-evenly">
                <Grid item xs={6} className={classes.innerGridWrapper}>
                  <Button
                    variant="outlined"
                    color="primary"
                    className={classes.featureButton}
                    startIcon={<FeatureIcon src={featureIcon3} />}
                  >
                    Confirm Bookings Faster
                  </Button>
                </Grid>
                <Grid item xs={6} className={classes.innerGridWrapper}>
                  <Button
                    variant="outlined"
                    color="primary"
                    className={classes.featureButton}
                    startIcon={<FeatureIcon src={featureIcon4} />}
                  >
                    Pay Safely Online
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </section>
        <section className={classes.sectionClass}>
          <Typography
            variant="h4"
            color="secondary"
            gutterBottom
            className={classes.bookingHeader}
          >
            BOOK WITH US
          </Typography>
          <Typography
            variant="h6"
            gutterBottom
            className={classes.bookingSubHeader}
          >
            In 4 Easy Steps
          </Typography>
          <Grid container justify="center" style={{ marginTop: "3rem" }}>
            <Grid item xs={3} style={{ textAlign: "center" }}>
              <img
                src={illustration1}
                alt="Search For Dog Sitter"
                style={{ marginBottom: "2rem" }}
              />
              <Typography
                variant="h6"
                gutterBottom
                className={classes.bookingSubHeader}
              >
                Search For Dog Sitter
              </Typography>
            </Grid>
            <Grid item xs={3} style={{ textAlign: "center" }}>
              <img
                src={illustration2}
                alt="Setup A Meeting"
                style={{ marginBottom: "2.6rem" }}
              />
              <Typography
                variant="h6"
                gutterBottom
                className={classes.bookingSubHeader}
              >
                Setup a Meeting
              </Typography>
            </Grid>
            <Grid item xs={3} style={{ textAlign: "center" }}>
              <img
                src={illustration3}
                alt="Confirm Booking Details"
                style={{ marginBottom: "2rem" }}
              />
              <Typography
                variant="h6"
                gutterBottom
                className={classes.bookingSubHeader}
              >
                Confirm Booking Details
              </Typography>
            </Grid>
            <Grid item xs={3} style={{ textAlign: "center" }}>
              <img
                src={illustration4}
                alt="Safely Pay Online"
                style={{ marginBottom: "2rem" }}
              />
              <Typography
                variant="h6"
                gutterBottom
                className={classes.bookingSubHeader}
              >
                Safely Pay Online
              </Typography>
            </Grid>
          </Grid>
          <Grid container justify="center" style={{ marginTop: "3rem" }}>
            <Button
              variant="contained"
              color="primary"
              className={classes.orangeBtn}
            >
              Learn How
            </Button>
          </Grid>
        </section>
        <section className={classes.sectionClass}>
          <Typography
            variant="h4"
            color="secondary"
            gutterBottom
            className={classes.bookingHeader}
          >
            TESTIMONIALS
          </Typography>
          <Typography
            variant="h6"
            gutterBottom
            className={classes.bookingSubHeader}
          >
            Paw Of Approval
          </Typography>
          <div className={classes.carouselWrapper}>
            <Slider {...settings}>
              <div></div>
              <div>
                <Card className={classes.testimonialCard}>
                  <CardContent>
                    <Typography
                      className={classes.title}
                      color="primary"
                      gutterBottom
                      variant="h5"
                    >
                      Eena
                    </Typography>
                    <Typography
                      variant="h6"
                      gutterBottom
                      style={{ fontSize: 18 }}
                    >
                      Parent : Camila Choubey
                    </Typography>
                    <Rating name="read-only" value={4} readOnly />
                    <img src={testimonialImg} alt="Testimonial Dog" />
                    <Typography variant="body1" style={{ marginTop: "1rem" }}>
                      Commodo eiusmod magna cillum deserunt dolor est minim nisi
                      velit laboris ex. Amet cupidatat deserunt cillum commodo.
                      Id id veniam anim magna et consequat ex ut pariatur eu
                      culpa. Elit non aute amet officia excepteur dolore aliquip
                      irure cupidatat duis. Sit voluptate dolor cillum dolor sit
                      adipisicing. Adipisicing aute quis exercitation veniam
                      aute ut. Proident proident exercitation sint labore
                      deserunt proident. Aliqua excepteur consectetur labore
                      quis cupidatat excepteur Lorem aute. Consectetur mollit
                      ipsum fugiat sunt incididunt consequat sint ad sit in eu
                      cupidatat. Eiusmod ex commodo elit eu amet adipisicing
                      sint adipisicing amet pariatur sint et proident.
                    </Typography>
                  </CardContent>
                </Card>
              </div>
              <div>
                <h3 style={{ display: "none" }}>3</h3>
              </div>
            </Slider>
          </div>
          <Grid container justify="center" style={{ marginTop: "3rem" }}>
            <Button
              variant="contained"
              color="primary"
              className={classes.orangeBtn}
            >
              View More
            </Button>
          </Grid>
        </section>
        <section className={classes.sectionClass}>
          <Typography
            variant="h4"
            color="secondary"
            gutterBottom
            className={classes.bookingHeader}
          >
            BLOGS
          </Typography>
          <Typography
            variant="h6"
            gutterBottom
            className={classes.bookingSubHeader}
          >
            Some Of Our Top Blog Posts
          </Typography>
          <Grid container justify="center" className={classes.carouselWrapper}>
            <Grid item xs={4}>
              <Card className={classes.blogCard}>
                <CardMedia
                  component="img"
                  className={classes.media}
                  image={require("../assets/home/blog_image.jpg")}
                />
                <CardContent>
                  <Typography
                    className={classes.blogTitle}
                    gutterBottom
                    variant="h5"
                  >
                    Tips Preparing Your Dog For Life Returning To Normal
                  </Typography>
                  <Typography variant="body1" className={classes.blogSubtitle}>
                    Blog Category
                  </Typography>
                  <Typography variant="body1" style={{ marginTop: "1.25rem" }}>
                    Sunt magna et incididunt eu ipsum. Ullamco sint voluptate
                    elit magna quis nulla excepteur occaecat excepteur. Ut sint
                    magna occaecat duis do et. Exercitation duis dolor ullamco
                    do enim do dolore. Adipisicing labore proident amet
                    reprehenderit laboris ad et.
                  </Typography>
                </CardContent>
                <CardActions disableSpacing>
                  <Button
                    endIcon={<DoubleArrowIcon />}
                    className={classes.blogButton}
                  >
                    Read More
                  </Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={4}>
              <Card className={classes.blogCard}>
                <CardMedia
                  component="img"
                  className={classes.media}
                  image={require("../assets/home/blog_image.jpg")}
                />
                <CardContent>
                  <Typography
                    className={classes.blogTitle}
                    gutterBottom
                    variant="h5"
                  >
                    Tips Preparing Your Dog For Life Returning To Normal
                  </Typography>
                  <Typography variant="body1" className={classes.blogSubtitle}>
                    Blog Category
                  </Typography>
                  <Typography variant="body1" style={{ marginTop: "1.25rem" }}>
                    Sunt magna et incididunt eu ipsum. Ullamco sint voluptate
                    elit magna quis nulla excepteur occaecat excepteur. Ut sint
                    magna occaecat duis do et. Exercitation duis dolor ullamco
                    do enim do dolore. Adipisicing labore proident amet
                    reprehenderit laboris ad et.
                  </Typography>
                </CardContent>
                <CardActions disableSpacing>
                  <Button
                    endIcon={<DoubleArrowIcon />}
                    className={classes.blogButton}
                  >
                    Read More
                  </Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={4}>
              <Card className={classes.blogCard}>
                <CardMedia
                  component="img"
                  className={classes.media}
                  image={require("../assets/home/blog_image.jpg")}
                />
                <CardContent>
                  <Typography
                    className={classes.blogTitle}
                    gutterBottom
                    variant="h5"
                  >
                    Tips Preparing Your Dog For Life Returning To Normal
                  </Typography>
                  <Typography variant="body1" className={classes.blogSubtitle}>
                    Blog Category
                  </Typography>
                  <Typography variant="body1" style={{ marginTop: "1.25rem" }}>
                    Sunt magna et incididunt eu ipsum. Ullamco sint voluptate
                    elit magna quis nulla excepteur occaecat excepteur. Ut sint
                    magna occaecat duis do et. Exercitation duis dolor ullamco
                    do enim do dolore. Adipisicing labore proident amet
                    reprehenderit laboris ad et.
                  </Typography>
                </CardContent>
                <CardActions disableSpacing>
                  <Button
                    endIcon={<DoubleArrowIcon />}
                    className={classes.blogButton}
                  >
                    Read More
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
          <Grid container justify="center" style={{ marginTop: "5rem" }}>
            <Button
              variant="contained"
              color="primary"
              className={classes.orangeBtn}
            >
              Read More
            </Button>
          </Grid>
        </section>
        <section className={classes.sectionClass}>
          <Typography
            variant="h4"
            color="secondary"
            gutterBottom
            className={classes.bookingHeader}
          >
            INSTAGRAM
          </Typography>
          <Typography
            variant="h6"
            gutterBottom
            className={classes.bookingSubHeader}
          >
            Some (Bow) Wow Moments
          </Typography>
          <div className={classes.instaMosaic}>
            <GridList
              cellHeight={200}
              spacing={12}
              className={classes.gridList}
            >
              <GridListTile rows={2} className={classes.gridImg}>
                <img src={galleryImg1} alt="Instagram gallery 1" />
              </GridListTile>
              <GridListTile rows={2} className={classes.gridImg}>
                <img src={galleryImg3} alt="Instagram gallery 2" />
              </GridListTile>
              <GridListTile rows={2} className={classes.gridImg}>
                <img src={galleryImg2} alt="Instagram gallery 3" />
              </GridListTile>
              <GridListTile rows={2} className={classes.gridImg}>
                <img src={galleryImg4} alt="Instagram gallery 4" />
              </GridListTile>
            </GridList>
          </div>
          <Grid
            container
            justify="center"
            style={{ marginTop: "5rem", marginBottom: "10rem" }}
          >
            <Button
              variant="contained"
              color="primary"
              className={classes.orangeBtn}
            >
              Load More
            </Button>
          </Grid>
        </section>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Home);
