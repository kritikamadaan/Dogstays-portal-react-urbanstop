import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import BookingRequest from "./BookingRequest";
import MeetingRequest from "./MeetingRequest";
import axios from "axios";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import {
  Typography,
  Grid,
  FormControl,
  Select,
  MenuItem,
  TextField,
  Divider,
  Button,
  Link,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Card,
  CardMedia,
  CardContent,
  Chip,
  Avatar,
  CardActionArea,
  CardActions,
  IconButton,
} from "@material-ui/core";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import EuroSymbolIcon from "@material-ui/icons/EuroSymbol";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Rating from "@material-ui/lab/Rating";
import bgImage from "../assets/login/bg_image.jpg";
import dogImage from "../assets/sitterSearch/Vector.png";
import catImage from "../assets/sitterSearch/group.png";
import otherImg from "../assets/sitterSearch/shape.png";
import allSizes from "../assets/sitterSearch/group_1.png";
import defaultPic from "../assets/sitterSearch/defaultPic.png";
import { Link as RouterLink } from "react-router-dom";
import { sitterActions } from "../actions";
import ContactModal from "./dashboard/components/ContactComponent";
const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const styles = (theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
    height: "100%",
  },
  searchSection: {
    backgroundColor: theme.palette.primary.main,
    color: "#FFFFFF",
  },
  searchGrid: {
    background: "#FFFFFF",
    color: theme.palette.primary.main,
    margin: theme.spacing(0, 8),
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",
    padding: theme.spacing(4, 3),
  },
  dialogContent: {
    padding: "0px !important",
  },
  dialogTitle: {
    "& .MuiTypography-h6": {
      paddingLeft: "32px !important",
      color: "#ffffff",
      fontFamily: "Fredoka One",
      fontSize: 30,
    },
    background: "rgba(245, 130, 32, 0.85)",
  },
  selectService: {
    "& .MuiInputBase-root": {
      color: theme.palette.primary.main,
      fontSize: 22,
      margin: theme.spacing(0, 2),
      maxWidth: 210,
    },
    "& .MuiInput-underline:before": {
      display: "none",
    },
    "& .MuiSelect-icon": {
      color: theme.palette.primary.main,
    },
  },
  datePicker: {
    width: 180,
    "& label": {
      color: theme.palette.primary.main,
    },
    "& .MuiInputBase-root": {
      color: theme.palette.primary.main,
      fontSize: 22,
    },
    "& .MuiInput-underline:before": {
      display: "none",
    },
    "& .MuiButtonBase-root": {
      color: theme.palette.primary.main,
    },
    "& .MuiInput-underline:after": {
      display: "none",
    },
  },
  pincode: {
    "& label": {
      top: -20,
      color: theme.palette.primary.main,
      fontSize: 22,
    },
    "& label + .MuiInput-formControl": {
      marginTop: 0,
      color: theme.palette.primary.main,
    },
    "& .MuiInput-underline:before": {
      borderColor: theme.palette.primary.main,
    },
  },
  orangeBtn: {
    color: "white",
    fontSize: 22,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    padding: theme.spacing(1, 7),
  },
  chooseDateText: {
    fontFamily: "Fredoka One",
    fontSize: 32,
    textAlign: "center",
    // position: "absolute",
    // top: "5%",
    // left: "6.5%",
    marginTop: "2.4rem",
    maxWidth: 920,
    margin: "0 auto",
    "& :after": {
      position: "absolute",
      content: "",
      display: "block",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      background:
        "linear-gradient(179.95deg, rgba(255, 255, 255, 0.3) 0.09%, rgba(255, 255, 255, 0) 73.47%), linear-gradient(178.6deg, rgba(255, 255, 255, 0.9) 2.38%, rgba(255, 255, 255, 0.651562) 90.11%, rgba(255, 255, 255, 0) 194.8%)",
    },
  },
  advancedFilterHeading: {
    color: "#FFFFFF",
    borderBottom: "1px solid",
    "&:hover": {
      textDecoration: "none !important",
    },
    cursor: "pointer",
  },
  advancedFilterHeaders: {
    color: "#FFFFFF",
    marginBottom: theme.spacing(1),
  },
  advanceFilterContainer: {
    padding: theme.spacing(0, 10),
  },
  advancedFilterGrid: {
    margin: "0 auto",
    maxWidth: 1560,
    paddingBottom: theme.spacing(3),
  },
  sortBy: {
    background: "#FFFFFF",
    width: "50%",
    borderRadius: 4,
    "& .MuiSelect-filled.MuiSelect-filled": {
      paddingTop: theme.spacing(2),
    },
    "& .MuiFilledInput-underline:before": {
      display: "none",
    },
  },
  ownershipTypeCheckbox: {
    color: "#FFFFFF !important",
  },
  sitterGrid: {
    display: "flex",
  },
  sitterCard: {
    display: "flex",
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
    opacity: 0.7,
    border: "1px solid #E8770E",
    width: 480,
  },
  sitterInfoCard: {
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.15) !important",
    border: "1px solid #E8770E",
  },
  sitterMedia: {
    width: 192,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    opacity: 0.95,
  },
  paper: {
    backgroundColor: "white",
    // padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxHeight: "90%",
    maxWidth: "90%",
    overflowY: "auto",
  },
  goToDetailedProfileLink: {
    color: theme.palette.primary.main,
    cursor: "pointer",
    "&:hover": {
      textDecoration: "none !important",
    },
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: "#ffffff",
  },
});

let sitterListArray = [];

class SitterSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      selectedService: "",
      selectedServiceName: "",
      selectedSitter: [],
      services: [],
      startDate: "",
      endDate: "",
      postal_code: "",
      advancedFilters: false,
      bookingModalOpen: false,
      meetingModalOpen: false,
      workHours: "Select Hours",
      sitterList: [],
      sitterListRates: [],
      sitterInfo: false,
      sortBy: "Relevance",
      sitterPets: [],
      ownershipType: {
        hasDog: true,
        hasCat: false,
        hasOther: false,
      },
      searchEmpty: false,
      contactOpen: false,
      sitterProfileAddressPurposeId: null,
      sittercardLoading: true,
    };
  }

  componentDidMount() {
    this.getAllSitterServices();
  }

  getAllSitterServices(id) {
    axios
      .get(`${config.apiUrl}/api/serviceslist/`)
      .then((response) => {
        console.log("Res", response.data);
        this.setState({
          services: response.data,
          sitterCardLoading: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleServiceChange = (event) => {
    event.preventDefault();
    this.setState(
      {
        selectedService: event.target.value,
        searchEmpty: false,
      },
      () => {
        this.state.services.map((service) => {
          if (service.id === this.state.selectedService) {
            this.setState({
              selectServicePropData: service,
            });
          }
        });
      }
    );
  };
  handleContactModal = () => {
    this.setState({ contactOpen: !this.state.contactOpen });
  };
  handleStartDate = (date) => {
    const isoDate = new Date(date).toISOString();
    this.setState({
      startDate: isoDate,
    });
  };

  handleEndDate = (date) => {
    const isoDate = new Date(date).toISOString();
    this.setState({
      endDate: isoDate,
    });
  };

  openBookingModal = () => {
    this.setState({
      bookingModalOpen: true,
    });
  };

  closeBookingModal = () => {
    this.setState({
      bookingModalOpen: false,
    });
  };

  openMeetingModal = () => {
    this.setState({
      meetingModalOpen: true,
    });
  };

  closeMeetingModal = () => {
    this.setState({
      meetingModalOpen: false,
    });
  };

  handleSearch = () => {
    this.setState({ searchEmpty: false });
    const search = {
      selectedService: this.state.selectedService,
      has_pet_choice:
        this.state.ownershipType.hasDog === true
          ? 1
          : this.state.ownershipType.hasCat === true
          ? 2
          : this.state.ownershipType.hasOther === true
          ? 3
          : "",
      hasDog: this.state.ownershipType.hasDog,
      hasCat: this.state.ownershipType.hasCat,
      hasOther: this.state.ownershipType.hasOther,
      postal_code: this.state.postal_code,
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      workHours:
        this.state.workHours === "Select Hours" ? "" : this.state.workHours,
    };
    console.log("serch is wok", search);
    axios
      .get(
        `${config.apiUrl}/api/testsearch/?service_id=${search.selectedService}&endDate=${search.endDate}&startDate=${search.startDate}&postal_code=${search.postal_code}&work_schedule=${search.workHours}&has_pet_choice=${search.has_pet_choice}`
        //in correct order
      )
      .then((response) => {
        this.setState({
          sitterList: response.data,
        });
        sitterListArray = response.data;
        console.log("Length", response.data.length);
        if (response.data.length === 0) {
          this.setState({ searchEmpty: true });
        }
        response.data.map((d) => {
          this.getSitterServicePrice(d.id, sitterListArray);
        });
      })
      .catch((err) => {
        console.log(err);
      });
    // if (search.selectedService === "" || search.selectedService === null) {
    //   alert("Please Select a service");
    // } else {
    //   axios
    //     .get(
    //       `${config.apiUrl}/api/servicesprice/list?service_id=${search.selectedService}&has_dog=${search.hasDog}&has_cat=${search.hasCat}&other_animal=${search.hasOther}&work_hours=${search.workHours}`
    //     )
    //     .then((response) => {
    //       this.setState({
    //         sitterList: response.data,
    //       });
    //       sitterListArray = response.data;
    //       response.data.map((d) => {
    //         this.getSitterServicePrice(d.id, sitterListArray);
    //         console.log("Test", d.id, response.data);
    //       });
    //     })
    //     .catch((err) => {
    //       console.log(err);
    //     });
    // }
  };

  getSitterServicePrice = (id, sitterListArray) => {
    axios
      .get(`${config.apiUrl}/api/servicesprice/list?sitters=${id}`)
      .then((response) => {
        response.data.map((d) => {
          let serviceId = d.service_id.id;
          console.log(
            "Testo",
            id,
            this.state.selectedService,
            serviceId,
            sitterListArray,
            d
          );
          if (
            // d.sitters.id === id &&
            this.state.selectedService === serviceId
          ) {
            sitterListArray = sitterListArray.map(function (el) {
              console.log("d.sitters.id", d.sitters.id);
              if (el.id === d.sitters.id) {
                const o = Object.assign({}, el);
                o.serviceRate = d.serviceRate;
                console.log("Testo1", o, d);
                return o;
              } else return el;
            });
            this.setState({
              sitterList: sitterListArray,
            });
            console.log("TestState", this.state.sitterList);
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleSortBy = (event) => {
    this.setState({
      sortBy: event.target.value,
    });
  };

  handleOwnershipType = (event) => {
    this.setState({
      ownershipType: {
        [event.target.name]: event.target.checked,
      },
    });
  };

  handleSelectedSitter = (index, sitterServiceId, sitter) => {
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/${sitterServiceId}`)
      .then((response) => {
        console.log("%%%%%%%%%%%%%", response);
        this.setState(
          {
            selectedSitter: response.data,
          },
          () => {
            console.log(
              "this.state.selectedSitter",
              this.state.selectedSitter.user.id
            );
            axios
              .get(
                `${config.apiUrl}/api/servicesprice/list?sitters=${response.data.id}`
              )
              .then((response) => {
                response.data.map((d) => {
                  console.log(
                    "****************",
                    d,
                    sitter.service_id.service_name
                  );
                  if (
                    d.sitters.id === sitterServiceId &&
                    d.service_id.id === sitter.service_id.id
                  ) {
                    this.setState({
                      individualrate: d.serviceRate,
                      selectedBookingId: d.id,
                      selectedMeetingId: d.id,
                      sitterData: this.state.selectedSitter,
                      selectedServiceName: sitter.service_id.service_name,
                      sitterProfileAddressPurposeId: this.state.selectedSitter
                        .user.id,
                    });
                  }
                });
              })
              .catch((err) => {
                console.log(err);
              });
            this.setState({
              sitterInfo: true,
            });
            this.getSitterPets();
          }
        );
      })
      .catch((err) => {
        console.log(err);
      });
  };
  getSitterPets = () => {
    if (this.state.selectedSitter.user.id) {
      axios
        .get(
          `${config.apiUrl}/api/petprofile/?user=${this.state.selectedSitter.user.id}`
        )
        .then((response) => {
          this.setState({
            sitterPets: response.data,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  handleWorkHours = (event) => {
    this.setState({
      workHours: event.target.value,
    });
  };

  clearAll = () => {
    // sitterList = [0];
    // setAdvancedFilters(!advancedFilters);
    // setWorkHours("Select Hours");

    this.setState({
      advancedFilters: false,
      ownershipType: {
        hasDog: false,
        hasCat: false,
        hasOther: false,
      },
      workHours: "Select Hours",
      sitterList: [],
      sitterInfo: false,
      selectedService: "",
    });
  };

  setAdvancedFilters = () => {
    this.setState({
      advancedFilters: !this.state.advancedFilters,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <section className={classes.searchSection}>
          <div style={{ padding: "3rem 0" }}>
            <div className={classes.searchGrid}>
              <FormControl className={classes.selectService}>
                <Select
                  value={this.state.selectedService}
                  onChange={this.handleServiceChange}
                  displayEmpty
                  className={classes.selectEmpty}
                  inputProps={{ "aria-label": "select service" }}
                >
                  <MenuItem value="">Select Service</MenuItem>
                  {this.state.services.length &&
                    this.state.services.map((service) => {
                      return service.service_type === "core services" ? (
                        <MenuItem key={service.id} value={service.id}>
                          {service.service_name}
                        </MenuItem>
                      ) : null;
                    })}
                </Select>
              </FormControl>
              <div style={{ marginLeft: 48 }}>
                <Grid container spacing={1} alignItems="center">
                  <Grid item style={{ paddingTop: 8 }}>
                    <LocationOnIcon />
                  </Grid>
                  <Grid item>
                    <TextField
                      id="input-with-icon-grid"
                      type="number"
                      label="Enter pincode"
                      className={classes.pincode}
                      name="postal_code"
                      onChange={(event) => {
                        console.log(event.target.value);
                        this.setState({
                          postal_code: event.target.value,
                        });
                      }}
                    />
                  </Grid>
                </Grid>
              </div>
              <Divider
                orientation="vertical"
                flexItem
                style={{ margin: "0 40px" }}
              />
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  margin="normal"
                  format="MM/dd/yyyy"
                  id="date-picker-inline"
                  value={this.state.startDate}
                  onChange={this.handleStartDate}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  InputAdornmentProps={{ position: "start" }}
                  className={classes.datePicker}
                  invalidDateMessage=""
                  placeholder="Start Date"
                />
              </MuiPickersUtilsProvider>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  margin="normal"
                  format="MM/dd/yyyy"
                  id="date-picker-inline"
                  value={this.state.endDate}
                  onChange={this.handleEndDate}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  InputAdornmentProps={{ position: "start" }}
                  className={classes.datePicker}
                  invalidDateMessage=""
                  placeholder="End Date"
                />
              </MuiPickersUtilsProvider>
              <Button
                variant="contained"
                color="primary"
                className={classes.orangeBtn}
                onClick={this.handleSearch}
              >
                Search
              </Button>
            </div>
          </div>
          <div style={{ textAlign: "center", paddingBottom: "2rem" }}>
            <Link
              className={classes.advancedFilterHeading}
              variant="h5"
              onClick={this.setAdvancedFilters}
            >
              Advanced Filters
            </Link>
          </div>
          {this.state.advancedFilters && (
            <div className={classes.advanceFilterContainer}>
              <Grid
                container
                justify="space-around"
                className={classes.advancedFilterGrid}
                spacing={1}
              >
                <Grid item xs={3}>
                  <Typography
                    variant="h6"
                    className={classes.advancedFilterHeaders}
                  >
                    Sort By
                  </Typography>
                  <FormControl variant="filled" className={classes.sortBy}>
                    <Select
                      labelId="demo-simple-select-filled-label"
                      id="demo-simple-select-filled"
                      value={this.state.sortBy}
                      onChange={this.handleSortBy}
                    >
                      <MenuItem value="Relevance">Relevance</MenuItem>
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={5}>
                  <Typography
                    variant="h6"
                    className={classes.advancedFilterHeaders}
                  >
                    Pet Ownership Type
                  </Typography>
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.ownershipType.hasDog}
                          onChange={this.handleOwnershipType}
                          name="hasDog"
                          className={classes.ownershipTypeCheckbox}
                        />
                      }
                      label="Has Dog"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.ownershipType.hasCat}
                          onChange={this.handleOwnershipType}
                          name="hasCat"
                          className={classes.ownershipTypeCheckbox}
                        />
                      }
                      label="Has Cat"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.ownershipType.hasOther}
                          onChange={this.handleOwnershipType}
                          name="hasOther"
                          className={classes.ownershipTypeCheckbox}
                        />
                      }
                      label="Has Other Animals"
                    />
                  </FormGroup>
                </Grid>
                <Grid item xs={3}>
                  <Typography
                    variant="h6"
                    className={classes.advancedFilterHeaders}
                  >
                    Sitter Work Schedule
                  </Typography>
                  <FormControl variant="filled" className={classes.sortBy}>
                    <Select
                      labelId="demo-simple-select-filled-label"
                      id="demo-simple-select-filled"
                      value={this.state.workHours}
                      onChange={this.handleWorkHours}
                    >
                      <MenuItem
                        value={"Select Hours"}
                        style={{ display: "none" }}
                      >
                        Select Schedule
                      </MenuItem>
                      <MenuItem value="don't work">Don't work</MenuItem>
                      <MenuItem value="work full time">Work full-time</MenuItem>
                      <MenuItem value="work part time">Work part-time</MenuItem>
                      <MenuItem value="Flexible work">Flexible work</MenuItem>
                      <MenuItem value="available mostly on weekends">
                        Available mostly on weekends
                      </MenuItem>
                      <MenuItem value="work from home">Work from home</MenuItem>
                      <MenuItem value="other">other</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={1}
                  style={{ display: "flex", alignItems: "flex-end" }}
                >
                  <Button onClick={this.clearAll} style={{ color: "#FFFFFF" }}>
                    Clear All
                  </Button>
                </Grid>
              </Grid>
            </div>
          )}
        </section>
        <section className={classes.sitterList}>
          {console.log("Search", this.state.sitterList, this.state.searchEmpty)}
          {this.state.sitterList.length === 0 && !this.state.searchEmpty && (
            <div
              style={{
                position: "relative",
                display: "flex",
                justifyContent: "center",
                height: "calc(100vh - 287px)",
                width: "100%",
                background: `linear-gradient(179.95deg, rgba(255, 255, 255, 0.3) 0.09%, rgba(255, 255, 255, 0) 73.47%), linear-gradient(178.6deg, rgba(255, 255, 255, 0.9) 2.38%, rgba(255, 255, 255, 0.651562) 90.11%, rgba(255, 255, 255, 0) 194.8%), url(${bgImage})`,
              }}
            >
              <Typography
                color="secondary"
                variant="h4"
                className={classes.chooseDateText}
              >
                Enter the Start Date & End Date above to search for the perfect
                sitter
              </Typography>
            </div>
          )}
          {this.state.searchEmpty && (
            <div
              style={{
                position: "relative",
                display: "flex",
                justifyContent: "center",
                height: "calc(100vh - 287px)",
                width: "100%",
                background: `linear-gradient(179.95deg, rgba(255, 255, 255, 0.3) 0.09%, rgba(255, 255, 255, 0) 73.47%), linear-gradient(178.6deg, rgba(255, 255, 255, 0.9) 2.38%, rgba(255, 255, 255, 0.651562) 90.11%, rgba(255, 255, 255, 0) 194.8%), url(${bgImage})`,
              }}
            >
              <Typography
                color="secondary"
                variant="h4"
                className={classes.chooseDateText}
              >
                No results
              </Typography>
            </div>
          )}
          {this.state.sitterList.length !== 0 && (
            <div style={{ padding: "64px 120px" }}>
              <Grid className={classes.sitterGrid}>
                <Grid
                  item
                  xs={5}
                  style={{ maxHeight: 700, overflowY: "scroll" }}
                >
                  {this.state.sitterList.map((sitter, index) => {
                    if (!sitter.vacation_mode) {
                      return (
                        <Card
                          className={classes.sitterCard}
                          style={{ marginBottom: 42, borderRadius: 8 }}
                          key={index}
                        >
                          <CardActionArea>
                            <div
                              onClick={() => {
                                this.handleSelectedSitter(
                                  index,
                                  sitter.sitters.id,
                                  sitter
                                );
                                this.setState({ sitterPets: null });
                              }}
                              style={{ display: "flex" }}
                            >
                              <CardMedia
                                component="img"
                                classes={{ media: classes.sitterMedia }}
                                width={192}
                                height={192}
                                image={
                                  sitter.sitters.profile_photo
                                    ? sitter.sitters.profile_photo
                                    : defaultPic
                                }
                                style={{ width: 192, height: 192 }}
                              />
                              <CardContent
                                style={{
                                  position: "relative",
                                  width: "100%",
                                }}
                              >
                                <Typography variant="h5" color="secondary">
                                  {sitter.sitters.user.first_name}
                                </Typography>
                                {/* <Rating name="read-only" value={4} readOnly />
                              <div style={{ display: "flex" }}>
                                <LocationOnIcon />
                                <Typography
                                  variant="body1"
                                  style={{ color: "#5E5A5A" }}
                                >
                                  1.2 mi
                                </Typography>
                              </div> */}
                                <Typography
                                  variant="body1"
                                  style={{
                                    color: "#979797",
                                    marginTop: "8px",
                                  }}
                                >
                                  {sitter.sitters.profile_intro}
                                </Typography>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                  }}
                                >
                                  <Chip
                                    variant="outlined"
                                    color="primary"
                                    style={{ fontSize: 20 }}
                                    avatar={
                                      <Avatar
                                        style={{
                                          backgroundColor: "#ffffff",
                                          color: "#E8770E",
                                        }}
                                      >
                                        <EuroSymbolIcon />
                                      </Avatar>
                                    }
                                    label={sitter.serviceRate}
                                  />
                                </div>
                              </CardContent>
                            </div>
                          </CardActionArea>
                        </Card>
                      );
                    } else return null;
                  })}
                </Grid>
                <Grid item xs={7}>
                  {this.state.sitterInfo && (
                    <div>
                      <Card className={classes.sitterInfoCard}>
                        <CardContent>
                          <div style={{ display: "flex" }}>
                            <img
                              src={this.state.selectedSitter.profile_photo}
                              alt="Pet Sitter Profile ****"
                              width="192"
                              height="192"
                            />
                            <div style={{ width: "100%", paddingLeft: "1rem" }}>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center",
                                }}
                              >
                                <Typography variant="h5" color="secondary">
                                  <Link
                                    className={classes.goToDetailedProfileLink}
                                    variant="h6"
                                    component={RouterLink}
                                    to={`/sitter-profile/${this.state.selectedSitter.user.id}`}
                                  >
                                    {this.state.selectedSitter.user.first_name}{" "}
                                  </Link>
                                </Typography>
                                <div style={{ textAlign: "right" }}>
                                  <Typography
                                    variant="subtitle1"
                                    style={{ fontSize: 18, color: "#979797" }}
                                  >
                                    {this.state.selectedServiceName}
                                  </Typography>
                                  <Chip
                                    variant="outlined"
                                    color="primary"
                                    style={{ fontSize: 20 }}
                                    avatar={
                                      <Avatar
                                        style={{
                                          backgroundColor: "#ffffff",
                                          color: "#E8770E",
                                        }}
                                      >
                                        <EuroSymbolIcon />
                                      </Avatar>
                                    }
                                    label={this.state.individualrate}
                                  />
                                </div>
                              </div>
                              {/* <Rating
                                name="read-only"
                                value={4}
                                readOnly
                                style={{ marginTop: "0.5rem" }}
                              />
                              <div
                                style={{
                                  display: "flex",
                                  marginTop: "0.5rem",
                                  alignItems: "center",
                                }}
                              >
                                <LocationOnIcon />
                                <Typography
                                  variant="body1"
                                  style={{ color: "#5E5A5A", fontSize: 25 }}
                                >
                                  1.2 mi
                                </Typography>
                              </div> */}
                              <Typography
                                variant="body1"
                                style={{
                                  color: "#979797",
                                  fontSize: 17,
                                }}
                              >
                                {this.state.selectedSitter.profile_intro}
                              </Typography>
                            </div>
                          </div>
                          <div style={{ padding: "32px 16px" }}>
                            <Grid style={{ display: "flex" }}>
                              <Grid item xs={5}>
                                <Typography
                                  color="secondary"
                                  style={{ fontWeight: 500 }}
                                >
                                  About
                                </Typography>
                                <Typography
                                  style={{ fontSize: 17, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter.profile_bio}
                                </Typography>
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1.0rem",
                                  }}
                                >
                                  My Pet Ownership
                                </Typography>
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    maxWidth: 300,
                                  }}
                                >
                                  {/* {this.state.selectedSitter.has_pet_choice.map(
                                    (pet, i) => {
                                      return pet === 1 ? (
                                        <div key={i}>
                                          <img
                                            src={dogImage}
                                            alt="dog silhouette"
                                          />
                                          <Typography>Dog</Typography>
                                        </div>
                                      ) : pet === 2 ? (
                                        <div key={i}>
                                          <img
                                            src={catImage}
                                            alt="cat silhouette"
                                          />
                                          <Typography>Cat</Typography>
                                        </div>
                                      ) : pet === 3 ? (
                                        <div key={i}>
                                          <img
                                            src={otherImg}
                                            alt="other pet animal"
                                            style={{
                                              width: "100%",
                                              margin: "0.55rem 0",
                                            }}
                                          />
                                          <Typography>Other</Typography>
                                        </div>
                                      ) : null;
                                    }
                                  )} */}
                                  <Typography
                                    style={{ fontSize: 17, color: "#5E5A5A" }}
                                  >
                                    {this.state.sitterPets
                                      ? this.state.sitterPets.length == 1
                                        ? "1 pet:"
                                        : this.state.sitterPets.length +
                                          " Pets:"
                                      : "No pets"}
                                    <br />
                                    {this.state.sitterPets
                                      ? this.state.sitterPets.map(
                                          (pet, key) => (
                                            <div>
                                              {pet.pet_choice === "dog" ? (
                                                <p>Dog : {pet.name}</p>
                                              ) : null}
                                              {pet.pet_choice === "cat" ? (
                                                <p>Cat : {pet.name}</p>
                                              ) : null}
                                              {pet.pet_choice ===
                                              "other animal" ? (
                                                <p>
                                                  {" "}
                                                  Other Animal : {pet.name}
                                                </p>
                                              ) : null}
                                              {pet.pet_choice === "null" ? (
                                                <p>No Pets</p>
                                              ) : null}
                                            </div>
                                          )
                                        )
                                      : null}
                                  </Typography>
                                </div>
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Dog Size Preference
                                </Typography>
                                {this.state.selectedSitter.dog_size_preference.map(
                                  (space, i) => {
                                    return space === 1 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Small
                                      </Typography>
                                    ) : space === 2 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Medium
                                      </Typography>
                                    ) : space === 3 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Large
                                      </Typography>
                                    ) : space === 4 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        No Preference
                                      </Typography>
                                    ) : (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        No Preference
                                      </Typography>
                                    );
                                  }
                                )}

                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Dog Age Preference
                                </Typography>
                                {this.state.selectedSitter.dog_age_preference.map(
                                  (space, i) => {
                                    return space === 1 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Puppy: 2-6 Months
                                      </Typography>
                                    ) : space === 2 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Puppy: 6-24 Months
                                      </Typography>
                                    ) : space === 3 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Adult Dog: 2 Years+
                                      </Typography>
                                    ) : space === 4 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Elderly Dog: 10Years+
                                      </Typography>
                                    ) : space === 5 ? (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        Any Age
                                      </Typography>
                                    ) : (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#5E5A5A",
                                        }}
                                      >
                                        No
                                      </Typography>
                                    );
                                  }
                                )}

                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Work Schedule
                                </Typography>
                                <Typography
                                  style={{ fontSize: 17, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter.work_schedule}
                                </Typography>
                              </Grid>
                              <Grid
                                item
                                xs={2}
                                style={{
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <Divider
                                  orientation="vertical"
                                  flexItem
                                  style={{
                                    height: "100%",
                                    backgroundColor: "#F58220",
                                  }}
                                />
                              </Grid>
                              <Grid item xs={5}>
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Type Of Home
                                </Typography>
                                <Typography
                                  style={{ fontSize: 17, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter.home_type}
                                </Typography>
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Size Of Home
                                </Typography>
                                <Typography
                                  style={{ fontSize: 17, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter.home_type_area
                                    ? this.state.selectedSitter.home_type_area +
                                      " " +
                                      this.state.selectedSitter
                                        .home_type_area_unit
                                    : "-"}
                                </Typography>
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  In My Home, I have
                                </Typography>
                                <Typography
                                  style={{ fontSize: 17, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter
                                    .home_type_description
                                    ? this.state.selectedSitter
                                        .home_type_description
                                    : "-"}
                                </Typography>

                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Open spaces in my home
                                </Typography>
                                <Typography
                                  style={{ fontSize: 20, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter.open_spaces_available.map(
                                    (space, i) => {
                                      return space === 1 ? (
                                        <Typography
                                          style={{
                                            color: "#5E5A5A",
                                            fontSize: 17,
                                          }}
                                        >
                                          Yes
                                        </Typography>
                                      ) : space === 2 ? (
                                        <Typography
                                          style={{
                                            color: "#5E5A5A",
                                            fontSize: 17,
                                          }}
                                        >
                                          Balcony
                                        </Typography>
                                      ) : space === 3 ? (
                                        <Typography
                                          style={{
                                            color: "#5E5A5A",
                                            fontSize: 17,
                                          }}
                                        >
                                          Patio
                                        </Typography>
                                      ) : space === 4 ? (
                                        <Typography
                                          style={{
                                            color: "#5E5A5A",
                                            fontSize: 17,
                                          }}
                                        >
                                          Terrace
                                        </Typography>
                                      ) : space === 5 ? (
                                        <Typography
                                          style={{
                                            color: "#5E5A5A",
                                            fontSize: 17,
                                          }}
                                        >
                                          Garden
                                        </Typography>
                                      ) : null;
                                    }
                                  )}
                                </Typography>

                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Open Spaces Outside
                                </Typography>
                                <Typography
                                  style={{ fontSize: 20, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter.access_to_green_spaces_outside.map(
                                    (space, i) => {
                                      return space === 1 ? (
                                        <Typography
                                          style={{
                                            fontSize: 17,
                                            color: "#5E5A5A",
                                          }}
                                        >
                                          Dog Park
                                        </Typography>
                                      ) : space === 2 ? (
                                        <Typography
                                          style={{
                                            fontSize: 17,
                                            color: "#5E5A5A",
                                          }}
                                        >
                                          Forest
                                        </Typography>
                                      ) : space === 3 ? (
                                        <Typography
                                          style={{
                                            fontSize: 17,
                                            color: "#5E5A5A",
                                          }}
                                        >
                                          Open Area or Ground
                                        </Typography>
                                      ) : (
                                        <Typography
                                          style={{
                                            fontSize: 17,
                                            color: "#5E5A5A",
                                          }}
                                        >
                                          No
                                        </Typography>
                                      );
                                    }
                                  )}
                                </Typography>
                                <Typography
                                  color="secondary"
                                  style={{
                                    fontWeight: 500,
                                    marginTop: "1rem",
                                  }}
                                >
                                  Children At Home
                                </Typography>
                                <Typography
                                  style={{ fontSize: 17, color: "#5E5A5A" }}
                                >
                                  {this.state.selectedSitter.children_at_home
                                    ? this.state.selectedSitter.children_at_home
                                    : "-"}
                                </Typography>
                              </Grid>
                            </Grid>
                          </div>
                          <CardActions style={{ display: "block" }}>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "flex-end",
                                width: "100%",
                              }}
                            >
                              <Link
                                className={classes.goToDetailedProfileLink}
                                variant="h6"
                                component={RouterLink}
                                to={`/sitter-profile/${this.state.selectedSitter.user.id}`}
                              >
                                Go To Detailed Profile &gt;
                              </Link>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                width: "95%",
                                margin: "0 auto",
                                marginTop: "2rem",
                                marginRight: "8px",
                              }}
                            >
                              <Button
                                variant="contained"
                                color="primary"
                                style={{
                                  color: "#FFFFFF",
                                  padding: "4 24px",
                                  borderRadius: 8,
                                  textTransform: "none",
                                  boxShadow:
                                    "0px 4px 4px rgba(128, 128, 128, 0.25)",
                                  fontSize: 20,
                                }}
                                onClick={this.handleContactModal}
                              >
                                Contact Sitter
                              </Button>
                              <Button
                                variant="contained"
                                color="primary"
                                style={{
                                  color: "#FFFFFF",
                                  padding: "4 24px",
                                  borderRadius: 8,
                                  textTransform: "none",
                                  boxShadow:
                                    "0px 4px 4px rgba(128, 128, 128, 0.25)",
                                  fontSize: 20,
                                }}
                                onClick={this.openMeetingModal}
                              >
                                Meeting Request
                              </Button>
                              <Button
                                variant="contained"
                                color="primary"
                                style={{
                                  color: "#FFFFFF",
                                  padding: "4 24px",
                                  borderRadius: 8,
                                  textTransform: "none",
                                  boxShadow:
                                    "0px 4px 4px rgba(128, 128, 128, 0.25)",
                                  fontSize: 20,
                                }}
                                // onClick={() => {
                                // window.location.href = `/booking-request/${2}`;
                                // }}
                                onClick={this.openBookingModal}
                              >
                                Book Directly
                              </Button>
                            </div>
                          </CardActions>
                        </CardContent>
                      </Card>
                    </div>
                  )}
                </Grid>

                {/* Booking request */}
                <Dialog
                  aria-labelledby="customized-dialog-title"
                  className={classes.modal}
                  open={this.state.bookingModalOpen}
                  onClose={this.closeBookingModal}
                  fullScreen={false}
                  maxWidth={"lg"}
                >
                  <DialogTitle
                    id="customized-dialog-title"
                    onClose={this.closeBookingModal}
                    className={classes.dialogTitle}
                  >
                    Booking Details
                    <IconButton
                      aria-label="close"
                      className={classes.closeButton}
                      onClick={this.closeBookingModal}
                    >
                      <CloseIcon />
                    </IconButton>
                  </DialogTitle>
                  <DialogContent className={classes.dialogContent}>
                    <BookingRequest
                      bookingID={this.state.selectedBookingId}
                      sitterID={this.state.selectedSitter.id}
                      sitterData={this.state.selectedSitter}
                      selectedService={this.state.selectServicePropData}
                    />
                    {console.log(
                      "wcbchcbkcbchkebckbcke",
                      this.state.selectedBookingId,
                      this.state.selectedSitter.id,
                      this.state.selectedSitter,
                      this.state.selectServicePropData
                    )}
                  </DialogContent>
                </Dialog>

                {/* <Modal
                  aria-labelledby="transition-modal-title"
                  aria-describedby="transition-modal-description"
                  className={classes.modal}
                  open={this.state.bookingModalOpen}
                  onClose={this.closeBookingModal}
                  closeAfterTransition
                  BackdropComponent={Backdrop}
                  BackdropProps={{
                    timeout: 500,
                  }}
                >
                  <Fade in={this.state.bookingModalOpen}>
                    <div className={classes.paper}>
                      <BookingRequest
                        bookingID={2}
                        sitterID={this.state.selectedSitter.id}
                      />
                    </div>
                  </Fade>
                </Modal> */}

                {/* Meeting request */}
                <Dialog
                  aria-labelledby="customized-dialog-title"
                  className={classes.modal}
                  open={this.state.meetingModalOpen}
                  onClose={this.closeMeetingModal}
                  fullScreen={false}
                  maxWidth={"lg"}
                >
                  <DialogTitle
                    id="customized-dialog-title"
                    onClose={this.closeMeetingModal}
                    className={classes.dialogTitle}
                  >
                    Create a Meeting Request
                    <IconButton
                      aria-label="close"
                      className={classes.closeButton}
                      onClick={this.closeMeetingModal}
                    >
                      <CloseIcon />
                    </IconButton>
                  </DialogTitle>
                  <DialogContent className={classes.dialogContent}>
                    <MeetingRequest
                      bookingID={this.state.selectedMeetingId}
                      sitterProfileAddressPurposeId={
                        this.state.sitterProfileAddressPurposeId
                      }
                      sitterID={this.state.selectedSitter.id}
                      sitterData={this.state.selectedSitter}
                      selectedService={this.state.selectServicePropData}
                    />
                  </DialogContent>
                </Dialog>
                {/* Contact sitter modal */}
                <ContactModal
                  modalOpen={this.state.contactOpen}
                  handleModal={this.handleContactModal}
                  profile_photo={
                    this.state.selectedSitter.profile_photo
                      ? this.state.selectedSitter.profile_photo
                      : null
                  }
                  name={
                    this.state.selectedSitter.user
                      ? this.state.selectedSitter.user.first_name +
                        " " +
                        this.state.selectedSitter.user.last_name
                      : null
                  }
                  userID={
                    this.state.selectedSitter.user
                      ? this.state.selectedSitter.user.id
                      : null
                  }
                />
                {/* <Modal
                  aria-labelledby="transition-modal-title"
                  aria-describedby="transition-modal-description"
                  className={classes.modal}
                  open={this.state.meetingModalOpen}
                  onClose={this.closeMeetingModal}
                  closeAfterTransition
                  BackdropComponent={Backdrop}
                  BackdropProps={{
                    timeout: 500,
                  }}
                >
                  <Fade in={this.state.meetingModalOpen}>
                    <div className={classes.paper}>
                      <MeetingRequest
                        bookingID={2}
                        sitterID={this.state.selectedSitter.id}
                      />
                    </div>
                  </Fade>
                </Modal> */}
              </Grid>
            </div>
          )}
        </section>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SitterSearch);
