import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import homeBanner from "../assets/home/home-banner.jpg";
import {
  Typography,
  Grid,
  Divider,
  IconButton,
  Avatar,
  FormControlLabel,
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  Checkbox,
  Button,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker,
} from "@material-ui/pickers";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import AddIcon from "@material-ui/icons/Add";

import { sitterActions } from "../actions";

let AddOnArray = [];
let PostAddOnArray = [];
let PetArray = [];

const useStyles = makeStyles((theme) => ({
  root: {
    // marginTop: 92,
    flexGrow: 1,
  },
  homeBanner: {
    background: `linear-gradient(0deg, rgba(94, 90, 90, 0.5), rgba(94, 90, 90, 0.5)), url(${homeBanner})`,
    backgroundSize: "cover",
    backgroundPosition: "top",
    minHeight: "100vh",
    padding: theme.spacing(12),
  },
  bookingOverlay: {
    backgroundColor: "rgba(245, 130, 32, 0.85)",
    width: "100%",
  },
  bookingHeader: {
    color: "white",
    fontSize: 30,
    fontFamily: "Fredoka One",
    textAlign: "left",
  },
  leftGrid: {
    borderRight: "1px solid #FFFFFF",
  },
  dateTextField: {
    color: "white !important",
    "& .MuiInput-underline:before": {
      display: "none",
    },
    "& .MuiInput-underline:after": {
      display: "none",
    },
  },
  dateInput: {
    color: "white !important",
  },
  dateIcon: {
    "& button": {
      color: "white !important",
    },
  },
  whiteDivider: {
    backgroundColor: "white",
    marginTop: 16,
    marginBottom: 8,
    height: 32,
    width: 1,
    marginRight: 48,
  },
  selectPetHeader: {
    fontSize: 22,
    fontWeight: 500,
    color: "white",
    textAlign: "left",
    marginTop: "2rem",
  },
  serviceWrapper: {
    marginRight: theme.spacing(5),
    border: "1px solid #FFFFFF",
    padding: theme.spacing(1),
    width: 209,
    borderRadius: 4,
  },
  bookingBtn: {
    color: "#FFFFFF",
    fontSize: 16,
    fontWeight: 500,
    border: "1px solid #FFFFFF",
    padding: theme.spacing(1),
  },
}));
const today = new Date();
const BookingRequest = (props) => {
  const classes = useStyles();
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [petData, setPetData] = useState([]);
  const [selectPet, setSelectPet] = useState(null);
  const [serviceList, setServiceList] = useState({});
  const [serviceName, setServiceName] = useState({});
  const [servicePrice, setServicePrice] = useState("0");
  const [ownerData, setOwnerData] = useState(null);
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [totalDays, setTotalDays] = useState(0);
  const [countStartDate, setCountStartDate] = useState(today);
  const [countEndDate, setCountEndDate] = useState(today);
  const [finalPrice, setFinalPrice] = useState(null);
  const [addOnData, setAddOnData] = useState([]);
  const [addOnArrayData, setAddOnArrayData] = useState([]);
  const [postAddOnArrayData, setPostAddOnArrayData] = useState([]);
  const [addOnRate, setAddOnRate] = useState(0);
  const [petArrayData, setPetArrayData] = useState([]);
  const [petArrayDataLength, setPetArrayDataLength] = useState(0);
  const [successBooking, setSuccessBooking] = useState(false);
  const [serviceType, setServiceType] = useState(null);
  const [selectedFinalPrice, setSelectedFinalPrice] = useState(0);
  const dispatch = useDispatch();
  const sitterServices = useSelector((state) => state.sitter.sitterServices);
  const sitter = props.sitterData;

  // const { service_id } = props.match.params.id || props.bookingID;

  // const getMyPet = () => {
  //   const userData = JSON.parse(localStorage.getItem("user"));
  //   const user_id = userData.user.id;

  //   fetch(`http://3.215.2.1:8000/api/petprofile/?user=${user_id}`, {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //   })
  //     .then((res) => {
  //       return res.json();
  //     })
  //     .then((data) => {
  //       console.log(data);
  //       setPetData(data);
  //     });
  // };

  const getServiceData = () => {
    fetch(`http://3.215.2.1:8000/api/servicesprice/list/${props.bookingID}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setServiceList(data);
        setServiceName(data.service_id.service_name);
        setServicePrice(data.serviceRate);
      });
  };

  const getSitterServiceData = () => {
    fetch(
      `http://3.215.2.1:8000/api/servicesprice/list?sitters=${props.sitterID}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        setAddOnData(data);
      });
  };

  const serviceListData = () => {
    return serviceList;
  };

  const serviceFinalPrice = () => {
    console.log("pet array data length **************", petArrayData);
    console.log("petArray 3", petArrayDataLength);
    const final =
      (parseFloat(servicePrice) * (totalDays === 0 ? 1 : totalDays) +
        addOnRate) *
      petArrayDataLength;
    setSelectedFinalPrice(final);
  };

  const getPetOwnerData = () => {
    const userData = JSON.parse(localStorage.getItem("user"));
    const user_id = userData.user.id;

    fetch(`http://3.215.2.1:8000/api/petownerprofile/list/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log("*********************", data);
        data.filter((d) => {
          if (d.user.id === user_id) {
            setOwnerData(d.id);
          }
        });
      });
  };

  const getPetData = () => {
    const userData = JSON.parse(localStorage.getItem("user"));
    const user_id = userData.user.id;

    fetch(`http://3.215.2.1:8000/api/petprofile/?user=${user_id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setPetData(data);
        // console.log("*********************", data);
        // data.filter((d) => {
        //   if (d.user.id === user_id) {
        //     setOwnerData(d.id);
        //   }
        // });
      });
  };

  const sendBookingRequest = () => {
    const user_id = JSON.parse(localStorage.getItem("user")).user.id;

    if (petArrayData.length === 0) {
      alert("Please Select Pets to continue");
    } else if (serviceType === null) {
      alert("Please select the service");
    } else {
      console.log(serviceType);
      const data = {
        startDate: startDate,
        endDate: endDate,
        num_dogs: petArrayData.length,
        total: selectedFinalPrice,
        services: JSON.stringify(serviceType),
        status: "open",
        startTime: startTime,
        endTime: endTime,
        textArea: null,
        sitterID: props.sitterID,
        ownerID: ownerData,
        pets: petArrayData,
        booking_completed: false,
        booking_id: `B${moment(startDate).format("DDMMYYYY")}`,
      };

      fetch("http://3.215.2.1:8000/api/booking/", {
        method: "POST", // or 'PUT'
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          setSuccessBooking(true);
          PetArray = [];
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  };

  const isBefore = (t1, t2) => {
    const t1Hours = t1.hours();
    const t1Minutes = t1.minutes();

    const t2Hours = t2.hours();
    const t2Minutes = t2.minutes();

    if (t2Hours < t1Hours) {
      return true;
    }

    return false;
  };

  const isAfter = (t1, t2) => {
    const t1Hours = t1.hours();
    const t1Minutes = t1.minutes();

    const t2Hours = t2.hours();
    const t2Minutes = t2.minutes();

    if (t2Hours > t1Hours) {
      return true;
    } else if (t2Hours === t1Hours) {
      return t2Minutes > t1Minutes ? true : false;
    }

    return false;
  };

  const countTotalDays = (formattedTime) => {
    console.log("startDate === endDate", startDate === endDate);

    if (countEndDate === null) {
      return setTotalDays(0);
    }
    if (countStartDate === countEndDate) {
      return setTotalDays(0);
    }

    const momentStartDate = moment(countStartDate);
    const momentEndDate = moment(countEndDate);

    const momentStartTime = moment()
      .set("hour", moment(startTime).hours())
      .set("minute", moment(startTime).minutes());

    const momentEndTime = moment()
      .set("hour", moment(endTime).hours())
      .set("minute", moment(endTime).minutes());

    if (
      serviceName === "Overnight Boarding at Dog Sitter's Home" ||
      serviceName === "Overnight Boarding at Dog Parent's Home"
    ) {
      const timeStart = moment(13, "HH");

      let diffDays = Math.floor(
        moment.duration(momentEndDate.diff(momentStartDate)).asDays()
      );

      console.log("diff days day caresss are", diffDays);

      if (
        startTime == null ||
        endTime == null ||
        (startTime == null && endTime == null)
      ) {
        console.log("start time or end Time are null", diffDays);
        return setTotalDays(diffDays);
      }

      if (
        isBefore(timeStart, momentStartTime) &&
        !isAfter(timeStart, momentEndTime)
      ) {
        console.log(
          `Extra charged because ${momentStartTime.hours()}:${momentStartTime.minutes()} is less than ${timeStart.hours()}:${timeStart.minutes()}`
        );
        diffDays += 0.5;
      }
      if (
        isAfter(timeStart, momentEndTime) &&
        !isBefore(timeStart, momentStartTime)
      ) {
        console.log(
          `Extra charged because ${momentEndTime.hours()}:${momentEndTime.minutes()} is greater than ${timeStart.hours()}:${timeStart.minutes()}`
        );
        diffDays += 0.5;
      }
      if (
        isBefore(timeStart, momentStartTime) &&
        isAfter(timeStart, momentEndTime)
      ) {
        console.log(
          `Extra charged because ${momentStartTime.hours()}:${momentStartTime.minutes()} is less than and after ***${timeStart.hours()}:${timeStart.minutes()}`
        );
        console.log("diff days are   before adding adnd multiplying", diffDays);
        diffDays += 0.5 * diffDays;
      }
      console.log("**********", diffDays);
      setTotalDays(diffDays);
    }
    console.log("service Name", serviceName);

    if (
      serviceName === "Day Care at Dog Parent's Home" ||
      serviceName === "Day Care at Dog Sitter's Home"
    ) {
      const timeStart = moment(9, "HH");
      const timeEnd = moment(18, "HH");

      let diffDays =
        Math.floor(
          moment.duration(momentEndDate.diff(momentStartDate)).asDays()
        ) + 1;

      if (
        startTime == null ||
        endTime == null ||
        (startTime == null && endTime == null)
      ) {
        console.log("start time or end Time are null", diffDays);
        return setTotalDays(diffDays);
      }
      if (
        isBefore(timeStart, momentStartTime) &&
        !isAfter(timeEnd, momentEndTime)
      ) {
        console.log(
          `Extra charged because ${momentStartTime.hours()}:${momentStartTime.minutes()}is Less than  ${timeStart.hours()}: ${timeStart.minutes()})`
        );
        diffDays += 0.5;
      }
      if (
        isAfter(timeEnd, momentEndTime) &&
        !isBefore(timeStart, momentStartTime)
      ) {
        console.log(
          `Extra charged because ${momentEndTime.hours()}:${momentEndTime.minutes()} is greater than ${timeEnd.hours()}:${timeEnd.minutes()}`
        );
        diffDays += 0.5;
      }
      if (
        isBefore(timeStart, momentStartTime) &&
        isAfter(timeEnd, momentEndTime)
      ) {
        console.log(
          `Extra charged because of  ${momentStartTime.hours()}:${momentStartTime.minutes()}is before & is after**** ${timeStart.hours()}: ${timeStart.minutes()})`
        );
        console.log(
          " ^^^^^^^^^^^^^^ diff days are before adding adnd multiplying",
          diffDays
        );

        diffDays += 0.5 * diffDays;
      }
      setTotalDays(diffDays);
    }
  };

  const handleStartDate = (date) => {
    const formatted = moment(date).format("YYYY-MM-DD");
    setCountStartDate(date);
    setStartDate(formatted);
  };

  const handleEndDate = (date) => {
    const formatted = moment(date).format("YYYY-MM-DD");
    setEndDate(formatted);
    setCountEndDate(date);
    countTotalDays();
  };

  const handleStartTime = (time) => {
    const formatted = moment(time);
    setStartTime(formatted);
    countTotalDays(formatted);
  };

  const handleEndTime = (time) => {
    const formatted = moment(time);
    setEndTime(formatted);
    countTotalDays(formatted);
  };

  const handlePetChange = (event) => {
    console.log("event target value***", event.target.value);

    if (event.target.checked === true) {
      PetArray.push(event.target.value);
      setSelectPet(event.target.value);
      console.log("petArray 1", PetArray);
    } else {
      const index = PetArray.indexOf(event.target.value);
      PetArray.splice(index, 1);
      setSelectPet(event.target.value);
      console.log("petArray 2", PetArray);
    }
    setPetArrayData(PetArray);
    setPetArrayDataLength(PetArray.length);
  };

  const handleServiceChange = (event, service) => {
    console.log(event.target.checked);
    setServiceType({
      core: { core_service: service },
      addOn: {},
    });
  };

  const handleAddOnChange = (event, service) => {
    let total = 0;
    if (event.target.checked === true) {
      AddOnArray.push(service);
      PostAddOnArray.push(service.id);
    } else {
      AddOnArray.pop(service);
      PostAddOnArray.pop(service.id);
    }

    setAddOnArrayData(AddOnArray);
    setPostAddOnArrayData(PostAddOnArray);

    setServiceType({
      ...serviceType,
      addOn: service,
    });

    AddOnArray.map((d) => {
      console.log(d);
      total += parseFloat(d.serviceRate);
    });
    setAddOnRate(total);
  };

  const selectedSitterServices = () => {
    console.log("selected sitter services", sitterServices, sitter);
    return (
      sitterServices &&
      sitterServices.filter((service) => {
        if (service.sitters.user.id === sitter.user.id) {
          console.log(service);
          return service;
        }
      })
    );
  };

  useEffect(() => {
    dispatch(sitterActions.getSingleSitter(4));
    dispatch(sitterActions.getSitterServices(4));
    getServiceData();
    // getMyPet();
    getPetOwnerData();
    serviceListData();
    getSitterServiceData();
    countTotalDays();
    getPetData();
    serviceFinalPrice();
    // setPetArrayData([]);
  }, [totalDays, addOnRate, petArrayData, startTime, endTime]);

  return successBooking === false ? (
    <div className={classes.root}>
      <div className={classes.bookingOverlay}>
        <Grid container>
          <Grid item xs={8} className={classes.leftGrid}>
            <div style={{ padding: 56 }}>
              <Typography className={classes.selectPetHeader}>
                Service Type
              </Typography>
              <div style={{ display: "flex", marginTop: "1rem" }}>
                {selectedSitterServices().map((service) => {
                  if (service.id === props.bookingID) {
                    return (
                      <div className={classes.serviceWrapper}>
                        <FormControlLabel
                          control={
                            <Checkbox
                              onChange={(event) => {
                                handleServiceChange(event, service);
                              }}
                              name={`service${service.id}`}
                              value={service.id}
                              style={{ color: "white" }}
                              checked="true"
                            />
                          }
                          label={service.service_id.service_name}
                          style={{
                            color: "white",
                            textAlign: "left !important",
                          }}
                        />
                        <div style={{ textAlign: "right" }}>
                          <Typography style={{ color: "white" }}>
                            {service.serviceRate}/
                            {service.service_id.id === 1 ||
                              service.service_id.id === 2
                              ? "day"
                              : "night"}
                          </Typography>
                        </div>
                      </div>
                    );
                  } else return <div />;
                })}
              </div>
              <Typography className={classes.selectPetHeader}>
                Dates When Service Required
              </Typography>
              <div style={{ display: "flex", marginTop: 16 }}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    margin="normal"
                    format="dd/MM/yyyy"
                    id="date-picker-inline"
                    value={startDate}
                    onChange={handleStartDate}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{
                      className: classes.dateIcon,
                      position: "start",
                    }}
                    className={classes.dateTextField}
                    invalidDateMessage=""
                    placeholder="Start Date"
                  />
                </MuiPickersUtilsProvider>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    margin="normal"
                    format="dd/MM/yyyy"
                    id="date-picker-inline"
                    value={endDate}
                    onChange={handleEndDate}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{
                      className: classes.dateIcon,
                      position: "start",
                    }}
                    className={classes.dateTextField}
                    invalidDateMessage=""
                    placeholder="End Date"
                  />
                </MuiPickersUtilsProvider>
                <Divider
                  orienation="vertical"
                  className={classes.whiteDivider}
                />
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardTimePicker
                    variant="inline"
                    margin="normal"
                    id="time-picker"
                    value={startTime}
                    onChange={handleStartTime}
                    KeyboardButtonProps={{
                      "aria-label": "change time",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{
                      className: classes.dateIcon,
                      position: "start",
                    }}
                    className={classes.dateTextField}
                    placeholder="Start Time"
                  />
                </MuiPickersUtilsProvider>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardTimePicker
                    variant="inline"
                    margin="normal"
                    id="time-picker"
                    value={endTime}
                    onChange={handleEndTime}
                    KeyboardButtonProps={{
                      "aria-label": "change time",
                    }}
                    inputProps={{ className: classes.dateInput }}
                    InputLabelProps={{ className: classes.dateInput }}
                    InputAdornmentProps={{
                      className: classes.dateIcon,
                      position: "start",
                    }}
                    className={classes.dateTextField}
                    placeholder="End Time"
                  />
                </MuiPickersUtilsProvider>
              </div>
              <Typography className={classes.selectPetHeader}>
                Select Your Pet
              </Typography>
              {/* <div style={{ display: "flex" }}>
                  <div style={{ marginRight: "1rem" }}>
                    <IconButton>
                      <Avatar src={require("../assets/home/gallery_3.jpg")} />
                    </IconButton>
                    <Typography style={{ textAlign: "center", color: "white" }}>
                      IIla
                    </Typography>
                  </div>
                  <div style={{ marginRight: "1rem" }}>
                    <IconButton>
                      <Avatar src={require("../assets/home/gallery_3.jpg")} />
                    </IconButton>
                    <Typography style={{ textAlign: "center", color: "white" }}>
                      Cody
                    </Typography>
                  </div>
                  <div style={{ marginRight: "1rem" }}>
                    <IconButton>
                      <Avatar src={require("../assets/home/gallery_3.jpg")} />
                    </IconButton>
                    <Typography style={{ textAlign: "center", color: "white" }}>
                      Brian
                    </Typography>
                  </div>
                  <div style={{ marginRight: "1rem" }}>
                    <IconButton>
                      <Avatar
                        style={{ backgroundColor: "white", color: "#F58220" }}
                      >
                        <AddIcon />
                      </Avatar>
                    </IconButton>
                    <Typography style={{ textAlign: "center", color: "white" }}>
                      Add
                    </Typography>
                  </div>
                </div> */}

              <FormControl
                component="fieldset"
                style={{
                  color: "white",
                  textAlign: "left",
                  display: "block",
                }}
              >
                {/* comment */}
                {petData.map((d, i) => {
                  if (d.pet_choice === "dog") {
                    console.log("selected pet ", d.id);
                    return (
                      <div
                        style={{ display: "flex", alignItems: "center" }}
                        key={i}
                      >
                        <input
                          type="checkbox"
                          name={d.id}
                          value={d.id}
                          onChange={handlePetChange}
                        />
                        <label style={{ color: "#ffffff", fontSize: 16 }}>
                          {d.name}
                        </label>
                      </div>
                    );
                  } else {
                    return null;
                  }
                })}
              </FormControl>

              <Typography className={classes.selectPetHeader}>
                Sitter also provides
              </Typography>

              <div style={{ display: "flex", marginTop: "1rem" }}>
                {addOnData.length !== 0 ? (
                  addOnData.map((addOn) => {
                    return addOn.service_id.service_type ===
                      "add-on Services" ? (
                        <div className={classes.serviceWrapper}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                onChange={(event) => {
                                  handleAddOnChange(event, addOn);
                                }}
                                name={addOn.service_id.service_name}
                                style={{ color: "white" }}
                              />
                            }
                            label={addOn.service_id.service_name}
                            style={{ color: "white" }}
                          />
                          <div style={{ textAlign: "right" }}>
                            <Typography style={{ color: "white" }}>
                              € {addOn.serviceRate} /hr
                          </Typography>
                          </div>
                        </div>
                      ) : null;
                  })
                ) : (
                    <p style={{ color: "#ffffff", fontSize: 14 }}>
                      No Add-On available
                    </p>
                  )}
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginTop: "3rem",
                }}
              >
                <Button
                  variant="outlined"
                  className={classes.bookingBtn}
                  onClick={() => {
                    if (petData.length === 0) {
                      alert("Please a add pet first to your profile");
                    } else {
                      sendBookingRequest();
                    }
                  }}
                >
                  Send Booking Request >>
                </Button>
              </div>
            </div>
          </Grid>
          <Grid item xs={4}>
            <div
              style={{
                padding: 40,
                textAlign: "left",
                width: 280,
                margin: "0 auto",
                marginTop: "3rem",
              }}
            >
              <Typography
                style={{ color: "white", fontSize: 22, fontWeight: 500 }}
              >
                Order Summary
              </Typography>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: 200,
                  margin: "0 auto",
                  marginTop: "1rem",
                }}
              >
                <Typography style={{ fontSize: 18, color: "white" }}>
                  No. of Days&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;
                </Typography>
                <Typography style={{ fontSize: 18, color: "white" }}>
                  &nbsp;{totalDays}&nbsp;
                </Typography>
              </div>
              <Typography
                style={{
                  color: "white",
                  fontSize: 18,
                  fontWeight: 500,
                  marginTop: "1.5rem",
                }}
              >
                Chosen Services
              </Typography>
              {selectedSitterServices() &&
                selectedSitterServices().map((service) => {
                  if (service.id === props.bookingID) {
                    return (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          width: 200,
                          margin: "0 auto",
                          marginTop: "1rem",
                        }}
                      >
                        <Typography style={{ fontSize: 18, color: "white" }}>
                          {service.service_id.service_name}
                          &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;
                        </Typography>
                        <Typography style={{ fontSize: 18, color: "white" }}>
                          &nbsp;€ {service.serviceRate}&nbsp;
                        </Typography>
                      </div>
                    );
                  } else return <div />;
                })}

              <Typography
                style={{
                  color: "white",
                  fontSize: 18,
                  fontWeight: 500,
                  marginTop: "1.5rem",
                }}
              >
                Add Ons
              </Typography>

              {addOnArrayData.map((d, i) => {
                console.log(d);
                return addOnArrayData.length > 0 ? (
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      width: 200,
                      margin: "0 auto",
                      marginTop: "1rem",
                      alignItems: "flex-end",
                    }}
                  >
                    <Typography style={{ fontSize: 18, color: "white" }}>
                      Add On <br></br>
                      {d.service_id.service_name}{" "}
                      &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;
                    </Typography>
                    <Typography style={{ fontSize: 18, color: "white" }}>
                      &nbsp;€ {d.serviceRate}&nbsp;
                    </Typography>
                  </div>
                ) : null;
              })}

              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: 200,
                  margin: "0 auto",
                  marginTop: "3rem",
                  alignItems: "flex-end",
                }}
              >
                <Typography style={{ fontSize: 18, color: "white" }}>
                  No Of Dogs&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;
                </Typography>
                <Typography style={{ fontSize: 18, color: "white" }}>
                  &nbsp;x {petArrayDataLength}&nbsp;
                </Typography>
              </div>
              <Divider style={{ backgroundColor: "white", margin: "1rem 0" }} />
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: 200,
                  margin: "0 auto",
                  marginTop: "1rem",
                  alignItems: "flex-end",
                }}
              >
                <Typography
                  style={{ fontSize: 25, color: "white", fontWeight: 500 }}
                >
                  Total&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;
                </Typography>
                <Typography style={{ fontSize: 18, color: "white" }}>
                  &nbsp;€ {selectedFinalPrice}&nbsp;
                </Typography>
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  ) : (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: 42,
        }}
      >
        <h2
          style={{
            color: "#E8770E",
            fontSize: 28,
            fontFamily: "Kanit",
          }}
        >
          Booking Request Sent Successfully
      </h2>
      </div>
    );
};

export default BookingRequest;
