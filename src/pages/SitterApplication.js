import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ApplyImg from "../assets/sitterApplication/apply 1.svg";
import PropTypes from "prop-types";
import moment from "moment";
import axios from "axios";
import MuiPhoneNumber from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { InputAdornment, IconButton } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import passwordUpdate from "../assets/accountSetting/passwordUpdate.png";
import Checkbox from "@material-ui/core/Checkbox";

import FormGroup from "@material-ui/core/FormGroup";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const DogBreeds = [
  {
    id: 1,
    title: "Retriever",
  },
  {
    id: 2,
    title: "German Shepherd",
  },
  {
    id: 3,
    title: "Golden Retriever",
  },
  {
    id: 4,
    title: "French Bulldog",
  },
  {
    id: 5,
    title: "Bulldog",
  },
  {
    id: 6,
    title: "Beagle",
  },
  // {
  //   id: 7,
  //   title: "",
  // },
  {
    id: 8,
    title: "Siberian Husky",
  },
  {
    id: 9,
    title: "Shiba Inu",
  },
  {
    id: 10,
    title: "Shih Tzu",
  },
  {
    id: 11,
    title: "St. Bernard",
  },
  {
    id: 12,
    title: "Basset Hound",
  },
  {
    id: 13,
    title: "Bernese Mountain Dog",
  },
  {
    id: 14,
    title: "Boston Terrier",
  },
  {
    id: 15,
    title: "Border Collie",
  },
  {
    id: 16,
    title: "Australian Shepherd",
  },
  {
    id: 17,
    title: "Chihuahua",
  },
  {
    id: 18,
    title: "Cocker Spaniel",
  },
  {
    id: 19,
    title: "Dachshund",
  },
  {
    id: 20,
    title: "Doberman Pinscher",
  },
  {
    id: 21,
    title: "French Bulldog",
  },
  {
    id: 22,
    title: "Great Dane",
  },
  {
    id: 23,
    title: "Havanese",
  },
  {
    id: 24,
    title: "Mastiff",
  },
  {
    id: 25,
    title: "Pembroke Welsh Corgi",
  },
  {
    id: 26,
    title: "Poodle",
  },
  {
    id: 27,
    title: "Pug",
  },
  {
    id: 28,
    title: "Rottweiler",
  },
  {
    id: 29,
    title: "Vizslas",
  },
  {
    id: 30,
    title: "Yorkshire Terrier",
  },
  {
    id: 31,
    title: "Dalmatian",
  },
  {
    id: 32,
    title: "Samoyed",
  },
  {
    id: 33,
    title: "Italian Greyhound",
  },
  {
    id: 34,
    title: "Russell Terrier",
  },
  {
    id: 35,
    title: "Japanese Chin",
  },
];

const styles = (theme) => ({
  section: {
    paddingTop: theme.spacing(12),
  },
  breadcrumbs: {
    color: "white",
    padding: theme.spacing(2),
    fontFamily: "kanit",
  },
  main: {},
  leftGrid: {
    backgroundColor: theme.palette.secondary.main,
    color: "white",
    textAlign: "center",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  rightGrid: {
    padding: theme.spacing(2),
  },
  form: {
    padding: theme.spacing(5),
    marginRight: theme.spacing(5),
    marginLeft: theme.spacing(5),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  formControlApplication: {
    margin: theme.spacing(1),
    width: "80%",
  },
  formHeading: {
    color: theme.palette.secondary.main,
  },
  formTextField: { width: "80%" },
  mobileInputField: {
    padding: "8px 20px",
  },
  alignMiddle: {
    position: "relative",
    top: "8%",
    marginLeft: 0,
    minHeight: "calc(100vh - 320px)",
  },
  navigation: {
    display: "flex",
    justifyContent: "space-evenly",
  },
  formSubtitle: {
    color: "#a1a1a1",
  },
  mobileNumberInput: {
    border: "none !important",
    borderBottom: "1px solid #949494 !important",
    borderRadius: "0px !important",
  },
});
const getSteps = ["Basic Info", "Additional Info", "Additional Information"];

class SitterApplication extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      mobile: null,
      state: null,
      country: null,
      radio1: null,
      isEmailModalOpen: true,
      city: null,
      pincode: null,
      confirm_password: null,
      showPassword: false,
      showModal: false,
      have_pet: null,
      dog_name: null,
      dog_breed: null,
      dog_date_of_birth: null,
      dog_gender: null,
      cat_name: null,
      cat_date_of_birth: null,
      cat_gender: null,
      other_animal: null,
      other_animal_type: null,
      other_date_of_birth: null,
      experience_with_dogs_choice: null,
      experience_with_dog_details: null,
      experience_with_other_animal: null,
      experience_with_other_animal_details: null,
      reason_for_joining: null,
      type_of_home: null,
      describe_home: null,
      size_home: null,
      open_spaces_in_your_home: null,
      open_spaces_outside: null,
      have_pet: {
        gilad: true,
        jason: false,
        antoine: false,
      },
    };
  }
  handlePetChange = (event) => {
    console.log(
      "have pet in **",
      this.state.have_pet,
      [event.target.name],
      event.target.checked
    );
    this.setState({
      ...this.state.have_pet,
      [event.target.name]: event.target.checked,
    });
  };

  handleNext = (e) => {
    if (this.state.activeStep == 0) {
      console.log("Sitter app", this.state.activeStep);
      this.setState((prevState) => {
        return { ...prevState, activeStep: prevState.activeStep + 1 };
      });
    } else if (this.state.activeStep == 1) {
      console.log("Sitter app", this.state.activeStep);
      this.setState((prevState) => {
        return { ...prevState, activeStep: prevState.activeStep + 1 };
      });
    } else if (this.state.activeStep == 2) {
      console.log("i am in active step 3");
      this.handleSubmit(e);
    }
    //   else {
    //   this.setState((prevState) => {
    //     return { ...prevState, activeStep: prevState.activeStep + 1 };
    //   });
    // }

    // this.handleEmailModal();
  };

  handleModal = () => {
    this.setState((prevState) => {
      return { showModal: !prevState.showModal };
    });
  };

  handleBack = () => {
    this.setState((prevState) => {
      return { ...prevState, activeStep: prevState.activeStep - 1 };
    });
  };

  handleReset = () => {
    this.setState({ activeStep: 0 });
  };
  handleChangeMobileCode = (value) => {
    this.setState({ mobile: value });
  };
  handleChangeState = (event) => {
    this.setState({ state: event.target.value });
  };
  handleChangeCountry = (event) => {
    this.setState({ country: event.target.value });
  };
  handleChangeRadio = (event) => {
    const { name, value } = event.target;
    console.log("event target value", name, value);
    this.setState({ [name]: value });
  };
  handleEmailModal = () => {
    this.setState((prevState) => {
      return { ...prevState, isEmailModalOpen: !prevState.isEmailModalOpen };
    });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    // let full_name = null;
    // console.log([name], value);
    if (name === "first_name") {
      const random_user_id = Math.floor(Math.random() * (999 - 100 + 1) + 100);
      console.log([name], value);
      this.setState((user) => ({
        ...user,
        [name]: value,
        username: `${this.state.first_name}_${this.state.last_name}_${random_user_id}`,
        is_sitter: true,
      }));
    } else {
      this.setState((user) => ({
        ...user,
        [name]: value,
        is_sitter: true,
      }));
    }

    console.log(this.state.have_pet);
  };

  handleTextChange = (event) => {
    this.setState((prevState) => {
      console.log("event ***", [event.target.name], event.target.value);
      return { ...prevState, [event.target.name]: event.target.value };
    });
  };
  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
  handleMouseDownPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
  handleConfirmPassword = (event) => {
    this.setState({ confirm_password: event.target.value });
  };
  handleSubmit = (e) => {
    console.log("submit handler   submit handler", this.state);
    e.preventDefault();
    const d = new Date();
    const today = moment(d).format("DDMMYYYY");
    const random_number = Math.floor(Math.random() * 100);
    const data = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      username: this.state.username,
      email: this.state.email,
      is_petowner: false,
      is_sitter: true,
      password1: this.state.password,
      password2: this.state.confirm_password,
      cust_id: `DS${today}${random_number}`,
    };

    axios
      .post(`${config.apiUrl}/rest-auth/registration/`, data)
      .then((response) => {
        console.log("Res ********", response);
        this.setState(
          (prevState) => {
            return {
              ...prevState,
              user_id: response.data.user.id,
              cust_id: response.data.user.cust_id,
            };
          },
          () => {
            this.createProfile(this.state.user_id, this.state.cust_id);
          }
        );
        // this.createProfile(response.data.user.id, response.data.user.cust_id);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  createProfile = (id, cust_id) => {
    const data = {
      cust_id: cust_id,
      profile_photo: null,
      mobile: this.state.mobile,
      // mobile_otp: null,
      street_address: this.state.street_address,
      house_no: this.state.house_no,
      city: this.state.city,
      state: this.state.state,
      state: this.state.state,
      postal_code: this.state.pincode,
      user: id,
      //
      experience_with_dogs_choice: this.state.experience_with_dogs_choice,
      experience_with_dog_details: this.state.experience_with_dog_details,
      experience_with_other_animal: this.state.experience_with_other_animal,
      experience_with_other_animal_details: this.state
        .experience_with_other_animal_details,
      reason_for_joining: this.state.reason_for_joining,
      home_type: this.state.type_of_home,
      home_type_description: this.state.describe_home,
      size_home: this.state.size_home,
      open_spaces_in_your_home: this.state.open_spaces_in_your_home,
      open_spaces_outside: this.state.open_spaces_outside,
      dog_name: this.state.dog_name,
      dog_breed: this.state.dog_breed,
      dog_date_of_birth: this.state.dog_date_of_birth,
      dog_gender: this.state.gender,
      cat_name: this.state.cat_name,
      cat_date_of_birth: this.state.cat_date_of_birth,
      cat_gender: this.state.cat_gender,
      other_animal: this.state.other_animal,
      other_animal_type: this.state.other_animal_type,
      other_date_of_birth: this.state.other_animal_type,
      experience_with_dogs_choice: this.state.experience_with_dogs_choice,
      experience_with_dog_details: this.state.experience_with_dogs_details,
      experience_with_other_animal: this.state.experience_with_other_animal,
      experience_with_other_animal_details: this.state
        .experience_with_other_animal,
      experience_with_pets_choice: this.state.experience_with_pets_choice,
      experience_with_pets: this.state.experience_with_pets,
      experience_with_dogs_choice: this.state.experience_with_dogs_choice,
      experience_with_dogs: this.state.experience_with_dogs,
      city: this.state.city,
      state: this.state.state,
      postal_code: this.state.postal_code,
      country: this.state.country,
      street_address: this.state.street_address,
      mobile: this.state.mobile,
      house_no: this.state.house_no,
    };
    console.log("data is available****", data);
    fetch(`${config.apiUrl}/api/sitterprofile/?user=${id}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
      .then((res) => {
        console.log("response in ********", res);
        return res;
      })
      .then((response) => {
        if (response.status === 201) {
          this.setState((prevState) => {
            return { ...prevState, activeStep: prevState.activeStep + 1 };
          });
          // window.location.href = "/login";
          this.addPets(id, cust_id);
          this.handleModal();
        } else {
          alert("Please try again");
        }
      });
  };

  addPets = (id, cust_id) => {
    const data = {
      user: id,
      cust_id: cust_id,
      pet_id: `${cust_id}_1`,
      type_of_pet: "dog",
      pet_choice: "dog",
      gender: this.state.gender,
      name: this.state.dog_name,
      breed: this.state.dog_breed,
      // profile_photo: this.state.profile_photo,
      dob: this.state.dog_date_of_birth,
      type_of_pet: "dog",
    };
    console.log("data in the backend ", data);
    axios
      .post(`${config.apiUrl}/api/petprofile/?user=${id}`, data)
      .then((response) => {
        console.log("Add Pets response", response);
        this.getSitterPets();
        this.handlePetModal();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getStepContent = (stepIndex) => {
    const { classes } = this.props;
    switch (stepIndex) {
      case 0:
        return (
          <div className={classes.form}>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    First Name
                  </Typography>
                  <TextField
                    id="first-name"
                    placeholder="Ex. John"
                    name="first_name"
                    value={this.state.first_name}
                    className={classes.formTextField}
                    onChange={this.handleChange}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Last Name
                  </Typography>
                  <TextField
                    id="last-name"
                    placeholder="Ex. Doe"
                    name="last_name"
                    value={this.state.last_name}
                    className={classes.formTextField}
                    onChange={this.handleChange}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Email
                  </Typography>
                  <TextField
                    id="email"
                    placeholder="johndoe@demo.com"
                    name="email"
                    value={this.state.email}
                    className={classes.formTextField}
                    onChange={this.handleChange}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Password
                  </Typography>
                  <TextField
                    id="password"
                    name="password"
                    placeholder="******"
                    type="password"
                    value={this.state.password}
                    className={classes.formTextField}
                    onChange={this.handleChange}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Confirm Password
                  </Typography>
                  <TextField
                    type={this.state.showPassword ? "text" : "password"} // <-- This is where the magic happens
                    onChange={this.handleConfirmPassword}
                    InputProps={{
                      // <-- This is where the toggle button is added.
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword ? (
                              <Visibility />
                            ) : (
                              <VisibilityOff />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                  {/* <TextField
                    id="confirm_password"
                    placeholder="******"
                    name="confirm_password"
                    type="password"
                    value={this.state.confirm_password}
                    className={classes.formTextField}
                    onChange={this.handleChange}
                  /> */}
                </Grid>
              </Grid>
            </form>
          </div>
        );
      case 1:
        return (
          <div className={classes.form}>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Country / Region
                    </Typography>
                    <TextField
                      id="country"
                      placeholder="Ex. India"
                      name="country"
                      className={classes.formTextField}
                      value={this.state.country}
                      onChange={this.handleChangeCountry}
                    />
                  </Grid>

                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Street Address
                    </Typography>
                    <TextField
                      id="street_address"
                      placeholder="Ex. Chruch Street"
                      name="street_address"
                      value={this.state.street_address}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      House No.
                    </Typography>
                    <TextField
                      id="house_no"
                      placeholder="Ex. Flat #3"
                      name="house_no"
                      value={this.state.house_no}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      City
                    </Typography>
                    <TextField
                      id="city"
                      placeholder="Ex. New York"
                      name="city"
                      value={this.state.city}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      State/ Province
                    </Typography>
                    <TextField
                      id="state"
                      placeholder="Ex. New York"
                      name="state"
                      value={this.state.state}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                    {/* <FormControl className={classes.formControlApplication}>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={this.state.state}
                        onChange={this.handleChangeState}
                      >
                        <MenuItem value={"Andra Pradesh"}>
                          Andra Pradesh
                        </MenuItem>
                        <MenuItem value={"Karnataka"}>Karnataka</MenuItem>
                        <MenuItem value={"Tamil Nadu"}>Tamil Nadu</MenuItem>
                      </Select>
                    </FormControl> */}
                  </Grid>

                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Zip code / Postcode
                    </Typography>
                    <TextField
                      id="postal_code"
                      placeholder="Ex. 567678"
                      name="postal_code"
                      className={classes.formTextField}
                      value={this.state.postal_code}
                      onChange={this.handleChange}
                    />
                  </Grid>

                  {/* <FormControl className={classes.formControlApplication}>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={this.state.country}
                        onChange={this.handleChangeCountry}
                      >
                        <MenuItem value={"India"}>India</MenuItem>
                        <MenuItem value={"USA"}>USA</MenuItem>
                        <MenuItem value={"Germany"}>Germany</MenuItem>
                      </Select>
                    </FormControl> */}

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Mobile Number
                    </Typography>
                    <MuiPhoneNumber
                      preferredCountries={["lu", "fr", "be", "de", "at"]}
                      onChange={this.handleChangeMobileCode}
                      style={{ margin: "10px 0px" }}
                      inputClass={classes.mobileNumberInput}
                    />
                    {/* <FormControl className={classes.formControl}>
                      <Select
                        labelId="mobileCode"
                        id="mobileCode"
                        value={this.state.mobileCode}
                        onChange={this.handleChangeMobileCode}
                      >
                        <MenuItem value={91}>+91</MenuItem>
                        <MenuItem value={92}>+92</MenuItem>
                        <MenuItem value={93}>+93</MenuItem>
                      </Select>
                    </FormControl>
                    <TextField
                      id="mobile"
                      placeholder="Ex. 9067"
                      className={classes.mobileInputField}
                      name="mobile"
                      value={this.state.mobile}
                      onChange={this.handleChange}
                    /> */}
                    <Button variant="outlined" justify="center" color="primary">
                      Verify
                    </Button>
                  </Grid>
                </Grid>
                {/* <Grid item xs={12}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Do you have your own dog?
                  </Typography>
                  <FormControl component="fieldset">
                    <RadioGroup
                      name="gender1"
                      value={this.state.radio1}
                      onChange={this.handleChangeRadio}
                      row
                    >
                      <FormControlLabel
                        value="yes"
                        control={<Radio />}
                        label="Yes"
                      />
                      <FormControlLabel
                        value="no"
                        control={<Radio />}
                        label="NO"
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid>
                {this.state.radio1 === "yes" ? (
                  <Grid item xs={12}>
                    <TextField id="radio1Text" />
                  </Grid>
                ) : null} */}

                {/* <Grid item xs={12}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Have you had dogs in the past??
                  </Typography>
                  <FormControl component="fieldset">
                    <RadioGroup
                      name="gender1"
                      value={this.state.radio1}
                      onChange={this.handleChangeRadio}
                      row
                    >
                      <FormControlLabel
                        value="yes"
                        control={<Radio />}
                        label="Yes"
                      />
                      <FormControlLabel
                        value="no"
                        control={<Radio />}
                        label="NO"
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid> */}
                {/* <Grid item xs={12}>
                  <TextField id="radio1Text" />
                </Grid> */}

                {/* <Grid item xs={12}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Do you have other animals at home?
                  </Typography>
                  <FormControl component="fieldset">
                    <RadioGroup
                      name="gender1"
                      value={this.state.radio1}
                      onChange={this.handleChangeRadio}
                      row
                    >
                      <FormControlLabel
                        value="yes"
                        control={<Radio />}
                        label="Yes"
                      />
                      <FormControlLabel
                        value="no"
                        control={<Radio />}
                        label="NO"
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid> */}
                {/* <Grid item xs={12}>
                  <TextField id="radio1Text" />
                </Grid> */}

                {/* <Grid item xs={12}>
                  <TypDhy
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Tell us why you want to be a dog sitter?
                  </Typography>
                </Grid> */}
                {/* <Grid item xs={12}>
                  <TextField id="radio1Text" />
                </Grid> */}
              </Grid>
            </form>
          </div>
        );
      case 2:
        return (
          <div className={classes.form}>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Do you have experience on looking after a dog?
                    </Typography>

                    <FormControl component="fieldset">
                      <RadioGroup
                        name="experience_with_dogs_choice"
                        value={this.state.experience_with_dogs_choice}
                        onChange={this.handleChangeRadio}
                        row
                      >
                        <FormControlLabel
                          value="Yes"
                          control={<Radio />}
                          label="Yes"
                        />
                        <FormControlLabel
                          value="No"
                          control={<Radio />}
                          label="NO"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      name="experience_with_dogs"
                      id="experience_with_dogs"
                      onChange={this.handleChange}
                      style={{ width: "80%" }}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Do you have experience on looking after any other animals?
                    </Typography>
                    <FormControl component="fieldset">
                      <RadioGroup
                        name="experience_with_pets_choice"
                        value={this.state.experience_with_pets_choice}
                        onChange={this.handleChangeRadio}
                        row
                      >
                        <FormControlLabel
                          value="Yes"
                          control={<Radio />}
                          label="Yes"
                        />
                        <FormControlLabel
                          value="No"
                          control={<Radio />}
                          label="NO"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      style={{ width: "80%" }}
                      id="experience_with_pets"
                      name="experience_with_pets"
                      onChange={this.handleChange}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Do you have pets at home? - select all that apply - dog,
                      cat, other animal
                    </Typography>
                    {/* <TextField
                      id="have_pet"
                      placeholder="Ex. Dog,Cat or Other Animal"
                      name="have_pet"
                      value={this.state.have_pet}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    /> */}

                    <FormControl
                      component="fieldset"
                      className={classes.formControl}
                    >
                      {/* <FormLabel component="legend">Assign responsibility</FormLabel> */}
                      <FormGroup>
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={this.state.dog}
                              onChange={this.handlePetChange}
                              name="dog"
                            />
                          }
                          label="Dog"
                        />
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={this.state.pet}
                              onChange={this.handlePetChange}
                              name="pet"
                            />
                          }
                          label="Pet"
                        />
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={this.state.other_animal}
                              onChange={this.handlePetChange}
                              name="other_animal"
                            />
                          }
                          label="Other Animal"
                        />
                      </FormGroup>
                    </FormControl>

                    {/* <input type='text'
                      id='have_pet'
                      placeholder="Ex. Dog,Cat or Other Animal"
                      name='have_pet'
                      style={{ display: "none" }}
                      value={this.state.have_pet}
                      className={classes.formTextField}
                      onChange={this.handleChange} /> */}
                    {/* <TextField
                      id='have_pet'
                      placeholder="Ex. Dog,Cat or Other Animal"
                      name='have_pet'
                      className={classes.formTextField}
                      value={this.state.have_pet}
                      style={{ width: "100%" }}
                      onChange={this.handleChange} /> */}
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                      style={{ color: "#212121" }}
                    >
                      Dog
                    </Typography>
                  </Grid>

                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Dog Name
                    </Typography>
                    <input
                      type="text"
                      id="have_pet"
                      placeholder="Ex. Dog,Cat or Other Animal"
                      name="have_pet"
                      style={{ display: "none" }}
                      value={this.state.have_pet}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                    <TextField
                      id="dog_name"
                      placeholder="Ex. Mickey"
                      name="dog_name"
                      value={this.state.dog_name}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Dog breed
                    </Typography>
                    {/* <input
                      type="text"
                      id="have_pet"
                      placeholder="Ex. Dog,Cat or Other Animal"
                      name="have_pet"
                      style={{ display: "none" }}
                      value={this.state.have_pet}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    /> */}
                    <Autocomplete
                      id="combo-box-demo"
                      options={DogBreeds}
                      getOptionLabel={(option) => option.title}
                      className={classes.dogBreeds}
                      onChange={(event, value) => {
                        event.preventDefault();
                        this.setState({
                          breed: value.id,
                        });
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label={"Dog Breed"}
                          variant="outlined"
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Date Of Birth
                    </Typography>
                    <TextField
                      id="date of Birth"
                      placeholder="Ex. 30,july,1990"
                      name="dog_date_of_birth"
                      type="date"
                      value={this.state.dog_date_of_birth}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Gender
                    </Typography>

                    <FormControl component="fieldset">
                      <RadioGroup
                        name="dog_gender"
                        value={this.state.dog_gender}
                        onChange={this.handleChangeRadio}
                        row
                      >
                        <FormControlLabel
                          value="male"
                          control={<Radio />}
                          label="Male"
                        />
                        <FormControlLabel
                          value="Female"
                          control={<Radio />}
                          label="Female"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  {/* <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Cat
                      </Typography>
                  </Grid>

                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Cat Name

                      </Typography>
                    <TextField
                      id="cat"
                      placeholder="Ex. India"
                      name="cat_name"
                      className={classes.formTextField}
                      value={this.state.cat_name}
                      onChange={this.handleChange}
                    />

                  </Grid> */}

                  {/* <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Date Of Birth

                      </Typography>
                    <TextField
                      id="cat_birth"
                      placeholder="Ex.12 june,1988 "
                      name="cat_date_of_birth"
                      type="date"
                      className={classes.formTextField}
                      value={this.state.cat_date_of_birth}
                      onChange={this.handleChange}
                    />

                  </Grid> */}

                  {/* <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Gender
                      </Typography>

                    <FormControl component="fieldset">
                      <RadioGroup
                        name="cat_gender"
                        value={this.state.cat_gender}
                        onChange={this.handleChangeRadio}
                        row
                      >
                        <FormControlLabel
                          value="male"
                          control={<Radio />}
                          label="Male"
                        />
                        <FormControlLabel
                          value="Female"
                          control={<Radio />}
                          label="Female"
                        />
                      </RadioGroup>
                    </FormControl>

                  </Grid> */}
                  {/* <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Other Animal
                      </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Other Animal Name
                      </Typography>
                    <TextField
                      id="other_animal"
                      placeholder="Ex. other animal name "
                      name="other_animal"
                      className={classes.formTextField}
                      value={this.state.other_animal}
                      onChange={this.handleChange}
                    />

                  </Grid> */}

                  {/* <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Type of Other Animal
                      </Typography>
                    <TextField
                      name="other_animal_type"
                      id="type_of_pet"
                      placeholder="Ex.cat "
                      className={classes.formTextField}
                      value={this.state.other_animal_type}
                      onChange={this.handleChange}
                    />

                  </Grid> */}
                  {/* <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Date Of Birth
                      </Typography>
                    <TextField
                      type="date"
                      id="other_date_of_birth"
                      placeholder="Ex.9 june,1990 "
                      className={classes.formTextField}
                      value={this.state.other_date_of_birth}
                      onChange={this.handleChange}
                    />
                  </Grid> */}
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Do you have experience on looking after any other dogs?
                    </Typography>
                    <FormControl component="fieldset">
                      <RadioGroup
                        name="experience_with_dogs_choice"
                        value={this.state.experience_with_dogs_choice}
                        onChange={this.handleChangeRadio}
                        row
                      >
                        <FormControlLabel
                          value="Yes"
                          control={<Radio />}
                          label="Yes"
                        />
                        <FormControlLabel
                          value="No"
                          control={<Radio />}
                          label="NO"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Experience With dogs Details
                    </Typography>
                    <TextField
                      id="experience_with_dog_details"
                      placeholder="Ex. 2 years"
                      className={classes.formTextField}
                      value={this.state.experience_with_dogs_details}
                      onChange={this.handleChange}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Do you have experience on looking after any other Animal?
                    </Typography>
                    <FormControl component="fieldset">
                      <RadioGroup
                        name="experience_with_other_animal"
                        value={this.state.experience_with_other_animal}
                        onChange={this.handleChangeRadio}
                        row
                      >
                        <FormControlLabel
                          value="Yes"
                          control={<Radio />}
                          label="Yes"
                        />
                        <FormControlLabel
                          value="No"
                          control={<Radio />}
                          label="NO"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Experience With Other Animal Details
                    </Typography>
                    <TextField
                      id="experience_with_dogs_details"
                      name="experience_with_dogs_details"
                      placeholder="Ex. 2 years"
                      className={classes.formTextField}
                      value={this.state.experience_with_dogs_details}
                      onChange={this.handleChange}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Tell us why you want to be a dog sitter? (reason for
                      joining)
                    </Typography>
                    <TextField
                      id="reason_for_joining"
                      name="reason_for_joining"
                      placeholder="Ex. 2 years"
                      className={classes.formTextField}
                      value={this.state.reason_for_joining}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Which Type Of Home You Live In?
                    </Typography>
                    <FormControl component="fieldset">
                      <RadioGroup
                        name="type_of_home"
                        value={this.state.type_of_home}
                        onChange={this.handleChangeRadio}
                        row
                      >
                        <FormControlLabel
                          value="house"
                          control={<Radio />}
                          label="House"
                        />
                        <FormControlLabel
                          value="Apartment"
                          control={<Radio />}
                          label="Apartment"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Describe your home
                    </Typography>
                    <TextField
                      id="describe_home"
                      name="describe_home"
                      placeholder="Ex. My Home is a Sweet Home."
                      className={classes.formTextField}
                      value={this.state.describe_home}
                      onChange={this.handleChange}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      What is the size Of Home
                    </Typography>
                    <TextField
                      id="size_home"
                      name="size_home"
                      placeholder="Ex. 24 Sq.feet"
                      className={classes.formTextField}
                      value={this.state.size_home}
                      onChange={this.handleChange}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Open Spaces In your Home
                    </Typography>
                    <TextField
                      id="open_spaces"
                      name="open_spaces_in_your_home"
                      placeholder="Ex.12 sq feet"
                      className={classes.formTextField}
                      value={this.state.open_spaces_in_your_home}
                      onChange={this.handleChange}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Open Spaces OutSide
                    </Typography>
                    <TextField
                      id="size_home"
                      name="open_spaces_outside"
                      placeholder="Ex. 12 sq feet"
                      className={classes.formTextField}
                      value={this.state.open_spaces_outside}
                      onChange={this.handleChange}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </div>
        );
      default:
        return "Unknown stepIndex";
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <section className={classes.section}>
          <Grid container spacing={0} className={classes.main}>
            <Grid item xs={4} className={classes.leftGrid}>
              <Breadcrumbs
                aria-label="breadcrumb"
                className={classes.breadcrumbs}
              >
                <Link color="inherit" href="/" o>
                  Become a sitter
                </Link>
                <Typography color="white">Apply as sitter</Typography>
              </Breadcrumbs>
              <div className={classes.alignMiddle}>
                <h1
                  style={{
                    fontFamily: "Fredoka One",
                    fontSize: 38,
                    textAlign: "left",
                    marginLeft: 82,
                  }}
                >
                  Apply as Sitter
                </h1>
                <p
                  style={{
                    fontFamily: "Kanit",
                    fontSize: 24,
                    textAlign: "left",
                    marginTop: 4,
                    maxWidth: 270,
                    marginLeft: 82,
                  }}
                >
                  Fill the forms to become a sitter...
                </p>
                <img
                  src={ApplyImg}
                  style={{
                    height: 246,
                  }}
                ></img>
              </div>
            </Grid>
            <Grid item xs={8} className={classes.rightGrid}>
              <Stepper activeStep={this.state.activeStep} alternativeLabel>
                {getSteps.map((label) => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
              <div>
                {this.state.activeStep === getSteps.length ? (
                  <div>
                    <Typography className={classes.instructions}>
                      All steps completed
                    </Typography>
                    <Button onClick={this.handleReset}>Reset</Button>
                  </div>
                ) : (
                  <div>
                    <Typography className={classes.instructions}>
                      {this.getStepContent(this.state.activeStep)}
                    </Typography>
                    <div className={classes.navigation}>
                      <Button
                        disabled={this.state.activeStep === 0}
                        onClick={this.handleBack}
                        className={classes.backButton}
                      >
                        Back
                      </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.handleNext}
                      >
                        <span style={{ color: "#ffffff !important" }}>
                          {this.state.activeStep === getSteps.length - 1
                            ? "Submit"
                            : "Next"}
                        </span>
                      </Button>
                    </div>
                  </div>
                )}
              </div>
            </Grid>
          </Grid>
        </section>

        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={this.state.showModal}
          onClose={this.handleModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.showModal}>
            <div className={classes.paper}>
              <Typography color="primary" className={classes.modalhead1}>
                You're registered, please verify your email
              </Typography>
              <Typography
                className={classes.modalhead2}
                style={{ maxWidth: 520 }}
              >
                Congratulations, you have successfully registered as Dog Parent,
                you're one step away, please verify your email address using the
                link recieved at {this.state.email}.
              </Typography>
              <img src={passwordUpdate} style={{ marginBotton: 24 }} />

              <Grid container justify="center" style={{ marginTop: "1rem" }}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.orangeBtn}
                  onClick={() => {
                    this.handleModal();
                    window.location.href = "/login";
                  }}
                >
                  Continue
                </Button>
              </Grid>
            </div>
          </Fade>
        </Modal>
      </div>
    );
  }
}

SitterApplication.propTypes = {};

export default withStyles(styles, { withTheme: true })(SitterApplication);

{
  /* <TextField
  id="have_pet"
  placeholder="Ex. Dog,Cat or Other Animal"
  name="have_pet"
  value={this.state.have_pet}
  className={classes.formTextField}
  onChange={this.handleChange}
/>


<TextField
  id="street_address"
  placeholder="Ex. Chruch Street"
  name="street_address"
  value={this.state.street_address}
  className={classes.formTextField}
  onChange={this.handleChange}
/> */
}
