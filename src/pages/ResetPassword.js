import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Card,
  CardContent,
  FormControlLabel,
  Checkbox,
  Button,
  TextField,
  Link,
  Icon,
} from "@material-ui/core";
import google from "../assets/login/google.png";
import facebook from "../assets/login/facebook.svg";
import { Link as RouterLink } from "react-router-dom";
import loginBanner from "../assets/login/login-banner.png";
import { userActions } from "../actions";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
  },
  bannerContainer: {
    position: "absolute",
    padding: 64,
    "& h1": {
      fontSize: 32,
      fontFamily: "Fredoka One",
      color: "#ffffff",
      maxWidth: 380,
    },
    "& p": {
      fontSize: 24,
      fontFamily: "Fredoka One",
      color: "#ffffff",
      maxWidth: 380,
    },
  },
  loginCard: {
    width: 577,
    margin: "0 auto",
    marginTop: theme.spacing(18),
    border: "1px solid rgba(232, 119, 14, 0.7)",
    boxSizing: "border-box",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
  },
  CardContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  topCardHeader: {
    width: 250,
    height: 56,
    border: "1px solid #e87837",
    borderRadius: "4px",
    position: "absolute",
    top: 182,
    backgroundColor: "#ffffff",
    borderBottom: "1px solid #ffffff",
    "& h3": {
      textAlign: "center",
      color: "#E8770E",
      fontSize: 22,
      fontWeight: "normal",
      margin: 12,
    },
  },
  login: {
    padding: theme.spacing(2),
    "& div": {
      width: "100%",
      marginBottom: theme.spacing(1),
    },
  },

  checkbox: {
    color: "#E8770E",
  },
  checkboxLabel: {
    color: "#E8770E !important",
    "& span": {
      fontSize: 14,
    },
  },
  dividerContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: theme.spacing(3),
  },
  socialButtons: {
    margin: theme.spacing(0, 5),
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
  },
  registerSection: {
    margin: theme.spacing(4, 0),
    textAlign: "center",
  },
  //   dividerContainer: {
  //     display: "flex",
  //     alignItems: "center",
  //     justifyContent: "center",
  //     marginTop: theme.spacing(3),
  //   },
  dividerBorder: {
    borderBottom: "1px solid #E8770E",
    width: "20% !important",
    marginBottom: "0 !important",
  },
  dividerContent: {
    padding: theme.spacing(0, 1),
    color: "#E8770E",
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    padding: theme.spacing(1, 4),
  },
}));

const CssTextField = withStyles({
  root: {
    color: "#E8770E",
    "& label": {
      color: "#E8770E",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "1px solid #E8770E",
    },
    "& label.Mui-focused": {
      color: "#3EB3CD",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#3EB3CD",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "2px solid #3EB3CD",
    },
  },
})(TextField);

const FeatureIcon = (props) => {
  return (
    <Icon>
      <img
        alt="edit"
        src={props.src}
        width={props.width ? props.width : "100%"}
        height={props.height ? props.height : ""}
      />
    </Icon>
  );
};

const Divider = ({ children }) => {
  const classes = useStyles();

  return (
    <div className={classes.dividerContainer}>
      <div className={classes.dividerBorder} />
      <span className={classes.dividerContent}>{children}</span>
      <div className={classes.dividerBorder} />
    </div>
  );
};

const ResetPassword = () => {
  const classes = useStyles();
  const [user, setUser] = useState({
    name: "",
    username: "",
    first_name: "",
    last_name: "",
    email: "",
    password1: "",
    password2: "",
  });
  const [checked, setCheckedValue] = useState(true);

  const dispatch = useDispatch();

  function handleChange(e) {
    const { name, value } = e.target;
    let full_name = null;
    console.log(e.target, [name], value);
    if (name === "name") {
      full_name = value.split(" ");
      const random_user_id = Math.floor(Math.random() * (999 - 100 + 1) + 100);

      setUser((user) => ({
        ...user,
        [name]: value,
        first_name: full_name !== null ? full_name[0] : "",
        last_name: full_name !== null ? full_name[1] : "",
        username: `${full_name[0]}_${full_name[1]}_${random_user_id}`,
        is_petowner: true,
      }));
    } else {
      setUser((user) => ({
        ...user,
        [name]: value,
        is_petowner: true,
      }));
    }
  }

  const handleCheckboxChange = (event) => {
    setCheckedValue(event.target.checked);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    window.location.href = "/login";
  };

  return (
    <div className={classes.root}>
      <Grid container justify="center">
        <Grid item xs={5}>
          <div className={classes.bannerContainer}>
            <h1>Welcome to DogStays</h1>
            <p>
              We’ll connect you with just the right dog person to look after
              your dog.
            </p>
          </div>
          <img
            src={loginBanner}
            alt="login banner"
            width="100%"
            height="100%"
          />
        </Grid>
        <Grid item xs={7}>
          <div>
            <Card className={classes.loginCard}>
              <div className={classes.CardContainer}>
                <div className={classes.topCardHeader}>
                  <h3>Reset Password ?</h3>
                </div>
              </div>
              <CardContent>
                <div className={classes.login}>
                  <CssTextField
                    id="password"
                    label="New Password"
                    type="password"
                    name="password1"
                    value={user.password1}
                    onChange={handleChange}
                  />
                  <CssTextField
                    id="confirm-password"
                    label="Confirm Password"
                    type="text"
                    name="password2"
                    value={user.password2}
                    onChange={handleChange}
                  />

                  <Grid
                    container
                    justify="center"
                    style={{ marginTop: "1rem" }}
                  >
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.orangeBtn}
                      onClick={handleSubmit}
                    >
                      Confirm
                    </Button>
                  </Grid>
                </div>
              </CardContent>
            </Card>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default ResetPassword;
