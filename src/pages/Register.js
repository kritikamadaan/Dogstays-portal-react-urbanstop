import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Card,
  CardContent,
  FormControlLabel,
  Checkbox,
  Button,
  TextField,
  Link,
  Icon,
} from "@material-ui/core";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import google from "../assets/login/google.png";
import facebook from "../assets/login/facebook.svg";
import { Link as RouterLink } from "react-router-dom";
import regBanner from "../assets/register/reg.svg";
import regBanner2 from "../assets/register/2.svg";
import regBanner3 from "../assets/register/3.svg";
import regBanner4 from "../assets/register/4.svg";
import regBanner5 from "../assets/register/5.svg";
import regBanner6 from "../assets/register/6.svg";
import { userActions } from "../actions";
import moment from "moment";
import axios from "axios";
import "react-phone-input-2/lib/style.css";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { InputAdornment, IconButton } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import passwordUpdate from "../assets/accountSetting/passwordUpdate.png";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const styles = (theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
  sliderContainer: {
    height: "100%",
    width: "100%",
    background:
      "linear-gradient(136.96deg, #21B9CC 32.83%, rgba(33, 185, 204, 0) 135.72%)",
  },
  sliderBody: {
    padding: theme.spacing(12),
    display: "flex !important",
    flexDirection: "column",
    justifyContent: "center",
    "& h2": {
      color: "#ffffff",
      fontSize: 24,
      fontFamily: "Fredoka One",
      lineHeight: "32px",
    },
  },
  sliderContent: {
    "& h2": {
      color: "#ffffff",
      fontSize: 20,
      textAlign: "center",
      fontFamily: "Kanit",
    },
    "& p": {
      color: "#ffffff",
      fontSize: 16,
      textAlign: "center",
      fontFamily: "Kanit",
    },
  },
  slickMain: {
    height: "100%",
    "& div.slick-list": {
      height: "100%",
    },
    "& div.slick-track": {
      height: "100%",
    },
    // "& div.slick-slide div": {
    //   height: "100%",
    // },
  },
  button__bar: {
    position: "relative",
    bottom: 132,
    display: "flex !important",
    justifyContent: "center",
    listStyle: "none",
    "& li": {
      margin: 12,
    },
    "& .slick-active": {
      height: 12,
      width: 12,
      border: "none",
      borderRadius: "50%",
      background: "#FFFFFF",
      opacity: "1 !important",
    },
    "& li button": {
      borderRadius: "50%",
      background: "#FFFFFF",
      opacity: 0.6,
      color: "transparent",
      height: 12,
      width: 12,
      border: "none",
    },
    "& li button:focus": {
      borderRadius: "50%",
      background: "#FFFFFF",
      opacity: 0.6,
      color: "transparent",
      height: 12,
      width: 12,
      border: "none",
      outline: "none",
    },
  },
  loginCard: {
    width: 577,
    margin: "0 auto",
    marginTop: theme.spacing(12),
    marginBottom: theme.spacing(10),
    border: "1px solid rgba(232, 119, 14, 0.7)",
    boxSizing: "border-box",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
  },
  CardContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  topCardHeader: {
    width: 350,
    height: 56,
    border: "1px solid #e87837",
    borderRadius: "4px",
    position: "absolute",
    top: 134,
    backgroundColor: "#ffffff",
    borderBottom: "1px solid #ffffff",
    "& h3": {
      textAlign: "center",
      color: "#E8770E",
      fontSize: 22,
      fontWeight: "normal",
      margin: 12,
    },
  },
  cardHead: {
    width: "100%",
    "& p": {
      textAlign: "center",
      color: "#E8770E",
      fontSize: "14px",
      marginTop: 42,
      marginBottom: 0,
    },
  },
  login: {
    padding: theme.spacing(2),
    "& div": {
      width: "100%",
      marginBottom: theme.spacing(1),
    },
  },
  checkbox: {
    color: "#E8770E",
  },
  checkboxLabel: {
    color: "#E8770E !important",
    "& span": {
      fontSize: 14,
    },
  },
  dividerContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: theme.spacing(3),
  },
  socialButtons: {
    margin: theme.spacing(0, 5),
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
  },
  registerSection: {
    margin: theme.spacing(4, 0),
    textAlign: "center",
  },
  // dividerContainer: {
  //   display: "flex",
  //   alignItems: "center",
  //   justifyContent: "center",
  //   marginTop: theme.spacing(3),
  // },
  dividerBorder: {
    borderBottom: "1px solid #E8770E",
    width: "20% !important",
    marginBottom: "0 !important",
  },
  dividerContent: {
    padding: theme.spacing(0, 1),
    color: "#E8770E",
  },
  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    padding: theme.spacing(1, 4),
  },
});

const CssTextField = withStyles({
  root: {
    color: "#E8770E",
    "& label": {
      color: "#E8770E",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "1px solid #E8770E",
    },
    "& label.Mui-focused": {
      color: "#3EB3CD",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#3EB3CD",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "2px solid #3EB3CD",
    },
  },
})(TextField);

const FeatureIcon = (props) => {
  return (
    <Icon>
      <img
        alt="edit"
        src={props.src}
        width={props.width ? props.width : "100%"}
        height={props.height ? props.height : ""}
      />
    </Icon>
  );
};

const Divider = (props, { children }) => {
  const { classes } = props.classes;

  return (
    <div className={classes.dividerContainer}>
      <div className={classes.dividerBorder} />
      <span className={classes.dividerContent}>{children}</span>
      <div className={classes.dividerBorder} />
    </div>
  );
};

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      full_name: null,
      first_name: "",
      last_name: "",
      username: "",
      is_petowner: false,
      is_sitter: false,
      checked: false,
      showPassword: false,
      showModal: false,
    };
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    let full_name = null;
    // console.log(e.target, [name], value);
    if (name === "first_name") {
      const random_user_id = Math.floor(Math.random() * (999 - 100 + 1) + 100);

      this.setState((user) => ({
        ...user,
        [name]: value,
        username: `${this.state.first_name}_${this.state.last_name}_${random_user_id}`,
        is_petowner: true,
        is_sitter: false,
      }));
    } else {
      this.setState((user) => ({
        ...user,
        [name]: value,
        is_petowner: true,
        is_sitter: false,
      }));
    }
  };
  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
  handleMouseDownPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
  handleConfirmPassword = (event) => {
    this.setState({ password2: event.target.value });
  };
  handleCheckboxChange = (event) => {
    this.setState({
      checked: event.target.checked,
    });
  };

  handleModal = () => {
    this.setState((prevState) => {
      return { showModal: !prevState.showModal };
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const d = new Date();
    const today = moment(d).format("DDMMYYYY");
    const random_number = Math.floor(Math.random() * 100);
    const data = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      username: this.state.username,
      email: this.state.email,
      is_petowner: this.state.is_petowner,
      is_sitter: this.state.is_sitter,
      password1: this.state.password1,
      password2: this.state.password2,
      cust_id: `DP${today}${random_number}`,
    };

    axios
      .post(`${config.apiUrl}/rest-auth/registration/`, data)
      .then((response) => {
        console.log("Res", response);
        this.createProfile(response.data.user.id, response.data.user.cust_id);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  createProfile = (id, cust_id) => {
    const data = {
      id: 1,
      cust_id: cust_id,
      profile_photo: null,
      mobile: "",
      mobile_otp: null,
      address: "",
      postal_code: "",
      have_insurance: null,
      insurance_details: "",
      id_type: "",
      id_number: "",
      id_image: null,
      pet_gallary: null,
      user: id,
      has_pet: [],
    };

    fetch(`${config.apiUrl}/api/petownerprofile/?user=${id}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
      .then((res) => {
        return res;
      })
      .then((response) => {
        if (response.status === 201) {
          // window.location.href = "/login";
          this.handleModal();
        } else {
          alert("Please try again");
        }
      });
  };

  render() {
    const { classes } = this.props;

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      className: classes.slickMain,
      dotsClass: classes.button__bar,
    };

    return (
      <div className={classes.root}>
        <Grid container justify="center">
          <Grid item xs={5}>
            <div className={classes.sliderContainer}>
              <Slider {...settings}>
                <div className={classes.sliderBody}>
                  <h2>
                    Now register to create your own DogStays account and find
                    and book a dog sitter even faster!
                  </h2>

                  <img src={regBanner} alt="" />

                  <div className={classes.sliderContent}>
                    <h2>
                      View More Search Results and Detailed Sitter Profiles
                    </h2>
                    <p>
                      By registering you get access to more sitter search
                      results and detailed sitter profiles
                    </p>
                  </div>
                </div>

                <div className={classes.sliderBody}>
                  <h2>
                    Now register to create your own DogStays account and find
                    and book a dog sitter even faster!
                  </h2>

                  <img src={regBanner2} alt="" />

                  <div className={classes.sliderContent}>
                    <h2>Use Advanced Search Filters</h2>
                    <p>
                      As a registered user, you will be able to use more
                      advanced options to filter sitter search results
                    </p>
                  </div>
                </div>

                <div className={classes.sliderBody}>
                  <h2>
                    Now register to create your own DogStays account and find
                    and book a dog sitter even faster!
                  </h2>

                  <img src={regBanner3} alt="" />

                  <div className={classes.sliderContent}>
                    <h2>Create a Detailed Profile for Your Dog</h2>
                    <p>
                      We will automatically tell you sitter all about your dog
                      before your bookings start
                    </p>
                  </div>
                </div>

                <div className={classes.sliderBody}>
                  <h2>
                    Now register to create your own DogStays account and find
                    and book a dog sitter even faster!
                  </h2>

                  <img src={regBanner4} alt="" />

                  <div className={classes.sliderContent}>
                    <h2>Access Reviews and ratings</h2>
                    <p>View ratings and reviews by other dog parents</p>
                  </div>
                </div>

                <div className={classes.sliderBody}>
                  <h2>
                    Now register to create your own DogStays account and find
                    and book a dog sitter even faster!
                  </h2>

                  <img src={regBanner5} alt="" />

                  <div className={classes.sliderContent}>
                    <h2>Add Sitters to Favourites</h2>
                    <p>
                      Mark sitters as favourites to find them even quicker next
                      time you need to make a booking{" "}
                    </p>
                  </div>
                </div>

                <div className={classes.sliderBody}>
                  <h2>
                    Now register to create your own DogStays account and find
                    and book a dog sitter even faster!
                  </h2>

                  <img src={regBanner6} alt="" />

                  <div className={classes.sliderContent}>
                    <h2>Make Quicker Payments</h2>
                    <p>
                      Add payment information to your profile to pay and
                      finalize bookings much faster{" "}
                    </p>
                  </div>
                </div>
              </Slider>
            </div>
          </Grid>
          <Grid item xs={7}>
            <Card className={classes.loginCard}>
              <div className={classes.CardContainer}>
                <div className={classes.topCardHeader}>
                  <h3>Create Your DogStays Account</h3>
                </div>
              </div>
              <CardContent>
                <div className={classes.login}>
                  <CssTextField
                    id="first-name"
                    label="First Name"
                    name="first_name"
                    value={this.state.first_name}
                    onChange={this.handleChange}
                  />
                  <CssTextField
                    id="last-name"
                    label="Last Name"
                    name="last_name"
                    value={this.state.last_name}
                    onChange={this.handleChange}
                  />
                  <CssTextField
                    id="email-address"
                    label="Email Address"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                  <CssTextField
                    id="password"
                    label="Password"
                    type="password"
                    name="password1"
                    value={this.state.password1}
                    onChange={this.handleChange}
                  />
                  <CssTextField
                    label="Confirm Password"
                    type={this.state.showPassword ? "text" : "password"} // <-- This is where the magic happens
                    onChange={this.handleConfirmPassword}
                    InputProps={{
                      // <-- This is where the toggle button is added.
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword ? (
                              <Visibility />
                            ) : (
                              <VisibilityOff />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                  {/* <CssTextField
                    id="confirm-password"
                    label="Confirm Password"
                    type="text"
                    name="password2"
                    value={this.state.password2}
                    onChange={this.handleChange}
                  /> */}
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.checked}
                        onChange={this.handleCheckboxChange}
                        name="checked"
                        color="primary"
                        className={classes.checkbox}
                      />
                    }
                    label="I agree to Terms of Service & Privacy Policy"
                    className={classes.checkboxLabel}
                  />
                  <Grid
                    container
                    justify="center"
                    style={{ marginTop: "1rem" }}
                  >
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.orangeBtn}
                      onClick={this.handleSubmit}
                    >
                      Register
                    </Button>
                  </Grid>
                  <Divider classes={this.props}>Or Register With</Divider>
                  <Grid
                    container
                    justify="center"
                    style={{ marginTop: "2rem" }}
                  >
                    <Button
                      variant="outlined"
                      className={classes.socialButtons}
                      startIcon={<FeatureIcon src={google} width="18px" />}
                    >
                      Google
                    </Button>
                    <Button
                      variant="outlined"
                      className={classes.socialButtons}
                      startIcon={<FeatureIcon src={facebook} height="100%" />}
                    >
                      Facebook
                    </Button>
                  </Grid>
                  <Typography
                    variant="subtitle1"
                    component="div"
                    color="primary"
                    className={classes.registerSection}
                  >
                    Existing User ?
                    <Link
                      component={RouterLink}
                      to="/login"
                      color="secondary"
                      style={{ marginLeft: 10 }}
                    >
                      Login
                    </Link>
                  </Typography>
                </div>
              </CardContent>
            </Card>
          </Grid>
        </Grid>

        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={this.state.showModal}
          onClose={this.handleModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={this.state.showModal}>
            <div className={classes.paper}>
              <Typography color="primary" className={classes.modalhead1}>
                You're registered, please verify your email
              </Typography>
              <Typography
                className={classes.modalhead2}
                style={{ maxWidth: 520 }}
              >
                Congratulations, you have successfully registered as Dog Parent,
                you're one step away, please verify your email address using the
                link recieved at {this.state.email}.
              </Typography>
              <img src={passwordUpdate} style={{ marginBotton: 24 }} />

              <Grid container justify="center" style={{ marginTop: "1rem" }}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.orangeBtn}
                  onClick={() => {
                    this.handleModal();
                    window.location.href = "/login";
                  }}
                >
                  Continue
                </Button>
              </Grid>
            </div>
          </Fade>
        </Modal>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Register);
