import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { Typography, Grid, Link, TextField, Button } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import axios from "axios";

import login from "../../assets/accountSetting/login.png";
import passwordUpdate from "../../assets/accountSetting/passwordUpdate.png";

import { Link as RouterLink } from "react-router-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";

const local_user = JSON.parse(localStorage.getItem("user"));
const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
    paddingBottom: theme.spacing(15),
  },

  heading2: {
    color: theme.palette.secondary.main,
    fontFamily: theme.cursiveFont,
    fontSize: "25px",
    padding: theme.spacing(1, 0, 0, 0),
  },

  heading3: {
    color: theme.palette.common.black,
    fontSize: "18px",
    lineHeight: "24px",
    padding: theme.spacing(1, 0, 1, 0),
  },

  heading4: {
    color: theme.palette.common.black,
    padding: theme.spacing(1, 0, 1, 0),
    fontSize: "20px",
    lineWidth: "24px",
  },

  heading5: {
    color: theme.palette.primary.main,
    padding: theme.spacing(1, 0, 1, 0),
    fontSize: "20px",
    lineWidth: "24px",
  },

  heading6: {
    color: theme.palette.secondary.main,
    padding: theme.spacing(1, 0, 1, 0),
    fontSize: "20px",
    lineWidth: "24px",
    float: "right",
  },

  centerAlign: {
    textAlign: "center",
  },

  box: {
    width: "100%",
    padding: theme.spacing(3, 4),
    margin: theme.spacing(5, 4, 2, 4),
  },

  box2: {
    width: "100%",
    border: "1px solid rgba(232, 119, 14, 0.7)",
    boxSizing: "border-box",
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.15)",
    padding: theme.spacing(1, 3),
    margin: theme.spacing(5, 7, 5, 7),
    borderRadius: "6px",
    width: "800px",
  },

  box3: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    textAlign: "right",
    padding: theme.spacing(2, 0, 3, 0),
  },

  orangeBtn: {
    color: "white",
    fontSize: 16,
    textTransform: "none",
    boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    width: "max-content",
    padding: theme.spacing(1, 4),
  },

  marginAuto: {
    padding: theme.spacing(2),
    margin: "auto",
  },

  link: {
    textDecoration: "none !important",
    padding: theme.spacing(0),
    margin: theme.spacing(0, 8, 0, 1),
  },

  form: {
    padding: theme.spacing(2),
    "& div": {
      width: "100%",
      marginBottom: theme.spacing(1),
    },
  },

  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },

  modalhead1: {
    fontSize: "30px",
  },

  modalhead2: {
    fontSize: "20px",
    color: theme.palette.common.black,
  },
}));

const CssTextField = withStyles({
  root: {
    color: "#E8770E",
    "& label": {
      color: "rgba(94, 90, 90, 0.7)",
    },
    "& .MuiInput-underline:before": {
      borderBottom: "1px solid #E8770E",
    },
    "& label.Mui-focused": {
      color: "#3EB3CD",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#3EB3CD",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottom: "2px solid #3EB3CD",
    },
  },
})(TextField);

const Update1 = () => {
  const classes = useStyles();

  return (
    <Grid container className={classes.box2}>
      <Grid item xs={9}>
        <Typography className={classes.heading5}>Password</Typography>

        {/* <Typography className={classes.heading4}>
          Last updated 3 years ago
        </Typography> */}
      </Grid>

      <Grid item xs={3} className={classes.marginAuto}>
        <Typography className={classes.heading6}>
          <Link component={RouterLink} to="/onclick/form" color="secondary">
            Change Password
          </Link>
        </Typography>
      </Grid>
    </Grid>
  );
};

const Update2 = () => {
  let history = useHistory();

  const classes = useStyles();
  const [user, setUser] = useState({
    old_password: "",
    new_password1: "",
    new_password2: "",
  });
  const [checked, setCheckedValue] = useState(true);
  const [open, setOpen] = React.useState(false);

  const dispatch = useDispatch();

  function handleChange(e) {
    const { name, value } = e.target;
    setUser((user) => ({ ...user, [name]: value }));
  }

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = (props, context) => {
    setOpen(false);
    history.push("/onclick");
  };

  const handleSubmit = () => {
    const data = user;
    console.log(data);
    axios
      .post(`${config.apiUrl}/password/change/`, data, {
        headers: {
          Authorization: `Token ${local_user.key}`,
        },
      })
      .then((response) => {
        console.log("API response", response);
        handleOpen();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Grid container className={classes.box2}>
      <Grid item xs={9} style={{ marginBottom: "90px" }}>
        <Typography className={classes.heading5}>Password</Typography>

        {/* <Typography className={classes.heading4}>
          Last updated 3 years ago
        </Typography> */}

        <div className={classes.form}>
          <CssTextField
            id="cur-password"
            label="Current Password"
            type="password"
            name="old_password"
            value={user.old_password}
            onChange={handleChange}
          />
          <CssTextField
            id="new-password"
            label="New Password"
            type="password"
            name="new_password1"
            value={user.new_password1}
            onChange={handleChange}
          />
          <CssTextField
            id="con-password"
            label="Confirm Password"
            type="password"
            name="new_password2"
            value={user.new_password2}
            onChange={handleChange}
          />
        </div>
      </Grid>

      <Grid item xs={3} className={classes.box3}>
        <Typography className={classes.heading6}>
          <Link component={RouterLink} to="/onclick" color="secondary">
            Cancel
          </Link>
        </Typography>

        <Button
          variant="contained"
          color="primary"
          className={classes.orangeBtn}
          onClick={handleSubmit}
        >
          Change Password
        </Button>
      </Grid>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Typography color="primary" className={classes.modalhead1}>
              Updated Sucessfully !
            </Typography>
            <Typography className={classes.modalhead2}>
              Your new password has benn updated successfully
            </Typography>
            <img src={passwordUpdate} />
          </div>
        </Fade>
      </Modal>
    </Grid>
  );
};

const routes = [
  {
    path: "/onclick",
    exact: true,
    main: () => <Update1 />,
  },
  {
    path: "/onclick/form",
    exact: true,
    main: () => <Update2 />,
  },
];

const OnClickLnS = () => {
  const classes = useStyles();

  return (
    <Router>
      <div className={classes.root}>
        <Grid container>
          <Grid container className={classes.box}>
            <Grid item xs={9}>
              <img src={login} />
              <Typography className={classes.heading2}>
                Login & Security
              </Typography>

              <Typography className={classes.heading3}>
                Manage your payouts here.
              </Typography>
            </Grid>
          </Grid>

          <div style={{ width: "100%" }}>
            <Switch>
              {routes.map((route, index) => (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={route.main}
                />
              ))}
            </Switch>
          </div>
        </Grid>
      </div>
    </Router>
  );
};

export default OnClickLnS;
