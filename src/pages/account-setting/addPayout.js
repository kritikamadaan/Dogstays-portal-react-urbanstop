import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ApplyImg from "../../assets/accountSetting/withdraw1.png";
import PropTypes from "prop-types";
import moment from "moment";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import axios from "axios";

import passwordUpdate from "../../assets/accountSetting/passwordUpdate.png";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const userData = JSON.parse(localStorage.getItem("user"));

const styles = (theme) => ({
  section: {
    paddingTop: theme.spacing(12),
  },
  breadcrumbs: {
    color: "white",
    padding: theme.spacing(2),
    fontFamily: "kanit",
  },
  main: {},
  leftGrid: {
    backgroundColor: theme.palette.primary.main,
    color: "white",
    textAlign: "center",
  },
  rightGrid: {
    padding: theme.spacing(2),
  },
  form: {
    padding: theme.spacing(5),
    marginRight: theme.spacing(5),
    marginLeft: theme.spacing(5),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  formControlApplication: {
    margin: theme.spacing(1),
    width: "80%",
  },
  formHeading: {
    color: theme.palette.secondary.main,
  },
  formBody: {
    color: theme.palette.primary.main,
    fontSize: "18px !important",
    marginTop: 8,
  },
  formPara: {
    color: "#5E5A5A",
    fontSize: "16px !important",
    marginTop: 8,
  },
  formTextField: { width: "80%" },
  mobileInputField: {
    padding: "8px 20px",
  },
  alignMiddle: {
    position: "relative",
    top: "8%",
    marginLeft: 0,
    minHeight: "calc(100vh - 320px)",
  },
  navigation: {
    display: "flex",
    justifyContent: "space-between",
    paddingLeft: 24,
    paddingRight: 24,
  },
  startNavigation: {
    display: "flex",
    justifyContent: "flex-end",
    paddingLeft: 24,
    paddingRight: 24,
  },
  formSubtitle: {
    color: "#a1a1a1",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  paper: {
    backgroundColor: "white",
    padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
  },
});

const AddPayoutMethod = (props) => {
  const classes = props.classes;
  return (
    <div>
      <section className={classes.section}>
        <Grid container spacing={0} className={classes.main}>
          <Grid item xs={4} className={classes.leftGrid}>
            <Breadcrumbs
              aria-label="breadcrumb"
              className={classes.breadcrumbs}
            >
              <a
                style={{
                  color: "inherit",
                  textDecoration: "none",
                }}
                href="/pet-sitter-as"
                to="/pet-sitter-as"
              >
                Account Settings
              </a>
              <a
                style={{
                  color: "inherit",
                  textDecoration: "none",
                }}
                href="/payout"
                to="/payout"
              >
                Payout
              </a>
              <Typography color="white">Add Payout</Typography>
            </Breadcrumbs>
            <div className={classes.alignMiddle}>
              <h1
                style={{
                  fontFamily: "Fredoka One",
                  fontSize: 38,
                  textAlign: "left",
                  marginLeft: 82,
                }}
              >
                Payout Methods
              </h1>
              <p
                style={{
                  fontFamily: "Kanit",
                  fontSize: 24,
                  textAlign: "left",
                  marginTop: 4,
                  maxWidth: 270,
                  marginLeft: 82,
                }}
              >
                Manage your payouts here...
              </p>
              <img
                src={ApplyImg}
                style={{
                  height: 246,
                }}
              ></img>
            </div>
          </Grid>
          <Grid item xs={8} className={classes.rightGrid}>
            {props.start === false ? (
              <Stepper activeStep={props.activeStep} alternativeLabel>
                {getSteps.map((label) => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
            ) : null}
            {props.start === true ? (
              <div>
                <div>
                  <div style={{ padding: 24 }}>
                    <p
                      style={{
                        color: "#5E5A5A",
                        fontFamily: "Kanit",
                        fontSize: 20,
                      }}
                    >
                      When you receive a payment for a booking, we call that
                      payment to you a “payout”. Our secure payment system
                      supports several payment methods, which can be set up
                      below.{" "}
                      <a
                        href="!#"
                        style={{ color: "#3EB3CD", textDecoration: "none" }}
                      >
                        Go To FAQ’s
                      </a>
                    </p>
                    <h4
                      style={{
                        color: "#5E5A5A",
                        fontFamily: "Kanit",
                        fontSize: 24,
                      }}
                    >
                      To get paid you need to set up a payout method
                    </h4>
                    <p
                      style={{
                        color: "#5E5A5A",
                        fontFamily: "Kanit",
                        fontSize: 20,
                      }}
                    >
                      DogStays releases payouts every 15 days. The time it takes
                      for the funds to appear in your account depends on your
                      payout method{" "}
                      <a
                        href="!#"
                        style={{ color: "#3EB3CD", textDecoration: "none" }}
                      >
                        Learn more
                      </a>
                    </p>
                  </div>
                  <div className={classes.startNavigation}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={props.handleStart}
                      style={{
                        color: "#ffffff",
                      }}
                    >
                      <span style={{ color: "#ffffff !important" }}>
                        Proceed to Add Banking Method
                      </span>
                    </Button>
                  </div>
                </div>
              </div>
            ) : (
              <div>
                <div>
                  <Typography className={classes.instructions}>
                    {props.getStepContent}
                  </Typography>

                  <div className={classes.navigation}>
                    <Button
                      disabled={props.activeStep === 0}
                      onClick={props.handleBack}
                      className={classes.backButton}
                    >
                      Back
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={props.handleNext}
                      onClick={
                        props.activeStep === getSteps.length - 1
                          ? props.handleSubmit
                          : props.handleNext
                      }
                    >
                      <span style={{ color: "#ffffff !important" }}>
                        {props.activeStep === getSteps.length - 1
                          ? "Submit"
                          : "Next"}
                      </span>
                    </Button>
                  </div>
                </div>
              </div>
            )}
          </Grid>
        </Grid>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={props.successModal}
          onClose={props.handleSuccessModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={props.successModal}>
            <div className={classes.paper}>
              <Typography color="primary" className={classes.modalhead1}>
                Bank Details Updated Sucessfully !
              </Typography>
              <Typography className={classes.modalhead2}>
                You're ready to start recieving payments
              </Typography>
              <img src={passwordUpdate} />
            </div>
          </Fade>
        </Modal>
      </section>
    </div>
  );
};

const getSteps = ["", "", "", ""];

class AddPayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      mobile: null,
      state: null,
      country: null,
      radio1: null,
      isEmailModalOpen: true,
      city: null,
      pincode: null,
      start: true,
      successModal: false,
    };
  }

  handleNext = (e) => {
    this.setState((prevState) => {
      return { ...prevState, activeStep: prevState.activeStep + 1 };
    });
  };

  handleBack = () => {
    this.setState((prevState) => {
      return { ...prevState, activeStep: prevState.activeStep - 1 };
    });
  };

  handleReset = () => {
    this.setState({ activeStep: 0 });
  };
  handleChangeMobileCode = (event) => {
    this.setState({ mobileCode: event.target.value });
  };
  handleChangeState = (event) => {
    this.setState({ state: event.target.value });
  };
  handleChangeCountry = (event) => {
    this.setState({ country: event.target.value });
  };
  handleChangeRadio1 = (event) => {
    this.setState({ radio1: event.target.value });
  };
  handleEmailModal = () => {
    this.setState((prevState) => {
      return { ...prevState, isEmailModalOpen: !prevState.isEmailModalOpen };
    });
  };

  handleStart = () => {
    this.setState({
      start: false,
    });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    let full_name = null;
    // console.log(e.target, [name], value);
    if (name === "first_name") {
      const random_user_id = Math.floor(Math.random() * (999 - 100 + 1) + 100);

      this.setState((user) => ({
        ...user,
        [name]: value,
        username: `${this.state.first_name}_${this.state.last_name}_${random_user_id}`,
        is_sitter: true,
      }));
    } else {
      this.setState((user) => ({
        ...user,
        [name]: value,
        is_sitter: true,
      }));
    }
  };

  handleTextChange = (event) => {
    this.setState((prevState) => {
      return { ...prevState, [event.target.name]: event.target.value };
    });
  };

  handleSuccessModal = () => {
    this.setState((prevState) => {
      return { successModal: !prevState.successModal };
    });
  };

  handleAddPayoutModal = () => {
    this.setState((prevState) => {
      return { addPayoutModal: !prevState.addPayoutModal };
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      userid: userData.user.id,
      user: userData.user.id,
      billing_country: this.state.billing_country,
      payout_method: this.state.payout_method,
      street_address: this.state.street_address,
      building: this.state.building,
      city: this.state.city,
      state: this.state.state,
      zip: this.state.zip,
      country: this.state.country,
      account_type: this.state.account_type,
      bank_country: this.state.bank_country,
      currency: this.state.currency,
      bank_name: this.state.bank_name,
      account_holder_name: this.state.account_holder_name,
      account_number: this.state.account_number,
      ifsc: this.state.ifsc,
      // amount: 1000,
    };

    axios
      .post(`${config.apiUrl}/api/payout/`, data)
      .then((response) => {
        console.log("Res", response);
        if (response.status === 201) {
          this.setState(
            (prevState) => {
              return {
                ...prevState,
                activeStep: prevState.activeStep,
              };
            },
            () => {
              this.handleSuccessModal();
              setTimeout(() => {
                window.location.href = "/dashboard";
              }, 3000);
            }
          );
        } else {
          alert("something went wrong");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  createProfile = (id, cust_id) => {
    this.setState((prevState) => {
      return { ...prevState, activeStep: prevState.activeStep + 1 };
    });
  };

  getStepContent = (stepIndex) => {
    const { classes } = this.props;
    switch (stepIndex) {
      case 0:
        return (
          <div className={classes.form}>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Billing Country / Region
                  </Typography>

                  <FormControl className={classes.formControl}>
                    <Select
                      labelId="billing_country"
                      id="billing_country"
                      name="billing_country"
                      placeholder="Ex. Luxembourg"
                      value={this.state.mobileCode}
                      onChange={this.handleChangeMobileCode}
                    >
                      <MenuItem value={"India"}>India</MenuItem>
                      <MenuItem value={"Canada"}>Canada</MenuItem>
                      <MenuItem value={"Europe"}>Europe</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>

                <Grid item xs={12}>
                  <Typography
                    variant="h5"
                    component="h5"
                    className={classes.formHeading}
                  >
                    Payout Methods
                  </Typography>

                  <FormControl component="fieldset">
                    <RadioGroup
                      name="gender1"
                      value={this.state.radio1}
                      onChange={this.handleChangeRadio1}
                      row
                    >
                      <FormControlLabel
                        value="Bank Transfer in Euro"
                        control={<Radio />}
                        label="Bank Transfer in Euro"
                      />
                    </RadioGroup>
                    <span
                      style={{
                        marginLeft: 32,
                        color: "#979797",
                        fontSize: 16,
                        maxWidth: 200,
                      }}
                    >
                      Enroll your bank account to enable direct transfer
                    </span>
                  </FormControl>
                </Grid>
              </Grid>
            </form>
          </div>
        );
      case 1:
        return (
          <div className={classes.form}>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Street Address
                    </Typography>
                    <TextField
                      id="street_address"
                      placeholder="Ex : 123 main road , 1/1 st "
                      name="street_address"
                      value={this.state.street_address}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Flat No / Suite / Building
                    </Typography>
                    <TextField
                      id="building"
                      placeholder="Ex: Flat #3"
                      name="building"
                      value={this.state.building}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      City
                    </Typography>
                    <TextField
                      id="city"
                      placeholder="Ex. Luxembourg"
                      name="city"
                      value={this.state.city}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      State/ Province
                    </Typography>
                    <TextField
                      id="state"
                      placeholder="Ex. Luxembourg"
                      name="state"
                      value={this.state.state}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>

                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Zip code / Postcode
                    </Typography>
                    <TextField
                      id="zip"
                      placeholder="Ex. 94013"
                      name="zip"
                      className={classes.formTextField}
                      value={this.state.zip}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Country / Region
                    </Typography>
                    <TextField
                      id="country"
                      placeholder="Ex. 567678"
                      name="country"
                      className={classes.formTextField}
                      value={this.state.country}
                      onChange={this.handleChange}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </div>
        );
      case 2:
        return (
          <div className={classes.form}>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Bank Transfer
                    </Typography>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formBody}
                    >
                      Weekends an holiday might extend processing time.
                    </Typography>
                  </Grid>
                  <div style={{ marginTop: 62, marginLeft: 12 }}>
                    <Grid item xs={12}>
                      <Typography
                        variant="h5"
                        component="h5"
                        className={classes.formBody}
                      >
                        No Fees.
                      </Typography>
                      <Typography
                        variant="h5"
                        component="h5"
                        className={classes.formBody}
                      >
                        Get paid in 3 - 5 business days
                      </Typography>

                      <Typography
                        variant="h5"
                        component="h5"
                        className={classes.formPara}
                      >
                        When you click Next, you will be directed to another
                        website where you may securely enter your banking
                        details. After completing the requested information you
                        will be redirected back to DogStays. Here are some tips
                        you should know when entering your information.
                      </Typography>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </form>
          </div>
        );
      case 3:
        return (
          <div className={classes.form}>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Bank Transfer Setup
                    </Typography>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formPara}
                    >
                      Please provide your bank details. This will let you
                      recieve funds to your bank account.
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl component="fieldset">
                      <RadioGroup
                        name="account_type"
                        value={this.state.radio1}
                        onChange={this.handleChangeRadio1}
                        row
                      >
                        <FormControlLabel
                          value="Personal account"
                          control={<Radio />}
                          label="Personal account"
                        />

                        <FormControlLabel
                          value="Business account"
                          control={<Radio />}
                          label="Business account"
                        />
                      </RadioGroup>
                    </FormControl>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Bank Country
                    </Typography>
                    <TextField
                      id="bank_country"
                      placeholder="Ex. Flat #3"
                      name="bank_country"
                      value={this.state.bank_country}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Currency
                    </Typography>
                    <TextField
                      id="currency"
                      placeholder="Ex. Indian Ruppee"
                      name="currency"
                      value={this.state.currency}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  {/* <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Bank Name
                    </Typography>
                    <TextField
                      id="bank_name"
                      placeholder="Ex. State bank of India"
                      name="bank_name"
                      value={this.state.bank_name}
                      className={classes.formTextField}
                      onChange={this.handleChange}
                    />
                  </Grid> */}

                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      Account Holder Name
                    </Typography>
                    <TextField
                      id="account_holder_name"
                      placeholder="Ex. 567678"
                      name="account_holder_name"
                      className={classes.formTextField}
                      value={this.state.account_holder_name}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      IBAN/ Bank Account Information
                    </Typography>
                    <TextField
                      id="account_number"
                      placeholder="Ex. 567678"
                      name="account_number"
                      className={classes.formTextField}
                      value={this.state.account_number}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  {/* <Grid item xs={12}>
                    <Typography
                      variant="h5"
                      component="h5"
                      className={classes.formHeading}
                    >
                      IIFC code
                    </Typography>
                    <TextField
                      id="ifsc"
                      placeholder="Ex. 567678"
                      name="ifsc"
                      className={classes.formTextField}
                      value={this.state.ifsc}
                      onChange={this.handleChange}
                    />
                  </Grid> */}
                </Grid>
              </Grid>
            </form>
          </div>
        );
      default:
        return "Unknown stepIndex";
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <AddPayoutMethod
        activeStep={this.state.activeStep}
        successModal={this.state.successModal}
        classes={classes}
        handleSuccessModal={this.handleSuccessModal}
        handleNext={this.handleNext}
        handleBack={this.handleBack}
        getStepContent={this.getStepContent(this.state.activeStep)}
        handleSubmit={this.handleSubmit}
        handleStart={this.handleStart}
        start={this.state.start}
      />
    );
  }
}

AddPayout.propTypes = {};

export default withStyles(styles, { withTheme: true })(AddPayout);
