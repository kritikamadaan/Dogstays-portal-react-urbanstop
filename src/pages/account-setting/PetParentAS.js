import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { Typography, Grid, Card, Link } from "@material-ui/core";

import pay from "../../assets/accountSetting/pay.png";
import login from "../../assets/accountSetting/login.png";
import leftArrow from "../../assets/accountSetting/leftArrow.png";

import { Link as RouterLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
    paddingBottom: theme.spacing(10),
  },

  heading1: {
    color: theme.palette.secondary.main,
    fontWeight: theme.bolderFont,
    fontSize: "30px",
    padding: theme.spacing(2, 0, 0, 0),
  },

  heading2: {
    color: theme.palette.secondary.main,
    fontFamily: theme.cursiveFont,
    fontSize: "25px",
    padding: theme.spacing(1, 0, 0, 0),
  },

  heading3: {
    color: theme.palette.common.black,
    fontSize: "16px",
    lineHeight: "24px",
    padding: theme.spacing(1, 0, 1, 0),
  },

  centerAlign: {
    textAlign: "center",
  },
  padAound: { padding: theme.spacing(4) },

  colorGray: {
    color: theme.palette.common.gray,
    fontWeight: theme.bolderFont,
  },

  box: {
    width: "100%",
    border: "1px solid rgba(232, 119, 14, 0.7)",
    boxSizing: "border-box",
    boxShadow: "4px 4px 4px rgba(128, 128, 128, 0.15)",
    padding: theme.spacing(3, 4),
    margin: theme.spacing(5, 4, 2, 4),
    borderRadius: "6px",
  },

  arrow: {
    padding: theme.spacing(2),
    margin: "auto",
  },

  arrowInner: {
    width: "25px",
    height: "35px",
    float: "right",
  },

  link: {
    textDecoration: "none !important",
    padding: theme.spacing(0),
    margin: theme.spacing(0, 8, 0, 1),
  },
}));

const Divider = ({ children }) => {
  const classes = useStyles();

  return (
    <div className={classes.dividerContainer}>
      <div className={classes.dividerBorder} />
      <span className={classes.dividerContent}>{children}</span>
      <div className={classes.dividerBorder} />
    </div>
  );
};

const PetParentAS = () => {
  const classes = useStyles();
  // const [user, setUser] = useState({
  //     username: '',
  //     email: '',
  //     password1: '',
  //     password2: ''
  // });
  // const [checked, setCheckedValue] = useState(true);

  // const dispatch = useDispatch()
  const user = useSelector((state) => state.authentication.user);
  return (
    <div className={classes.root}>
      <Grid container justify="center">
        <Grid item xs={12} className={classes.padAound}>
          <Typography className={classes.heading1}>Account</Typography>
          <Typography variant="h6">
            <span style={{ fontWeight: "600" }}>
              {user.user.first_name} {"  "} {user.user.last_name}
            </span>
            <br />
            <span className={classes.colorGray}> {user.user.email}</span>
            <br />
            <a
              href="/dashboard/profile"
              color="primary"
              style={{
                marginLeft: 10,
                color: "#E8770E",
                textDecoration: "none",
              }}
            >
              Go to profile
            </a>
          </Typography>
        </Grid>

        <Grid container>
          <Grid item xs={4} className={classes.box}>
            <Link component={RouterLink} className={classes.link} to="/payout">
              <img src={pay} />
              <Typography className={classes.heading2}>Payouts</Typography>

              <Typography className={classes.heading3}>
                Manage your payouts here
              </Typography>

              <img src={leftArrow} className={classes.arrowInner} />
            </Link>
          </Grid>
          <Grid item xs={4} className={classes.box}>
            <Link component={RouterLink} className={classes.link} to="/onclick">
              <img src={login} />
              <Typography className={classes.heading2}>
                Login & Security
              </Typography>

              <Typography className={classes.heading3}>
                Manage your payouts here.
              </Typography>

              <img src={leftArrow} className={classes.arrowInner} />
            </Link>{" "}
          </Grid>{" "}
        </Grid>
      </Grid>
    </div>
  );
};

export default PetParentAS;
