import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import {
  Typography,
  Tabs,
  Tab,
  Box,
  Grid,
  Card,
  CardMedia,
  CardActionArea,
  CardHeader,
  FormControl,
  RadioGroup,
  Radio,
  CardContent,
  FormControlLabel,
  Divider,
  IconButton,
  Button,
  Avatar,
  Link,
} from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import CloseIcon from "@material-ui/icons/Close";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import BookingRequest from "./BookingRequest";
import MeetingRequest from "./MeetingRequest";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import moment from "moment";
import profileFallback from "../assets/sitterDashboard/profileFallback.png";
import allSizes from "../assets/sitterSearch/group_1.png";
import Rating from "@material-ui/lab/Rating";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined";
import FavoriteIcon from "@material-ui/icons/Favorite";
import MessageIcon from "@material-ui/icons/Message";
import axios from "axios";
import { Link as RouterLink } from "react-router-dom";
import mastercard from "../assets/sitterProfile/mastercard.svg";
import visa from "../assets/sitterProfile/visa.svg";
import paypal from "../assets/sitterProfile/paypal.svg";
import americanExpress from "../assets/sitterProfile/american-express.svg";
import galleryImg1 from "../assets/home/gallery_1.jpg";
import galleryImg2 from "../assets/home/gallery_2.jpg";
import galleryImg3 from "../assets/home/gallery_3.jpg";
import galleryImg4 from "../assets/home/gallery_4.jpg";
import dogImage from "../assets/sitterSearch/Vector.png";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import DogProfileDashboard from "./dashboard/DogProfileDashboard";
import { sitterActions } from "../actions";
import ContactModal from "./dashboard/components/ContactComponent";
import CircularProgress from "@material-ui/core/CircularProgress";

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const user = JSON.parse(localStorage.getItem("user"));

const styles = (theme) => ({
  root: {
    marginTop: 92,
    flexGrow: 1,
    minHeight: "80vh",
  },
  banner: {
    backgroundColor: theme.palette.primary.main,
    color: "#FFFFFF",
    display: "block",
    padding: theme.spacing(2, 5),
    padding: "95px 25px",
    minHeight: 198,
  },
  profileImg: {
    border: "0.5px solid #F58220",
    borderRadius: 8,
    textAlign: "center",
  },
  sitterName: {
    fontSize: 32,
    fontFamily: "Fredoka One",
  },
  ratingText: {
    fontSize: 16,
    marginLeft: theme.spacing(1),
  },
  sitterBio: {
    fontSize: 16,
    marginTop: theme.spacing(1),
  },
  sitterImg: {
    position: "absolute",
    zIndex: 10,
    top: 208,
    width: 240,
    height: 192,
    marginTop: 52,
  },
  profileTabs: {
    fontSize: 22,
    fontFamily: "Kanit",
    textTransform: "none",
  },
  indicatorColor: {
    backgroundColor: "white",
  },
  tabPanels: {
    marginBottom: theme.spacing(7),
  },
  sitterDescription: {
    fontSize: 17,
    color: "#979797",
    fontFamily: "Kanit",
  },
  sitterProfileHeadings: {
    fontWeight: 500,
    marginBottom: 2,
    fontSize: "1rem",
  },
  petCards: {
    position: "relative",
    marginRight: 52,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "white",
    // padding: theme.spacing(2, 8, 2, 8),
    textAlign: "center",
    "&:focus": {
      outline: "none !important",
    },
    borderRadius: "6px",
    maxHeight: "90%",
    maxWidth: "90%",
    overflowY: "auto",
  },
  overlay: {
    position: "absolute",
    top: 0,
    color: "white",
    background:
      "linear-gradient(0deg, rgba(94, 90, 90, 0.3), rgba(94, 90, 90, 0.3)), linear-gradient(0deg, rgba(94, 90, 90, 0.6), rgba(94, 90, 90, 0.6))",
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 28,
    fontFamily: "Fredoka One",
  },
  calendar: {
    width: 368,
    height: 368,
    backgroundColor: "white",
    marginTop: "3rem",
  },
  serviceHeader: {
    backgroundColor: theme.palette.primary.main,
    color: "white",
    textAlign: "center",
  },
  serviceCardDivider: {
    height: 2,
    width: "100%",
    margin: theme.spacing(2, 0),
    backgroundColor: theme.palette.primary.main,
  },
  cancellationContent: {
    color: "#979797",
    fontSize: 18,
  },
  serviceCardControls: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: theme.spacing(3),
  },
  serviceCardBtns: {
    color: "white",
    fontSize: 20,
    fontWeight: 500,
    textTransform: "none",
    borderRadius: theme.spacing(1),
  },
  paymentOptions: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    padding: theme.spacing(3),
  },
  experienceImg: {
    width: 200,
    height: 200,
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 8,
    objectFit: "cover",
    marginRight: 52,
  },
  multiplePhotos: {
    width: 200,
    height: 120,
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 8,
    objectFit: "cover",
  },
  multiplePets: {
    width: 180,
    height: 180,
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 8,
    objectFit: "cover",
  },
  reviewCard: {
    borderBottom: "2px solid rgba(232, 119, 14, 0.7)",
    marginTop: theme.spacing(4),
  },
  reviewerName: {
    fontSize: 28,
    fontWeight: 600,
  },
  whiteRating: {
    color: "#FFFFFF",
  },
  orangeRating: {
    color: theme.palette.primary.main,
    marginTop: theme.spacing(0.5),
  },
  reviewCardLeft: {
    textAlign: "center",
    paddingTop: theme.spacing(1),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  goToDetailedProfileLink: {
    color: "white",
    cursor: "pointer",
    "&:hover": {
      textDecoration: "none !important",
    },
  },
  root: {
    color: "white",
    "&$checked": {
      color: theme.palette.grey[500],
    },
  },
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function ServiceLabels(props) {
  const { label, price, description } = props;

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "space-between",
      }}
    >
      <div style={{ width: 212 }}>
        <Typography
          color="secondary"
          style={{ fontWeight: 500, maxWidth: 180 }}
        >
          {label}
        </Typography>
        <Typography style={{ color: "#5E5A5A", maxWidth: 180 }}>
          {description}
        </Typography>
      </div>
      <div>
        <Typography color="secondary" style={{ fontWeight: 500 }}>
          {price}
        </Typography>
      </div>
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const date = new Date();

class SitterPublicProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      tabValue: 0,
      serviceType: null,
      sitter: {},
      sitterDogs: [],
      sitterCats: [],
      sitterOthers: [],
      selectedDate: date,
      bookingModalOpen: false,
      meetingModalOpen: false,
      openPetView: false,
      petViewId: null,
      owner: null,
      PetImageFiles: [],
      HomeImageFiles: [],
      contactOpen: false,
      selectedValue: null,
      servicesLoading: true,
      petsLoading: true,
    };
  }
  handleChange = (event) => {
    console.log("handle change clicked ", event);
    // this.setState({
    // selectedValue:event.target.value
    // })
  };
  componentDidMount = () => {
    const id = this.props.match.params.id;
    this.getSitterDetails(id);
    this.getServiceDetils(id);
    this.getOwnerPets(id);
    this.getOwnerDetails(user.user.id);
  };
  handleContactModal = () => {
    this.setState({ contactOpen: !this.state.contactOpen });
  };
  getSitterDetails = (id) => {
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/?user=${id}`)
      .then((response) => {
        axios
          .get(`${config.apiUrl}/api/sitterprofile/list/${response.data[0].id}`)
          .then((res) => {
            this.setState({
              sitter: res.data,
              PetImageFiles: res.data.my_photos_with_pets
                ? JSON.parse(res.data.my_photos_with_pets)
                : [],
              HomeImageFiles: res.data.home_images
                ? JSON.parse(res.data.home_images)
                : [],
            });
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getServiceDetils = (id) => {
    axios
      .get(`${config.apiUrl}/api/servicesprice/list?user_id=${id}`)
      .then((response) => {
        console.log(response);
        this.setState({
          selectedSitterServices: response.data,
          servicesLoading: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  getOwnerPets = (id) => {
    axios
      .get(`${config.apiUrl}/api/petprofile/list?user=${id}`)
      .then((response) => {
        this.setState({
          ownerPets: response.data,
          petsLoading: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  markAsFavorite = (e) => {
    // console.log(this.state.sitter,"Sitter");
    e.preventDefault();
    // const id = this.props.match.params.id;
    const id = this.state.sitter.id;
    const data = {
      user: user.user.id,
      favourite_sitters: [id],
    };
    console.log("Fav Trigger", user, this.state);
    axios
      .put(`${config.apiUrl}/api/petownerprofile/${this.state.owner.id}`, data)
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          console.log("Success");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  getOwnerDetails = () => {
    axios
      .get(`${config.apiUrl}/api/petownerprofile/list/?user=${user.user.id}`)
      .then((response) => {
        this.setState({
          owner: response.data[0],
        });
        console.log("Owner data", this.state.owner);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handlePetViewModal = (pet) => {
    this.setState((prevState) => {
      return {
        openPetView: !prevState.openPetView,
        petViewId: pet,
      };
    });
  };

  onChangeDate = (e) => {
    this.setState({
      selectedDate: e,
    });
  };

  handleTabChange = (event, newValue) => {
    this.setState({
      tabValue: newValue,
    });
  };

  handleServiceTypeChange = (event, value) => {
    event.persist();
    event.preventDefault();
    // let ServiceObj = sitterServices.filter(
    //   (service) => service.id.toString() === event.target.value
    // );
    // console.log(event.target.value, value, ServiceObj[0].id);
    // setServiceType(`${ServiceObj[0].id}`);
    this.setState({
      selectedValue: event.target.value,
      serviceType: event.target.value,
    });
  };

  goToMeeting = () => {
    if (this.state.serviceType !== null) {
      window.location.href = `/meeting-request/${this.state.serviceType}`;
    } else {
      alert("Please Select a Service");
    }
  };

  goToBooking = () => {
    if (this.state.serviceType !== null) {
      window.location.href = `/booking-request/${this.state.serviceType}`;
    } else {
      alert("Please Select a Service");
    }
  };

  openBookingModal = () => {
    this.setState({
      bookingModalOpen: true,
    });
  };

  closeBookingModal = () => {
    this.setState({
      bookingModalOpen: false,
    });
  };

  openMeetingModal = () => {
    this.setState({
      meetingModalOpen: true,
    });
  };

  closeMeetingModal = () => {
    this.setState({
      meetingModalOpen: false,
    });
  };

  selectedSitterServices = () => {
    // console.log("sitterService", sitterServices);
    // return (
    //   sitterServices &&
    //   sitterServices.filter((service) => {
    //     if (service.sitters && service.sitters.user === sitter.user) {
    //       return service;
    //     }
    //   })
    // );
  };

  // const getSitterWorkSchedule = () => {
  //   if (sitter.work_schedule === 1) return "Full Time Work";
  //   else if (sitter.work_schedule === 2) return "Part Time Work";
  //   else if (sitter.work_schedule === 3) return "Flexible Work";
  //   else if (sitter.work_schedule === 4) return "Available only on weekends";
  //   else return "-";
  // };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.state.sitter && Object.entries(this.state.sitter).length !== 0 && (
          <div>
            <section className={classes.banner}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "baseline",
                }}
              >
                <div>
                  <Link
                    className={classes.goToDetailedProfileLink}
                    variant="h6"
                    component={RouterLink}
                    to={`/sitter-search/`}
                    style={{ padding: "10 10 0 10", fontSize: "1rem" }}
                  >
                    Back to search reults &gt;
                  </Link>
                  <Typography className={classes.sitterName}>
                    {this.state.sitter.user.first_name}{" "}
                    {this.state.sitter.user.last_name}
                  </Typography>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <Rating
                      name="read-only"
                      value={4.5}
                      precision={0.5}
                      readOnly
                      className={classes.whiteRating}
                    />
                    <Typography className={classes.ratingText}>
                      4.5 (18)
                    </Typography>
                  </div>
                  <Typography className={classes.sitterBio}>
                    {this.state.sitter.profile_intro}
                  </Typography>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: "8px",
                    }}
                  >
                    <LocationOnIcon />
                    <Typography className={classes.ratingText}>
                      {this.state.sitter.address
                        ? this.state.sitter.address
                        : "-"}
                    </Typography>
                  </div>
                </div>
                <div>
                  {this.state.owner &&
                    this.state.owner.favourite_sitters &&
                    this.state.owner.favourite_sitters.find(
                      (id) => id === parseInt(this.state.sitter.id)
                    ) ? (
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                          zIndex: 2,
                          marginTop: 24,
                        }}
                      >
                        <FavoriteIcon />
                        <Typography className={classes.ratingText}>
                          Favourite
                      </Typography>
                      </div>
                    ) : (
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          cursor: "pointer",
                          zIndex: 2,
                          marginTop: 24,
                        }}
                        onClick={this.markAsFavorite}
                      >
                        <FavoriteBorderOutlinedIcon />
                        <Typography className={classes.ratingText}>
                          Mark As Favourite
                      </Typography>
                      </div>
                    )}
                </div>
              </div>
              <div
                alt="profile-fallback"
                style={{
                  height: this.state.sitter.profile_photo ? 240 : 48,
                  width: this.state.sitter.profile_photo ? 250 : 48,
                  position: "absolute",
                  marginTop: 24,
                  borderRadius: 8,
                  border: "none",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                  backgroundImage: this.state.sitter.profile_photo
                    ? "url(" + this.state.sitter.profile_photo + ")"
                    : profileFallback,
                }}
              />
              {/* <img
                src={this.state.sitter.profile_photo}
                alt="Sitter Profile"
                className={classes.sitterImg}
              /> */}

              <Tabs
                value={this.state.tabValue}
                onChange={this.handleTabChange}
                aria-label="simple tabs example"
                style={{ marginLeft: 350, marginTop: 24 }}
                variant="fullWidth"
                classes={{ indicator: classes.indicatorColor }}
              >
                <Tab
                  label="About"
                  {...a11yProps(0)}
                  className={classes.profileTabs}
                />
                <Tab
                  label="Experience"
                  {...a11yProps(1)}
                  className={classes.profileTabs}
                />
                <Tab
                  label="Home & Family"
                  {...a11yProps(2)}
                  className={classes.profileTabs}
                />
                {/* <Tab
                  label="Other Details"
                  {...a11yProps(3)}
                  className={classes.profileTabs}
                /> */}
                <Tab
                  label="Reviews"
                  {...a11yProps(4)}
                  className={classes.profileTabs}
                />
              </Tabs>
            </section>
            <section className={classes.tabPanels}>
              <Grid container>
                <Grid item xs={8} style={{ paddingTop: 120 }}>
                  <TabPanel value={this.state.tabValue} index={0}>
                    <div style={{ paddingRight: "10rem" }}>
                      <Typography className={classes.sitterDescription}>
                        {this.state.sitter.profile_bio
                          ? this.state.sitter.profile_bio
                          : "-"}
                      </Typography>
                      <div style={{ marginTop: "2rem" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Reasons For Joining
                        </Typography>
                        <Typography
                          style={{
                            fontSize: 17,
                            color: "#979797",

                            // marginTop: "1rem",
                          }}
                        >
                          {this.state.sitter.reason_for_joining
                            ? this.state.sitter.reason_for_joining
                            : "-"}
                        </Typography>
                      </div>
                      <div style={{ marginTop: "2rem" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Work Schedule
                        </Typography>
                        <Typography
                          style={{
                            fontSize: 17,
                            color: "#979797",
                          }}
                        >
                          {this.state.sitter.work_schedule}
                        </Typography>
                      </div>
                      <div style={{ marginTop: "2rem" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          My Pets
                        </Typography>
                        <div
                          style={{
                            display: "flex",
                          }}
                        >
                          {this.state.ownerPets &&
                            this.state.ownerPets.map((pet, index) => {
                              return (
                                <div
                                  style={{
                                    marginRight: 18,
                                  }}
                                  key={pet.id}
                                >
                                  <img
                                    src={
                                      pet.profile_photo
                                        ? pet.profile_photo
                                        : dogImage
                                    }
                                    alt="Pet"
                                    width="200"
                                    className={classes.multiplePets}
                                    style={{ marginRight: 12 }}
                                    onClick={() =>
                                      this.handlePetViewModal(pet.id)
                                    }
                                  />
                                  {/* <Avatar
                                    alt="Pet"
                                    
                                    style={{ width: 72, height: 72 }}
                                    onClick={() =>
                                      this.handlePetViewModal(pet.id)
                                    }
                                    component={RouterLink}
                                    // to={
                                    //   pet.type_of_pet === "Dog"
                                    //     ? `/dog-dashboard/${pet.id}`
                                    //     : pet.type_of_pet === "Cat"
                                    //     ? `/cat-dashboard/${pet.id}`
                                    //     : `/otherpet-dashboard/${pet.id}`
                                    // }
                                  /> */}
                                  <p
                                    style={{
                                      color: "#F58220",
                                      fontSize: 17,
                                      fontFamily: "Kanit",
                                      textAlign: "center",
                                    }}
                                  >
                                    {pet.name}
                                  </p>
                                </div>
                              );
                            })}
                        </div>
                      </div>
                    </div>
                    <div style={{ paddingRight: "10rem" }}>
                      <Typography
                        color="secondary"
                        className={classes.sitterProfileHeadings}
                      >
                        Dog Size Preference
                      </Typography>
                      {/* <div style={{ margin: "2rem 0" }}>
                        <img src={dogImage} alt="Medium dog" />
                        <Typography style={{ color: "#979797", fontSize: 20 }}>
                          Medium <br />
                          (0-10Kg)
                        </Typography>
                      </div> */}
                      {this.state.sitter.dog_size_preference &&
                        this.state.sitter.dog_size_preference.map(
                          (space, i) => {
                            return space === 1 ? (
                              <Typography
                                style={{
                                  fontSize: 17,
                                  color: "#979797",
                                }}
                              >
                                Small
                              </Typography>
                            ) : space === 2 ? (
                              <Typography
                                style={{
                                  fontSize: 17,
                                  color: "#979797",
                                }}
                              >
                                Medium
                              </Typography>
                            ) : space === 3 ? (
                              <Typography
                                style={{
                                  fontSize: 17,
                                  color: "#979797",
                                }}
                              >
                                Large
                              </Typography>
                            ) : space === 4 ? (
                              <Typography
                                style={{
                                  fontSize: 17,
                                  color: "#979797",
                                }}
                              >
                                No Preference
                              </Typography>
                            ) : (
                                      <Typography
                                        style={{
                                          fontSize: 17,
                                          color: "#979797",
                                        }}
                                      >
                                        No Preference
                                      </Typography>
                                    );
                          }
                        )}
                      {/* {this.state.sitter.dog_size_preference.map((size, i) => {
                        return size === 1 ? (
                          <div key={i}>
                            <img
                              src={allSizes}
                              style={{ marginTop: "1rem" }}
                              alt=""
                            />
                            <Typography
                              style={{
                                fontSize: 20,
                                color: "#5E5A5A",
                              }}
                            >
                              Small
                            </Typography>
                          </div>
                        ) : size === 2 ? (
                          <div key={i}>
                            <img
                              src={allSizes}
                              style={{ marginTop: "1rem" }}
                              alt=""
                            />
                            <Typography
                              style={{
                                fontSize: 20,
                                color: "#5E5A5A",
                              }}
                            >
                              Medium
                            </Typography>
                          </div>
                        ) : size === 3 ? (
                          <div key={i}>
                            <img
                              src={allSizes}
                              style={{ marginTop: "1rem" }}
                              alt=""
                            />
                            <Typography
                              style={{
                                fontSize: 20,
                                color: "#5E5A5A",
                              }}
                            >
                              Large
                            </Typography>
                          </div>
                        ) : size === 4 ? (
                          <div key={i}>
                            <img
                              src={allSizes}
                              style={{ marginTop: "1rem" }}
                              alt=""
                            />
                            <Typography
                              style={{
                                fontSize: 20,
                                color: "#5E5A5A",
                              }}
                            >
                              All sizes welcome
                            </Typography>
                          </div>
                        ) : (
                          <div key={i}>
                            <img
                              src={allSizes}
                              style={{ marginTop: "1rem" }}
                              alt=""
                            />
                            <Typography
                              style={{
                                fontSize: 20,
                                color: "#5E5A5A",
                              }}
                            >
                              All sizes welcome
                            </Typography>
                          </div>
                        );
                      })} */}
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Dog Age Preference
                        </Typography>

                        {this.state.sitter.dog_age_preference.map(
                          (space, i) => {
                            return space === 1 ? (
                              <Typography
                                style={{ fontSize: 17, color: "#979797" }}
                              >
                                Puppy: 2-6 Months
                              </Typography>
                            ) : space === 2 ? (
                              <Typography
                                style={{ fontSize: 17, color: "#979797" }}
                              >
                                Puppy: 6-24 Months
                              </Typography>
                            ) : space === 3 ? (
                              <Typography
                                style={{ fontSize: 17, color: "#979797" }}
                              >
                                Adult Dog: 2 Years+
                              </Typography>
                            ) : space === 4 ? (
                              <Typography
                                style={{ fontSize: 17, color: "#979797" }}
                              >
                                Elderly Dog: 10Years+
                              </Typography>
                            ) : space === 5 ? (
                              <Typography
                                style={{ fontSize: 17, color: "#979797" }}
                              >
                                Any Age
                              </Typography>
                            ) : (
                                        <Typography
                                          style={{ fontSize: 17, color: "#979797" }}
                                        >
                                          No
                                        </Typography>
                                      );
                          }
                        )}
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Maximum Sitting Capacity
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {this.state.sitter.max_sitting_capacity
                            ? this.state.sitter.max_sitting_capacity
                            : "-"}
                        </Typography>
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Booking Overlap
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {this.state.sitter.booking_overlap
                            ? this.state.sitter.booking_overlap
                            : "-"}
                          <br />
                          {this.state.sitter.booking_overlap_parameter
                            ? this.state.sitter.booking_overlap_parameter
                            : "-"}
                        </Typography>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel value={this.state.tabValue} index={1}>
                    <div style={{ paddingRight: "10rem" }}>
                      <div>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Experience With Dogs
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {this.state.sitter.experience_with_dogs ? "Yes" : "-"}
                          <br></br>
                          {this.state.sitter.experience_with_dogs
                            ? this.state.sitter.experience_with_dogs
                            : ""}
                        </Typography>
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Experience With Other Animals
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {this.state.sitter.experience_with_pets
                            ? "Yes"
                            : "No"}
                          <br></br>
                          {this.state.sitter.experience_with_pets
                            ? this.state.sitter.experience_with_pets
                            : ""}
                        </Typography>
                      </div>
                      <Typography
                        color="secondary"
                        className={classes.sitterProfileHeadings}
                      >
                        Photos of my pets
                      </Typography>
                      <div>
                        {this.state.PetImageFiles
                          ? this.state.PetImageFiles.map((file) => {
                            return (
                              <img
                                src={file.location}
                                alt="Pet 1"
                                width="200"
                                className={classes.multiplePhotos}
                                style={{ marginRight: 12 }}
                              />
                            );
                          })
                          : null}
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel value={this.state.tabValue} index={2}>
                    <div style={{ paddingRight: "10rem" }}>
                      <div>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Type Of Home
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {this.state.sitter.home_type
                            ? this.state.sitter.home_type
                            : "-"}
                        </Typography>
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Home Description
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {this.state.sitter.home_type_description
                            ? this.state.sitter.home_type_description
                            : "-"}
                        </Typography>
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Size Of Home
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {this.state.sitter.home_type_area
                            ? this.state.sitter.home_type_area +
                            " " +
                            this.state.sitter.home_type_area_unit
                            : "-"}
                        </Typography>
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Open spaces in my home
                        </Typography>

                        {this.state.sitter.open_spaces_available.map(
                          (space, i) => {
                            return space === 1 ? (
                              <Typography
                                style={{ color: "#979797", fontSize: 17 }}
                              >
                                Yes
                              </Typography>
                            ) : space === 2 ? (
                              <Typography
                                style={{ color: "#979797", fontSize: 17 }}
                              >
                                Balcony
                              </Typography>
                            ) : space === 3 ? (
                              <Typography
                                style={{ color: "#979797", fontSize: 17 }}
                              >
                                Patio
                              </Typography>
                            ) : space === 4 ? (
                              <Typography
                                style={{ color: "#979797", fontSize: 17 }}
                              >
                                Terrace
                              </Typography>
                            ) : space === 5 ? (
                              <Typography
                                style={{ color: "#979797", fontSize: 17 }}
                              >
                                Garden
                              </Typography>
                            ) : null;
                          }
                        )}
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Open spaces outside
                        </Typography>
                        {this.state.sitter.access_to_green_spaces_outside.map(
                          (space, i) => {
                            return space === 1 ? (
                              <Typography
                                style={{ fontSize: 18, color: "#979797" }}
                              >
                                Dog Park
                              </Typography>
                            ) : space === 2 ? (
                              <Typography
                                style={{ fontSize: 17, color: "#979797" }}
                              >
                                Forest
                              </Typography>
                            ) : space === 3 ? (
                              <Typography
                                style={{ fontSize: 17, color: "#979797" }}
                              >
                                Open Area or Ground
                              </Typography>
                            ) : (
                                    <Typography
                                      style={{ fontSize: 17, color: "#979797" }}
                                    >
                                      No
                                    </Typography>
                                  );
                          }
                        )}
                      </div>
                      <div style={{ margin: "2rem 0" }}>
                        <Typography
                          color="secondary"
                          className={classes.sitterProfileHeadings}
                        >
                          Children at Home
                        </Typography>
                        <Typography style={{ color: "#979797", fontSize: 17 }}>
                          {/* {this.state.sitter.children_at_home ? "Yes" : "No"} */}
                          {this.state.sitter.children_at_home
                            ? this.state.sitter.children_at_home
                            : "-"}
                        </Typography>
                      </div>
                      <Typography
                        color="secondary"
                        className={classes.sitterProfileHeadings}
                      >
                        Photos Of My Home
                      </Typography>
                      <div>
                        {this.state.HomeImageFiles
                          ? this.state.HomeImageFiles.map((file) => {
                            return (
                              <img
                                src={file.location}
                                alt="Home 1"
                                width="200"
                                className={classes.multiplePhotos}
                                style={{ marginRight: 12 }}
                              />
                            );
                          })
                          : null}
                      </div>
                    </div>
                  </TabPanel>
                  {/* <TabPanel value={this.state.tabValue} index={3}>
                    
                  </TabPanel> */}
                  <TabPanel value={this.state.tabValue} index={4}>
                    <div style={{ paddingRight: "10rem", paddingLeft: "1rem" }}>
                      <Typography
                        style={{
                          fontSize: 25,
                          fontWeight: 500,
                          color: "#979797",
                        }}
                      >
                        10 Comments
                      </Typography>
                      <div className={classes.reviewCard}>
                        <Grid style={{ display: "flex" }}>
                          <Grid item xs={1} className={classes.reviewCardLeft}>
                            <img
                              src="https://cdn.quasar.dev/img/avatar.png"
                              style={{ borderRadius: "50%", width: "50px" }}
                            />
                          </Grid>
                          <Grid item xs={11}>
                            <Typography
                              color="secondary"
                              className={classes.reviewerName}
                            >
                              Angel D'Souza{" "}
                              <span
                                style={{
                                  color: "#979797",
                                  fontWeight: "lighter",
                                  paddingLeft: 16,
                                }}
                              >
                                Mar 20th
                              </span>
                            </Typography>
                            <Rating
                              name="read-only"
                              value={4}
                              readOnly
                              classes={{ root: classes.orangeRating }}
                            />
                            <Typography
                              style={{
                                fontSize: 22,
                                fontWeight: 500,
                                color: "#5E5A5A",
                                marginTop: "1rem",
                              }}
                            >
                              Great Experience
                            </Typography>
                            <Typography
                              style={{
                                color: "#979797",
                                fontSize: 18,
                                margin: "1rem 0",
                              }}
                            >
                              Lorem occaecat nostrud reprehenderit nisi fugiat
                              veniam mollit incididunt commodo. Commodo sit
                              tempor excepteur sint. Amet proident irure
                              cupidatat eu Lorem ullamco deserunt ea anim ipsum
                              laboris sit ea fugiat. Aliquip minim minim cillum
                              reprehenderit deserunt et culpa aliqua incididunt
                              aute tempor
                            </Typography>
                          </Grid>
                        </Grid>
                      </div>
                      <div
                        className={classes.reviewCard}
                        style={{ marginTop: "3rem" }}
                      >
                        <Grid style={{ display: "flex" }}>
                          <Grid item xs={1} className={classes.reviewCardLeft}>
                            <img
                              src="https://cdn.quasar.dev/img/avatar.png"
                              style={{ borderRadius: "50%", width: "50px" }}
                            />
                          </Grid>
                          <Grid item xs={11}>
                            <Typography
                              color="secondary"
                              className={classes.reviewerName}
                            >
                              Angel D'Souza{" "}
                              <span
                                style={{
                                  color: "#979797",
                                  fontWeight: "lighter",
                                  paddingLeft: 16,
                                }}
                              >
                                Mar 20th
                              </span>
                            </Typography>
                            <Rating
                              name="read-only"
                              value={4}
                              readOnly
                              classes={{ root: classes.orangeRating }}
                            />
                            <Typography
                              style={{
                                fontSize: 22,
                                fontWeight: 500,
                                color: "#5E5A5A",
                                marginTop: "1rem",
                              }}
                            >
                              Great Experience
                            </Typography>
                            <Typography
                              style={{
                                color: "#979797",
                                fontSize: 18,
                                margin: "1rem 0",
                              }}
                            >
                              Lorem occaecat nostrud reprehenderit nisi fugiat
                              veniam mollit incididunt commodo. Commodo sit
                              tempor excepteur sint. Amet proident irure
                              cupidatat eu Lorem ullamco deserunt ea anim ipsum
                              laboris sit ea fugiat. Aliquip minim minim cillum
                              reprehenderit deserunt et culpa aliqua incididunt
                              aute tempor
                            </Typography>
                          </Grid>
                        </Grid>
                      </div>
                      <div
                        className={classes.reviewCard}
                        style={{ borderBottom: "none", marginTop: "3reem" }}
                      >
                        <Grid style={{ display: "flex" }}>
                          <Grid item xs={1} className={classes.reviewCardLeft}>
                            <img
                              src="https://cdn.quasar.dev/img/avatar.png"
                              style={{ borderRadius: "50%", width: "50px" }}
                            />
                          </Grid>
                          <Grid item xs={11}>
                            <Typography
                              color="secondary"
                              className={classes.reviewerName}
                            >
                              Angel D'Souza{" "}
                              <span
                                style={{
                                  color: "#979797",
                                  fontWeight: "lighter",
                                  paddingLeft: 16,
                                }}
                              >
                                Mar 20th
                              </span>
                            </Typography>
                            <Rating
                              name="read-only"
                              value={4}
                              readOnly
                              classes={{ root: classes.orangeRating }}
                            />
                            <Typography
                              style={{
                                fontSize: 22,
                                fontWeight: 500,
                                color: "#5E5A5A",
                                marginTop: "1rem",
                              }}
                            >
                              Great Experience
                            </Typography>
                            <Typography
                              style={{
                                color: "#979797",
                                fontSize: 18,
                                margin: "1rem 0",
                              }}
                            >
                              Lorem occaecat nostrud reprehenderit nisi fugiat
                              veniam mollit incididunt commodo. Commodo sit
                              tempor excepteur sint. Amet proident irure
                              cupidatat eu Lorem ullamco deserunt ea anim ipsum
                              laboris sit ea fugiat. Aliquip minim minim cillum
                              reprehenderit deserunt et culpa aliqua incididunt
                              aute tempor
                            </Typography>
                          </Grid>
                        </Grid>
                      </div>
                    </div>
                  </TabPanel>
                </Grid>
                <Grid item xs={4} style={{ paddingTop: "2rem" }}>
                  <div
                    style={{
                      paddingTop: "3rem",
                      width: 475,
                      paddingRight: "1.5rem",
                    }}
                  >
                    <Card>
                      <CardHeader
                        className={classes.serviceHeader}
                        title="Service Rates"
                      ></CardHeader>
                      <CardContent
                        style={{
                          padding: "24px",
                          height: "560px",
                          overflowY: "auto",
                        }}
                      >
                        {this.state.servicesLoading === true ? (
                          <Grid style={{ height: "80vh", textAlign: "center" }}>
                            <CircularProgress color="primary" centered />
                          </Grid>
                        ) : (
                            <FormControl component="fieldset">
                              <RadioGroup
                                // aria-label={this.state.serviceType}
                                name="ServiceCard"
                                value={this.state.serviceType}
                                onChange={this.handleServiceTypeChange}
                              >
                                {this.state.selectedSitterServices &&
                                  this.state.selectedSitterServices.map(
                                    (service) => {
                                      const serviceType = service.service_id
                                        ? service.service_id.service_name ===
                                          "Day Care at Sitter's Home"
                                          ? "EUR / Day"
                                          : service.service_id.service_name ===
                                            "Day Care at Dog Parent's Home"
                                            ? "EUR / Day"
                                            : service.service_id.service_name ===
                                              "Day Care at Dog Sitter's Home"
                                              ? "EUR / Day"
                                              : service.service_id.service_name ===
                                                "Overnight Boarding at Sitter's Home"
                                                ? "EUR / Night"
                                                : service.service_id.service_name ===
                                                  "Overnight Boarding at Dog Parent's Home"
                                                  ? "EUR / Night"
                                                  : service.service_id.service_name ===
                                                    "Overnight Boarding at Dog Sitter's Home"
                                                    ? "EUR / Night"
                                                    : service.service_id.service_name ===
                                                      "Pick Up & Drop"
                                                      ? "EUR / Booking"
                                                      : service.service_id.service_name ===
                                                        "Vet Visit"
                                                        ? "EUR / Visit"
                                                        : service.service_id.service_name ===
                                                          "Dog Walking"
                                                          ? "EUR / Visit"
                                                          : ""
                                        : "";
                                      return service &&
                                        service.service_id.service_type ===
                                        "core services" &&
                                        parseInt(service.sitters.user.id) ===
                                        parseInt(this.props.match.params.id) ? (
                                          <FormControlLabel
                                            style={{ alignItems: "flex-start" }}
                                            value={service.id}
                                            control={<Radio />}
                                            key={service.id}
                                            checked={
                                              parseInt(this.state.selectedValue) ===
                                                service.id
                                                ? true
                                                : this.state.selectedValue === null
                                                  ? false
                                                  : false
                                            }
                                            label={
                                              <div>
                                                <ServiceLabels
                                                  price={`${service.serviceRate} ${serviceType}`}
                                                  label={
                                                    service.service_id.service_name
                                                  }
                                                  description={
                                                    service.service_id
                                                      .service_description
                                                  }
                                                />
                                                <Divider
                                                  className={
                                                    classes.serviceCardDivider
                                                  }
                                                />
                                              </div>
                                            }
                                          />
                                        ) : null;
                                    }
                                  )}
                              </RadioGroup>
                            </FormControl>
                          )}

                        <div style={{ marginTop: "2rem" }}>
                          <Typography
                            color="primary"
                            variant="h6"
                            style={{ fontWeight: 600 }}
                          >
                            Cancellation Policy
                          </Typography>
                          <Typography className={classes.cancellationContent}>
                            Lorem occaecat nostrud reprehenderit nisi fugiat
                            veniam mollit incididunt commodo. Commodo sit tempor
                            excepteur sint.
                          </Typography>
                        </div>
                      </CardContent>
                      <div className={classes.serviceCardControls}>
                        <IconButton
                          aria-label="delete"
                          style={{
                            backgroundColor: "#E8770E",
                            borderRadius: 8,
                            color: "white",
                            padding: "12px 16px",
                          }}
                          onClick={this.handleContactModal}
                        >
                          <MessageIcon />
                        </IconButton>
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.serviceCardBtns}
                          component={RouterLink}
                          onClick={this.openMeetingModal}
                        >
                          Meeting Request
                        </Button>
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.serviceCardBtns}
                          component={RouterLink}
                          onClick={this.openBookingModal}
                        >
                          Book Now
                        </Button>
                      </div>
                      <Divider className={classes.serviceCardDivider} />
                      <div className={classes.paymentOptions}>
                        <img src={paypal} alt="Paypal" width="45" />
                        <img
                          src={americanExpress}
                          alt="American Express"
                          width="45"
                        />
                        <img src={visa} alt="Visa" width="45" />
                        <img src={mastercard} alt="Mastercard" width="45" />
                      </div>
                      <Typography
                        style={{
                          color: "#979797",
                          fontSize: 17,
                          padding: 24,
                          paddingTop: 0,
                        }}
                      >
                        You can read our detailed cancellation terms{" "}
                        <span style={{ color: "#E8770E" }}>here</span>
                      </Typography>
                    </Card>
                  </div>
                  <div className={classes.calendar}>
                    <Calendar
                      onChange={this.onChangeDate}
                      value={this.state.selectedDate}
                    />
                  </div>
                </Grid>
                {/* Booking request */}

                <Modal
                  aria-labelledby="transition-modal-title"
                  aria-describedby="transition-modal-description"
                  className={classes.modal}
                  open={this.state.bookingModalOpen}
                  onClose={this.closeBookingModal}
                  closeAfterTransition
                  BackdropComponent={Backdrop}
                  BackdropProps={{
                    timeout: 500,
                  }}
                >
                  <Fade in={this.state.bookingModalOpen}>
                    <div className={classes.paper}>
                      <BookingRequest
                        bookingID={parseInt(this.state.selectedValue)}
                        sitterID={this.state.sitter.id}
                        sitterData={this.state.sitter}
                      />
                    </div>
                  </Fade>
                </Modal>

                {/* Meeting request */}
                <Modal
                  aria-labelledby="transition-modal-title"
                  aria-describedby="transition-modal-description"
                  className={classes.modal}
                  open={this.state.meetingModalOpen}
                  onClose={this.closeMeetingModal}
                  closeAfterTransition
                  BackdropComponent={Backdrop}
                  BackdropProps={{
                    timeout: 500,
                  }}
                >
                  <Fade in={this.state.meetingModalOpen}>
                    <div className={classes.paper}>
                      <MeetingRequest
                        bookingID={parseInt(this.state.selectedValue)}
                        sitterID={this.state.sitter.id}
                        sitterData={this.state.sitter}
                        sitterProfileAddressPurposeId={
                          this.state.sitter.user.id
                        }
                      />
                    </div>
                  </Fade>
                </Modal>
                {/* View Pet Modal */}
                <Dialog
                  onClose={this.handlePetViewModal}
                  aria-labelledby="customized-dialog-title"
                  open={this.state.openPetView}
                  fullWidth={true}
                  maxWidth={"lg"}
                // className={classes.petViewModal}
                >
                  <DialogTitle
                    id="customized-dialog-title"
                    onClose={this.handlePetViewModal}
                  >
                    View Pet
                    <IconButton
                      aria-label="close"
                      className={classes.closeButton}
                      onClick={this.handlePetViewModal}
                    >
                      <CloseIcon />
                    </IconButton>
                  </DialogTitle>
                  <DialogContent dividers>
                    <DogProfileDashboard
                      petId={this.state.petViewId}
                      getSitterPets={this.getOwnerPets}
                      editable={false}
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      autoFocus
                      onClick={this.handlePetViewModal}
                      color="primary"
                    >
                      Close
                    </Button>
                  </DialogActions>
                </Dialog>
                {/* Contact sitter modal */}
                <ContactModal
                  modalOpen={this.state.contactOpen}
                  handleModal={this.handleContactModal}
                  profile_photo={
                    this.state.sitter.profile_photo
                      ? this.state.sitter.profile_photo
                      : null
                  }
                  name={
                    this.state.sitter.user
                      ? this.state.sitter.user.first_name +
                      " " +
                      this.state.sitter.user.last_name
                      : null
                  }
                  userID={
                    this.state.sitter.user ? this.state.sitter.user.id : null
                  }
                />
              </Grid>
            </section>
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SitterPublicProfile);
