import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector, useDispatch } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import logo from "../assets/common/dogstays-logo.png";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { Link as RouterLink } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import { userActions } from "../actions";
import { useHistory } from "react-router-dom";
import axios from "axios";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const config = {
  apiUrl: "http://3.215.2.1:8000",
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  headerBg: {
    backgroundColor: "white",
    display: "flex",
    justifyContent: "space-between",
  },
  logoWrapper: {
    paddingLeft: theme.spacing(3),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  headerLinksWrapper: {
    //   marginLeft: theme.spacing(30),
    paddingTop: theme.spacing(2),
  },
  headerLinks: {
    fontSize: 18,
    cursor: "pointer",
    fontFamily: "'Kanit', sans-serif",
    color: theme.palette.common.black,
    paddingRight: theme.spacing(6),
    "&:hover": {
      textDecoration: "none",
    },
  },
  rightHeaderWrapper: {
    //   position: 'absolute',
    //   right: 36,
    paddingTop: theme.spacing(1),
  },
  rightHeaderLinks: {
    fontSize: 18,
    cursor: "pointer",
    fontFamily: "'Kanit', sans-serif",
    "&:hover": {
      textDecoration: "none",
    },
  },
  verticalDivider: {
    margin: theme.spacing(0, 1),
  },
  langMenu: {
    display: "flex",
    marginLeft: theme.spacing(2),
    alignItems: "center",
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

const Header = function DenseAppBar() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [language, setLanguage] = useState("en");
  const [showMoreMenu, setShowMoreMenu] = useState(false);
  const [showProfileMenu, setProfileMenu] = useState(null);
  const [profilePhoto, setProfilePhoto] = useState("");
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleMoreChange = (event) => {
    setShowMoreMenu(event.currentTarget);
  };

  const handleProfileDropdown = (event) => {
    setProfileMenu(event.currentTarget);
  };

  const handleClose = () => {
    setShowMoreMenu(false);
    setProfileMenu(null);
  };

  const handleLogout = () => {
    handleClose();
    dispatch(userActions.logout());
  };

  const goToDashboard = () => {
    handleClose();
    if (user.user.is_sitter) {
      history.push("/dashboard");
    } else {
      history.push("/dashboard");
    }
  };

  const goToAccount = () => {
    handleClose();
    if (user.user.is_sitter) {
      history.push("/pet-sitter-as");
    } else {
      history.push("/pet-parent-as");
    }
  };
  const getSitterDetails = () => {
    console.log("User", user);
    axios
      .get(`${config.apiUrl}/api/sitterprofile/list/?user=${user.user.id}`)
      .then((response) => {
        console.log("Response", response, response.data[0].id);
        setProfilePhoto(response.data[0].profile_photo);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const getOwnerDetails = () => {
    axios
      .get(`${config.apiUrl}/api/petownerprofile/list/?user=${user.user.id}`)
      .then((response) => {
        setProfilePhoto(response.data[0].profile_photo);
      })
      .catch((err) => {
        console.log(err);
      });
  };


  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleDropdownClose = () => {
    setAnchorEl(null);
  };


  useEffect(() => {
    if (user && user.user_type.is_sitter) {
      getSitterDetails();
    } else if (user && user.user_type.is_petowner) {
      getOwnerDetails();
    }
  });

  const user = useSelector((state) => state.authentication.user);
  const currentUrl = window.location.href;
  const url = currentUrl.split("/");
  const lastUrl = url[url.length - 1]
  console.log('last url is ', lastUrl)
  return (
    <div className={classes.root}>
      {console.log('window.location.href', window.location.href)}
      <AppBar position="fixed">
        <Toolbar className={classes.headerBg}>
          <div className={classes.logoWrapper}>
            <img src={logo} alt="App logo" style={{ width: "150px" }} />
          </div>
          <div className={classes.headerLinksWrapper}>
            <Link to="/" component={RouterLink} className={classes.headerLinks}>
              Home
            </Link>
            {user ? (
              user.user_type.is_sitter === true ? null : (
                <Link
                  to="/sitter-search"
                  component={RouterLink}
                  className={classes.headerLinks}
                >
                  Search &amp; Book
                </Link>
              )
            ) : null}
            {console.log('window location href', window.location.href)}
            <Link
              to="/sitter-application"
              component={RouterLink}
              className={classes.headerLinks}
            >
              Become a Sitter
            </Link>
            <Link to="/" className={classes.headerLinks}>
              Reviews
            </Link>

            <Link to="/help-and-support"
              component={RouterLink}
              className={classes.headerLinks}>
              FAQ's
            </Link>

            <Link to="/"
              className={classes.headerLinks}
              onClick={handleClick}
            >
              More
            </Link>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleDropdownClose}
            >

              <MenuItem onClick={handleDropdownClose}>
                <Link to="/help-and-support"
                  component={RouterLink}
                // className={classes.headerLinks}
                >
                  Help & Support
                </Link>
              </MenuItem>
            </Menu>

          </div>
          <div className={classes.rightHeaderWrapper}>
            <Grid container alignItems="center">
              {console.log("User", user)}
              {!user && (
                <div style={{ display: "flex" }}>
                  <Link
                    to="/login"
                    component={RouterLink}
                    className={classes.rightHeaderLinks}
                  >
                    Login
                  </Link>
                  <Divider
                    orientation="vertical"
                    flexItem
                    className={classes.verticalDivider}
                  />
                  <Link
                    to="/register"
                    component={RouterLink}
                    className={classes.rightHeaderLinks}
                  >
                    Register
                  </Link>
                </div>
              )}
              {user && user.key && (
                <div>
                  <IconButton onClick={handleProfileDropdown}>
                    <Avatar
                      src={
                        profilePhoto &&
                          profilePhoto !== "" &&
                          profilePhoto !== null
                          ? profilePhoto
                          : <AccountCircleIcon />
                      }
                    />
                  </IconButton>
                  <Menu
                    id="simple-menu"
                    anchorEl={showProfileMenu}
                    keepMounted
                    open={Boolean(showProfileMenu)}
                    onClose={handleClose}
                  >
                    <MenuItem onClick={goToDashboard}>Dashboard</MenuItem>
                    <MenuItem onClick={goToAccount}>My account</MenuItem>
                    <MenuItem onClick={handleLogout}>Logout</MenuItem>
                  </Menu>
                </div>
              )}
              <div className={classes.langMenu}>
                <Typography
                  variant="subtitle2"
                  style={{ marginTop: 8, color: "#979797" }}
                >
                  en
                </Typography>
                <IconButton
                  aria-label="delete"
                  className={classes.margin}
                  size="small"
                  onClick={handleMoreChange}
                >
                  <ArrowDropDownIcon fontSize="inherit" />
                </IconButton>
              </div>
            </Grid>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
