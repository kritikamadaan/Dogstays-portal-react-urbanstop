import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import footerPaw from "../assets/home/pawFooter.svg";
import copyright from "../assets/common/copyright.svg";
import { Typography, Link, IconButton } from "@material-ui/core";
import country from "../assets/common/country.svg";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import logo from "../assets/common/dogstays-logo.png";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles((theme) => ({
  appBar: {
    top: "auto",
    bottom: 0,
    backgroundColor: "white",
    boxShadow: "none",
    borderTop: "1px solid #E8770E",
  },
  fabButton: {
    position: "relative",
    left: "50%",
    zIndex: 1,
    cursor: "pointer",
  },
  footerGrid: {
    padding: theme.spacing(0, 6),
  },
  copyrightText: {
    marginLeft: theme.spacing(1),
    fontSize: 18,
  },
  footerLinks: {
    fontSize: 18,
    color: "#3EB3CD",
    lineHeight: "35px",
    "& a": {
      margin: theme.spacing(0, 2),
    },
  },
  headerText: {
    fontSize: 22,
    padding: theme.spacing(3, 8),
  },
  subText: {
    padding: theme.spacing(1, 8),
  },
  logoWrapper: {
    padding: theme.spacing(1, 3),
  },
  detailsGrid: {
    // display:"none",
    padding: theme.spacing(0, 6),
  },
  dividerWrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  divider: {
    width: "100%",
    backgroundColor: theme.palette.primary.main,
    opacity: 0.7,
  },
  padAround: {
    padding: theme.spacing(0, 10),
  },
}));
const Details = (props) => {
  const classes = useStyles();
  if (props.displayDetails)
    return (

      <Grid container className={classes.detailsGrid}>
        <Grid item xs={4}>
          <div className={classes.logoWrapper}>
            <img src={logo} alt="App logo" style={{ width: "150px" }} />
          </div>
          <Typography
            style={{ fontSize: 18, color: "#5E5A5A", paddingLeft: 24 }}
          >
            Sunt magna et incididunt eu ipsum. Ullamco sint voluptate elit magna
            quis nulla excepteur occaecat excepteur. Ut sint magna occaecat duis
            do et. Exercitation duis dolor ullamco do enim do dolore.
            Adipisicing labore proident amet reprehenderit laboris ad et.
          </Typography>
        </Grid>
        <Grid item xs={4} className={classes.padAround}>
          <Typography color="primary" className={classes.headerText}>
            Community
          </Typography>
          <Typography color="secondary" className={classes.subText}>
            Reviews
          </Typography>
          <Typography color="secondary" className={classes.subText}>
            Blog
          </Typography>
          <Typography color="secondary" className={classes.subText}>
            Gallery
          </Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography color="primary" className={classes.headerText}>
            Help &amp; Support
          </Typography>
          <Typography color="secondary" className={classes.subText}>
            FAQ’s
          </Typography>
          <Typography color="secondary" className={classes.subText}>
            How It Works
          </Typography>
          <Typography color="secondary" className={classes.subText}>
            Become a Sitter
          </Typography>
          <Typography color="secondary" className={classes.subText}>
            Contact Support
          </Typography>
        </Grid>
        <Divider className={classes.divider} style={{ marginTop: 10 }} variant="middle" />
      </Grid>
    );
  else return null;
};
const Footer = () => {
  const classes = useStyles();
  const [displayDetails, setDisplayDetails] = useState(false);
  return (
    <AppBar
      position="static"
      className={classes.appBar}
    // style={{ padding: displayDetails ? "24px 0px" : 0 }}
    >
      <div
        className={classes.dividerWrapper}
        style={{ position: "relative", top: "-28px" }}
      >
        <img
          src={footerPaw}
          alt="Paw Footer"
          className={classes.fabButton}
          onClick={(e) => {
            e.preventDefault();
            setDisplayDetails(!displayDetails);
          }}
        />{" "}
        <Divider className={classes.divider} variant="middle" />
      </div>
      <Details displayDetails={displayDetails} />

      <Toolbar style={displayDetails ? {} : { position: "relative", top: "-28px" }}>
        <Grid
          container
          justify="center"
          className={classes.footerGrid}
          alignItems="center"
        >
          <Grid item xs={4}>
            <div style={{ display: "flex" }}>
              <img src={copyright} alt="copyright logo" width="20" />
              <Typography
                variant="subtitle1"
                color="primary"
                className={classes.copyrightText}
              >
                Dogstays_SARLS . All Rights Reserved.
              </Typography>
            </div>
          </Grid>
          <Grid item xs={5} style={{ textAlign: "center" }}>
            <Typography className={classes.footerLinks}>
              <Link color="primary" style={{ margin: "0 10px" }}>
                About Us
              </Link>
              |<Link color="primary">Terms & Conditions</Link>|
              <Link color="primary">Privacy Policy</Link>|
              <Link color="primary">Sitemap</Link>
            </Typography>
          </Grid>
          <Grid item xs={3} style={{ textAlign: "center" }}>
            <IconButton color="secondary">
              <img src={country} alt="country" />
              <ArrowDropDownIcon />
            </IconButton>
            <IconButton color="secondary">
              <Typography style={{ fontSize: 20 }}>en</Typography>
              <ArrowDropDownIcon />
            </IconButton>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Footer;
