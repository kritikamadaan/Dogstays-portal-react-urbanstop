import React, { useEffect } from "react";
import { MuiThemeProvider, CssBaseline } from "@material-ui/core";
import { Router, Route, Switch } from "react-router-dom";
import { history } from "./helpers";
import theme from "./theme";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import SitterSearch from "./pages/SitterSearch";
import SitterPublicProfile from "./pages/SitterPublicProfile";
import SitterDogProfile from "./pages/SitterDogProfile";
import SitterCatProfile from "./pages/SitterCatProfile";
import SitterOtherPetProfile from "./pages/SitterOtherPetProfile";
import CalendarView from "./pages/dashboard/Calendar";
import BookingRequest from "./pages/BookingRequest";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import MeetingRequest from "./pages/MeetingRequest";
import DashboardWrapper from "./pages/dashboard/DashboardWrapper";
import PetSitterAS from "./pages/account-setting/PetSitterAS";
import PetParentAS from "./pages/account-setting/PetParentAS";
import OnClickLnS from "./pages/account-setting/OnClickLnS";
import SitterApplication from "./pages/SitterApplication";
import DogProfileDashboard from "./pages/dashboard/DogProfileDashboard";
import CatProfileDashboard from "./pages/dashboard/CatProfileDashboard";
import OtherPetProfileDashboard from "./pages/dashboard/CatProfileDashboard";
import BookingDetails from "./pages/dashboard/BookingDetails";
import MeetingDetails from "./pages/dashboard/MeetingDetails";
import Message from "./pages/dashboard/Message";
import DashboardInsights from "./pages/dashboard/Insights";
import Payout from "./pages/account-setting/payout";
import DashboardProfile from "./pages/dashboard/DashboardProfile";
import DashboardBooking from "./pages/dashboard/DashboardBooking";
import { MessageSharp } from "@material-ui/icons";
import DashboardOwnerProfile from "./pages/dashboard/DashboardOwnerProfile";
import DashboardWrap from "./pages/dashboard/DashboardWrap";
import DashboardHome from "./pages/dashboard/DashboardHome";
import HomeIcon from "@material-ui/icons/Home";
import PersonIcon from "@material-ui/icons/Person";
import ChatIcon from "@material-ui/icons/Chat";
import HistoryIcon from "@material-ui/icons/History";
import DateRangeIcon from "@material-ui/icons/DateRange";
import AddPayout from "./pages/account-setting/addPayout";
import DashboardFavourite from "./pages/dashboard/DashboardFavourite.js";
import DashboardSupport from "./pages/dashboard/DashboardSupport";

const user = JSON.parse(localStorage.getItem("user"));

const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/register",
    component: Register,
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/forgot-password",
    component: ForgotPassword,
  },
  {
    path: "/reset-password",
    component: ResetPassword,
  },
  {
    path: "/sitter-search",
    component: SitterSearch,
  },
  {
    path: "/sitter-profile/:id",
    component: SitterPublicProfile,
  },
  {
    path: "/sitter-profile/dog-profile/:id",
    component: SitterDogProfile,
  },
  {
    path: "/sitter-profile/cat-profile",
    component: SitterCatProfile,
  },
  {
    path: "/sitter-profile/other-profile",
    component: SitterOtherPetProfile,
  },
  {
    path: "/booking-request/:id",
    component: BookingRequest,
  },
  {
    path: "/meeting-request/:id",
    component: MeetingRequest,
  },
  {
    path: "/dashboard",
    component: DashboardWrapper,
  },
  {
    path: "/pet-sitter-as",
    component: PetSitterAS,
  },
  {
    path: "/pet-parent-as",
    component: PetParentAS,
  },
  {
    path: "/onclick",
    component: OnClickLnS,
  },
  {
    path: "/onclick/form",
    component: OnClickLnS,
  },
  {
    path: "/payout",
    component: Payout,
  },
  {
    path: "/add-payout",
    component: AddPayout,
  },
  {
    path: "/sitter-application",
    component: SitterApplication,
  },
  {
    path: "/dog-dashboard/:id",
    component: DogProfileDashboard,
  },
  {
    path: "/cat-dashboard/:id",
    component: CatProfileDashboard,
  },
  {
    path: "/otherpet-dashboard/:id",
    component: OtherPetProfileDashboard,
  },
  {
    path: "/dashboard/calendar",
    component: CalendarView,
  },
  {
    path: "/dashboard/meetings/details/:id",
    component: MeetingDetails,
  },
  {
    path: "/dashboard/bookings/details/:id",
    component: BookingDetails,
  },
  {
    path: "/dashboard/messages",
    component: Message,
  },
  {
    path: "/dashboard/profile",
    component: DashboardOwnerProfile,
  },
  // {
  //   path: "/dashboard/favourite-sitters",
  //   component: DashboardFavourite,
  // },
  {
    path: "/dashboard/bookings-meetings",
    component: DashboardBooking,
  },
  {
    path: "/dashboard/messages",
    component: Message,
  },
  {
    path: "/dashboard/favourite-sitters",
    component: DashboardFavourite,
  },
];

const dashboardroutes = [
  {
    name: "Home",
    path: "/dashboard",
    icon: <HomeIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <DashboardHome />,
  },
  {
    name: "Insights",
    path: "/dashboard/insights",
    icon: null,
    exact: true,
    main: () => <DashboardInsights />,
  },
  {
    name: "Meeting Details",
    path: "/dashboard/bookings/details/:id",
    icon: null,
    exact: true,
    main: () => <BookingDetails />,
  },
  {
    name: "Booking Details",
    path: "/dashboard/meetings/details/:id",
    icon: null,
    exact: true,
    main: () => <MeetingDetails />,
  },
  {
    name: "Profile",
    path: "/dashboard/profile",
    icon: <PersonIcon style={{ color: "white" }} />,
    exact: true,
    main: () =>
      user && user.user_type.is_sitter ? (
        <DashboardProfile />
      ) : (
        <DashboardOwnerProfile />
      ),
  },
  {
    name: "Meetings & Bookings",
    icon: <HistoryIcon style={{ color: "white" }} />,
    path: "/dashboard/bookings-meetings",
    exact: true,
    main: () => <DashboardBooking />,
  },
  {
    name: "Calendar",
    path: "/dashboard/calendar",
    icon: <DateRangeIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <CalendarView />,
  },
  {
    name: "Reviews for me",
    path: "/dashboard/reviews",
    icon: <ChatIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <div />,
  },
  {
    name: "Messages",
    path: "/dashboard/messages",
    icon: <ChatIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <Message />,
  },
  {
    name: "My Favourite Sitters",
    path: "/dashboard/favourite-sitters",
    icon: <DateRangeIcon style={{ color: "white" }} />,
    exact: true,
    main: () => <DashboardFavourite />,
  },
];

const CommonRoutes = [
  {
    path: "/dashboard",
    component: DashboardWrapper,
  },
  {
    path: "/",
    component: Home,
  },
  {
    path: "/sitter-application",
    component: SitterApplication,
  },
  {
    path: "/sitter-search",
    component: SitterSearch,
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/register",
    component: Register,
  },
  {
    path: "/reset-password",
    component: ResetPassword,
  },
  {
    path: "/pet-sitter-as",
    component: PetSitterAS,
  },
  {
    path: "/pet-parent-as",
    component: PetParentAS,
  },
  {
    path: "/help-and-support",
    component: DashboardSupport,
  },
];

const isActivePage = () => {
  let currentUrl = window.location.href;
  var activePage = console.log("Current active url is ", activePage);
};
const getLastUrl = (url) => {
  url = url.split("/");
  return url[url.length - 1];
  console.log("last url is ", url);
};

//Route wrapper to handle nested routes
function RouteWithSubRoutes(route) {
  console.log("all routes in subroutes", route);
  return (
    <Route
      path={route.path}
      render={(props) => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}

function App() {
  let currentUrl = window.location.href;

  //=currentUrl.split("/");

  console.log("Current active url is ", currentUrl, typeof currentUrl);
  //console.log('last url is ', currentUrl.location.pathname)
  return (
    <Router history={history}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Header />
        <Switch>
          {CommonRoutes.map((route, index) => (
            <Route exact path={route.path} component={route.component} />
          ))}

          {currentUrl.search("dashboard") > 0 ? (
            <DashboardWrap routes={routes}>
              <div style={{ width: "100%" }}>
                <Switch>
                  {dashboardroutes.map((route, index) => (
                    <Route
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      children={route.main}
                    />
                  ))}
                </Switch>
              </div>
            </DashboardWrap>
          ) : (
            routes.map((route, i) => {
              console.log("the roue and i in app", route, i);
              //   const dashboard = route.filter((rout,i)=> rout.path === "/dashboard/")
              // console.log('the dashboard in route',dashboard)
              return (
                <RouteWithSubRoutes
                  key={i}
                  {...route}
                  exact
                  component={route.component}
                />
              );
            })
          )}
        </Switch>

        {/* {currentUrl.search("dashboard") ?
        <DashboardWrap
        routes={routes}
        >
          <div style={{ width: "100%" }}>
          <Switch>
             
             { dashboardroutes.map((route, index) => (
                  <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    children={route.main}
                  />
                ))}
          </Switch>
          </div>
          </DashboardWrap> 
          : null} */}
        <Footer />
      </MuiThemeProvider>
    </Router>
  );
}

export default App;
