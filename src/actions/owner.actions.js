import { ownerConstants } from '../constants';
import { ownerService } from '../services';

export const ownerActions = {
    getOwnerProfile,
    getOwnerPets
}

function getOwnerProfile(id) {
    return dispatch => {
        dispatch(request(id))

        ownerService.getOwnerProfile(id)
            .then (
                data => {
                    dispatch(success(data))
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            )
    }
    function request(id) { return { type: ownerConstants.GET_OWNER_DETAILS_REQUEST, id } }
    function success(data) { return { type: ownerConstants.GET_OWNER_DETAILS_SUCCESS, data } }
    function failure(error) { return { type: ownerConstants.GET_OWNER_DETAILS_FAILURE, error } }
}

function getOwnerPets(id) {
    return dispatch => {
        dispatch(request(id))

        ownerService.getOwnerPets(id)
            .then (
                data => {
                    console.log(data)
                    dispatch(success(data))
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            )
    }
    function request(id) { return { type: ownerConstants.GET_OWNER_PETS_REQUEST, id } }
    function success(data) { return { type: ownerConstants.GET_OWNER_PETS_SUCCESS, data } }
    function failure(error) { return { type: ownerConstants.GET_OWNER_PETS_FAILURE, error } }
}