import { sitterConstants } from '../constants';
import { sitterService } from '../services';
import { history } from '../helpers';
import { sitter } from '../reducers/sitter.reducer';

export const sitterActions = {
    searchSitters,
    getSitter,
    getSitterDogs,
    getSitterPets,
    getSingleSitter,
    editSitterProfile,
    getSitterServices,
    getAllSitterServices
}

function searchSitters(search) {
    return dispatch => {
        dispatch(request(search))

        sitterService.searchAll(search)
            .then (
                data => {
                    dispatch(success(data))
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            )
    };

    function request(search) { return { type: sitterConstants.SEARCH_REQUEST, search } }
    function success(data) { return { type: sitterConstants.SEARCH_SUCCESS, data } }
    function failure(error) { return { type: sitterConstants.SEARCH_FAILURE, error } }
}

function getSitter(id) {
    return dispatch => {
        dispatch(request(id))

        sitterService.getSelectedSitter(id)
            .then (
                data => {
                    dispatch(success(data))
                    // dispatch(getSitterDogs(id))
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            )
    };

    function request(id) { return { type: sitterConstants.GET_SITTER_REQUEST, id } }
    function success(data) { return { type: sitterConstants.GET_SITTER_SUCCESS, data } }
    function failure(error) { return { type: sitterConstants.GET_SITTER_FAILURE, error } }
}

//get sitter details for dashboard
function getSingleSitter(id) {
    return dispatch => {
        dispatch(request(id))

        sitterService.getSitter(id)
            .then (
                data => {
                    dispatch(success(data))
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            )
    };

    function request(id) { return { type: sitterConstants.GET_SINGLE_SITTER_REQUEST, id } }
    function success(data) { return { type: sitterConstants.GET_SINGLE_SITTER_SUCCESS, data } }
    function failure(error) { return { type: sitterConstants.GET_SINGLE_SITTER_FAILURE, error } }
}

function getSitterDogs(id) {
    return dispatch => {
        dispatch(request(id))

        sitterService.getSitterDogs(id)
            .then (
                data => {
                    dispatch(success(data))
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            )
    };

    function request(id) { return { type: sitterConstants.SITTER_DOGS_REQUEST, id } }
    function success(data) { return { type: sitterConstants.SITTER_DOGS_SUCCESS, data } }
    function failure(error) { return { type: sitterConstants.SITTER_DOGS_FAILURE, error } }
}

function getSitterPets(id) {
    return dispatch => {
        dispatch(request(id))

        sitterService.getSitterPets(id)
            .then (
                data => {
                    dispatch(success(data))
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            )
    };

    function request(id) { return { type: sitterConstants.SITTER_PETS_REQUEST, id } }
    function success(data) { return { type: sitterConstants.SITTER_PETS_SUCCESS, data } }
    function failure(error) { return { type: sitterConstants.SITTER_PETS_FAILURE, error } }
}

function editSitterProfile(data) {
    return dispatch => {
        dispatch(request())
        
        sitterService.editSitterProfile(data)
            .then(
                res => {
                    console.log(res)
                    dispatch(success(res))
                },
                error => {
                    dispatch(failure(error.toString()))
                }
            )
    }
    function request() { return {type: sitterConstants.EDIT_SITTER_PROFILE}}
    function success(res) { return { type: sitterConstants.EDIT_SITTER_PROFILE_SUCCESS, res } }
    function failure(error) { return { type: sitterConstants.EDIT_SITTER_PROFILE_FAILURE, error } }
}

function getSitterServices(id) {
    return dispatch => {
        dispatch(request())

        sitterService.getSitterServices(id)
            .then(
                res => {
                    dispatch(success(res))
                },
                error => {
                    dispatch(failure(error.toString()))
                }
            )
    }
    function request() { return {type: sitterConstants.GET_SITTER_SERVICES_REQUEST}}
    function success(res) { return { type: sitterConstants.GET_SITTER_SERVICES_SUCCESS, res } }
    function failure(error) { return { type: sitterConstants.GET_SITTER_SERVICES_FAILURE, error } }
}

function getAllSitterServices() {
    return dispatch => {
        dispatch(request())

        sitterService.getAllSitterServices()
            .then(
                res => {
                    dispatch(success(res))
                },
                error => {
                    dispatch(failure(error.toString()))
                }
            )
    }
    function request() { return {type: sitterConstants.GET_ALL_SITTER_SERVICES_REQUEST}}
    function success(res) { return { type: sitterConstants.GET_ALL_SITTER_SERVICES_SUCCESS, res } }
    function failure(error) { return { type: sitterConstants.GET_ALL_SITTER_SERVICES_FAILURE, error } }
}